//
//  NotificationService.swift
//  LiveMPush
//
//  Created by Vengababu Maparthi on 06/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UserNotifications
import UIKit
import CoreLocation
import Foundation
import SwiftKeychainWrapper
struct NotificationLinks {
   
    static let AcknowledgeService  =  "https://api.90210notary.com/provider/bookingAck"
    static let locationUpdates     =  "https://api.90210notary.com/provider/location"
    static let groupIdentifier     =  "group.com.notary.provider"
}

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    var locationManager: CLLocationManager?
    
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        let dict = request.content.userInfo
        
        print("push data \(dict)")
        
        if let parseData = dict as? [String:Any]{
            print("booking data1", parseData)
            print("booking data2", dict)
            let a:Int = Int(parseData["a"] as! String)!
            print("identifier :", request.identifier)
            
            if a == 506{
                
                locationManager = NotificationService .sharedLocationManager()
                self.locationManager?.delegate = self
                self.locationManager?.startUpdatingLocation()
                
            }else{
                
                self.acknowledgingThebookingServer(pushData:self.convertToDictionary(text: parseData["bookingData"]! as! String)!)
                UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.set(self.convertToDictionary(text: parseData["bookingData"]! as! String)!, forKey: "bookingData")
                let distanceTime = Date().timeIntervalSince1970
                UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.set(Int(distanceTime), forKey: "timeStamp")
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func acknowledgingThebookingServer(pushData:[String:Any]) {
        let ud = UserDefaults.standard
        var latit = ""
        var longit = ""
        
        if ud.object(forKey: "currentLat") != nil {
            latit = ud.object(forKey: "currentLat") as! String
            longit = ud.object(forKey: "currentLog") as! String
        }else{
            longit = UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"currentLat")! as Any as! String
            latit = UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"currentLog")! as Any as! String
        }
        
        
        let params:[String:Any] = ["bookingId": String(describing:pushData["bid"]!),
                                   "latitude" : latit,
                                   "longitude" : longit]
        
        let jsondata: Data? = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        
//        let sessionToken = UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"sessionToken")! as Any
        let sessionToken = KeychainWrapper(serviceName: NotificationLinks.groupIdentifier).object(forKey: "sessionToken") as Any
        
        let confi = URLSessionConfiguration.default
        confi.httpAdditionalHeaders = ["authorization":sessionToken]
        let session = URLSession.init(configuration: confi)
        let url = URL(string: NotificationLinks.AcknowledgeService)!
        var request1 = URLRequest(url: url)
        request1.httpMethod = "PATCH"
        request1.httpBody = jsondata
        request1.timeoutInterval = 20.0
        request1.addValue("application/json", forHTTPHeaderField:"Content-Type")
        
        let task1 = session.dataTask(with: request1) { data, response, error in
            guard let data = data, error == nil else {
                print("error=\(String(describing: error))")
                return
            }
            print("response = \(String(describing: response))")
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            self.contentHandler!(self.bestAttemptContent!)
        }
        
        task1.resume()
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
    
    class func sharedLocationManager() -> CLLocationManager {
        var _locationManager: CLLocationManager?
        DispatchQueue.main.sync {
            if _locationManager == nil {
                _locationManager = CLLocationManager()
                _locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                
            }
        }
        return _locationManager!
    }
}

extension NotificationService:CLLocationManagerDelegate{
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        UserDefaults.standard.set(coord, forKey: "currentCoordinate")
        UserDefaults.standard.set(coord.latitude, forKey: "currentLat")
        UserDefaults.standard.set(coord.longitude, forKey: "currentLog")
        UserDefaults.standard.synchronize()
        
        self.updateTheLocationToService(latitude: Float(coord.latitude), longitude: Float(coord.longitude))
        locationManager?.stopUpdatingLocation()
    }
    
    func updateTheLocationToService(latitude:Float,longitude:Float){
        self.bestAttemptContent?.title = "Location updating..";
        let todoEndpoint: String = NotificationLinks.locationUpdates
        self.contentHandler!(self.bestAttemptContent!)
        
        let dict :[String:Any] = ["latitude"            : latitude,
                                  "longitude"            : longitude,
                                  "batteryPercentage"   :  UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"batteryPer")! as Any,
                                  "appVersion"   :  UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"appVersion")! as Any,
                                  "status": "1",
                                  "bookingStr"     :  UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"bookingStr")! as Any,
                                  "locationHeading": "0.00",
                                  "transit":1]
        
        let jsondata: Data? = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        
//        let sessionToken = UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"sessionToken")! as Any
        let sessionToken = KeychainWrapper(serviceName: NotificationLinks.groupIdentifier).object(forKey: "sessionToken") as Any
        
        let confi = URLSessionConfiguration.default
        confi.httpAdditionalHeaders = ["authorization":sessionToken]
        let session = URLSession.init(configuration: confi)
        let url = URL(string: todoEndpoint)!
        var request1 = URLRequest(url: url)
        request1.httpMethod = "PATCH"
        request1.httpBody = jsondata
        request1.timeoutInterval = 20.0
        request1.addValue("application/json", forHTTPHeaderField:"Content-Type")
        
        let task1 = session.dataTask(with: request1) { data, response, error in
            guard let data = data, error == nil else {
                print("error=\(String(describing: error))")
                return
            }
            print("response = \(String(describing: response))")
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        }
        task1.resume()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("dsfdsfdsfdsf",error)
    }
}



