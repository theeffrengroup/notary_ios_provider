//
//  AppDelegate.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 12/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import GoogleMaps
import UserNotifications
import AWSS3
import AWSCore
import AVFoundation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID

import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var bookingDict = [String : Any]()
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    var player: AVAudioPlayer?
    var countDown:Int = 0
    var isNewBooking = false
    
    var viewController = UIApplication.shared.keyWindow?.rootViewController
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey(SERVER_CONSTANTS.googleMapsApiKey)
        
        let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
        
        amazonWrapper.setConfigurationWithRegion(AWSRegionType.USWest2, accessKey: AmazonAccessKey, secretKey: AmazonSecretKey)
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
            // For iOS 10 data message (sent via FCM)
            //  FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        // [END register_for_notifications]
        
        FIRApp.configure()
        application.registerForRemoteNotifications()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(tokenRefreshNotification(notification:)),
                                               name: NSNotification.Name.firInstanceIDTokenRefresh,
                                               object: nil)
        
        
        Fabric.with([Crashlytics.self])
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        return true
    }
    
    // [START connect_to_fcm]
    func connectToFcm() {
        if (FIRInstanceID.instanceID().token() == nil) {
            return
        }
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                FIRMessaging.messaging().subscribe(toTopic: "/topics/ios")
                FIRMessaging.messaging().subscribe(toTopic: "/topics/all")
                print("Connected to FCM.")
            }
        }
    }
    
    // [START refresh_token]
    func tokenRefreshNotification(notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            let defaults = UserDefaults.standard
            defaults.set(refreshedToken , forKey: USER_INFO.PUSH_TOKEN)
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }

    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // With swizzling disabled you must set the APNs token here.
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)

    }

    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
         self.doBackgroundTask()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    
    
    func doUpdate() {
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
           _ = self.beginBackgroundUpdateTask()
            
            //  self.endBackgroundUpdateTask(taskID: taskID)
        })
    }
    
    func beginBackgroundUpdateTask() {
        self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundUpdateTask()
        })
    }
    
    func endBackgroundUpdateTask() {
        UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskInvalid
    }
    
    func doBackgroundTask() {
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            self.beginBackgroundUpdateTask()
            
            // Do something with the result.
            //            var timer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "displayAlert", userInfo: nil, repeats: false)
            //            NSRunLoop.currentRunLoop().addTimer(timer, forMode: RunLoopMode.defaultRunLoopMode)
            //            RunLoop.currentRunLoop().run()
            
            // End the background task.
            self.endBackgroundUpdateTask()
        })
    }
    
    func displayAlert() {
        let note = UILocalNotification()
        note.alertBody = "As a test I'm hoping this will run in the background every X number of seconds..."
        note.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(note)
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        if isNewBooking == true{
                self.openNewBookingVC(dict: bookingDict )
            //acknowledgingTheBooking(dict:bookingDict  )
        }
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            
        }else{
            getTypeMakeNModel()
        }
    }
    
    func networkStatusChanged(_ notification: Notification) {
        //  let userInfo = (notification as NSNotification).userInfo
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            ReachabilityView.instance.show()
        case .online(.wwan):
            ReachabilityView.instance.hide()
        case .online(.wiFi):
            ReachabilityView.instance.hide()
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
    func getTypeMakeNModel(){
        NetworkHelper.requestGETURL(method: API.METHOD.CONFIG + Utility.sessionToken,
                                    success: { (response) in
                                        Helper.hidePI()
                                        if response.isEmpty == false {
                                            if (response["statusCode"] != nil){
                                                let statCode:Int = response["statusCode"] as! Int
                                                if statCode == 401 {
                                                    Helper.hidePI()
                                                    self.sessionExpried()
                                                }
                                            }else{
                                                let dict = response["data"] as? [String:Any]
                                                self.handlingTheCongigData(response: dict!)
                                            }
                                        } else {
                                            
                                        }
                                        
        })
        { (Error) in
            
        }
    }
    
    func sessionExpried(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let signIN: SplashViewController? = storyboard.instantiateViewController(withIdentifier: storyBoardIDs.splashVC!) as? SplashViewController
        
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = signIN
        _ = Utility.deleteSavedData()
    }
    
    
    func handlingTheCongigData(response:[String:Any]) {
        let ud = UserDefaults.standard
        ud.set(response["currencySymbol"] as! String, forKey: USER_INFO.CURRENCYSYMBOL)
        ud.set(response["mileage_metric"] as! String, forKey: USER_INFO.DISTANCE)
        ud.set(response["mileage_metric"] as! String, forKey: USER_INFO.PRESENCE)
        
        let dict1 = response["pubnubkeys"]  as? [String:Any]
        
        let x : String = dict1?["publishKey"] as! String
        let y: String  = Utility.publishKey
        let isEqual = (x == y)
        if !isEqual {
            ud.set(dict1?["publishKey"]      as! String, forKey: USER_INFO.PUBLISHKEY)
            ud.set(dict1?["subscribeKey"]      as! String, forKey: USER_INFO.SUBSCRIBEKEY)
            VNHPubNubWrapper.deleteInstance()
            VNHPubNubWrapper.sharedInstance()
        }
        ud.synchronize()
    }
    
    func updateDriverState(){
        let pub = VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper
        pub.delegate = self
    }
    
    
    //MARK:- New booking popup
    func openNewBookingVC(dict:[String:Any]) {
        DispatchQueue.main.async {
            let df = self.dateFormatterProper()
            let bookingDate: Date? = df.date(from: UserDefaults.standard.object(forKey: "currentbookingDateNtime") as! String)
            let timeDiff: TimeInterval = Date().timeIntervalSince(bookingDate!)
            var expTime:Int = 0
            expTime = Int(dict["ExpiryTimer"] as! String)!
            
            if (expTime - Int(timeDiff)) > 0{
                self.isNewBooking = false
                self.player?.stop()
                let Vc:NewBookingPopup = NewBookingPopup.instance
                Vc.show()
                Vc.newbookingDic(dict: dict)
            }else{
                self.viewController?.present(Helper.alertVC(title: "Message", message:"This Booking has been expired" as NSString), animated: true, completion: nil)
            }
        }
    }
    
    
    func acknowledgingTheBooking(dict:[String:Any]) {
        
        Helper.showPI(message:"loading..")
        let params:[String:Any] = [
            "token":Utility.sessionToken,
            "ent_booking_id": String(describing:dict["bid"]!),
            "serverTime":dict["serverTime"]! as Any,
            "PingId":dict["PingId"]! as Any]
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.ACKNOWLEDGEBK,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("signup response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let flag:Int = response["errFlag"] as! Int
                                        if flag == 1{
                                            self.viewController?.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }else{
                                            self.handlingTheBookingAfterAcknowledging(dict: dict)
                                        }
                                    } else {
                                    }
        }) { (Error) in
            Helper.hidePI()
        }
    }
    
    
    func playSound() {
        player?.stop()
        guard let url = Bundle.main.url(forResource: "taxina-1", withExtension: "mp3") else {
            print("error")
            return
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
            player.numberOfLoops = Int(countDown/4)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func dateFormatterProper() -> DateFormatter {
        var formatter: DateFormatter?
        formatter = DateFormatter()
        formatter?.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter!
    }
    
    
    func handlingTheBookingAfterAcknowledging(dict:[String : Any]){
        let state: UIApplicationState = UIApplication.shared.applicationState
        let ud = UserDefaults.standard
        ud.set(Helper.currentDateTime, forKey: "currentbookingDateNtime")
        if state == .background || state == .inactive {
            UIApplication.shared.applicationIconBadgeNumber = 0
            if ud.bool(forKey: "isSound") == false {
                self.playSound()
                ud.set(true, forKey: "isSound")
                ud.synchronize()
            }
            isNewBooking = true
            if #available(iOS 10.0, *) {
                bookingDict   = dict
                countDown     =   Int(bookingDict["ExpiryTimer"] as! String)!
                let content   = UNMutableNotificationContent()
                content.title = "New booking"
                content.body  = "Congratulations you got New Booking"
                content.categoryIdentifier = "NewBooking"
                content.badge = 1
                let trigger   = UNTimeIntervalNotificationTrigger(timeInterval:1, repeats:false)
                let request   = UNNotificationRequest(identifier: "NEWBOOKING", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler:nil)
            }else{
                bookingDict   = dict
                let date      = Date(timeIntervalSinceNow:1)
                let notification = UILocalNotification()
                notification.fireDate = date
                notification.alertTitle = "New booking"
                notification.alertBody  = "Congratulations you got New Booking"
                notification.repeatInterval = NSCalendar.Unit.hour
                UIApplication.shared.scheduleLocalNotification(notification)
            }
        }else{
            self.openNewBookingVC(dict: dict )
            //  acknowledgingTheBooking(dict: dict )
        }
    }
    
    func handlingThePubnubResponse(pubDict:[String : Any])  {
        let status:Int = pubDict["a"] as! Int
 
           let ud = UserDefaults.standard
        switch status {
        case 11:
            if (ud.value(forKey: "lastBid") != nil) {
                let bid = pubDict["bid"] as? NSNumber
                if ud.value(forKey: "lastBid") as? NSNumber == bid{
                    return
                }
                else{
                    ud.setValue(pubDict["bid"],forKey:"lastBid" )
                    ud.synchronize()
                }
            }else{
                ud.setValue(pubDict["bid"],forKey:"lastBid" )
                ud.synchronize()
            }
            
            acknowledgingTheBooking(dict: pubDict )
            break
        case 21:
            break
        default:
            break
        }
    }
}

//MARK:- Pubnub Delegate methods
extension AppDelegate:VNHPubNubWrapperDelegate{
    func receivedMessage(_ message: [AnyHashable : Any]!, andChannel channel: String!) {
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
        }else{
            
        self.handlingThePubnubResponse(pubDict: message as! [String : Any])
            
        }
    }
}


@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        
        print(userInfo)
        
        completionHandler([])
    }

    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        isNewBooking = false
          self.openNewBookingVC(dict: bookingDict )
    //    acknowledgingTheBooking(dict:bookingDict  )
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}

// [END ios_10_message_handling]
// [START ios_10_data_message_handling]
extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("firebase data:",remoteMessage.appData)
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
          print("firebase data:")
    }
}

