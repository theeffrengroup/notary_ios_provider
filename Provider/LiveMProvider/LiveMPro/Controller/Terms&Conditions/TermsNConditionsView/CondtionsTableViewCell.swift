//
//  CondtionsTableViewCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 26/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class CondtionsTableViewCell: UITableViewCell {

    @IBOutlet weak var termsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateTheCellData(termsText:String)  {
        termsLabel.text = termsText
    }
}
