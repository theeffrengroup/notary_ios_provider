//
//  WebViewVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 26/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController,WKUIDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    
//    @IBOutlet weak var webView: UIWebView!
    var urlFrom = ""
    var titleOfController = ""
    var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleOfController
        if urlFrom.length != 0 {
            Helper.showPI(message: loading.load)
            webView.load(NSURLRequest(url: URL(string: urlFrom)!) as URLRequest)
        }
    }
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    
}
/*
extension WebViewVC : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Helper.hidePI()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Helper.hidePI()
    }
} */

extension WebViewVC : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Helper.hidePI()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Helper.hidePI()    }
}
