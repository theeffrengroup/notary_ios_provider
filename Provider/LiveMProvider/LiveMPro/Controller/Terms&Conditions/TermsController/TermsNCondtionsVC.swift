//
//  TermsNCondtionsVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 26/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class TermsNCondtionsVC: UIViewController {

    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var termsTableview: UITableView!
    var dictTermsCondtions = [[String:Any]]()
    var indexSelected = 0
    override func viewDidLoad() {
        dictTermsCondtions = TermsMod().conditions
        super.viewDidLoad()
        termsTableview.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToVcAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    /// segue to web view controller
    ///
    /// - Parameters:
    ///   - segue: terms and conditions to
    ///   - sender: passes the url and title
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toWebview"{
            if let nextScene = segue.destination as? WebViewVC {
                nextScene.urlFrom = (dictTermsCondtions[indexSelected]["url"] as? String)!
                nextScene.titleOfController = (dictTermsCondtions[indexSelected]["title"] as? String)!
            }
        }
    }
}
///*********** tableview datasource************///
extension TermsNCondtionsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dictTermsCondtions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CondtionsTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "conditions") as! CondtionsTableViewCell?)!
        cell.updateTheCellData(termsText:(dictTermsCondtions[indexPath.row]["title"] as? String)!)
        return cell
    }
}

///*********** tableview Delegate************///
extension TermsNCondtionsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        indexSelected = indexPath.row
        performSegue(withIdentifier: "toWebview", sender: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}


