//
//  LocationController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 21/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces



protocol AddressFromMapDelegate {
    func getTheAddressWithLatLongs(addressData: [String:Any] , addressDetails:AddressDetails? )
}


class LocationController: UIViewController {
    var delegate: AddressFromMapDelegate?
    @IBOutlet weak var heightOFAddressView: NSLayoutConstraint!
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var searchResult: UILabel!
    @IBOutlet weak var searchBarView: UIView!
    
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    
    @IBOutlet weak var workButton: UIButton!
    @IBOutlet weak var heightOfInnerView: NSLayoutConstraint!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var heightOfAddressText: NSLayoutConstraint!
    var locationModel = LocationModelClass()
    
    let locationManager = CLLocationManager()
    var dropLatitude: Float = 0.00
    var dropLongitude: Float = 0.00
    var flag = 0
  
    
    @IBOutlet weak var currentLocation: UIImageView!
    @IBOutlet weak var othersTF: UITextField!
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    var fromSignup = false
    
    var addressData:AddressDetails?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeButton.isSelected = true
        heightOfInnerView.constant = 0
        googleMapView.bringSubviewToFront(searchBarView)
        googleMapView.bringSubviewToFront(currentLocation)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        googleMapView.delegate = self
        self.othersTF.placeholder = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
         setupGestureRecognizer()  //@locate LocationControllerExtn
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        locationManager.delegate = nil
        locationManager.stopUpdatingLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.delegate = nil
        locationManager.stopUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        if searchResult.text == "" {
            Helper.alertVC(errMSG: "Please select the location")
        }
        else {
            if fromSignup{
                var taggedAS = ""
                if homeButton.isSelected {
                    taggedAS = "home"
                }else if workButton.isSelected{
                    taggedAS = "office"
                }else{
                    if (othersTF.text?.isEmpty)!{
                        Helper.alertVC(errMSG: "please provide the title of this address")
                        return
                    }else{
                        taggedAS = othersTF.text!
                    }
                }
                
                let params: [String : Any] =  ["addLine1" :searchResult.text! as Any,
                                               "latitude":dropLatitude,
                                               "longitude":dropLongitude,
                                               "taggedAs":taggedAS]
                self.delegate?.getTheAddressWithLatLongs(addressData:params, addressDetails: addressData)
                _ = self.navigationController?.popViewController(animated:true)
            }else{
                
                var taggedAS = ""
                if homeButton.isSelected {
                    taggedAS = "home"
                }else if workButton.isSelected{
                    taggedAS = "office"
                }else{
                    if (othersTF.text?.isEmpty)!{
                        Helper.alertVC(errMSG: "please provide the title of this address")
                        return
                    }else{
                        taggedAS = othersTF.text!
                    }
                }
                
                
                let params: [String : Any] =  ["addLine1" :searchResult.text! as Any,
                                               "latitude":dropLatitude,
                                               "longitude":dropLongitude,
                                               "taggedAs":taggedAS,
                                               "city":(addressData?.city)!,
                                               "state":(addressData?.state)!,
                                               "country":(addressData?.country)!,
                                               "pincode":(addressData?.pinCode)!,
                                               "userType":2]
                
                locationModel.saveTheAddress(param: params,completionHandler: { (succeeded) in
                    
                    if succeeded{
                        _ = self.navigationController?.popViewController(animated:true)
                    }else{
                        
                    }
                })
            }
        }
        
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated:true)
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        performSegue(withIdentifier: "searchResult", sender: self)
    }
    
    @IBAction func homeAddress(_ sender: Any) {
        self.othersTF.placeholder = ""
        heightOfInnerView.constant = 0
        homeButton.isSelected = true
        otherButton.isSelected = false
        workButton.isSelected = false
        self.view.endEditing(true)
    }
    
    @IBAction func workAddress(_ sender: Any) {
        self.othersTF.placeholder = ""
        heightOfInnerView.constant = 0
        homeButton.isSelected = false
        otherButton.isSelected = false
        workButton.isSelected = true
        self.view.endEditing(true)
    }
    
    @IBAction func otherAddress(_ sender: Any) {
        self.othersTF.placeholder = "others"
        homeButton.isSelected = false
        otherButton.isSelected = true
        workButton.isSelected = false
        heightOfInnerView.constant = 40
        othersTF.becomeFirstResponder()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "searchResult" {
            let nav = segue.destination as! UINavigationController
            if let searchVC:SearchAddressController  = nav.viewControllers.first as? SearchAddressController {
                searchVC.delegate = self
                
            }
        }
    }
}

extension LocationController: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        googleMapView.isMyLocationEnabled = true
        if status == .authorizedWhenInUse || status == .authorizedAlways
        {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        print(coord.longitude)
        print(coord.latitude)
        
        if let location = locations.first
        {
            googleMapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 18, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D)
    {
        
        // 1
        let labelHeight = self.searchResult.intrinsicContentSize.height
        self.googleMapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0,
                                                  bottom: labelHeight, right: 0)
        self.dropLatitude = Float(coordinate.latitude)
        self.dropLongitude = Float(coordinate.longitude)
        UIView.animate(withDuration: 0.25, animations:
            {
                //2
                //
                self.view.layoutIfNeeded()
        })
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate)
        {
            response, error in
            if let address = response?.firstResult()
            {
                // 3
                
                var city = ""
                var state = ""
                var contry = ""
                var pinCode = ""
                
                if let addCity = address.locality{
                    city = addCity
                }
                
                if let addState = address.administrativeArea{
                    state = addState
                }
                
                if let addCountry = address.country{
                    contry = addCountry
                }
                
                if let addPincode = address.postalCode{
                    pinCode = addPincode
                }
                
                self.addressData =  AddressDetails.init(selCity: city, stat: state, contry: contry, pinCod: pinCode, placeid: "")
                
                let lines = address.lines! as [String]
                
                if self.flag == 0 {
                  self.searchResult.text = lines.joined(separator: "\n")
                }
                
                let labelHeight1 = self.searchResult.intrinsicContentSize.height
                // 4
                UIView.animate(withDuration: 0.25, animations:
                    {
                        self.heightOfAddressText.constant = labelHeight1 + 10
                        self.view.layoutIfNeeded()
                })
            }
        }
    }
}

// MARK: - GMSMapViewDelegate
extension LocationController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {

        reverseGeocodeCoordinate(position.target)
        
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool)
    {
        print("willMove gesture")
    }
}

extension LocationController: SearchAddressControllerDelegate {
    
    func didSelectAddress(lat: Float, long: Float, address1: String, address2: String) {
        let completeAddress: String = "\(address1), \(address2)"
        self.searchResult.text = completeAddress
        flag = 1
        let labelHeight = self.searchResult.intrinsicContentSize.height
        UIView.animate(withDuration: 0.25, animations:
            {
                self.heightOfAddressText.constant = labelHeight + 10
                self.view.layoutIfNeeded()
        })
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long), zoom: 18)
        self.googleMapView.camera = camera
        
    }
}



extension LocationController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension LocationController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}



