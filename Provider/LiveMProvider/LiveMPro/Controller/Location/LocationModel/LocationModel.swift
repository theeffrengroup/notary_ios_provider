//
//  LocationModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 30/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire
import SwiftKeychainWrapper
class LocationModelClass {
    var id = ""
    var address = ""
    var addressType = ""
    var defaultAddress = 0
    let disposebag = DisposeBag()
    
    var apiCall = APILibrary()
    var locationData = [LocationModelClass]()
    
    
    
    /// get the saved artist address
    ///
    /// - Parameter completionHandler: returns the saved addresses
    func getTheAddressesOfArtist(completionHandler: @escaping(Bool,[LocationModelClass])->()) {
            Helper.showPI(message:loading.load)
        let rxApiCall = LocationAPI()
        rxApiCall.getLocationData(method:API.METHOD.ADDRESS)
        rxApiCall.LocationData_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .UserLoggedOut:
                    Session.expired()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                            }
                        })
                    }
                    break
                case .SuccessResponse:
                    if let dict = responseModel.data["data"] as? [[String:Any]]{
                        completionHandler(true,self.sortTheAddressData(addressDict:dict ))
                    }
                    break
                default:
                    
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    func sortTheAddressData(addressDict:[[String:Any]])->[LocationModelClass]{
        locationData = [LocationModelClass]()
        for dict in addressDict{
            let locationDict = LocationModelClass()
            
            if let locID = dict["_id"] as? String{
                locationDict.id = locID
            }
            
            if let address = dict["addLine1"] as? String{
                locationDict.address = address
            }
            if let tagID = dict["taggedAs"] as? String{
                locationDict.addressType = tagID
            }
            
            if let defaultID = dict["defaultAddress"] as? NSNumber{
                locationDict.defaultAddress = Int(defaultID)
            }
            
            locationData.append(locationDict)
        }
        return locationData
    }
    
    
    /// saves the new address
    ///
    /// - Parameters:
    ///   - param: contains address, address type
    ///   - completionHandler: return  true of address saved successfull
    func saveTheAddress(param:[String:Any],completionHandler: @escaping(Bool)->()) {
              Helper.showPI(message:loading.saving)
        let rxApiCall = LocationAPI()
        rxApiCall.postLocationData(paramDict:param,method:API.METHOD.ADDRESS)
        rxApiCall.LocationData_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .UserLoggedOut:
                    Session.expired()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    
                    break
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    
    /// deletes the undefault address
    ///
    /// - Parameters:
    ///   - addressID: address id of particular address to be delete
    ///   - completionHandler: return true if the address deleted successfully
    func deleteSavedData(addressID:String,completionHandler: @escaping(Bool)->()) {
        Helper.showPI(message:loading.deleting)
        let rxApiCall = LocationAPI()
        rxApiCall.deleteArtistAddress(method:API.METHOD.ADDRESS + "/" + addressID)
        rxApiCall.LocationData_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .UserLoggedOut:
                    Session.expired()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    
                    break
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}
