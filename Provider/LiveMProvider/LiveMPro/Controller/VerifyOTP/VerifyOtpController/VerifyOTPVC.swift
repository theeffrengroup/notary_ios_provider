//
//  VerifyOTPVC.swift
//  LiveM Provider
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

protocol verifyOtpDelegate:class
{
    func phoneNumberVerified()
    func updatedNumber(numberNew: String,countryCode:String)
}


import UIKit
import RxSwift
import RxKeyboard
import RxCocoa

class VerifyOTPVC: UIViewController {
    
    open weak var delegate: verifyOtpDelegate?
    var activeTextField = UITextField()

    @IBOutlet weak var otpHeaderLabel: UILabel!
    
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var resendOTP: UIButton!
    var defineTheOtp:Int = 0   // 1: signup, 2:forgot passwrod, 3:change number
    var count:Int = 60
    var timer       = Timer()
    
    @IBOutlet weak var otpView: VPMOTPView!
    var mobileNumber = String()
    var countryCode = String()
    var providerID = String()
    var signupMsg = String() // for signup
    
    var verifyModl = VerifyModel()
    let disposeBag = DisposeBag()
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    @IBOutlet weak var timerForResend: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        verifyModl.trigger.value = defineTheOtp
        verifyModl.providerID.value = providerID
        
        otpView.otpFieldsCount = 4
        otpView.otpFieldDefaultBorderColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
        otpView.otpFieldEnteredBorderColor = COLOR.APP_COLOR
        otpView.otpFieldBorderWidth = 3
        otpView.delegate = self
        // Create the UI
        otpView.initalizeUI()
        
        otpHeaderLabel.text = "Verification code has been sent to \(countryCode) \(mobileNumber) please enter it below"
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         setupGestureRecognizer()
    }
    
    override func viewDidAppear(_ animated: Bool) {

        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0
            , target: self, selector: #selector(update), userInfo: nil, repeats: true)
       
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.timer.invalidate()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func signUpDone(){
        //        delegate?.phoneNumberVerified()
        //        self.navigationController?.popViewController(animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController  = storyboard.instantiateViewController(withIdentifier: "HomeTabBarController")
        mainViewController.navigationController?.isNavigationBarHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.viewControllers.append(mainViewController)
    }
    
    
    
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toCreateNewpassword" {
            let nextScene = segue.destination as? SetNewpasswordViewController
            nextScene?.secureOtp =  verifyModl.otpField.value
            nextScene?.providerID = providerID
        }
    }
}

extension VerifyOTPVC: VPMOTPViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) {
        print("Has entered all OTP? \(hasEntered)")
    }
    
    func enteredOTP(otpString: String) {
        
        verifyModl.otpField.value = otpString //"1111"
        verifyTheOtp()
        print("OTPString: \(otpString)")
    }
}


extension VerifyOTPVC: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

