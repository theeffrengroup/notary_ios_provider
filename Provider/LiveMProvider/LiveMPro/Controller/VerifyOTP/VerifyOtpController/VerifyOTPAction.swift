//
//  VerifyOTPAction.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension VerifyOTPVC{
    @IBAction func backToController(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //***********Move to vc to create new password*******//
    @IBAction func verifyOTP(_ sender: Any) {
       // verifyTheOtp()
        if verifyModl.otpField.value == ""{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:verify.fillOTP), animated: true, completion: nil)
        }else{
            verifyTheOtp()
        }
    }
    
    //******hide the keyboard******//
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func resendOtp(_ sender: Any) {
        switch self.defineTheOtp{
            
        case 1:
            // Resend otp for signup time
            verifyModl.resendOtpForSignup(completionHandler: { success in
                if success{
                    self.count = 60
                    self.timer.invalidate()
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0
                        , target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
                }else{
                    
                }
            })
            
            break
        default:
            // resend otp for change password and change number
            verifyModl.resendOtpForChangePassword(completionHandler: { success in
                if success{
                    self.count = 60
                    self.timer.invalidate()
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0
                        , target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
                }else{
                    
                }
            })
            break
        }
    }
    
    // count down timer for requesting another otp
    @objc func update() {
        
        if(count > 0){
            count -= 1
            let minutes = String(count / 60)
            let seconds = String(count % 60)
            timerForResend.text = minutes + ":" + seconds
            resendOTP.isEnabled = false
            resendOTP.setTitleColor(Helper.UIColorFromRGB(rgbValue: 0xCACACA), for: .normal)
            
        }else{
            timerForResend.text = "00:00"
            resendOTP.setTitleColor(Helper.UIColorFromRGB(rgbValue: 0x707070), for: .normal)
            resendOTP.isEnabled = true
            self.timer.invalidate()
        }
    }
    

    func handlingTheSignupResponse(){
        
        self.signUpDone()
    }
    
    func verifyTheOtp()  {
        switch self.defineTheOtp{
        case 1:
            // for signup verification
            self.verifyModl.sendRequestForSignup(completionHandler: { succeeded in
                if succeeded{
                    self.handlingTheSignupResponse()
                }else{
                  //  self.otpView.initalizeUI()
                }
            })
            break
        case 2:
            // for change password
            self.verifyModl.verifyOTP( completionHandler: { (succeeded) in
                if succeeded{
                    self.performSegue(withIdentifier: "toCreateNewpassword", sender: nil)
                }else{
                    //      self.otpView.initalizeUI()
                }
            })
            break
        case 3:
            // for change number
            self.verifyModl.verifyOTP(completionHandler: { (succeeded) in
                if succeeded{
                    self.delegate?.updatedNumber(numberNew: self.mobileNumber, countryCode: self.countryCode)
                    _ = self.navigationController?.popToRootViewController(animated: true)
                }else{
                     //      self.otpView.initalizeUI()
                }
            })
            break
        default:
            break
        }
    }
}
