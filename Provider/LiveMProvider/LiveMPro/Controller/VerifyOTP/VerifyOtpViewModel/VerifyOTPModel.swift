//
//  VerifyOtpModel.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 22/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import SwiftKeychainWrapper

class VerifyModel:NSObject {
    
    var otpField = Variable<String>("")

    var providerID = Variable<String>("")
    var trigger = Variable<Int>(0)
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    //MARK: - Resend OTP API For change password
    func resendOtpForChangePassword(completionHandler:@escaping (Bool) -> ()) {
        let dict : [String : Any] =  [ VERIFY_OTP_REQUEST.USERID:providerID.value,
                                       VERIFY_OTP_REQUEST.USERTYPE : 2,
                                       "trigger":trigger.value]
               Helper.showPI(message:loading.resend)
        let rxApiCall = VerifyOTPAPI()
        rxApiCall.resendOtpForSignup(params:dict ,method: API.METHOD.RESENDOTP)
        rxApiCall.verify_response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .tooManyAttempts:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .dataNotFound:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                    
                case .SuccessResponse:
                    Helper.hidePI()
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    Helper.hidePI()
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    //MARK: - Verify OTP API for forgotPassword and phone number
    func verifyOTP(completionHandler:@escaping (Bool) -> ()) {
     
        let dict : [String : Any] =  [ VERIFY_OTP_REQUEST.CODE : otpField.value,
                                       VERIFY_OTP_REQUEST.USERID : providerID.value,
                                       VERIFY_OTP_REQUEST.USERTYPE : 2,
                                       "trigger":trigger.value]
        Helper.showPI(message:loading.verify)
        let rxApiCall = VerifyOTPAPI()
        rxApiCall.verifyOTPForNumberAndPassword(params:dict ,method: API.METHOD.verifyOTP)
        rxApiCall.verify_response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .otpExpired:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .invalidEnter:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .adminNotAccepted:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .uauthoriedUser:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .SuccessResponse:
                    Helper.hidePI()
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    completionHandler(true)
                    break
                case .tooManyAttempts:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                default:
                    Helper.hidePI()
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    //MARK: - Signup Phone verification API
    func sendRequestForSignup(completionHandler:@escaping (Bool) -> ()) {
       
        let dict : [String : Any] =  [ VERIFY_OTP_REQUEST.CODE :  otpField.value,
        VERIFY_OTP_REQUEST.PROVIDERID : providerID.value]
        Helper.showPI(message:loading.verify)
        let rxApiCall = VerifyOTPAPI()
        rxApiCall.verifyPhoneNumberForSignup(params:dict ,method: API.METHOD.VERIFYPHONESIGNUP)
        rxApiCall.verify_response
            .subscribe(onNext: {response in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                    
                case .SuccessResponse:
                    
                    if  self.savingTheData(dict: response.data["data"]  as! [String : Any]){
                       completionHandler(true)
                    }
                  //  Helper.alertVC(errMSG: response.data["message"] as! String)
                   
                    break
                case .otpExpired:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    
                case .uauthoriedUser:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .invalidEnter:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .adminNotAccepted:  //wrong code
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .tooManyAttempts:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    /// saves the data in nsuserdefaults
    ///
    /// - Parameter dict: signin response data
    /// - Returns: return true when the saving got complete
    func savingTheData(dict:[String:Any]) -> Bool{
        let defaults = UserDefaults.standard
        
        if let emailID = dict["email"]  as? String  {
            defaults.set(emailID , forKey: USER_INFO.USER_EMAIL)
        }
        
        if let firstName = dict["firstName"]  as? String  {
            defaults.set(firstName , forKey: USER_INFO.FIRSTNAME)
            if let last = dict["lastName"]  as? String  {
                defaults.set(firstName + " " + last , forKey: USER_INFO.USER_NAME)
            }
        }
        
        if let bidType = dict["bid"] as? NSNumber {
            defaults.set(String(describing:bidType), forKey: USER_INFO.WHICHPROVIDERTYPE)
        }
        
        
        if let lastName = dict["lastName"]  as? String  {
            defaults.set(lastName , forKey: USER_INFO.LASTNAME)
        }
        
        if let requestID = dict["requester_id"] as? NSNumber {
            defaults.set(requestID, forKey: USER_INFO.REQUEST_ID)
        }
        
        if let masterID  = dict["id"]  as? String  {
            defaults.set(masterID , forKey: USER_INFO.USER_ID)
        }
        
        if let sessionToken =  dict["token"]   as? String  {
            KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
//            UserDefaults(suiteName: API.groupIdentifier)!.set( sessionToken, forKey: "sessionToken")
            KeychainWrapper(serviceName: API.groupIdentifier).set( sessionToken, forKey: "sessionToken")
            
        }
        
        if let fcmTopci =  dict["fcmTopic"]   as? String  {
            defaults.set(fcmTopci, forKey: USER_INFO.FCMTOPIC)
        }
        
        if let pPic =  dict["profilePic"]   as? String  {
            defaults.set(pPic, forKey: USER_INFO.USERIMAGE)
        }
        
        if let referral =  dict["referralCode"]   as? String  {
            defaults.set(referral, forKey: USER_INFO.REFERRAL_CODE)
        }
        defaults.synchronize()
        
        let configModel = ConfigModelClass()
        configModel.makeServiceCallForConfigData()
        
        return true
    }
    
    //MARK: - Resend OTP API For SIGNUP
    func resendOtpForSignup(completionHandler:@escaping (Bool) -> ()) {
        let dict : [String : Any] =  [ VERIFY_OTP_REQUEST.USERID:providerID.value,
                                       VERIFY_OTP_REQUEST.USERTYPE : 2,
                                       "trigger":trigger.value]
           Helper.showPI(message:loading.resend)
        
        let rxApiCall = VerifyOTPAPI()
        rxApiCall.resendOtpForSignup(params:dict ,method: API.METHOD.RESENDOTP)
        rxApiCall.verify_response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                    
                case .tooManyAttempts:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                    
                case .dataNotFound:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                    
                case .SuccessResponse:
                    Helper.hidePI()
                    completionHandler(true)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                default:
                    Helper.hidePI()
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}


