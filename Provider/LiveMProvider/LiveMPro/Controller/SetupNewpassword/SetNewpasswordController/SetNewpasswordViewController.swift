//
//  SetNewpasswordViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 19/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SetNewpasswordViewController: UIViewController {
    
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var bottomContraint: NSLayoutConstraint!
    @IBOutlet var confirmPassword: FloatLabelTextField!
    @IBOutlet var newPassword: FloatLabelTextField!
    var activeTextField = UITextField()
    var secureOtp = String()
    var providerID = String()
    var newPasswordModel = NewPasswordModel()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        setupGestureRecognizer()
    }
    

    func setNewpassword() {

        
        let params : [String : Any] =  ["password"       : newPassword.text!,
                                        "userType"       :2,
                                        "userId"     :providerID]
        
        newPasswordModel.updateNewPassWordAPi(params: params) { (succeeded) in
            if succeeded{
                _ = self.navigationController?.popToRootViewController(animated: true)
            }else{
                
            }
        }
    }
    
    func alertVC(errMSG:String){
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1
        alertWindow.makeKeyAndVisible()
        
        alertWindow.rootViewController?.present(Helper.alertVC(title: alertMsgCommom.Message, message:errMSG), animated: true, completion: nil)
    }
    

    
    //********* Hide the keyboard********//
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    //*********back to root vc*************//
    @IBAction func backToVC(_ sender: Any) {
        
         _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveThePAssword(_ sender: Any) {
      self.changePassword()
    }
    
    func changePassword(){
        if (newPassword.text?.isEmpty)! {
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Please enter new password"), animated: true, completion: nil)
        }else if (confirmPassword.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Please enter confirm Password"), animated: true, completion: nil)
        }else if newPassword.text! != confirmPassword.text!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"password Mismatched"), animated: true, completion: nil)
        }else{
            self.setNewpassword()
        }
    }

    
   func dismisskeyBord(){
    self.view.endEditing(true)
    }
}

// MARK: - Textfield delegate method
extension SetNewpasswordViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        func textFieldDidBeginEditing(_ textField: UITextField) {
            if textField == confirmPassword{
                if Helper.isValidPassword(password:(newPassword.text)!){
                    confirmPassword.becomeFirstResponder()
                }else{
                    newPassword.becomeFirstResponder()
                    newPassword.text = ""
                    self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.PasswordInvalid), animated: true, completion: nil)
                }
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case newPassword:
            confirmPassword.becomeFirstResponder()
            break
        case confirmPassword:
            dismisskeyBord()
            break
        default:
             self.changePassword()
            dismisskeyBord()
            break
        }
        return true
    }
}



extension SetNewpasswordViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}




