//
//  PBDriverDetailsCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 11/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class PBDriverDetailsCell: UITableViewCell {
    @IBOutlet weak var howMuchRatedLabel: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet var rating: UILabel!
    @IBOutlet weak var alignmentForSenderNAme: NSLayoutConstraint!
    
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet var pickAddress: UILabel!
    
    @IBOutlet var totalAmount: UILabel!
    @IBOutlet var senderName: UILabel!
    @IBOutlet var senderImage: UIImageView!
    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var bookingStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
