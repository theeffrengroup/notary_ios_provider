//
//  PastBookingController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 11/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

enum PBSectionType : Int {
    case PBDriverDetails = 0
    case PBBillDetails = 1
    case PBGrandTot = 2
    case PBServies = 3
    case PBPaymentType = 4
    case PBReceiversDetails = 5
}

enum PBRowType : Int {
    case PBHeaderSec = 0
    case PBRFirstRow = 1
    case PBSecondRow = 2
    case PBThirdRow = 3
    case PBFourthRow = 4
    case PBFifthRow = 5
    case PBSixthRow = 6
    case PBSeventhRow = 7
}


class PastBookingController: UIViewController {
    
    @IBOutlet weak var cancellationReason: UILabel!
    @IBOutlet var bookingID: UILabel!
    @IBOutlet var dateOfBooking: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var bookingArray:CompletedBookings?
    var selectedIndex:Int = 0
    var invoiceDict = [String:Any]()
    var bookingId = ""
    var scheduleModel = ScheduleBookingViewModel()
    var serviceAPI = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        tableView.estimatedRowHeight = 10
        tableView.rowHeight = UITableView.automaticDimension
        if bookingId == ""{ // history booking data
            
            bookingID.text = pastBooking.eventID + String(describing:(bookingArray!.bookingId))
            dateOfBooking.text = Helper.getTheDateFromTimeStamp(timeStamp:(bookingArray?.bookingRequestedFor)!)
            
            if bookingArray!.statusCode == 11 || bookingArray!.statusCode == 12{
                cancellationReason.text = pastBooking.cancelledReason + bookingArray!.cancellationReason
            }
        }else{
            getThebookingData() // schedule booking data
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func helpView(_ sender: Any) {
        //   self.performSegue(withIdentifier: "toChat", sender:  nil)
        //   self.performSegue(withIdentifier: "fromHistory", sender:  nil)
    }
    
    func getThebookingData() {
        scheduleModel.getBookingSchedulebookingData(bookingID: bookingId) { (success, scheduledata) in
            if success{
                self.serviceAPI = true
                self.bookingArray = scheduledata.bookingsData
                self.bookingID.text = pastBooking.eventID + String(describing:(self.bookingArray!.bookingId))
                self.dateOfBooking.text = Helper.getTheDateFromTimeStamp(timeStamp:(self.bookingArray?.bookingRequestedFor)!)
                self.tableView.reloadData()
            }
        }
    }
}

extension PastBookingController : showTheVehicleDocPopupDelegate{
    func showVehicleGalley(gallery: INSPhotosViewController) {
        self.present(gallery, animated: true, completion: nil)
    }
}
