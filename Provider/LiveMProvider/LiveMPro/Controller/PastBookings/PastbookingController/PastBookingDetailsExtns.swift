//
//  PastBookingDetailsExtns.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 11/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import Kingfisher


extension PastBookingController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if bookingId == ""{
            if bookingArray!.statusCode == 5 || bookingArray!.statusCode == 4{
                return 1
            }
            else  if bookingArray!.statusCode == 11 || bookingArray!.statusCode == 12{
                return 5
            }else{
                return 6
            }
        }else{
            if self.serviceAPI {
                return 6
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : PBSectionType = PBSectionType(rawValue: section)!
        
        switch sectionType {
            
        case .PBDriverDetails:
            return 1
            
        case .PBBillDetails :
            if bookingArray!.statusCode == 11 || bookingArray!.statusCode == 12{
                return 2
            }else{
                return 1 + bookingArray!.services.count
                //return 1 + bookingArray!.services.count + bookingArray!.additionalSerice.count
            }
            
        case .PBGrandTot:
            
            return 1 + bookingArray!.additionalSerice.count
            
        case .PBServies:
            return 3
            
        case .PBPaymentType :
            if (bookingArray?.isPaidByWallet)! {
                return 3
            }
            else {
                return 2
            }
            
            
        case .PBReceiversDetails :
            
            return 2
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType : PBSectionType = PBSectionType(rawValue: indexPath.section)!
        
        
        let header: PBHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "header") as! PBHeaderTableViewCell
        
        let rowType : PBRowType = PBRowType(rawValue: indexPath.row)!
        
        switch sectionType {
        case .PBDriverDetails:
            
            let cell: PBDriverDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBDriverDetailsCell") as! PBDriverDetailsCell
            
            switch bookingArray!.statusCode{
            case 5:
                cell.statusView.backgroundColor = COLOR.bookingExpiredColor
                break
            case 4:
                cell.statusView.backgroundColor = COLOR.cancelDeclinedRed
                break
            case 12:
                cell.statusView.backgroundColor = COLOR.cancelDeclinedRed
                break
            case 11:
                cell.statusView.backgroundColor = COLOR.cancelDeclinedRed
                break
            case 10:
                cell.statusView.backgroundColor = COLOR.completedBookingColor
                break
            default:
                cell.statusView.backgroundColor = COLOR.onGoingBlue
                break
            }
            
            if bookingArray!.statusCode == 5 || bookingArray!.statusCode == 4 || bookingArray!.statusCode == 11 || bookingArray!.statusCode == 12{
                cell.alignmentForSenderNAme.constant = 0
                cell.rating.isHidden = true
                cell.starImage.isHidden = true
                cell.howMuchRatedLabel.isHidden = true
                
            }else{
                cell.rating.isHidden = false
                cell.starImage.isHidden = false
                cell.howMuchRatedLabel.isHidden = false
                cell.alignmentForSenderNAme.constant = -6
                cell.rating.text  = String(describing:bookingArray!.averageRating)
            }
            
            cell.bookingStatus.text = bookingArray?.statusMsg
            cell.pickAddress.text  = bookingArray?.addLine1
            
            cell.totalAmount.text  = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingArray!.price))
            cell.senderName.text   = bookingArray?.firstName
            cell.senderImage.kf.setImage(with: URL(string: (bookingArray?.profilePic)!),
                                         placeholder:UIImage.init(named: "signup_profile_default_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
            },
                                         completionHandler: { image, error, cacheType, imageURL in
            })
            
            
            
            
            return cell
            
        case .PBBillDetails:
            let cell :PBBillDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBBillDetailsCell") as! PBBillDetailsCell
            switch indexPath.row {
            case 0:
                header.sectionHeader.text = pastBooking.details
                return header
                
           // case 1 + (bookingArray?.services.count)!:

            default:
                if bookingArray!.statusCode == 11 || bookingArray!.statusCode == 12{
                    cell.key.text = pastBooking.cancellationFee
                    cell.value.text  = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.cancellationFees))
                }else{
                    if bookingArray?.serviceType == 1{
                        if (bookingArray?.services[indexPath.row - 1].serviceName)! == "Visit Fee" || (bookingArray?.services[indexPath.row - 1].serviceName)! == "Travel Fee" ||
                            (bookingArray?.services[indexPath.row - 1].serviceName)! == "Last Due" ||
                            (bookingArray?.services[indexPath.row - 1].serviceName)! == "Discount"  {
                            cell.key.text = (bookingArray?.services[indexPath.row - 1].serviceName)!
                        }
                        else{
                            cell.key.text = (bookingArray?.services[indexPath.row - 1].serviceName)! + "*" + String(format: "%d", (bookingArray?.services[indexPath.row - 1].quantity)!)
                        }
                        cell.value.text  = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", (bookingArray?.services[indexPath.row - 1].servicePrice)!))
                    }else{
                        
                        
                        if (bookingArray?.services[indexPath.row - 1].serviceName)! == "Visit Fee" || (bookingArray?.services[indexPath.row - 1].serviceName)! == "Travel Fee" ||
                            (bookingArray?.services[indexPath.row - 1].serviceName)! == "Last Due" ||
                            (bookingArray?.services[indexPath.row - 1].serviceName)! == "Discount"{
                            cell.key.text = (bookingArray?.services[indexPath.row - 1].serviceName)!
                            cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingArray!.services[indexPath.row - 1].servicePrice))
                        }
                        else {
                            
                            
                            cell.key.text = Helper.timeInHourMin((bookingArray?.totalJobTime)!)
                            cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingArray!.totalHourlyFee))
                        }
                        
                    }
                }
                

             return cell
                
            
            }
           
            
            
        case .PBGrandTot:

                

            switch indexPath.row{
                
    
            case 0:
                let cell : GrandTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "grandtotal") as! GrandTotalTableViewCell
                cell.grandTotal.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.price))
                return cell


            default:

                if bookingArray?.additionalSerice.count != 0 {
                    let cell :PBBillDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBBillDetailsCell") as! PBBillDetailsCell
                    cell.key.text = bookingArray?.additionalSerice[indexPath.row - 1].serviceName
                    let extraAmt  = String(format: "%.2f", Double(bookingArray!.additionalSerice[indexPath.row - 1].price)!)
                    cell.value.text = Helper.getTheAmtTextWithSymbol(amt:extraAmt)

                    return cell
                 }


                else{
                    let cell : GrandTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "grandtotal") as! GrandTotalTableViewCell
                    cell.grandTotal.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.price))
                    return cell
                }

            }
            
         
        case .PBServies:
            let cell : ServicesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "service") as! ServicesTableViewCell
            switch indexPath.row {
            case 0:
                header.sectionHeader.text = pastBooking.yourEarnings
                return header
            case 1:
                cell.key.text =  pastBooking.earnedAmt
                cell.value.text =  Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.myEarnings))
                return cell
            default:
                cell.key.text =  pastBooking.appcomm
                
                cell.value.text =  Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.appEarnings))
                return cell
            }
            
            
        case .PBPaymentType:
            
            switch rowType {
            case .PBHeaderSec:
                
                header.sectionHeader.text = pastBooking.paymentMethod
                
                return header
                
            case .PBRFirstRow:
                
                let cell :PBPaymentTypeCell = tableView.dequeueReusableCell(withIdentifier: "PBPaymentTypeCell") as! PBPaymentTypeCell
                if bookingArray!.paymentMethod == 2{
                    cell.cashOrCardImage.image = #imageLiteral(resourceName: "history_card_icon")
                    cell.cashOrCardOption.text = pastBooking.card
                }else{
                    cell.cashOrCardImage.image = #imageLiteral(resourceName: "cash")
                    cell.cashOrCardOption.text = pastBooking.cash
                }
                cell.amountLabel.text =  Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.cardOrCashPaid))
                return cell
                
            default:
                let cell :PBPaymentTypeCell = tableView.dequeueReusableCell(withIdentifier: "PBPaymentTypeCell") as! PBPaymentTypeCell
                cell.cashOrCardImage.image = #imageLiteral(resourceName: "wallet")
                cell.cashOrCardOption.text = "WALLET"
                cell.amountLabel.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", bookingArray!.walletAmtPaid))
               
                return cell
            }
            
        case .PBReceiversDetails:
            
            switch rowType {
            case .PBHeaderSec:
                header.sectionHeader.text = pastBooking.signatureConfirm
                return header
                
            default:
                let cell :PBReceiversDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBReceiversDetailsCell") as! PBReceiversDetailsCell
                cell.activityIndicator.startAnimating()
                cell.signatureImage.kf.setImage(with: URL(string: (bookingArray?.signature)!),
                                                placeholder:UIImage.init(named: "profile"),
                                                options: [.transition(ImageTransition.fade(1))],
                                                progressBlock: { receivedSize, totalSize in
                },
                                                completionHandler: { image, error, cacheType, imageURL in
                                                    cell.activityIndicator.stopAnimating()
                })
                
                return cell
            }
            
        }
    }
}

extension PastBookingController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
