//
//  SplashViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
class SplashViewController: UIViewController {
    var window: UIWindow?
    
    @IBOutlet var splashImage: UIImageView!
    
    var mask: CALayer?
    var imageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // adjust the splash image to screen according to the screen height
        if(iPHONE.IS_iPHONE_5) //640x1136
        {
           
            splashImage.image = UIImage(named:"splash_640x1136.png")
        }else if(iPHONE.IS_iPHONE_4s) //640x960
        {
            splashImage.image = UIImage(named:"splash_640x960.png")
        }else if(iPHONE.IS_iPHONE_6) //750x1134
        {
            splashImage.image = UIImage(named:"splash_750x334.png")
        }else if(iPHONE.IS_iPHONE_6_Plus) //1242x2208
        {
            splashImage.image = UIImage(named:"splash_1242x2208.png")
        }else{
            splashImage.image = UIImage(named:"splash_1125x2436.png")
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let defaults = UserDefaults.standard
        defaults.setValue(deviceID, forKey:USER_INFO.DEVICE_ID)  // storing the device id
        defaults.synchronize()
        
        Timer.scheduledTimer(timeInterval: 2.0,
                             target: self,
                             selector: #selector(self.methodToBeCalled),
                             userInfo: nil,
                             repeats: false); 
        
    }
    
    /// checks whether the session token is there r not, if "yes" it moves inside r goes to signin
    @objc func methodToBeCalled(){
        UIApplication.shared.isStatusBarHidden = false
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            //vani 06/04/2020
//            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//            let nextVC = storyboard.instantiateViewController(withIdentifier: "LiveMHelpContainer") as! HelpViewController //storyBoard.instantiateViewController(identifier: "LiveMHelpContainer") as! HelpViewController
//            self.navigationController?.pushViewController(nextVC, animated: false)
            self.performSegue(withIdentifier: "toHelpVC", sender: nil)
            _ = Utility.deleteSavedData()
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController  = storyboard.instantiateViewController(withIdentifier: "HomeTabBarController")
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.viewControllers.append(mainViewController)
        }
    }
}


