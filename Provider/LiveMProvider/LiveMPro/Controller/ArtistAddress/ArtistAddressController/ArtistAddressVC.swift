//
//  ArtistAddressVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 30/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
protocol ArtistAddressDelegate {
    func updateArtistDelegate(address: String,addressID:String)
}


class ArtistAddressVC: UIViewController {
    var delegate: ArtistAddressDelegate?
    var updateEventsModel = ProfileListModel()
    var  locationdata = [LocationModelClass]()
    var locationModel = LocationModelClass()
    var fromSchedule = false
    var indexDeleted = -1
    var defaultIndex = -1
    
    
    @IBOutlet weak var addressTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       self.updateTheTableViewWithData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveAction(_ sender: Any) {
        self.performSegue(withIdentifier: "toAddAddress", sender: nil)
    }
    
    @IBAction func backToVc(_ sender: Any) {
        if fromSchedule{
            _ = navigationController?.popViewController(animated:true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //get the saved address data from locationModel
    func updateTheTableViewWithData(){
        locationModel.getTheAddressesOfArtist { (succeeded, responeData) in
            self.locationdata = responeData
            self.addressTableView.reloadData()
        }
    }
    
    // delete target from cellForRowAtIndex
    @objc func deleteTheAddress(_ sender : UIButton){
        if defaultIndex == sender.tag{
            let alertView = DefaultAddressAlert.instance
            alertView.show()
            
        }else{
            indexDeleted = sender.tag
            self.alertForDeleteAddress()
        }
    }
    
    /// delete artist address which is saved
    func alertForDeleteAddress(){

        let alert = UIAlertController(title: "Confirm", message: "Are you sure, you want to Delete??", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.locationModel.deleteSavedData(addressID:self.locationdata[self.indexDeleted].id, completionHandler: { (succeeded) in
                if  succeeded{
                    self.updateTheTableViewWithData()
                }
            })
        })
        let noButton = UIAlertAction(title: "No", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            print("you pressed No, thanks button")
            // call method whatever u need
        })
        alert.addAction(yesButton)
        alert.addAction(noButton)
        present(alert, animated: true) //{ _ in }
    }
    
    ///This function calculates the height of content Label  ListingCell row's and returns the height of the label.
    func heightForView(text: String, width: CGFloat) -> CGFloat {
        
        let label: UILabel = UILabel(frame: CGRect(x: 0,
                                                   y: 0,
                                                   width: width,
                                                   height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }

}

extension ArtistAddressVC:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let params: [String : Any] =  [
            "address" :self.locationdata[indexPath.row].id]
        
        if fromSchedule{
            self.delegate?.updateArtistDelegate(address:self.locationdata[indexPath.row].address,addressID:self.locationdata[indexPath.row].id)
            _ = self.navigationController?.popViewController(animated:true)
        }else{
            updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeeded) in
                
                if succeeded{
                    self.delegate?.updateArtistDelegate(address:self.locationdata[indexPath.row].address,addressID:"")
                    
                    self.dismiss(animated: true, completion: nil)
                }else{
                    
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var width: CGFloat = 0.0
        var height: CGFloat = 0.0
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "artistAddress") as! ArtistAddressTableCell
        width = cell.contentView.frame.width
        width = width - 145.0
        cell.address.text = locationdata[indexPath.row].address
        height =  heightForView(text: cell.address.text! , width: width)
        height = height + 50
        return height
    }
}


extension ArtistAddressVC:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locationdata.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "artistAddress") as! ArtistAddressTableCell
        cell.address.text = locationdata[indexPath.row].address
        cell.addressTitle.text = locationdata[indexPath.row].addressType
        
        if locationdata[indexPath.row].defaultAddress == 0{
            cell.deleteAddress.isSelected = false
            cell.defaultButton.isSelected = false
        }else{
            cell.deleteAddress.isSelected = true
            cell.defaultButton.isSelected = true
            defaultIndex = indexPath.row
        }
        
        cell.deleteAddress.tag = indexPath.row
        cell.deleteAddress.addTarget(self, action: #selector(deleteTheAddress(_:)), for:.touchUpInside)
        
        switch locationdata[indexPath.row].addressType {
        case "home":
            cell.addressTypeImage.image = #imageLiteral(resourceName: "your_address_home_icon")
        case "office":
            cell.addressTypeImage.image = #imageLiteral(resourceName: "your_address_briefcase_icon")
        default:
            cell.addressTypeImage.image = #imageLiteral(resourceName: "your_address_heart_icon")
        }
        cell.selectionStyle = .none
        return cell
    }
}


