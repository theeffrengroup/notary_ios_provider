//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import UIKit
import Photos
import Kingfisher
import Firebase

import CoreLocation

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UINavigationControllerDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var inputText: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var widthMore: NSLayoutConstraint!
    @IBOutlet weak var heightOFContainer: NSLayoutConstraint!
    @IBOutlet weak var bottomContainerConstraint: NSLayoutConstraint!
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    let locationManager = CLLocationManager()
    var items = [Message]()
    let barHeight: CGFloat = 50
    var canSendLocation = true
    var mqttManager = MQTT.sharedInstance
    var uploadImage = UploadImageModel()
    var chatModel = Chat()
    var photos: [INSPhotoViewable] = []
    var customerID = ""
    var bookingID = ""
    var custName = ""
    var custImage = ""
    var value = 0
    
    var pastBooking = false
    //MARK: Methods
    @IBOutlet weak var bookID: UILabel!
    
    @IBOutlet weak var customerName: UILabel!
    

    func customization() {
  
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 66.0
        self.locationManager.delegate = self
 
        bookID.text = "Booking ID: " + bookingID
        customerName.text =  custName

        if pastBooking{
            bottomContainerConstraint.constant = -55
        }else{
            bottomContainerConstraint.constant = 5
        }
    }
    
    
    @IBAction func dismissViewAction(_ sender: Any) {
        TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.reveal.rawValue,
                                                                  subType: CATransitionSubtype.fromBottom.rawValue,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    func updateTheMessageSendToServer(sendDict:[String:Any],completion:@escaping(Bool)->()) {
        
        self.chatModel.updateToServerchat(params: sendDict, completionHandler: { (succeeded) in
            if succeeded{
                completion(true)
            }else{
                completion(false)
            }
        })
    }
    
    func composeMessage(type: MessageType, content: Any)  {
        
        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int64(Date().timeIntervalSince1970), isRead: false)
     //let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int64(Date().timeIntervalSince1970 * 1000), isRead: false)
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.heightOFContainer.constant =   40
        })
        
        var dict  = [String : Any]()
        let messageType = message.type
        let content =   message.content
        let timestamp = message.timestamp
        
        switch messageType {
        case .photo:
            Helper.showPI(message: "uploading...")
            if let image = message.content as? UIImage{
                let data = image.jpegData(compressionQuality: 1.0) //UIImageJPEGRepresentation(image, 1.0)
                let imageDict:[String:Any] = ["bookingId":bookingID]
                ImageUploadAPI().requestWith(imageData: data, onCompletion: { (success,imageUrl) in
                    if success{
                        dict = ["type"  : "photo",
                                "content": imageUrl,
                                "fromID": "sender",
                                "timestamp":timestamp,
                                "bookingID":self.bookingID,
                                "custID":self.customerID]
                        
                        self.updateTheMessageSendToServer(sendDict:dict,completion: { (success) in
                            if success{
                                let message = Message.init(type: .photo, content: imageUrl, owner: .sender, timestamp: timestamp, isRead: false)
                                self.items.append(message)
                                 Helper.hidePI()
                                DispatchQueue.main.async {
                                    self.tableView.reloadData()
                                    self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                                }
                            }
                        })
                    }
                }, onError: { (error) in
                    Helper.alertVC(errMSG: error as! String)
                    print(error as Any)
                })
            }
            break
        case .location:
            break
        default:
            items.append(message)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
            }
            
            dict = ["type"  : "text",
                    "content": content,
                    "fromID": "sender",
                    "timestamp":timestamp,
                    "bookingID":bookingID,
                    "custID":customerID]
            Helper.showPI(message: "sending...")
            chatModel.updateToServerchat(params: dict, completionHandler: { (succeeded) in
                if succeeded{
                    Helper.hidePI()
                }else{
                    Helper.hidePI()
                }
            })
            break
        }
    }
    
    
    func checkLocationPermission() -> Bool {
        var state = false
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            state = true
        case .authorizedAlways:
            state = true
        default: break
        }
        return state
    }
    
    func animateExtraButtons(toHide: Bool)  {
        
    }
    
    @IBAction func showMessage(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
    }
    
    @IBAction func selectGallery(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
        chooseFromPhotoGallery()
    }
    
    @IBAction func selectCamera(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
        chooseFromCamera()
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        self.canSendLocation = true
        self.animateExtraButtons(toHide: true)
        if self.checkLocationPermission() {
            self.locationManager.startUpdatingLocation()
        } else {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    @IBAction func showOptions(_ sender: Any) {
        let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image",
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel",
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera",
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery",
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if let text = self.inputText.text {
            if text.length > 0 && text != "Type message here.."{
                self.composeMessage(type: .text, content: self.inputText.text!)
                self.inputText.text = ""
            }
        }
    }
    
    
    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        if tableView.isDragging {
    //            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
    //            UIView.animate(withDuration: 0.3, animations: {
    //                cell.transform = CGAffineTransform.identity
    //            })
    //        }
    //   }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.items[indexPath.row].owner {
        case .sender:
            
          let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
          
            switch self.items[indexPath.row].type {
            case .text:
                cell.message.text = self.items[indexPath.row].content as! String
                cell.messageBackground.backgroundColor = COLOR.senderColor
                cell.message.isHidden = false
                cell.actionOnImageView.isHidden = true
                cell.messageBackground.image = nil
                
            case .photo:
                cell.message.isHidden = true
                cell.actionOnImageView.isHidden = false
                cell.actionOnImageView.tag = indexPath.row
                cell.actionOnImageView.addTarget(self, action: #selector(self.showImage(_:)), for:.touchUpInside)
                cell.messageBackground.kf.setImage(with: URL(string: self.items[indexPath.row].content as! String),
                                                   placeholder:UIImage.init(named: "loading"),
                                                   options: [.transition(ImageTransition.fade(1))],
                                                   progressBlock: { receivedSize, totalSize in
                    },
                                                       completionHandler: { image, error, cacheType, imageURL in
                                                        
                    })
              
            case .location:
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
                cell.actionOnImageView.isHidden = true
            }
          return cell
        case .receiver:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            
            cell.profilePic.kf.setImage(with: URL(string: custImage),
                                        placeholder:UIImage.init(named: "loading"),
                                        options: [.transition(ImageTransition.fade(1))],
                                        progressBlock: { receivedSize, totalSize in
            },
                                        completionHandler: { image, error, cacheType, imageURL in
            })
            switch self.items[indexPath.row].type {
            case .text:
                cell.messageBackground.backgroundColor = COLOR.receiverColor
                cell.message.isHidden = false
                cell.actionOnImageView.isHidden = true
                cell.messageBackground.image = nil
                cell.message.text = self.items[indexPath.row].content as! String
         
            case .photo:
                cell.message.isHidden = true
                cell.actionOnImageView.isHidden = false
                cell.actionOnImageView.tag = indexPath.row
                cell.actionOnImageView.addTarget(self, action: #selector(self.showImage(_:)), for:.touchUpInside)

                cell.messageBackground.kf.setImage(with: URL(string: self.items[indexPath.row].content as! String),
                                                   placeholder:UIImage.init(named: "loading"),
                                                   options: [.transition(ImageTransition.fade(1))],
                                                   progressBlock: { receivedSize, totalSize in
                },
                                                   completionHandler: { image, error, cacheType, imageURL in
                })
                
            case .location:
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
                cell.actionOnImageView.isHidden = true
            }
            return cell
        }
    }
    
    
    // delete target from cellForRowAtIndex
    @objc func showImage(_ sender : UIButton){
        if let imageUrl = self.items[sender.tag].content as? String{
            let vc:ShowChatImageView = ShowChatImageView.instance
            vc.show(image:imageUrl)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.inputText.resignFirstResponder()
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /// Current time stamp
    static var currentTimeStamp: String {
        return String(format: "%0.0f", Date().timeIntervalSince1970 * 1000)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        if let lastLocation = locations.last {
            if self.canSendLocation {
                let coordinate = String(lastLocation.coordinate.latitude) + ":" + String(lastLocation.coordinate.longitude)
                let message = Message.init(type: .location, content: coordinate, owner: .sender, timestamp: Int64(Date().timeIntervalSince1970), isRead: false)
                items.append(message)
                self.canSendLocation = false
            }
        }
    }
    
    //MARK: ViewController lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Chat View"
        self.view.layoutIfNeeded()
    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gotNewMessage"), object: nil);
        NotificationCenter.default.removeObserver(self)
    }
    
    
    ///********notifies when the booking has been cancelled*******//
    @objc func gotNewMessage(_ notification: NSNotification) {
        if let bid = notification.userInfo?["bid"] as? String{
            if bid != bookingID{
                return
            }
        }
        
        let msg = notification.userInfo?["content"] as! String
        let timeStamp = notification.userInfo?["timestamp"] as! NSNumber
        
        
        if items.index(where: {$0.timestamp == Int64(timeStamp)}) != nil {
            return
        }
        
        switch notification.userInfo?["type"] as! NSNumber {
        case 2:
            let message = Message.init(type: .photo , content: msg, owner: .receiver, timestamp: Int64(truncating: timeStamp), isRead: false)
            items.append(message)
            break
        case 3:
            break
            
        default:
            let message = Message.init(type: .text , content: msg, owner: .receiver, timestamp: Int64(truncating: timeStamp), isRead: false)
            
            items.append(message)
            break
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatVC.showKeyboard),
                                               name:UIResponder.keyboardWillShowNotification,
                                               object: nil);
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatVC.keyboardDidShow),
                                               name:UIResponder.keyboardDidShowNotification,
                                               object: nil);
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatVC.keyboardWillHide),
                                               name:UIResponder.keyboardWillHideNotification,
                                               object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotNewMessage(_:)), name: Notification.Name("gotNewMessage"), object: nil)
              self.navigationController?.isNavigationBarHidden = false
     
    }
    
    // MARK: Keyboard Dodging Logic
    //MARK: NotificationCenter handlers
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.bottomContainerConstraint.constant = height + 10
                self.view.layoutIfNeeded()
            })
        }
    }
    
    
    @objc func keyboardDidShow(notification: NSNotification) {
        self.scrollToBottomMessage()
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomContainerConstraint.constant = 5
            self.view.layoutIfNeeded()
        })
    }
    
    // Scroll to bottom of table view for messages
    func scrollToBottomMessage() {
        if self.items.count == 0 {
            return
        }
        let bottomMessageIndex = IndexPath(row: items.count - 1, section: 0)
        tableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.customization()

        self.tableView.addPullRefresh { [weak self] in
            // some code
            sleep(1)
            self?.value += 1
            self?.fetchData(val: String(describing:self!.value))
        }
        fetchData(val:"0")
    }
    
    
    //Downloads messages
    func fetchData(val:String) {
        chatModel.getTheChatHistory(bookingID: bookingID ,indexVal: val)  {[weak weakSelf = self]  (success, messagedDict) in
            if success{
                if val == "0"{
                    weakSelf?.items = messagedDict
                }else{
                    weakSelf?.items += messagedDict
                }
                weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
                DispatchQueue.main.async {
                    if let state = weakSelf?.items.isEmpty, state == false {
                        weakSelf?.tableView.reloadData()
                        if val == "0"{
                            weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                        }
                        self.tableView.stopPullRefreshEver()
                    }
                }
            }else{
                self.tableView.stopPullRefreshEver()
            }
        }
    }
    
    
    //MARK: - open Photo Gallary
    func chooseFromPhotoGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    //MARK: - Open Camera
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
        }
    }
    
    func adjustFrames() {
        heightOFContainer.constant = inputText.contentSize.height
    }
    
    func resizeImage(image: UIImage) -> UIImage {
        var actualHeight  = image.size.height;
        var actualWidth = image.size.width;
        let maxHeight = 600.0;
        let maxWidth = 800.0;
        var imgRatio  = actualWidth  / actualHeight
        let maxRatio = maxWidth / maxHeight
        let compressionQuality = 0.5;
        
        if ( Double(actualHeight) > maxHeight) || ( Double(actualWidth) > maxWidth){
            if(Double(imgRatio) < maxRatio){
                
                imgRatio = CGFloat(maxHeight / Double(actualHeight));
                actualWidth = imgRatio * actualWidth;
                actualHeight = CGFloat(maxHeight);
            }else if (Double(imgRatio) > maxRatio){
                imgRatio = CGFloat(maxWidth / Double(actualWidth));
                actualHeight = imgRatio * actualHeight;
                actualWidth = CGFloat(maxWidth);
            }else{
                actualHeight = CGFloat(maxHeight);
                actualWidth = CGFloat(maxWidth);
            }
        }
        let rect = CGRect(x:0.0,y:0.0,width:actualWidth,height:actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality)) //UIImageJPEGRepresentation(img!, CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
}


//MARK: - Imagepicker delegate
extension ChatVC:UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            self.composeMessage(type: .photo, content: self.resizeImage(image: pickedImage))
        } else {
            let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            self.composeMessage(type: .photo, content: self.resizeImage(image: pickedImage))
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension UIImage {
    func imageWithColor(tintColor: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        tintColor.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension ChatVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type message here.."{
            textView.text = nil
            textView.textColor = UIColor.black
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.widthMore.constant = 0
            })
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Type message here.."
            textView.textColor = UIColor.lightGray
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.widthMore.constant = 32
            })
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            if let text = self.inputText.text {
                if text.length > 0 {
                    UIView.animate(withDuration: 0.5, animations: { () -> Void in
                        self.widthMore.constant = 32
                    })
                }
            }else{
                textView.text = "Type message here.."
                textView.textColor = UIColor.lightGray
            }
            textView.resignFirstResponder()
            return false
        }
        adjustFrames()
        return true;
    }
}

