//
//  ChatModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 09/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa


class Chat: NSObject {
    
    let disposebag = DisposeBag()
    var messageDict = [Message]()
    var manager  = ChatManager()
    
    
    /// get the chat history
    ///
    /// - Parameters:
    ///   - bookingID: chat history of particular booking id
    ///   - indexVal: paging added hence the index value starts from 0 to n
    ///   - completionHandler: returns the message data model on completion
    func getTheChatHistory(bookingID:String,indexVal:String,completionHandler:@escaping (Bool,[Message]) -> ()) {
        if indexVal == "0"{
            manager.downloadAllMessages(forUserID: bookingID) { (chatDict) in
                if chatDict.count == 0{
                    Helper.showPI(message: "loading..")
                }
                completionHandler(true,self.updateTheChatData(chatDict: chatDict ))
            }
        }
        let rxApiCall = ChatAPI()
        rxApiCall.getTheChatHist(bookingID: bookingID + "/" + indexVal)
        rxApiCall.chat_Response
            .subscribe(onNext: {response in
                switch response.httpStatusCode{
                case 200:
                    if let dict = response.data["data"] as? [[String:Any]]  {
                        let chatDict  : [String : Any] = ["chatDict": dict]
                        if indexVal == "0"{
                            self.manager.saveTheMessage(forUserID: bookingID, dict: chatDict)
                        }
                        completionHandler(true,self.updateTheChatData(chatDict: dict ))
                    }else{
                        completionHandler(false, self.messageDict)
                    }
                    break
                default:
                    break
                }
            }, onError: { error in
                completionHandler(false, self.messageDict)
                Helper.alertVC(errMSG: error.localizedDescription)
                print(error)
            }).disposed(by: disposebag)
    }
    
    
    /// update the chat history which got from api
    /// - Parameter chatDict: chat array of dictionary
    /// - Returns: return to message model
    func updateTheChatData(chatDict:[[String:Any]]) -> [Message] {
        messageDict = [Message]()
        for chat in chatDict {
            switch chat["type"] as!  NSNumber{
            case 1:
                if String(describing:chat["fromID"]!) == Utility.userId {
                    messageDict.append(Message.init(type: .text, content: chat["content"] as Any, owner: .sender, timestamp: chat["timestamp"] as! Int64, isRead: true))
                }else{
                    messageDict.append(Message.init(type: .text, content: chat["content"] as Any, owner: .receiver, timestamp: chat["timestamp"] as! Int64, isRead: true))
                }
            case 2:
                if String(describing:chat["fromID"]!) == Utility.userId  {
                    messageDict.append(Message.init(type: .photo, content: chat["content"] as Any, owner: .sender, timestamp: chat["timestamp"] as! Int64, isRead: true))
                }else{
                    messageDict.append(Message.init(type: .photo, content: chat["content"] as Any, owner: .receiver, timestamp: chat["timestamp"] as! Int64, isRead: true))
                }
                break
                
            default:
                break
            }
        }
        return messageDict
    }
    
    
    /// post the new message to server with customer id as target id
    ///
    /// - Parameters:
    ///   - params: params belongs to single msg,having timestamp, target id, my id, booking id
    ///   - completionHandler: return true when the message send successfull
    func updateToServerchat(params:[String:Any],completionHandler:@escaping (Bool) ->()){
        let rxApiCall = ChatAPI()
        var typeID = 0
        switch params["type"] as! String{
        case "photo":
            typeID = 2
            break
        case "text":
            typeID = 1
            break
        default:
            typeID = 3
            break
        }
        let paramDict : [String : Any] = ["type": typeID,
                                          "timestamp":params["timestamp"]! as Any,
                                          "content" :params["content"]! as Any,
                                          "fromID":Utility.userId,
                                          "bid":params["bookingID"]! as Any,
                                          "targetId":params["custID"]! as Any]
        
        rxApiCall.postTheMessage(parameters:paramDict)
        rxApiCall.chat_Response
            .subscribe(onNext: {response in
                
                switch response.httpStatusCode{
                case 200:
                    self.manager.saveTheSingleMessage(forUserID: params["bookingID"]! as! String, dict: paramDict)
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    break
                }
            }, onError: {  error in
                completionHandler(false)
                Helper.alertVC(errMSG: error.localizedDescription)
                print(error)
            }).disposed(by: disposebag)
    }
}



