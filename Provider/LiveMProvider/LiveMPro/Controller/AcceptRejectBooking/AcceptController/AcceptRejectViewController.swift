//
//  AcceptRejectViewController.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 29/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher

protocol MoveToUpcomingDelegate {
    func moveToUpcoming()
}

class AcceptRejectViewController: UIViewController,UIScrollViewDelegate {
    var delegate: MoveToUpcomingDelegate?
    
    @IBOutlet weak var acceptTableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    var bookingData:Request?
    var acceptRejectVC = AcceptRejectModel()
    var bookingMarker = GMSMarker()
    var tableHeaderViewHeight = 200 as CGFloat
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        acceptTableView.estimatedRowHeight = 10
        acceptTableView.rowHeight = UITableView.automaticDimension
        inititateTheview()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)
        self.tabBarController?.tabBar.isHidden = true
        setupGestureRecognizer()
        self.navigationController?.presentTransparentNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelHandlingAcceptedRejected(_:)), name: Notification.Name("cancelledBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleExpiredBooking), name: Notification.Name("BookingExpired"), object: nil)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func inititateTheview()  {
        self.view.layoutIfNeeded()
        mapView = self.acceptTableView.tableHeaderView as! GMSMapView
        self.acceptTableView.tableHeaderView = nil
        self.acceptTableView.addSubview(mapView)
        self.acceptTableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight,left: 0, bottom: 0, right: 0)
        self.acceptTableView.contentOffset = CGPoint(x:0,y:-tableHeaderViewHeight)
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees((bookingData?.latitude)!) ,longitude: CLLocationDegrees((bookingData?.longitude)!) , zoom: 16)
        var destinationLocation = CLLocation()
        destinationLocation = CLLocation(latitude: (bookingData?.latitude)!,  longitude: (bookingData?.longitude)!)
        bookingMarker.position = destinationLocation.coordinate
        bookingMarker.icon = #imageLiteral(resourceName: "home_pickup_icon")
        self.mapView.animate(to: camera)
        bookingMarker.map = self.mapView
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.acceptTableView.contentOffset.y < -tableHeaderViewHeight {
            mapView.frame = CGRect(x:0, y: self.acceptTableView.contentOffset.y, width:self.acceptTableView.bounds.size.width, height: -self.acceptTableView.contentOffset.y)
        }
    }
    
    ///********notifies when the booking has been cancelled*******//
    @objc func bookingCancelHandlingAcceptedRejected(_ notification: NSNotification) {
        if String(describing:notification.userInfo!["bookingId"]!)  == String(describing:bookingData!.bookingId) {
            _ = navigationController?.popViewController(animated:true)
        }
    }
    
    @objc func handleExpiredBooking() {
         _ = navigationController?.popViewController(animated:true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToVc(_ sender: Any) {
        _ = navigationController?.popViewController(animated:true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.hideTransparentNavigationBar()
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "BookingExpired"), object: nil);
        
    }
    
    @IBAction func declineAction(_ sender: Any) {
        self.updateTheAcceptRejectResponse(status:4)
        
        
        
    }
    
    func updateTheAcceptRejectResponse(status:Int){
        let ud = UserDefaults.standard
        Helper.showPI(message: loading.load)
        var dict = [String : Any]()
        if bookingData?.bookingType == 2{
            let reminderData = ReminderModel()
            reminderData.startDate = (bookingData?.bookingRequestedFor)!
            reminderData.enddate = (bookingData?.bookingRequestEnds)!
            reminderData.latit = (bookingData?.latitude)!
            reminderData.logit = (bookingData?.longitude)!
            reminderData.bookingID = (bookingData?.bookingId)!
            reminderData.address = (bookingData?.addLine1)!
            
            ReminderModel().eventReminder(data: reminderData, completion: { (reminderID) in
                dict =  ["bookingId" :self.bookingData!.bookingId as Any,
                         "latitude" : ud.object(forKey: "currentLat")! as Any,
                         "longitude" : ud.object(forKey: "currentLog")! as Any,
                         "status":status as Any,
                         "reminderId":reminderID]
                self.acceptRejectVC.acceptRejectModelBooking(params: dict ,completionHandler: { (succeeded) in
                    if succeeded{
                        Helper.hidePI()
                        self.bookingData?.statusCode = status
                        _ = self.navigationController?.popViewController(animated: true)
                    }else{
                        Helper.hidePI()
                    }
                })
            })
            
        }else{
            dict =  ["bookingId" :bookingData!.bookingId as Any,
                     "latitude" : ud.object(forKey: "currentLat")! as Any,
                     "longitude" : ud.object(forKey: "currentLog")! as Any,
                     "status":status as Any]
            self.acceptRejectVC.acceptRejectModelBooking(params: dict ,completionHandler: { (succeeded) in
                if succeeded{
                    Helper.hidePI()
                    self.bookingData?.statusCode = status
                    _ = self.navigationController?.popViewController(animated: true)
                    if status == 3 {
                       self.delegate?.moveToUpcoming()
                    }
                    
                }else{
                    Helper.hidePI()
                }
            })
        }
        
    }
    
    
    @IBAction func acceptAction(_ sender: Any) {
        self.updateTheAcceptRejectResponse(status:3)
        
    }
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        switch segue.identifier! as String {
        case "customerReviews":
            let nav = segue.destination as! UINavigationController
            if let review: CustReviewsVC = nav.viewControllers.first as! CustReviewsVC?
            {
                
                
                review.customerID = (bookingData?.customerId)!
                review.custPic = (bookingData?.profilePic)!
                review.cusName = (bookingData?.firstName)!
            }
            break
        default:
            break
        }
    }
}

extension AcceptRejectViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // customerReviews
        if indexPath.row == 0 && indexPath.section == 1{
            self.performSegue(withIdentifier: "customerReviews", sender: nil)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension AcceptRejectViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 2
        case 2:
            return 3
        case 3:
            return 2 + (bookingData?.services.count)!
        case 4:
            return 1
        case 5:
            return 2
        case 6:
            if bookingData?.jobDesc == "" {
                return 0
            }
            else{
                return 2
            }
            
        case 7:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableCell
        let mediumView = tableView.dequeueReusableCell(withIdentifier: "mediumView") as! MediumViewTableCell
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileAddress") as! AcceptAddressTableCell
            cell.address.text = bookingData?.addLine1
            return cell
            
        case 1:
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "profileProgress") as! ProfileProgressTableCell
                cell.updateTheRequestData(data:(bookingData)!)
                return cell
            }
        case 2:
            switch indexPath.row{
            case 0:
                return mediumView
            case 1 :
                header.headerOfCell.text = "Amount"
                return header
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
                let dateArray = Helper.getTheDateFromTimeStamp(timeStamp:bookingData!.actuallGigTimeStart).components(separatedBy: "|")
                cell.dateOfAppointment.text = "Date: " + dateArray[0]
                cell.timeOfAppointment.text = "Time: " + dateArray[1]
                
                cell.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",bookingData!.totalAmt))
                return cell
            }
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileData") as! ProfileDataTableCell
            switch indexPath.row{
            case 0:
                return mediumView
                
            case 1 :
                header.headerOfCell.text = "Selected Services"
                return header
                
//            case 2 + (bookingData?.services.count)! :
//                cell.key.text = onBookingVC.Discount
//                cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.discount))
//                return cell
                
//            case 2 + (bookingData?.services.count)! :
//                cell.key.text = onBookingVC.Total
//                cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.totalAmt))
//                return cell
//
            default:
                
                if bookingData?.serviceType == 1{
                    if (bookingData?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingData?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                        (bookingData?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                         (bookingData?.services[indexPath.row - 2].serviceName)! == "Discount"{
                        cell.key.text = (bookingData!.services[indexPath.row - 2].serviceName)
                    }
                    else{
                     cell.key.text = (bookingData?.services[indexPath.row - 2].serviceName)! + " * " + String(describing:bookingData!.services[indexPath.row - 2].quantity)
                    }
                    cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.services[indexPath.row - 2].servicePrice))
                }else{
                    
                    let amt = bookingData!.services[indexPath.row - 2].servicePrice / Double(bookingData!.services[indexPath.row - 2].quantity)
                    
                    if (bookingData?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingData?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                        (bookingData?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                        (bookingData?.services[indexPath.row - 2].serviceName)! == "Discount" {
                        cell.key.text = (bookingData!.services[indexPath.row - 2].serviceName)
                        cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.services[indexPath.row - 2].servicePrice))
                    }
                    else {
                         cell.key.text = Helper.timeInHourMin((bookingData?.totalJobTime)!)
                         cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.totalHourlyFee))
//                        //cell.key.text = (bookingData?.services[indexPath.row - 2].serviceName)! +
//                            " (" + String(format:"%.2f",amt)  + " * " + String(describing:bookingData!.services[indexPath.row - 2].quantity) + " hr)"
                    }
                   
                  
                }
                return cell
            }
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell
            cell.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.totalAmt))
            return cell
        case 5:
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentType") as! PaymentMethodTableCell
                if (bookingData?.paidByWallet)! {
                    cell.paymentType.text = "Wallet" + " + " + (bookingData?.paymentMethod)!
                }else{
                    cell.paymentType.text = bookingData?.paymentMethod
                }
                cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                return cell
            }
        case 6:
            switch indexPath.row{
            case 0:
                return mediumView
//            case 1:
//                header.headerOfCell.text =  "Job Description"
//                return header
            case 3:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "jobDesc") as! JobDescTableCell
                if bookingData?.jobDesc == "" {
                    cell.jobDescription.text = "This booking does not have a description"
                }
                else {
                    cell.jobDescription.text = bookingData!.jobDesc
                }
                
                return cell
            }
            
        default:
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                return cell
            }
        }
    }
}
extension AcceptRejectViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}
