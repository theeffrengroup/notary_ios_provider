//
//  ProfileProgressTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 29/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import FloatRatingView

class ProfileProgressTableCell: UITableViewCell {
    @IBOutlet weak var progressText: UILabel!
    @IBOutlet weak var custName: UILabel!
    
    @IBOutlet weak var bookingID: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    
    var expiryTime:Float = 0.0
    var remainingTime:Float = 0.0
    var fullTime:Float = 0.0
    
    var acceptTimer  = Timer()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateTheRequestData(data: Request) {
        custName.text = data.firstName
        categoryName.text = data.categoryName
        bookingID.text = "Booking ID: " + String(data.bookingId)
        
        
        fullTime = Float(data.bookingExpireTime - data.bookingRequestedFor)
        expiryTime = Float(self.getCreationTimeInt(expiryTimeStamp: data.bookingExpireTime))
        
        if !acceptTimer.isValid {
            acceptTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
        }
    }
    
    
    /**
     *  Calculate time for Remaining
     */
    @objc func timerTick(_ timer: Timer) {
        let remainingTime: Float = expiryTime - 1
        progressText.text = timeFormate(Int(remainingTime))
        expiryTime = remainingTime
        
        if expiryTime <= 0.0 {
            acceptTimer.invalidate()
        }
    }
    
    /**
     *  Check for Time formate
     *
     *  @param remainingTime Remaining time
     *
     *  @return Return must 00:00/00
     */
    func timeFormate(_ remainingTime: Int) -> String {
        let min: Int = remainingTime / 60
        let secs: Int = remainingTime % 60
        
        var minsString = ""
        var secsString = ""
        
        if min < 10{
            minsString = "0" + String(describing:min)
        }else{
            minsString = String(describing:min)
        }
        
        if secs < 10{
            secsString = "0" + String(describing:secs)
        }else{
            secsString = String(describing:secs)
        }
        return minsString + ":" + secsString
    }
    /**
     *  Get time interval between two times
     *
     *  @param dateFrom Datefrom
     *  @param dateTo   DateTo
     *
     *  @return Returns [01h:30m]
     */
    func getCreationTimeInt(expiryTimeStamp : Int64) -> Int64{
        let distanceTime = Date().timeIntervalSince1970
        return expiryTimeStamp - Int64(Int(distanceTime))
    }
}

