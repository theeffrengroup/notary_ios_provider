//
//  IncomingVideocallView.swift
//  webRtc_module
//
//  Created by Imma Web Pvt Ltd on 08/09/17.
//  Copyright © 2017 3embed. All rights reserved.
//

import UIKit
import AVFoundation
import CocoaLumberjack


class IncomingVideocallView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var remoteView: RTCEAGLVideoView!
    @IBOutlet weak var localView: RTCEAGLVideoView!
    @IBOutlet weak var callEndBtn: UIButton!
    @IBOutlet weak var callAcceptBtn: UIButton!


    @IBOutlet weak var videoCallLbl: UILabel!
    @IBOutlet weak var videoCallIcon: UIImageView!
    @IBOutlet weak var incomingVidLbl: UILabel!
    @IBOutlet weak var declineLbl: UILabel!
    @IBOutlet weak var acceptlbl: UILabel!
    
    
    @IBOutlet weak var switch_btn: UIButton!
    @IBOutlet weak var endVcall_btn: UIButton!
    @IBOutlet weak var muteBtn: UIButton!
    
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var calling_userName: UILabel!
    @IBOutlet weak var calling_status: UILabel!
    
    @IBOutlet weak var localViewConstantY: NSLayoutConstraint!
    @IBOutlet weak var endBtnConstantY: NSLayoutConstraint!
    
    
    

    var webRtc: webRTC?
    var callId : String?
    var messageDict : [String:Any]?
    var timer : Timer?
    var callerId : String?
    var isSwitch : Bool?
    
    var player: AVAudioPlayer?
    
    var tapGester : UITapGestureRecognizer?
    var swipeGester : UISwipeGestureRecognizer?
    var panGester : UIPanGestureRecognizer?
    var isvideoSwiped = false
    var otherCallerId : String?  = ""
    
    
    
    var captureSession = AVCaptureSession()
    var sessionOutput = AVCaptureStillImageOutput()

    var previewLayer = AVCaptureVideoPreviewLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    //set data here
    func setMessageData(messageData:[String : Any]) {
        messageDict = messageData
        callId = messageData["callId"] as? String
        callerId = messageData["callerId"] as? String
    }
    
    private func commonInit(){
        
        Bundle.main.loadNibNamed("IncomingVideocallView", owner: self, options: nil)
        
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
        
        localView.isHidden = true
        switch_btn.isHidden = true
        endVcall_btn.isHidden = true
        muteBtn.isHidden = true
        isSwitch = false
        userImageView.isHidden = true
        calling_userName.isHidden = true
        calling_status.isHidden = true
        
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.clipsToBounds = true
        
        //start timer 60 sec for incoming calling screen
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timeoutRemoveScreen), userInfo: nil, repeats: false)
        
        
        tapGester = UITapGestureRecognizer.init(target: self, action: #selector(tapGesterCliked))
        tapGester?.numberOfTapsRequired = 1
        contentView.addGestureRecognizer(tapGester!)
        
        swipeGester = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeGesterCliked))
        swipeGester?.numberOfTouchesRequired = 1
        swipeGester?.direction = .down
        contentView.addGestureRecognizer(swipeGester!)
        
        panGester = UIPanGestureRecognizer.init(target: self, action: #selector(panGesterCliked))
        
    }
    
    
    
    @objc func panGesterCliked(_ panGester: UIPanGestureRecognizer){
        
               if panGester.state == .began || panGester.state == .changed{
        
                    let traslation = panGester.translation(in: UIApplication.shared.keyWindow)
        
                    panGester.view?.center =  CGPoint(x: panGester.view!.center.x + traslation.x, y: panGester.view!.center.y + traslation.y)
        
                    panGester.setTranslation(CGPoint.zero, in: UIApplication.shared.keyWindow)
        }
    }
    
    
    @objc func swipeGesterCliked(_ swipeGester: UISwipeGestureRecognizer){
        
        
        if isvideoSwiped == false{
        
        self.frame = CGRect.init(x: 50, y: 300, width:150, height: 150)
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
        
        self.localView.isHidden = true
        self.switch_btn.isHidden = true
        self.endVcall_btn.isHidden = true
        self.muteBtn.isHidden = true
        isvideoSwiped = true
       
        self.addGestureRecognizer(panGester!)
            
        }else{
            
        }
        
    }
    
    @objc func tapGesterCliked() {
        
        if isvideoSwiped == false{
        
        DDLogDebug("tap gester cliked here:")
        UIView.animate(withDuration: 3 ,delay:5 , options: .showHideTransitionViews , animations: {
            self.localViewConstantY.constant = -(self.localViewConstantY.constant + self.localView.frame.size.height)
            self.endBtnConstantY.constant =  -(self.endBtnConstantY.constant + self.endVcall_btn.frame.size.height)
            }) { (isCompl) in}
            
        }else{
            
            isvideoSwiped = false
            self.frame = CGRect.init(x: 0, y: 0, width:UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.layer.cornerRadius = 0
            self.clipsToBounds = false
            
            self.localView.isHidden = false
            self.switch_btn.isHidden = false
            self.endVcall_btn.isHidden = false
            self.muteBtn.isHidden = false
           
            self.removeGestureRecognizer(panGester!)
        }
    }
    
    
    
    //timeout remove audioScreen
    @objc func timeoutRemoveScreen() {
        
        timer?.invalidate()
        player?.stop()
        webRtc?.disconnect()
        DDLogDebug("***\n\n**********time out called ***************\n*****")
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: "CallsAvailability/" + userID)
        
        if  var callerID = messageDict?["callerIdentifier"] as? String {
            
            if userID == callerID {
                callerID = messageDict?["callerId"] as! String
            }
            MQTTCallManager.sendTimeoutcallRequest(callID: callId!, callerID: callerID, type: 7)
        }
        
       
        self.removeFromSuperview()
        
    }

    
    
    
    //init webRtc
    func initWebrtc(messageData: [String:Any])  {
        
        webRtc = webRTC.init(localView: self.localView, remoteView: self.remoteView, callID: messageData["callId"] as! String)
        callId = messageData["callId"] as? String
        messageDict = messageData
        
    }

    
    @IBAction func endBtncliked(_ sender: Any) {
        
        timer?.invalidate()
        webRtc?.disconnect()
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }

        MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
        MQTTCallManager.sendEndcallRequest(callID: callId!, callerID: callerId!, targetID: userID, callType: AppConstants.CallTypes.videoCall)
        
        
        ///play endcall sound here
        self.playSound("end_of_call", loop: 1)
        
        let when = DispatchTime.now() + 0.30
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
           
            self.removeFromSuperview()
        }

    }
   
    
    @IBAction func acceptBtnCliked(_ sender: Any) {
        
        hideSubviews()
        player?.stop()
        timer?.invalidate()
        
        MQTTCallManager.sendAcceptCallStatus(messageData: messageDict!)
        self.removeCameraLayer()
        self.initWebrtc(messageData:messageDict! )
       
        // webRtc?.switchLocalRemoteView(_localView: self.localView, _remoteView: self.remoteView)
    
    }
    
    
    func switchViews() {
        
        webRtc?.switchLocalRemoteView(_localView: self.localView, _remoteView: self.remoteView)
        localView.isHidden = false
        userImageView.isHidden = true
        calling_userName.isHidden = true
        calling_status.isHidden = true

    }
    
    
    
    func hideSubviews() {
        localView.isHidden = false
        userName.isHidden = true
        videoCallLbl.isHidden = true
        videoCallIcon.isHidden = true
        incomingVidLbl.isHidden = true
        callEndBtn.isHidden = true
        callAcceptBtn.isHidden = true
        declineLbl.isHidden = true
        acceptlbl.isHidden = true
        switch_btn.isHidden = false
        endVcall_btn.isHidden = false
        muteBtn.isHidden = false
        userImageView.isHidden = true
        calling_userName.isHidden = true
        calling_status.isHidden = true
    }
    
    
    /*methodes after pick call*/
    
    
    //switch camera button action//
    @IBAction func switchBtnCliked(_ sender: Any) {
        
        if isSwitch == false{
            isSwitch = true
            webRtc?.client?.swapCameraToBack()
        }else{
            isSwitch = false
            webRtc?.client?.swapCameraToFront()
        }
        
    }
    
    
    
    
    //end video button action//
    @IBAction func endVideocallCliked(_ sender: Any) {
        player?.stop()
        timer?.invalidate()
        webRtc?.disconnect()
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
        
        if let uuid = messageDict?["userId"] as? String{
            messageDict?["callerId"] = uuid
            messageDict?["callerIdentifier"] = userID
        }
        
        if  let callerID = messageDict?["callerId"] as? String {
            if userID != callerID {
                otherCallerId = callerID
            }
            MQTTCallManager.sendEndcallRequest(callID: callId!, callerID: otherCallerId!, targetID: messageDict?["callerIdentifier"] as! String, callType: AppConstants.CallTypes.videoCall)
        }
        
        
        ///play endcall sound here
        self.playSound("end_of_call", loop: 1)
        
        let when = DispatchTime.now() + 0.30
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            self.removeFromSuperview()
        }
        
    }

    
    func playSound(_ soundName: String,loop: Int){
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "wav")else{ return}
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            player  = try AVAudioPlayer(contentsOf: url)
            player?.delegate = self
            guard let player = player else { return}
            player.play()
            
        }catch let error{
            DDLogDebug("error \(error.localizedDescription)")
        }
        
    }
    

    //mute button action//
    @IBAction func muteBtnCliked(_ sender: Any) {
        if muteBtn.isSelected == true{
            webRtc?.client?.unmuteAudioIn()
            muteBtn.isSelected = false
        }else{
            muteBtn.isSelected = true
            webRtc?.client?.muteAudioIn()}
    }
    
    
    func setCallId() {
        callId = randomString(length: 100)
        hideSubviews()
        localView.isHidden = true
        userImageView.isHidden = false
        calling_userName.isHidden = false
        calling_status.isHidden = false
    }
    
    
    //Add camera layer
    func addCameraView(){
        
        let device = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: AVCaptureDevice.Position.front) //AVCaptureDevice.defaultDevice(withDeviceType: AVCaptureDevice.DeviceType.builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: AVCaptureDevice.Position.front)
        do {
            if device != nil{
                let input = try AVCaptureDeviceInput.init(device: device!)
                if captureSession.canAddInput(input){
                    captureSession.addInput(input)
                    sessionOutput.outputSettings = [AVVideoCodecKey  : AVVideoCodecJPEG]
    
                    if captureSession.canAddOutput(sessionOutput){
                        captureSession.addOutput(sessionOutput)
                        captureSession.startRunning()
                        previewLayer = AVCaptureVideoPreviewLayer.init(session: captureSession)
                        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspect
                        previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                        self.contentView.layer.addSublayer(previewLayer)
                        previewLayer.position = CGPoint.init(x: self.frame.width/2, y: self.frame.height/2)
                        previewLayer.bounds = self.frame
                        self.contentView.bringSubviewToFront(callEndBtn)
                        self.contentView.bringSubviewToFront(acceptlbl)
                        self.contentView.bringSubviewToFront(declineLbl)
                        self.contentView.bringSubviewToFront(callAcceptBtn)
                        self.contentView.bringSubviewToFront(videoCallLbl)
                        self.contentView.bringSubviewToFront(videoCallIcon)
                        self.contentView.bringSubviewToFront(userName)
                        self.contentView.bringSubviewToFront(incomingVidLbl)
                        self.contentView.bringSubviewToFront(switch_btn)
                        self.contentView.bringSubviewToFront(endVcall_btn)
                        self.contentView.bringSubviewToFront(muteBtn)
                        self.contentView.bringSubviewToFront(userImageView)
                        self.contentView.bringSubviewToFront(calling_status)
                        self.contentView.bringSubviewToFront(calling_userName)
                    }
                }
            }
        }
        catch{
            DDLogDebug("print error")
        }
    }

    
    
    
    //remove camera layer
    func removeCameraLayer(){
        self.contentView.layer.addSublayer(previewLayer)
        captureSession.stopRunning()
        previewLayer.removeFromSuperlayer()
    }
    
}







extension IncomingVideocallView: AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
      DDLogDebug("sound finished here ...")
    }
    
    
    
}




