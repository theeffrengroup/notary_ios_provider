//
//  AudioCallView.swift
//  webRtc_module
//
//  Created by Imma Web Pvt Ltd on 05/09/17.
//  Copyright © 2017 3embed. All rights reserved.
//

import UIKit
import AVFoundation
import CocoaLumberjack


class AudioCallView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var callTypeLbl: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var callTimerLbl: UILabel!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var videoCallButton: UIButton!
    @IBOutlet weak var endCallButton: UIButton!
    
    var webRtc: webRTC?
    var callId : String?
    var messageDict : [String:Any]?
    var timer : Timer?
    
    var player: AVAudioPlayer?
    var callDisplayTimer = Timer()
    var secound = 0
    var isTimerRunning = false
    var resumeTapped = false
    var callerID: String? =  ""
    
    
    //init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    
    //set data here
    func setMessageData(messageData:[String : Any]) {
        messageDict = messageData
        callId = messageData["callId"] as? String
    }
    
    
    private func commonInit(){
        
        Bundle.main.loadNibNamed("AudioCallView", owner: self, options: nil)
        userImageView.layer.cornerRadius = userImageView.frame.width/2
        userImageView.clipsToBounds = true
        callTimerLbl.text = "connecting..."
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
        //start timer 60 sec for incoming calling screen
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timeoutRemoveScreen), userInfo: nil, repeats: false)
    }
    
    
    /// Stop timer here
    func stopTimer() {
        timer?.invalidate()
    }
    
    
    @objc func updateTimer(){
        secound += 1
        callTimerLbl.text = timeString(time: TimeInterval(secound))
    }
    
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    
    //timeout remove audioScreen
    @objc func timeoutRemoveScreen() {
        
        timer?.invalidate()
        DDLogDebug("***\n\n**********time out called ***************\n*****")
        
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        
        MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
        
        if  let id = messageDict?["callerId"] as? String{
            
            if userID != id {
                callerID = id
            }
            
            MQTTCallManager.sendTimeoutcallRequest(callID: callId!, callerID: callerID!, type: 7)
        }
        self.removeFromSuperview()
    }
    
    
    //init webRtc
    func initWebrtc(messageData: [String:Any])  {
        
        webRtc = webRTC.init(localView: nil, remoteView: nil, callID: messageData["callId"] as! String)
        webRtc?.delegate = self
        callId = messageData["callId"] as? String
        //messageDict = messageData
    }
    
    
    //mute button cliked..
    @IBAction func muteAction(_ sender: Any) {
        
        if muteButton.isSelected == true{
            webRtc?.client?.unmuteAudioIn()
            muteButton.isSelected = false
        }else{
            muteButton.isSelected = true
            webRtc?.client?.muteAudioIn()
        }
    }
    
    
    //speaker button cliked..
    @IBAction func speakerAction(_ sender: Any) {
        
        if speakerButton.isSelected == true{
            speakerButton.isSelected = false
            webRtc?.client?.enableSpeaker()
            
        }else{
            speakerButton.isSelected = true
            webRtc?.client?.disableSpeaker()
        }
    }
    
    
    //video button cliked..
    @IBAction func videoAction(_ sender: Any) {
        
        DDLogDebug("video button action")
        if videoCallButton.isSelected == true{
            videoCallButton.isSelected = false
        }else{
            videoCallButton.isSelected = true
        }
    }
    
    
    //end button cliked..
    @IBAction func endAction(_ sender: Any) {
        if  let callProvider = UIApplication.shared.delegate as? AppDelegate{
            callProvider.callProviderDelegate?.provider.reportCall(with: (callProvider.callProviderDelegate?.uuId)! , endedAt: nil, reason: .remoteEnded)
        }
        timer?.invalidate()
        webRtc?.disconnect()
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
        let id = messageDict?["callerId"] as! String ////end call crash here
        if userID != id {
            callerID = id
        }
        MQTTCallManager.sendEndcallRequest(callID: callId!, callerID: callerID!, targetID:messageDict?["callerIdentifier"] as! String , callType: AppConstants.CallTypes.audioCall)
        ///play endcall sound here
        self.playSound("end_of_call", loop: 1)
        let when = DispatchTime.now() + 0.30
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            self.removeFromSuperview()
        }
    }
    
    
    
    func playSound(_ soundName: String,loop: Int){
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "wav")else{ return}
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            player  = try AVAudioPlayer(contentsOf: url)
            player?.delegate = self
            guard let player = player else { return}
            player.play()
        }catch let error{
            DDLogDebug("error \(error.localizedDescription)")
        }
    }
}


extension AudioCallView: AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
        DDLogDebug("sound finished here ...")
    }
}


extension AudioCallView : webRTCdelegate{
    func appClientStatus(_ client: ARDAppClient, status: ARDAppClientState) {
        DDLogDebug("webRTC status changed =\(status)")
        switch (status){
        case ARDAppClientState.connected :
            
            if callDisplayTimer.isValid != true {
                callDisplayTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer) , userInfo: nil, repeats: true)
            }
            break
            
        case ARDAppClientState.connecting :
            
            self.callTimerLbl.text = "connecting.."
            break
            
        case ARDAppClientState.disconnected:
            
            self.callTimerLbl.text = "disconnected."
            break
        }

    }
}


