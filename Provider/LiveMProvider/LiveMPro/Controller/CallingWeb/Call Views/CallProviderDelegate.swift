//
//  CallProviderDelegate.swift
//  MQTT Chat Module
//
//  Created by Imma Web Pvt Ltd on 12/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import CallKit
import AVFoundation
import Kingfisher
import CocoaLumberjack

class CallProviderDelegate: NSObject,CXProviderDelegate {
    /// Called when the provider has been reset. Delegates must respond to this callback by cleaning up all internal call state (disconnecting communication channels, releasing network resources, etc.). This callback can be treated as a request to end all calls without the need to respond to any actions
    @available(iOS 10.0, *)
    func providerDidReset(_ provider: CXProvider) {
        
        NSLog("did Reset ********************** \(provider)")
    }

    
    let provider : CXProvider
    var callID : String = ""
    var messageData:[String:Any] = [:]
    var calleeName :String = ""
    var uuId : UUID?
    
    static var providerConfiguration: CXProviderConfiguration {
        
        let providerConfiguration: CXProviderConfiguration?
        providerConfiguration = CXProviderConfiguration.init(localizedName: "Jayesh Rajput")
        providerConfiguration!.supportsVideo = false
        providerConfiguration?.maximumCallGroups = 1
        
        if let iconmaskImg = UIImage.init(named: "video_call"){
            providerConfiguration?.iconTemplateImageData = iconmaskImg.pngData()
        }
        
        providerConfiguration?.ringtoneSound = "ring.caf"
        return providerConfiguration!
        
    }
    
    
     override init() {
    
        provider = CXProvider.init(configuration: CallProviderDelegate.providerConfiguration)
        super.init()
        provider.setDelegate(self, queue: nil)
    }
    
    
    
    func displayIncomingcall(uuid: UUID, handel : String, hasVideo:Bool = false,complition:((NSError?)-> Void)? = nil ){
        

        calleeName = handel
        uuId = uuid
        let update = CXCallUpdate()
        update.remoteHandle  = CXHandle.init(type: .phoneNumber, value: handel)

        update.hasVideo = hasVideo
        provider.reportNewIncomingCall(with: uuid, update: update) { error in
            complition?(error as NSError?)
        }
        
    }
    

    /// Call End
    func expireCall() {
        
        
        MQTTCallManager.sendAcceptCallStatus(messageData: messageData)

        provider.reportCall(with:uuId! , endedAt: nil, reason: .remoteEnded)
    }
    
    
    /// call Actions
    ///
    /// - Parameters:
    ///   - provider: CXProvide
    ///   - action: Actions
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        // configure audio session
        
        NSLog("CXxxxxxxxxxxxxxxxAnswercallAction \(action)")
        MQTTCallManager.sendAcceptCallStatus(messageData: messageData)
        
        
        let window = UIApplication.shared.keyWindow!
        let audioView = AudioCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
        audioView.tag = 15
        audioView.userImageView.kf.setImage(with: URL(string:messageData["callerImage"] as! String ), placeholder: #imageLiteral(resourceName: "defaultImage"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, CacheType, url) in
        })
        
        audioView.userNameLbl.text = calleeName
        audioView.initWebrtc(messageData: messageData)
        
        do {
            try AVAudioSession.sharedInstance().setActive(true)
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.none)
        } catch _ {
        }

        audioView.messageDict = messageData
        window.addSubview(audioView);
        action.fulfill()

    }
    
    
     func provider(_ provider: CXProvider, perform action: CXEndCallAction){
        
        NSLog("CXxxxxxxxxxxxxxxxEndcallAction \(action)")
        
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
        
        
        let id = messageData["callerId"] as! String ////end call crash here
        var callerId = ""
        if userID != id {
            callerId = id
        }
        
        MQTTCallManager.sendEndcallRequest(callID: callID, callerID: callerId, targetID:messageData["callerIdentifier"] as! String , callType: AppConstants.CallTypes.audioCall)
        
        
        
        if  let  audioView = UIApplication.shared.keyWindow!.viewWithTag(15) as? AudioCallView{
            if callID == audioView.callId{
                audioView.webRtc?.disconnect()
                audioView.playSound("end_of_call", loop: 1)
                let when = DispatchTime.now() + 0.30
                DispatchQueue.main.asyncAfter(deadline: when) {
                    audioView.removeFromSuperview()
                }
                
            }
        }
        
                
    
        
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.none)
             try AVAudioSession.sharedInstance().setActive(false)
        } catch _ {
        }
        provider.reportCall(with:uuId! , endedAt: nil, reason: .remoteEnded)
    }
    
    
    
     func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction){
        
        DDLogDebug("mute called here")
    
        
    }
    
    
    
    /// Called when the provider's audio session activation state changes.
    
    internal func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession){
        
        DDLogDebug("Audio session didActive")
        
        
    }
    
    
    internal func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession){
        
        DDLogDebug("Audio session didDeactivate")
        
    }
    
}



