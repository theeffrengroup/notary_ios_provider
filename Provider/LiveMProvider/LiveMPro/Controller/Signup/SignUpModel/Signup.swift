//
//  Signup.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 21/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

class AddressDetails:NSObject{

    var city:String
    var state:String
    var country:String
    var pinCode:String
    var placeID:String
    
    init(selCity:String,stat:String,contry:String,pinCod:String,placeid:String) {
        self.city = selCity
        self.state = stat
        self.country = contry
        self.pinCode = pinCod
        self.placeID = placeid
    }
}

// sign up params
class SignUPParams: NSObject {
    var countryCode:String
    //vani 1/03/2020
    var countryCodeSymbol:String
    var firstName:String
    var lastName:String
    var  emailAddress: String
    var  password : String
    var phoneNum:String
    var lat:Double
    var log:Double
    var proImg:String
    var  deviceId : String
    var DOB:String
    var catID:String
    var deviceType:String
    var pushToken : String
    var appVersion : String
    var deviceName : String
    var deviceModel : String
    var cityID:String
    var deviceVersion:String
    var addressArtist:String
    var taggedAs:String
    var documentDict:[String:Any]
    
    var state:String
    var city:String
    var country:String
    var pinCode:String
    var subCatList:String
    var gender:Int
    var referral: String
    
    
    //authorization
    
    init(firstNam:String,lastNam:String,email: String,phone: String,latit: Double,logit: Double,profilePic: String,dateOFBirth: String,pass: String,deviceID: String,tokenPush:String,categoryID:String,city:String,appVersion:String,nameDevice:String,modelDevice:String,typeDevice:String,deviceVer:String,countryCode:String,artistAddress:String,documentData:[String:Any],taggedAddress:String,addState:String,addCity:String,addCountry:String,addPinCode:String,subCatId:String,genderType:Int, referral:String,countryCodeSymbol:String)  {
        
        self.subCatList = subCatId
        self.firstName = firstNam
        self.lastName = lastNam
        self.phoneNum = phone
        self.lat = latit
        self.log = logit
        self.proImg = profilePic
        self.DOB = dateOFBirth
        self.catID = categoryID
        self.cityID = city
        self.emailAddress = email
        self.password = pass
        self.deviceId = deviceID
        self.pushToken = tokenPush
        self.appVersion = appVersion
        self.deviceName = nameDevice
        self.deviceModel = modelDevice
        self.deviceType = typeDevice
        self.deviceVersion = deviceVer
        self.countryCode = countryCode
        self.countryCodeSymbol = countryCodeSymbol
        self.addressArtist = artistAddress
        self.documentDict = documentData
        self.taggedAs = taggedAddress
        
        self.state =  addState
        self.country = addCountry
        self.city = addCity
        self.pinCode = addPinCode
        self.gender = genderType
        self.referral = referral
    }
}

// valdiation emails
class ValidatePhoneEmail: NSObject {
    var  emailAddress: String
    var  countryCode : String
    var phoneNum:String
    
    init(email:String,countryCod:String,phone: String){
        self.emailAddress = email
        self.countryCode = countryCod
        self.phoneNum = phone
    }
}
