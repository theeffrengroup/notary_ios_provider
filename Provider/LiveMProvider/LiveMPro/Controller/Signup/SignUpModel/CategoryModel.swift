
//
//  CategoryModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 13/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
// document fields
class CatFieldModel: NSObject {
    var fieldID = ""
    var fieldName = ""
    var type = 0
    var isMandatody = 0
    var data = ""
    
    var expiryDate = ""
    var othersTf = ""
    var docImage = UIImageView()
}

// documents
class CatModel: NSObject {
    var docID = ""
    var isMandatody = 0
    var docName = ""
    var subCatArray = [CatFieldModel]()
}

// documents
class SubCatModel: NSObject {
    var subCatID = ""
    var subCatName = ""
    var isSelected = 0
}
