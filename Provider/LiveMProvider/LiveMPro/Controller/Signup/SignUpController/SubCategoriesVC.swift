//
//  SubCategoriesVC.swift
//  LSP
//
//  Created by Vengababu Maparthi on 02/04/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class SubCategoriesVC: UIViewController {
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var subCatTableView: UITableView!
    @IBOutlet weak var subCatName: UILabel!
    var categoryData = Categories()
    var categorySelectedIndex = -1
    
    var shared =  SignUpViewController.sharedInstance
    
    override func viewDidLoad() {
        
        subCatName.text = categoryData.catName
        if self.categoryData.categoryDocs.count != 0{
            self.saveButton.setTitle("NEXT", for: .normal)
        }else{
            self.saveButton.setTitle("SAVE", for: .normal)
        }
        subCatTableView.tintColor = COLOR.APP_COLOR
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveTheSelectedSubCat(_ sender: Any) {
        
        var hasSelected = false
        if self.categoryData.categoryDocs.count != 0{
            for index in 0...categoryData.subCategories.count-1{
                let indexPath = IndexPath(row:index, section: 0)
                if let selectedRow = subCatTableView.cellForRow(at: indexPath) {
                    if selectedRow.accessoryType == .checkmark {
                        self.categoryData.subCategories[indexPath.row].isSelected = 1
                        hasSelected = true
                    }
                }
            }
            if hasSelected{
                self.performSegue(withIdentifier: "uploadDocs", sender: nil)
            }else{
                Helper.alertVC(errMSG: "Please select the sub categories")
            }
        }else{
            for index in 0...categoryData.subCategories.count-1{
                let indexPath = IndexPath(row:index, section: 0)
                if let selectedRow = subCatTableView.cellForRow(at: indexPath) {
                    if selectedRow.accessoryType == .checkmark {
                        self.categoryData.subCategories[indexPath.row].isSelected = 1
                        hasSelected = true
                    }
                }
            }
            if hasSelected{
                self.navigationController?.popViewController(animated: true)
            }else{
                Helper.alertVC(errMSG: "Please select the sub categories")
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if  let nextScene = segue.destination as? CategoryViewController{
            nextScene.categorytit = self.categoryData.catName
            nextScene.documents = self.categoryData.categoryDocs
            nextScene.categorySelectedIndex = categorySelectedIndex
            nextScene.fromWhichScreen = 0
        }
    }
    
    @IBAction func backtoVC(_ sender: Any) {
        
        if categoryData.subCategories.index(where: {$0.isSelected == 1}) != nil {
            self.navigationController?.popViewController(animated: true)
            return
        }else{
            shared.categoryDetailsUpdated(index: categorySelectedIndex)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension SubCategoriesVC:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath)?.accessoryType == .checkmark{
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }else{
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SubCategoriesVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.categoryData.subCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subCat") as! SubCatTableCell
        cell.subCatName.text = self.categoryData.subCategories[indexPath.row].subCatName
        if self.categoryData.subCategories[indexPath.row].isSelected == 0{
            cell.accessoryType = .none;
        }else{
            cell.accessoryType = .checkmark;
        }
        return cell
    }
}
