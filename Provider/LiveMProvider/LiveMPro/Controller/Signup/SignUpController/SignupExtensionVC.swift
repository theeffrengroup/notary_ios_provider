//
//  SignupExtensionVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 12/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
//import PhoneNumberKit

extension SignUpViewController{
    
    @IBAction func selectProfilePic(_ sender: Any) {
        selectImage()
    }
    
    
    @IBAction func addLocationToArtist(_ sender: Any) {
        self.performSegue(withIdentifier: "artistLocation", sender: nil)
    }
    
    
    @IBAction func didPressBackButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func termsAndConditions(_ sender: Any) {
        self.performSegue(withIdentifier: "toViewTermsAndConditions", sender: nil)
    }
    
    //signUpModel.documents.value
    @IBAction func submitForApprovalAction(_ sender: UIButton) {
        self.view.endEditing(true)
        signUpModel.artistCat.value = ""
        var count = 0
        for i in 0..<categories.count { // to get all the selected services
            let indexPath = IndexPath(row:i, section: 0)
            if let cell = collectionView.cellForItem(at: indexPath) as? ArtistTypeCell {
                if  cell.artistTypeLabel.layer.backgroundColor == COLOR.APP_COLOR.cgColor{
                    if signUpModel.artistCat.value.isEmpty{
                        if categories[indexPath.row].categoryDocs.count != 0 {
                            innerLoop: for categories in categories[indexPath.row].categoryDocs{
                                if categories.isMandatody == 1{
                                    count = count + 1
                                    break innerLoop
                                }
                            }
                        }
                        signUpModel.artistCat.value =  categories[indexPath.row].catID
                    }else{
                        if categories[indexPath.row].categoryDocs.count != 0 {
                            innerLoop: for categories in categories[indexPath.row].categoryDocs{
                                if categories.isMandatody == 1{
                                    count = count + 1
                                    break innerLoop
                                }
                            }
                        }
                        signUpModel.artistCat.value = signUpModel.artistCat.value + "," + categories[indexPath.row].catID
                    }
                }
            }
        }
        signUpModel.subCatIds.value = ""
        for categoryData in categories{
            
            for subCategor in categoryData.subCategories{
                if subCategor.isSelected == 1{
                    if signUpModel.subCatIds.value.count == 0{
                        signUpModel.subCatIds.value = subCategor.subCatID
                    }else{
                        signUpModel.subCatIds.value = signUpModel.subCatIds.value + "," + subCategor.subCatID
                    }
                }
            }
        }
        signupMethod(numberOfCatMandatory:count)
    }
    
    // MARK: - Tapgesture
    @IBAction func tapGestureAction(_ sender: Any) {
        self.dismisskeyBord()
    }
    
    @IBAction func selectCity(_ sender: Any) {
        if  self.emailService{
            typeModelMake = 1
            self.view.endEditing(true)
            performSegue(withIdentifier:"toCityAndCats" , sender: nil)
        }else{
            if emailAddress.text?.length != 0{
                if !(emailAddress.text?.isValidEmail)!{
                    shake(textField: emailAddress)
                    emailAddress.text = ""
                    emailAddress.becomeFirstResponder()
                    self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.InValidEmail), animated: true, completion: nil)
                }else{
                    if !emailService {
                        self.validateEmail(typeValidate: "2", fromCity: true)
                    }
                }
            }else{
                typeModelMake = 1
                self.view.endEditing(true)
                performSegue(withIdentifier:"toCityAndCats" , sender: nil)
            }
        }
    }
    
    ///*******Select country mobile code***************
    @IBAction func selectTheCountryCode(_ sender: Any) {
        performSegue(withIdentifier:segues.countryPicker!, sender: nil)
    }
    
    /// checks if the phone number exist then after phone validate picker initiates
    @IBAction func didPressDatePicker(_ sender: UIButton) {
        
        if (phoneNumber.text?.length)!>2{
            if !mobileService {
              //  self.parseNumber(phoneNumber.text!)
                self.validatePhone(typeValidate: "1")
                
            }else{
                self.dismisskeyBord()
                self.showTheDatePicker()
            }
        }else{
            self.dismisskeyBord()
            self.showTheDatePicker()
        }
    }
    

    
    // select the date from date picker and hides
    @IBAction func selectDate(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.datePickerConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        dateOfBirth.text = dateFormatter.string(from: datePicker.date)
        
        let dateFormatter1 = DateFormatter.initTimeZoneDateFormat()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        
        signUpModel.dateOfBirth.value = dateFormatter1.string(from: datePicker.date)
        view7.backgroundColor = COLOR.APP_COLOR
        
        let calendar = NSCalendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: self.datePicker.date)
        let date2 = calendar.startOfDay(for: Date())
        
        let components = calendar.dateComponents([.year], from: date1, to: date2)
        UserDefaults.standard.set(components.year!, forKey: "artistAge")
        UserDefaults.standard.synchronize()

    }
    
    // hide the date picker view
    @IBAction func cancelDatePicker(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.datePickerConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    
    
    // MARK: - SignupMethod verifies all the fields r filled
    func signupMethod(numberOfCatMandatory:Int){
      
        if pickedImage.image == nil{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.selectProfile), animated: true, completion: nil)
        }
        else if (firstNAme.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message, message:signup.enterFirstName), animated: true, completion: nil)

        }
        else if (lastName.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message, message:signup.lastFirstName), animated: true, completion: nil)
        }
        else if (emailAddress.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.enterEmailId), animated: true, completion: nil)
        }
            
        else if (selectYourCity.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.selectCity), animated: true, completion: nil)
        }
            
        else if (password.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.enterPassword), animated: true, completion: nil)
        }
            
        else if (phoneNumber.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.enterPhoneNumber), animated: true, completion: nil)
        }
            
        else if (dateOfBirth.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.enterDateOfBirth), animated: true, completion: nil)
        }
            
        else if (locationTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.selectLocation), animated: true, completion: nil)
        }
            
        else if (signUpModel.artistCat.value.count < 3){
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.selectGenres), animated: true, completion: nil)
        }
            
        else if(signUpModel.documents.value.count < numberOfCatMandatory){
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.selectDocuments), animated: true, completion: nil)
        }
            
        else if !(emailAddress.text?.isValidEmail)!{
            emailAddress.becomeFirstResponder()
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.InValidEmail), animated: true, completion: nil)
        }
        else{
            self.uploadTheDocumentImages()
            saveTheParameters()
        }
    }
    
    // upload the document images which is selected for category
    func uploadTheDocumentImages()  {
        temp = [UploadImage]()
        timeStamp = Helper.currentTimeStamp
        temp.append(UploadImage.init(image: profileImage.image!, path: AMAZONUPLOAD.PROFILEIMAGE +  timeStamp + "_0_01" + ".png"))
        
        for docImage in arrayDocumentImages {
            var count  = 0
            var url = ""
            if let documents = docImage["images"] as? [UIImage]{
                for doc in documents{
                    count  = count  + 1
                    url = AMAZONUPLOAD.DOCUMENTIMAGES + String(describing:docImage["timeStamp"]) + String(describing:count) + ".png"
                    temp.append(UploadImage.init(image: doc , path: url))
                }
            }
        }
        
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
    }
    
    // Api call for signup inititates
    func saveTheParameters(){
        
        
        if (referralcodeTF.text!.isEmpty){
            referralcodeTF.text! = ""
        }
        
        let profileIMG = Utility.amazonUrl + AMAZONUPLOAD.PROFILEIMAGE +  timeStamp + "_0_01" + ".png"
        signUpModel.proImage.value = profileIMG
        
        
        signUpModel.signupApiCall() { (succeeded, providerID,signUpMSg) in
            if succeeded{
                self.signUpMsg = signUpMSg
                self.artistID = providerID
                self.performSegue(withIdentifier: "toVerifyFromSignup", sender: nil)
            }else{
                
            }
        }
    }
}
