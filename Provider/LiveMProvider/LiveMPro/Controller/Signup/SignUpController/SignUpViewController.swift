//
//  SignUpViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift
import RxKeyboard
import RxCocoa
//import PhoneNumberKit


class SignUpViewController: UIViewController,UINavigationControllerDelegate,CountryPickerDelegate  {
    
    
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var countryImage: UIImageView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var countryCode: UILabel!
    @IBOutlet var SignupButton: UIButton!
    @IBOutlet weak var datePickerButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var othersButton: UIButton!
    
    @IBOutlet weak var lastName: FloatLabelTextField!
    @IBOutlet var dateOfBirth: FloatLabelTextField!
    @IBOutlet var firstNAme: FloatLabelTextField!
    @IBOutlet var password: FloatLabelTextField!
    @IBOutlet var emailAddress: FloatLabelTextField!
    @IBOutlet var phoneNumber: FloatLabelTextField!
    @IBOutlet var referralcodeTF: FloatLabelTextField!
    @IBOutlet weak var selectYourCity: FloatLabelTextField!
    @IBOutlet weak var locationTF: FloatLabelTextField!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view9: UIView!
    @IBOutlet weak var view10: UIView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var heightOFAlertView: NSLayoutConstraint!
    @IBOutlet weak var heightOfCategoryView: NSLayoutConstraint!
    @IBOutlet weak var datePickerConstraint: NSLayoutConstraint!
    
    
    
    static var obj: SignUpViewController? = nil
    static var sharedInstance: SignUpViewController {
        if obj != nil{
            return obj!
        }else{
            return SignUpViewController()
        }
    }
    
    var typeModelMake:Int = 0
    var selectImageTyep:Int = 0
    
    var activeTextField = UITextField()
    var catlist = String()
    var profileUrl  = String()
    
    var pickedImage = UIImageView()
    var latit  = 0.00
    var longit = 0.00
    var temp = [UploadImage]()
    var categories  = [Categories]()
    var artistID = ""
    var signUpMsg = ""
    var docTimeStamp = ""
    
    var mobileService = false
    var emailService = false
    var referralService = false
    
    var timeStamp = String()
    
    var arrayDocumentImages = [[String:Any]]()
    var myDocuments = [UIImage]()
    
    
    var link = String()
    var catIndexesSelected   = [Int]()
    
    var genreiDS = [String]()
    var CITYID = ""
    var cityIndex:Int = -1;
    var categoryIndex:Int = -1;
    var categoryIndexpath = IndexPath()
    
    var previousSelectedCell = [IndexPath]()
    
    let disposeBag = DisposeBag()
    
    var signUpModel = Signup()
    
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    
    override func viewDidLoad() {
        SignUpViewController.obj = self
        pickedImage.image = nil
        setCountryCode()
        super.viewDidLoad()
        updateObserveToVariables()
        self.categoryView.isHidden = true
        self.heightOfCategoryView.constant = 0
//        self.locationtracker.startLocationTracking()
        maleButton.isSelected = true
        signUpModel.gender.value = 1
        if CLLocationManager.locationServicesEnabled() {
                    switch CLLocationManager.authorizationStatus() {
                        
                    case .notDetermined, .restricted, .denied:
                        print("No access")
                    case .authorizedAlways, .authorizedWhenInUse:
                        print("Access")
                        locationtracker.startLocationTracking()
                    @unknown default:
                        break
                    }
                    
                }
        else {
                print("Location services are not enabled")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mainScrollView.bringSubviewToFront(datePickerButton)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.view.layoutIfNeeded()
        firstNAme.autocapitalizationType = .words
        lastName.autocapitalizationType = .words
        
        profileImage.layer.borderWidth = 2.0
        profileImage.layer.borderColor = COLOR.APP_COLOR.cgColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Binding the view model data and views data
    func updateObserveToVariables() {
        firstNAme.rx.text
            .orEmpty
            .bind(to: signUpModel.firstName)
            .disposed(by: disposeBag)
        lastName.rx.text
            .orEmpty
            .bind(to: signUpModel.latName)
            .disposed(by: disposeBag)
        emailAddress.rx.text
            .orEmpty
            .bind(to: signUpModel.emailAddress)
            .disposed(by: disposeBag)
        phoneNumber.rx.text
            .orEmpty
            .bind(to: signUpModel.phoneNumber)
            .disposed(by: disposeBag)
        dateOfBirth.rx.text
            .orEmpty
            .bind(to: signUpModel.dateOfBirth)
            .disposed(by: disposeBag)
        password.rx.text
            .orEmpty
            .bind(to: signUpModel.password)
            .disposed(by: disposeBag)
        
        locationTF.rx.text
            .orEmpty
            .bind(to: signUpModel.address)
            .disposed(by: disposeBag)
        
        referralcodeTF.rx.text
            .orEmpty
            .bind(to: signUpModel.referral)
            .disposed(by: disposeBag)
    }
    
    @IBAction func genderAction(_ sender: Any) {
        if (sender as AnyObject).tag == 20{
            maleButton.isSelected = true
            femaleButton.isSelected = false
            othersButton.isSelected = false
            signUpModel.gender.value = 1
        }else if (sender as AnyObject).tag == 40{
            maleButton.isSelected = false
            femaleButton.isSelected = true
            othersButton.isSelected = false
            signUpModel.gender.value = 2
        }
        else {
            maleButton.isSelected = false
            femaleButton.isSelected = false
            othersButton.isSelected = true
            signUpModel.gender.value = 3
        }
    }
    
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            mainScrollView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.mainScrollView.contentInset.bottom = keyboardVisibleHeight + 20
            })
            .disposed(by: disposeBag)
    }
    
    /// hide keyboard
    func dismisskeyBord() {
        self.view.endEditing(true)
        //if (phoneNumber.text?.length)!>2{  //bug id: card #71, desc:crash Signup.signupApiCall, fix: added phone number validation
            if !mobileService {
                self.validatePhone(typeValidate: "1")
            }
      //  }
                if  !(referralcodeTF.text!.isEmpty) && !referralService{
                    self.validateReferral()
                    referralService = true
                }
    }
    
    
    
    
    /// MARK: - actionSheet
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: signup.selectImage as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: signup.cancel as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: signup.camera,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: signup.gallery as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    // MARK: - photo gallery
    func removePhoto()
    {
        profileUrl = ""
        pickedImage.image = nil
        profileImage.image = UIImage(named: "userdefaulticon")
    }
    
    // MARK: - photo gallery
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    // MARK: - Camera selection
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            noCamera()
        }
    }
    
    //No camera alert for ipods and simulators
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        switch segue.identifier {
        case segues.countryPicker!?:
            let nav = segue.destination as! UINavigationController
            if let picker: CountryPicker = nav.viewControllers.first as! CountryPicker?
            {
                picker.delegate = self
            }
            break
            
        case "toCityAndCats"?:
            let nextScene = segue.destination as? VehicleTypeModelMakeVC
            nextScene?.delegate = self
            break
            
        case "toVerifyFromSignup"?:
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.defineTheOtp = 1
            nextScene?.providerID = artistID
            nextScene?.signupMsg = signUpMsg
            nextScene?.mobileNumber = phoneNumber.text!
            nextScene?.countryCode = countryCode.text!
            nextScene?.delegate = self
            break
            
        case "updateCategory"?:
            let nextScene = segue.destination as? CategoryViewController
            nextScene?.categorytit = categories[categoryIndex].catName
            nextScene?.delegate = self
            nextScene?.documents = categories[categoryIndex].categoryDocs
            nextScene?.categorySelectedIndex = categoryIndex
            nextScene?.fromWhichScreen = 0
            break
            
        case "subCat"?:
            let nextScene = segue.destination as? SubCategoriesVC
            nextScene?.categoryData = categories[sender as! Int]
            nextScene?.categorySelectedIndex = categoryIndex
            break
            
        case "artistLocation"?:
            let nextScene = segue.destination as? LocationController
            nextScene?.fromSignup = true
            nextScene?.delegate = self
            break
            
        default:
            break
        }
    }
    
    // MARK: - setCountrycode
    func setCountryCode(){
        let country = NSLocale.current.regionCode
        let picker = CountryPicker.dialCode(code: country!)
        countryCode.text = picker.dialCode
        countryImage.image = picker.flag
        signUpModel.countryCode.value = countryCode.text!
        /*
        Get country symbol code 
        Owner :vani
        Date : 13/03/2020
        */
        signUpModel.countryCodeSymbol.value   = picker.code
    }
    
    // MARK: - country delegate method
    internal func didPickedCountry(country: Country)
    {
        countryCode.text = country.dialCode
        countryImage.image = country.flag
        signUpModel.countryCode.value =  countryCode.text!
        signUpModel.countryCodeSymbol.value   = country.code
    }
    
    // shows the date picker to select the date of birth of artist
    func showTheDatePicker(){
        self.dismisskeyBord()
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.datePickerConstraint.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    /// validatePhone api initiate
    func validatePhone(typeValidate:String) {
        
        Helper.showPI(message:signup.verifyMobile)
        
        signUpModel.phoneValidate{ (succeeded) in
            if succeeded{
                self.dismisskeyBord()
                if self.dateOfBirth.text?.length == 0{
                    self.showTheDatePicker()
                }
                self.mobileService = true
            }else{
                self.mobileService = false
                self.phoneNumber.becomeFirstResponder()
                self.phoneNumber.text = ""
                self.shake(textField: self.phoneNumber)
            }
        }
    }
    
    
    //validateEmail api initiate
    func validateEmail(typeValidate:String,fromCity:Bool ) {
        
        Helper.showPI(message:signup.verifyEmail)
        signUpModel.emailValidate { (succeeded) in
            if succeeded{
                self.emailService = true
                self.typeModelMake = 1
                if fromCity{
                    self.view.endEditing(true)
                    self.performSegue(withIdentifier:"toCityAndCats" , sender: nil)
                }
            }else{
                self.emailService = false
                self.emailAddress.becomeFirstResponder()
                self.emailAddress.text = ""
                self.shake(textField: self.emailAddress)
            }
        }
    }
    
    //Referral validation api initiate
    func validateReferral() {
        
        signUpModel.validateReferral(apiMethod:API.METHOD.REFERRALCODE) { (succeeded) in
            if succeeded{
                self.dismisskeyBord()
                self.referralService = true
            }else{
                self.referralService = false
                self.referralcodeTF.becomeFirstResponder()
                self.referralcodeTF.text = ""
            }
        }
    }
    
    ///Get the categories by city id, and updates collection view
    func getCategoryByCityID(){
        
        Helper.showPI(message:loading.load)
        signUpModel.getTheCategoriesUsingCityID(method: API.METHOD.generes + "/" + CITYID) { (succeeded,categorieArray) in
            if succeeded{
                self.categories = categorieArray
                let tagCellLayout = TagCellLayout(tagAlignmentType: .Center, delegate: self)
                self.collectionView?.collectionViewLayout = tagCellLayout
                self.categoryView.isHidden = false
                self.collectionView.reloadData()
                self.view.layoutIfNeeded()
                self.heightOfCategoryView.constant = self.collectionView.contentSize.height + 35
                self.view.layoutIfNeeded()
                if !(self.selectYourCity.text?.isEmpty)!{
                    if self.categories.count == 0{
                        self.heightOFAlertView.constant = 40
                    }else{
                        self.heightOFAlertView.constant = 0
                    }
                }
            }else{
                self.categoryView.isHidden = true
                self.categories = categorieArray
                self.collectionView.reloadData()
                self.heightOfCategoryView.constant = 0
                if !(self.selectYourCity.text?.isEmpty)!{
                    if self.categories.count == 0{
                        self.heightOFAlertView.constant = 40
                    }else{
                        self.heightOFAlertView.constant = 0
                    }
                }
            }
        }
    }
    
    //MARK:-  Crop imageview
    func cropTheImageVC()  {
        guard let image = pickedImage.image else {
            return
        }
        let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)
    }
    
    
    func categoryDetailsUpdated(index:Int){
        categories[index].categorySelected = 0
        collectionView.reloadData()
    }
    
}

// MARK: - CollectionView datasource method
extension SignUpViewController:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: signupIdentifierds.collectionButton!, for: indexPath as IndexPath) as! ArtistTypeCell
        cell.artistTypeLabel.text = categories[indexPath.row].catName
        if categories[indexPath.row].categorySelected == 0{
            cell.artistTypeLabel.layer.backgroundColor = UIColor.white.cgColor
            cell.artistTypeLabel.textColor = COLOR.categoryDeselectColor
            cell.setLayout()
        }else{
            cell.artistTypeLabel.layer.backgroundColor = COLOR.APP_COLOR.cgColor
            cell.artistTypeLabel.textColor = UIColor.white
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}

// MARK: - CollectionView delegate method
extension SignUpViewController:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? ArtistTypeCell {
            
            if cell.artistTypeLabel.layer.backgroundColor == nil || cell.artistTypeLabel.layer.backgroundColor == UIColor.white.cgColor{
                cell.artistTypeLabel.layer.backgroundColor = COLOR.APP_COLOR.cgColor
                cell.artistTypeLabel.textColor = UIColor.white
                self.catlist =  cell.artistTypeLabel.text!  // unused
                categoryIndex = indexPath.row
                categoryIndexpath = indexPath
                categories[indexPath.row].categorySelected = 1
                
                if categories[categoryIndex].subCategories.count > 0{
                    
                    self.performSegue(withIdentifier: "subCat", sender: categoryIndex)
                    
                }else if categories[categoryIndex].categoryDocs.count > 0{
                    
                    self.performSegue(withIdentifier: "updateCategory", sender: categories[categoryIndex].catID)
                }
                
            }else{
                
                categories[indexPath.row].categorySelected = 0
                cell.artistTypeLabel.layer.backgroundColor = UIColor.white.cgColor
                cell.artistTypeLabel.textColor = COLOR.categoryDeselectColor
                categoryIndex = -1
                
                if signUpModel.documents.value[categories[indexPath.row].catID] is [[String:Any]]{
                    signUpModel.documents.value.removeValue(forKey: categories[indexPath.row].catID)
                }
                if categories[indexPath.row].subCategories.count != 0{
                    for index in 0...categories[indexPath.row].subCategories.count-1 {
                        categories[indexPath.row].subCategories[index].isSelected = 0
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? ArtistTypeCell {
            //            cell.artistTypeLabel.layer.backgroundColor = UIColor.white.cgColor
            //            cell.artistTypeLabel.textColor = COLOR.categoryDeselectColor
            //            categoryIndex = -1
        }
    }
}

extension SignUpViewController : UIGestureRecognizerDelegate{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: collectionView))! {
            return false
        }else{
            return true
        }
    }
}


// MARK: - ImagePicker delegate method
extension SignUpViewController : UIImagePickerControllerDelegate {
    
    /*The image is not getting dispalyed when when selected because this method is deprecated use th below method
    Owner : Vani
    Date : 27/02/20202
    */
    /*
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        pickedImage.image = Helper.resizeImage(image: image, newWidth: 200)
        // profileImage.image = Helper.resizeImage(image: image, newWidth: 200)
        self.dismiss(animated: true, completion: nil)
        self.cropTheImageVC()
    } */
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            pickedImage.image = Helper.resizeImage(image: image, newWidth: 200)
        }
        self.dismiss(animated: true, completion: nil)
        self.cropTheImageVC()
    }
}


extension SignUpViewController :CityAndCategoryDelegate{
    func didSelectMakeModelType(name:String,cityID:String,index:Int) {
        selectYourCity.text = name
        CITYID = cityID
        signUpModel.cityID.value = CITYID
        cityIndex = index
        view4.backgroundColor = COLOR.APP_COLOR
        self.getCategoryByCityID()
    }
}

extension  SignUpViewController: verifyOtpDelegate{
    func updatedNumber(numberNew: String, countryCode: String) {
        
    }
    
    func phoneNumberVerified() {
        self.dismiss(animated: true, completion: nil)
    }
}



extension SignUpViewController:TagCellLayoutDelegate{
    func tagCellLayoutTagWidth(layout: TagCellLayout, atIndex index: Int) -> CGFloat {
        let textLabel = UILabel.init()
        textLabel.text =  categories[index].catName
        return CGFloat(index%2 == 0 ? textLabel.intrinsicContentSize.width + 20:textLabel.intrinsicContentSize.width + 20)
    }
    
    func tagCellLayoutTagFixHeight(layout: TagCellLayout) -> CGFloat {
        return CGFloat(50.0)
    }
}

extension SignUpViewController : categoryDelegate{
    
    func categoryDetailsUpdated(certificates: [[String : Any]],documentImages:[UIImage], timeStamp: String) {
        var isExisting =  false
        if certificates.count != 0{
            signUpModel.documents.value[categories[categoryIndex].catID] = certificates
        }
        if documentImages.count != 0 {
            let dict:[String:Any] = ["images":documentImages,
                                     "timeStamp":timeStamp,
                                     "catID":categories[categoryIndex].catID]
            if arrayDocumentImages.count != 0{
                for index in 0...arrayDocumentImages.count - 1 {
                    if let catID = arrayDocumentImages[index]["catID"] as? String{
                        if catID == categories[categoryIndex].catID{
                            isExisting = true
                            arrayDocumentImages.remove(at: index)
                            arrayDocumentImages.append(dict)
                        }
                    }
                }
            }
            if !isExisting{
                arrayDocumentImages.append(dict)
            }
        }
        collectionView.reloadData()
    }
}

extension SignUpViewController :AddressFromMapDelegate{
    
    
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - addressData: <#addressData description#>
    ///   - addressDetails: <#addressDetails description#>
    func getTheAddressWithLatLongs(addressData: [String : Any],addressDetails: AddressDetails?) {
        locationTF.text = addressData["addLine1"] as? String
        latit = Double(addressData["latitude"] as! Float)
        longit = Double(addressData["longitude"] as! Float)
        signUpModel.taggedAs.value = (addressData["taggedAs"] as? String)!
        signUpModel.address.value =  locationTF.text!
        signUpModel.latit.value = latit
        signUpModel.logit.value = longit
        signUpModel.addressCity.value = (addressDetails?.city)!
        signUpModel.addressState.value = (addressDetails?.state)!
        signUpModel.addressCountry.value = (addressDetails?.country)!
        signUpModel.addressPinCode.value = (addressDetails?.pinCode)!
        view10.backgroundColor = COLOR.APP_COLOR
    }
}

extension SignUpViewController :CropViewControllerDelegate{
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        
        pickedImage.image = image
        profileImage.image = image
        controller.dismiss(animated: true, completion: nil)
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        pickedImage.image = nil
        controller.dismiss(animated: true, completion: nil)
    }
}


