//
//  CategoryViewController.swift
//  Provider
//
//  Created by Vengababu Maparthi on 24/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxKeyboard
import RxSwift

protocol categoryDelegate:class
{
    func categoryDetailsUpdated(certificates:[[String:Any]],documentImages:[UIImage],timeStamp:String)
}


class CategoryViewController: UIViewController,UINavigationControllerDelegate {
    
    @IBOutlet weak var editButton: UIButton!
    
    
    open weak var delegate: categoryDelegate?
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var categoryTableView: UITableView!
    
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerBottomConstraint: NSLayoutConstraint!
    
    let disposeBag  = DisposeBag()
    var documents   = [CatModel]()
    var temp        = [UploadImage]()
    var categorytit = ""
    
    var profileModel =  ProfileListModel()
    var myImages     = [UIImage]()
    
    var expiryIndexPath   = IndexPath()
    var documentIndexPath =  IndexPath()
    var otherIndexPath    = IndexPath()
    var activeTextField   = UITextField()
    
    @IBOutlet weak var heightOfTableView: NSLayoutConstraint!
    
    @IBOutlet weak var saveButton: ZFRippleButton!
    @IBOutlet weak var heightOfSaveButton: NSLayoutConstraint!
    var categoryID  = ""
    
    var categorySelectedIndex = -1
    var fromWhichScreen = -1  // 0 = signup, 1 = editCategoryVC , 2 = from profile
    
    let sharedInstance = SignUpViewController.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if fromWhichScreen == 0{
            self.heightOfSaveButton.constant = 50
            self.editButton.isHidden = true
        }
        else{
            self.heightOfSaveButton.constant = 0
            self.saveButton.isHidden = true
            self.categoryTableView.isUserInteractionEnabled = false
        }
        categoryTitle.text = categorytit
        categoryTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 08/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        heightOfTableView.constant = categoryTableView.contentSize.height
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            mainScrollView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.mainScrollView.contentInset.bottom = keyboardVisibleHeight
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func backToVC(_ sender: Any) {
        
        if fromWhichScreen == 0{
            
            if documents.index(where: {$0.isMandatody == 1}) != nil { // mandatory is there
                confirmTheBackAction()
                //                self.navigationController?.popToRootViewController(animated: true)
                //                sharedInstance.categoryDetailsUpdated(index: categorySelectedIndex)
            }else{
                self.navigationController?.popToRootViewController(animated: true)
            }
            
        }else if fromWhichScreen == 1{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        // confirmTheBackAction()
    }
    
    func confirmTheBackAction(){
        let alert = UIAlertController(title: "Alert", message: "Make sure you provided your mandatory details!!", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if self.fromWhichScreen == 0{
                self.navigationController?.popToRootViewController(animated: true)
                //  self.sharedInstance.categoryDetailsUpdated(index: self.categorySelectedIndex)
            }else if self.fromWhichScreen == 1{
                self.navigationController?.popViewController(animated: true)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        })
        let noButton = UIAlertAction(title: "No", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            print("you pressed No, thanks button")
            // call method whatever u need
        })
        
        alert.addAction(yesButton)
        alert.addAction(noButton)
        present(alert, animated: true) //{ _ in }
    }
    
    @IBAction func selectDate(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.datePickerBottomConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        documents[expiryIndexPath.section].subCatArray[expiryIndexPath.row].expiryDate = dateFormatter.string(from: datePicker.date)
        documents[expiryIndexPath.section].subCatArray[expiryIndexPath.row].data = dateFormatter.string(from: datePicker.date)
        categoryTableView.reloadData()
    }
    
    
    @IBAction func cancelExpireDate(_ sender: Any) {
        expiryIndexPath = IndexPath()
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.datePickerBottomConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    @objc func selectTheDocumentImage(_ sender : UIButton){
        self.view.endEditing(true)
        let section = sender.tag / 100
        let row = sender.tag % 100
        documentIndexPath = IndexPath(row: row, section: section)
        selectImage()
    }
    
    @objc func selectTheExpriyDate(_ sender : UIButton ){
        datePicker.date = Date.dateWithDifferentTimeInterval()
       // datePicker.minimumDate = Date()
        let section = sender.tag / 100
        let row = sender.tag % 100
        expiryIndexPath = IndexPath(row: row, section: section)
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.categoryTableView.contentInset.bottom = keyboardVisibleHeight
            })
            .disposed(by: disposeBag)
        
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.datePickerBottomConstraint.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    
    @IBAction func saveEditAction(_ sender: Any) {
        if editButton.isSelected{
            editButton.isSelected = false
            self.categoryTableView.isUserInteractionEnabled = false
            updateDocuments()
        }else{
            editButton.isSelected = true
            self.categoryTableView.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func saveTheCategoryData(_ sender: Any) {
        updateDocuments()
    }
    
    func updateDocuments(){
        var isAlertShows = false
        var imageArray  = [UIImage]()
        
        var finalDict = [[String:Any]]()
        
        let docTimeStamp  = Helper.currentTimeStamp
        var docCount = 0
        
        outLoop: for i in 0 ..< documents.count {
            
            for j in 0 ..< documents[i].subCatArray.count{
                var indexPath = IndexPath(row: j, section: i)
                if let cell: AddCatDetailsTableCell = categoryTableView.cellForRow(at: indexPath) as? AddCatDetailsTableCell{
                    
                    switch documents[indexPath.section].subCatArray[indexPath.row].type{
                    case 1:
                        var documentOther = [String:Any]()
                        if documents[indexPath.section].subCatArray[indexPath.row].isMandatody == 1{
                            if (cell.otherTF.text?.isEmpty)!{
                                if !isAlertShows{
                                    Helper.alertVC(errMSG: "Please fill the " + documents[indexPath.section].subCatArray[indexPath.row].fieldName)
                                    isAlertShows = true
                                    break outLoop
                                }
                            }else{
                                documentOther = ["fId":documents[indexPath.section].subCatArray[indexPath.row].fieldID,
                                                 "data":documents[indexPath.section].subCatArray[indexPath.row].data,
                                                 "fName":documents[indexPath.section].subCatArray[indexPath.row].fieldName,
                                                 "fType":documents[indexPath.section].subCatArray[indexPath.row].type,
                                                 "isManadatory":documents[indexPath.section].subCatArray[indexPath.row].isMandatody,]
                                finalDict.append(documentOther)
                            }
                        }else{
                            if !(cell.otherTF.text?.isEmpty)!{
                                documentOther = ["fId":documents[indexPath.section].subCatArray[indexPath.row].fieldID,
                                                 "data":documents[indexPath.section].subCatArray[indexPath.row].data,
                                                 "fName":documents[indexPath.section].subCatArray[indexPath.row].fieldName,
                                                 "fType":documents[indexPath.section].subCatArray[indexPath.row].type,
                                                 "isManadatory":documents[indexPath.section].subCatArray[indexPath.row].isMandatody,]
                                finalDict.append(documentOther)
                            }
                        }
                        break
                    case 2:
                        var documentExpiry = [String:Any]()
                        if documents[indexPath.section].subCatArray[indexPath.row].isMandatody == 1{
                            if (cell.expiryDateTF.text?.isEmpty)!{
                                if !isAlertShows{
                                    Helper.alertVC(errMSG: "Please provide the " + documents[indexPath.section].subCatArray[indexPath.row].fieldName)
                                    isAlertShows = true
                                    break outLoop
                                }
                            }else{
                                documentExpiry = ["fId":documents[indexPath.section].subCatArray[indexPath.row].fieldID,
                                                  "data":documents[indexPath.section].subCatArray[indexPath.row].data,
                                                  "fName":documents[indexPath.section].subCatArray[indexPath.row].fieldName,
                                                  "fType":documents[indexPath.section].subCatArray[indexPath.row].type,
                                                  "isManadatory":documents[indexPath.section].subCatArray[indexPath.row].isMandatody,]
                                finalDict.append(documentExpiry)
                            }
                        }else{
                            if !(cell.expiryDateTF.text?.isEmpty)!{
                                documentExpiry = ["fId":documents[indexPath.section].subCatArray[indexPath.row].fieldID,
                                                  "data":documents[indexPath.section].subCatArray[indexPath.row].data,
                                                  "fName":documents[indexPath.section].subCatArray[indexPath.row].fieldName,
                                                  "fType":documents[indexPath.section].subCatArray[indexPath.row].type,
                                                  "isManadatory":documents[indexPath.section].subCatArray[indexPath.row].isMandatody,]
                                finalDict.append(documentExpiry)
                            }
                        }
                        
                        break
                    default:
                        var documentImage = [String:Any]()
                        
                        if  fromWhichScreen == 0{
                            if documents[indexPath.section].subCatArray[indexPath.row].isMandatody == 1{
                                if documents[indexPath.section].subCatArray[indexPath.row].docImage.image == nil{
                                    if !isAlertShows{
                                        Helper.alertVC(errMSG: "Please add the " + documents[indexPath.section].subCatArray[indexPath.row].fieldName)
                                        isAlertShows = true
                                        break outLoop
                                    }
                                }else{
                                    docCount = docCount + 1
                                    documentImage = ["fId":documents[indexPath.section].subCatArray[indexPath.row].fieldID,
                                                     "data":Utility.amazonUrl  + AMAZONUPLOAD.DOCUMENTIMAGES + docTimeStamp + String(describing:docCount) + ".png",
                                                     "fName":documents[indexPath.section].subCatArray[indexPath.row].fieldName,
                                                     "fType":documents[indexPath.section].subCatArray[indexPath.row].type,
                                                     "isManadatory":documents[indexPath.section].subCatArray[indexPath.row].isMandatody,]
                                    imageArray.append(documents[indexPath.section].subCatArray[indexPath.row].docImage.image!)
                                    finalDict.append(documentImage)
                                }
                            }else{
                                if documents[indexPath.section].subCatArray[indexPath.row].docImage.image != nil {
                                    docCount = docCount + 1
                                    documentImage = ["fId":documents[indexPath.section].subCatArray[indexPath.row].fieldID,
                                                     "data":Utility.amazonUrl  + AMAZONUPLOAD.DOCUMENTIMAGES + docTimeStamp + String(describing:docCount) + ".png",
                                                     "fName":documents[indexPath.section].subCatArray[indexPath.row].fieldName,
                                                     "fType":documents[indexPath.section].subCatArray[indexPath.row].type,
                                                     "isManadatory":documents[indexPath.section].subCatArray[indexPath.row].isMandatody,]
                                    imageArray.append(documents[indexPath.section].subCatArray[indexPath.row].docImage.image!)
                                    finalDict.append(documentImage)
                                }
                            }
                        }else{
                            if documents[indexPath.section].subCatArray[indexPath.row].docImage.image != nil {
                                docCount = docCount + 1
                                documentImage = ["fId":documents[indexPath.section].subCatArray[indexPath.row].fieldID,
                                                 "data":Utility.amazonUrl  + AMAZONUPLOAD.DOCUMENTIMAGES + docTimeStamp + String(describing:docCount) + ".png",
                                                 "fName":documents[indexPath.section].subCatArray[indexPath.row].fieldName,
                                                 "fType":documents[indexPath.section].subCatArray[indexPath.row].type,
                                                 "isManadatory":documents[indexPath.section].subCatArray[indexPath.row].isMandatody,]
                                imageArray.append(documents[indexPath.section].subCatArray[indexPath.row].docImage.image!)
                                finalDict.append(documentImage)
                            }else{
                                docCount = docCount + 1
                                documentImage = ["fId":documents[indexPath.section].subCatArray[indexPath.row].fieldID,
                                                 "data":documents[indexPath.section].subCatArray[indexPath.row].data,
                                                 "fName":documents[indexPath.section].subCatArray[indexPath.row].fieldName,
                                                 "fType":documents[indexPath.section].subCatArray[indexPath.row].type,
                                                 "isManadatory":documents[indexPath.section].subCatArray[indexPath.row].isMandatody,]
                                finalDict.append(documentImage)
                            }
                        }
                        
                        break
                    }
                }
            }
        }
        if !isAlertShows{
            if fromWhichScreen == 0{
                sharedInstance.categoryDetailsUpdated(certificates: finalDict,documentImages:imageArray, timeStamp: docTimeStamp)
                //  delegate?.categoryDetailsUpdated(certificates: finalDict,documentImages:imageArray, timeStamp: docTimeStamp)
                self.uploadTheDocumentImages(images:imageArray ,timeStamp:docTimeStamp)
                self.navigationController?.popToRootViewController(animated: true)
            }else{
                self.updatetheDocuments(dict: finalDict)
                self.uploadTheDocumentImages(images:imageArray ,timeStamp:docTimeStamp)
            }
        }
    }
    
    
    func updatetheDocuments(dict:[[String:Any]]) {
        let dict:[String:Any] = ["categoryId":categoryID,
                                 "document":dict]
        profileModel.profileDataUpdateDocuments(params: dict) { (succeeded) in
            if succeeded{
                // self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func uploadTheDocumentImages(images:[UIImage] , timeStamp:String)  {
        self.temp = [UploadImage]()
        var count  = 0
        var url = ""
        for image in images{
            count  = count  + 1
            url = AMAZONUPLOAD.DOCUMENTIMAGES + String(describing:timeStamp) + String(describing:count) + ".png"
            self.temp.append(UploadImage.init(image: image , path: url))
        }
        
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
    }
    
    
    /// MARK: - actionSheet
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: signup.selectImage as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: signup.cancel as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: signup.camera,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: signup.gallery as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    // MARK: - photo gallery
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - Camera selection
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            noCamera()
        }
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
}


// MARK: - ImagePicker delegate method
extension CategoryViewController : UIImagePickerControllerDelegate {
    /*The image is not getting dispalyed when when selected because this method is deprecated use th below method
    Owner : Vani
    Date : 27/02/20202
    */
    /*
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        myImages.append(Helper.resizeImage(image: image, newWidth: 200)!)
        documents[documentIndexPath.section].subCatArray[documentIndexPath.row].docImage.image = Helper.resizeImage(image: image, newWidth: 200)!
        categoryTableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }*/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            myImages.append(Helper.resizeImage(image: image, newWidth: 200)!)
            documents[documentIndexPath.section].subCatArray[documentIndexPath.row].docImage.image = Helper.resizeImage(image: image, newWidth: 200)!
            categoryTableView.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension CategoryViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryHeader") as! CategoryHeaderTableCell
        cell.titleOfDoc.text = documents[section].docName
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension CategoryViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return documents.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documents[section].subCatArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch documents[indexPath.section].subCatArray[indexPath.row].type {
        case 1:
            let others = tableView.dequeueReusableCell(withIdentifier: "others") as! AddCatDetailsTableCell
            others.otherTF.tag =  (indexPath.section*100)+indexPath.row
            others.updateOtherFieldData(otherData:documents[indexPath.section].subCatArray[indexPath.row])
            return others
            
        case 2:
            let expDate = tableView.dequeueReusableCell(withIdentifier: "expiryDate") as! AddCatDetailsTableCell
            expDate.editExpriyDate.tag =  (indexPath.section*100)+indexPath.row
            expDate.editExpriyDate.addTarget(self, action: #selector(selectTheExpriyDate(_:)), for:.touchUpInside)
            expDate.expriyDateUpdate(expiryData:documents[indexPath.section].subCatArray[indexPath.row])
            return expDate
            
        default:
            let image = tableView.dequeueReusableCell(withIdentifier: "categoryDetails") as! AddCatDetailsTableCell
            image.selectCertificate.tag =  (indexPath.section*100)+indexPath.row
            image.selectCertificate.addTarget(self, action: #selector(selectTheDocumentImage(_:)), for:.touchUpInside)
            image.updateDocImage(docDate:documents[indexPath.section].subCatArray[indexPath.row] , fromProfile:fromWhichScreen)
            return image
        }
    }
}

extension CategoryViewController:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let section = textField.tag / 100
        let row = textField.tag % 100
        otherIndexPath = IndexPath(row: row, section: section)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        documents[otherIndexPath.section].subCatArray[otherIndexPath.row].othersTf = textField.text!
        documents[otherIndexPath.section].subCatArray[otherIndexPath.row].data = textField.text!
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}


