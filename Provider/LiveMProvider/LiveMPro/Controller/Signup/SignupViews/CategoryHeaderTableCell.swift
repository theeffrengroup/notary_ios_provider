//
//  CategoryHeaderTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 13/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class CategoryHeaderTableCell: UITableViewCell {

    @IBOutlet weak var titleOfDoc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
