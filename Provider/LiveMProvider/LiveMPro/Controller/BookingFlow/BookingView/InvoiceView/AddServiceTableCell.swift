//
//  AddServiceTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 13/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class AddServiceTableCell: UITableViewCell {
    @IBOutlet weak var extraServiceName: UITextField!
    
    @IBOutlet weak var removeAddedItem: UIButton!
    @IBOutlet weak var servicePriceField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        extraServiceName.leftView = paddingView
        extraServiceName.leftViewMode = .always



     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
