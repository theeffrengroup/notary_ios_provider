//
//  ICReceiverTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ICReceiverTableViewCell: UITableViewCell {


    @IBOutlet var signatureView: SignatureView!
    var isSignatureMade = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
         signatureView.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func retakeSignature(_ sender: Any) {
        signatureView.erase()
        let ud = UserDefaults.standard
        ud.set(false, forKey: "signatureMade")
        ud.synchronize()
         isSignatureMade = false
        
    }
    
}

extension ICReceiverTableViewCell :SignatureViewDelegate{
    public func signatureMade() {
        let ud = UserDefaults.standard
        ud.set(true, forKey: "signatureMade")
        ud.synchronize()
    }
}

