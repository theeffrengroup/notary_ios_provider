//
//  BookingModel.swift
//  LiveM Pro
//
//  Created by Vengababu Maparthi on 12/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import SwiftKeychainWrapper
class BookingModel:NSObject{
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    
    /// updating booking status to server
    ///
    /// - Parameters:
    ///   - dict: contains booking id, status
    ///   - completionHandler: return status updated r not and travel fee
    func updateBookingStatus(dict:[String:Any] ,
                             completionHandler:@escaping (Bool, Double) -> ()) {
        
        
        let bookingStat = BookingStatusAPI()
        Helper.showPI(message: loading.updateStat)
        bookingStat.updateBookingStatus(method: API.METHOD.UPDATEBOOKINGSTATUS, parameters: dict)
        bookingStat.BookingStatus_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                
                switch responseCodes {
                case .SuccessResponse:
                    Helper.hidePI()
                    self.updateTheBookingsString(bookingID: dict["bookingId"] as! Int64, status: dict["status"] as! Int)
                    
                    if let dataDict = responseModel.data["data"] as? [String:Any] {
                        
                        if  let travelFeeValue =  dataDict["travelFee"] as? Double {
                            completionHandler(true, travelFeeValue)
                            return
                        }
                    }
                    
                    
                    completionHandler(true, 0.0)
                    
                    
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                case .dataNotFound:
                    completionHandler(false, 0.0)
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                default:
                    Helper.hidePI()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false, 0.0)  //Message should present
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    //MARK: - updating the booking str data
    func updateTheBookingsString(bookingID:Int64,status:Int){
        let ud = UserDefaults.standard
        
        let onGoingBids:[String] = Utility.onGoingBid.components(separatedBy: ",")
        
        var bookingSumStr = String()
        
        for dict  in onGoingBids {
            
            let bookingData = dict.components(separatedBy: "|")
            
            if bookingData[0] == String(describing:bookingID){  //String(describing:bookingDict!.bookingId) {
                if bookingSumStr.isEmpty {
                    bookingSumStr = bookingData[0] + "|"  + String(describing:status)
                    
                }else{
                    bookingSumStr = bookingSumStr + "," + bookingData[0] + "|" + String(describing:status)
                }
            }else{
                if bookingSumStr.isEmpty {
                    bookingSumStr = bookingData[0] + "|" + bookingData[1]
                    
                }else{
                    bookingSumStr = bookingSumStr + "," + bookingData[0] +  "|" + bookingData[1]
                }
            }
        }
        ud.set(bookingSumStr, forKey: USER_INFO.SELBID)
        UserDefaults(suiteName: API.groupIdentifier)!.set(bookingSumStr, forKey: "bookingStr")
        UserDefaults.standard.synchronize()
        ud.synchronize()
    }
    
    
    
    /// updating booking start and pause timer
    ///
    /// - Parameters:
    ///   - dict: request params having the seconds remaining
    ///   - completionHandler: return if api succeeded
    func updateBookingTimer(dict:[String:Any] ,
                            completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.load) 
        
        let bookingStat = BookingStatusAPI()
        
        bookingStat.updateBookingTimer(method: API.METHOD.UPDATETIMERSTATUS, parameters: dict)
        bookingStat.BookingStatus_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes {
                case .UserLoggedOut:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    Session.expired()
                    break
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                case .dataNotFound:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .SuccessResponse:
                    completionHandler(true)
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false)  //Message should present
                }
            }, onError: {error in
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}


