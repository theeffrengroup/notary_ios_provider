//
//  OnBookingMapExtension.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

extension OnBookingViewController{
    
    // adding animated view to current location
    func animationForMarker(){
        
        animateView.frame =  CGRect(x:0, y: 0, width:100, height: 100)
        animateView.backgroundColor = UIColor(red: 52 / 255.0, green: 152 / 255.0, blue: 219 / 255.0, alpha: 0.2)
        animateView.layer.cornerRadius = 50
        animateView.layer.masksToBounds = true
        animateView.layer.borderWidth = 1.5
        animateView.layer.borderColor = COLOR.APP_COLOR.cgColor
        
        constantView.frame = CGRect(x:0, y: 0, width:100, height: 100)
        constantView.backgroundColor = UIColor(red: 52 / 255.0, green: 152 / 255.0, blue: 219 / 255.0, alpha: 0.2)
        constantView.layer.cornerRadius = 50
        constantView.layer.borderWidth = 1.5
        constantView.layer.borderColor = COLOR.APP_COLOR.cgColor
        constantView.layer.masksToBounds = true
        constantView.addSubview(animateView)
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 1.5
        scaleAnimation.repeatCount = 12345
        scaleAnimation.autoreverses = false
        scaleAnimation.fromValue = 0.1
        scaleAnimation.toValue = 1.2
        animateView.layer.add(scaleAnimation, forKey: "pulse")
        
        
        
    }
    
    
    //MARK: - set the delegate to map and location
    func initiateMap() {
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        locationManager.startUpdatingLocation()
    }
    
    //MARK: - slider to current position
    func resetSlider()
    {
        self.leftSlider.reset()
    }
    
    //MARK: - initiate the source adn drop location
    func setMarkers(sourceLatitude: Double,
                    sourceLongitude: Double,
                    destinationLatitude: Double,
                    destinationLongitude: Double) {
        
        setDestination(latitude: destinationLatitude,
                       longitude: destinationLongitude)
        
        addPolyline(sourceLatitude: sourceLatitude,
                    sourceLongitude: sourceLongitude,
                    destinationLatitude: destinationLatitude,
                    destinationLongitude: destinationLongitude)
    }
    
    //MARK: - update the marker on map and changes the path according to the lat longs
    func updateLocationoordinates(coordinates:CLLocationCoordinate2D) {
        
        if movingMarker.icon == nil
        {
            
            movingMarker = GMSMarker()
            movingMarker.position = coordinates
            //             movingMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            //             movingMarker.iconView = constantView
            let image =  #imageLiteral(resourceName: "location_music_map_pin_icon")
            movingMarker.icon = image
            movingMarker.map = self.mapView
            movingMarker.appearAnimation = .pop
            
            
        }
        else
        {
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            movingMarker.position =  coordinates
            CATransaction.commit()
        }
        
        
        
        
        
        if !isPathPlotted {
            self .setMarkers(sourceLatitude: coordinates.latitude, sourceLongitude: coordinates.longitude, destinationLatitude: (bookingDict?.latitude)! , destinationLongitude: (bookingDict?.longitude)! )
            isPathPlotted = true
        }
    }
    
    //MARK: - Draws the route from source to destination
    private func addPolyline(sourceLatitude: Double,
                             sourceLongitude: Double,
                             destinationLatitude: Double,
                             destinationLongitude: Double) {
        
        let origin = "\(sourceLatitude),\(sourceLongitude)"
        let destination = "\(destinationLatitude),\(destinationLongitude)"
        let API_KEY = SERVER_CONSTANTS.serverKey // // Get the Key/Server from Google Developer Console
        
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(API_KEY)"
        
        // Call Service
        NetworkHelper.requestPOST(urlString: url,
                                  success: { (response) in
                                    
                                    if response.isEmpty == false {
                                        
                                        if let routes: [AnyObject] = response["routes"].arrayObject as [AnyObject]? {
                                            
                                            for route in routes
                                            {
                                                let dict: [String: AnyObject] = route as! [String : AnyObject]
                                                
                                                let routeOverviewPolyline: [String: AnyObject] = dict["overview_polyline"] as! [String : AnyObject]
                                                let points: String = routeOverviewPolyline["points"] as! String
                                                self.path = GMSPath.init(fromEncodedPath: points)!
                                                
                                                let polyline = GMSPolyline.init(path: self.path)
                                                
                                                polyline.strokeColor = UIColor(hexString: "8cc2e5")
                                                polyline.geodesic = true
                                                polyline.strokeWidth = 5.0
                                                polyline.map = self.mapView
                                                self.timer = Timer.scheduledTimer(timeInterval: 0.0025, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
                                                
                                                self.setBounds()
                                                
                                            }
                                        }
                                    }
                                    else {
                                    }
        },
                                  failure: { (Error) in
                                    
        })
        setBounds()
    }
    
    @objc func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = COLOR.APP_COLOR
            self.animationPolyline.strokeWidth = 3
            self.animationPolyline.map = self.mapView
            
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    /// Set Bounds                      (Call 4)
    /// - Fit Two marker in limited area
    private func setBounds() {
        
        let bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: movingMarker.position,
                                                              coordinate: destination.position)
        mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 64 + 10, left: 10, bottom: 10, right: 10)))
    }
    
    
    
    /// Set Destination Marker          (Call 2)
    ///
    /// - Parameters:
    ///   - latitude: Destination Latitude
    ///   - longitude: Destination Longitude
    private func setDestination(latitude: Double, longitude: Double) {
        
        destination.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        destination.snippet = "Destination"
        destination.map = mapView
        destination.icon =  #imageLiteral(resourceName: "pick_marker")
    }
}


extension OnBookingViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse
        {
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        didFindMyLocation = false
        latitude = coord.latitude
        longitude = coord.longitude
        
        var destinationLocation = CLLocation()
        destinationLocation = CLLocation(latitude: coord.latitude,  longitude: coord.longitude)
        updateLocationoordinates(coordinates: destinationLocation.coordinate)
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude:coord.latitude, longitude: coord.longitude))
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        let newDirection = newHeading.trueHeading
        self.movingMarker.rotation = newDirection
        
    }
}

extension OnBookingViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        let point:CGPoint =  self.mapView.center
        self.mapView.projection.coordinate(for: point)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool)
    {
        print("willMove gesture")
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        print("changes the camera postion")
    }
}

