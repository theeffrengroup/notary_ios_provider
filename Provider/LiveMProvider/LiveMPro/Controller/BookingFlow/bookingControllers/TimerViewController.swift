//
//  TimerViewController.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 03/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import JaneSliderControl
import Kingfisher

class TimerViewController: UIViewController {
    @IBOutlet weak var startTimeButton: UIButton!
    @IBOutlet weak var sliderView: SliderControl!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var bookingIDLabel: UILabel!
    
    var bookingMod = BookingModel()
    var bookingDict:Accepted?
    var status:Int = 0
    var bookingTimer  = Timer()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    var remainingTime:Int = 0
    var travelfees = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
      
        
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        
        
        initateTheTimerControllerData()
        setupGestureRecognizer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
 
        self.navigationController?.hideTransparentNavigationBar()
    }
    
    override func viewDidDisappear(_ animated: Bool) {

        bookingTimer.invalidate()
    }
    
    func initateTheTimerControllerData()  {
        bookingIDLabel.text = "Booking ID \((bookingDict?.bookingId)!)"
        status = (bookingDict?.statusCode)!
        startTimeButton.layer.borderColor = COLOR.APP_COLOR.cgColor
        startTimeButton.layer.borderWidth = 2
        
        if status == 2 || status == 8 {
            startTimeButton.isHidden = false
            if bookingDict?.bookingTime?.status == 1 {
                startTimeButton.isSelected = true
                remainingTime = self.getCreationTimeInt(expiryTimeStamp:(bookingDict?.bookingTime?.startTimeStamp)!)  + (bookingDict?.bookingTime?.second)!
                if !bookingTimer.isValid  && remainingTime > 0{
                    bookingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
                }
            }
            else if bookingDict?.bookingTime?.status == 0 {
                startTimeButton.isSelected = false
                remainingTime = (bookingDict?.bookingTime?.second)!
            }
            timerLabel.text = self.timeFormate(Int(remainingTime))
        }else{
            startTimeButton.isHidden = true
        }
        self.updateCustomerInfoStatus()
    }
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }
    
    @IBAction func slideCancelled(_ sender: SliderControl) {
        print("Cancelled")
    }
    
    @IBAction func sliderFinished(_ sender: Any) {
        self.updateBookingStatus(status:status)
    }
    
    @IBAction func startTimeAction(_ sender: Any) {
        if startTimeButton.isSelected {
            startTimeButton.isSelected = false
            self.updateTimeStatusToServer(status:0)
            self.bookingTimer.invalidate()
        }else{
            startTimeButton.isSelected = true
            if remainingTime == 0{
                remainingTime = (bookingDict?.bookingTime?.second)!
            }
            self.updateTimeStatusToServer(status:1)
        }
    }
    
    @IBAction func messageViewAction(_ sender: Any) {
        
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatVC {
            controller.customerID = (bookingDict?.customerId)!
            controller.bookingID = String(describing:bookingDict!.bookingId)
            controller.custImage = (bookingDict?.profilePic)!
            controller.custName =  (bookingDict?.firstName)!
            if let navigator = navigationController {
                TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                          subType: CATransitionSubtype.fromTop.rawValue,
                                                                          for: (self.navigationController?.view)!,
                                                                          timeDuration: 0.3)
                navigator.pushViewController(controller, animated: false)
            }
        }
    }
    
    @IBAction func callingAction(_ sender: Any) {
        let dropPhone = "tel://" + (bookingDict?.mobileNum)!
        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
            
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    
    func updateTimeStatusToServer(status:Int)  {
        startTimeButton.isHidden = false
        
        let val =  remainingTime
        let params : [String : Any] = [
            "bookingId":bookingDict!.bookingId as Any,
            "status":status,
            "second": val]
        
        bookingMod.updateBookingTimer(dict: params ,completionHandler: { (succeeded) in
            if succeeded{
                self.bookingDict?.bookingTime?.status = status
                if  self.startTimeButton.isSelected {
                    if !self.bookingTimer.isValid {
                        self.bookingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
                    }
                }
            }else{
                
            }
        })
    }
    
    
    @objc func timerTick() {
        remainingTime = remainingTime + 1
        timerLabel.text = self.timeFormate(Int(remainingTime))
    }
    
    /**
     *  Check for Time formate
     *
     *  @param remainingTime Remaining time
     *
     *  @return Return must 00:00/00
     */
    func timeFormate(_ remainingTime: Int) -> String {
        
        let Hours : Int = remainingTime / 3600
        let min: Int = Int(fmod(Double(remainingTime), 3600) / 60)
        let secs: Int = remainingTime % 60
        

        if remainingTime <= 0 {
            bookingTimer.invalidate()
            return "00:00:00"
        }else{
            var hoursString = ""
            var minsString = ""
            var secsString = ""
            if Hours < 10{
                hoursString = "0" + String(describing:Hours)
            }else{
                hoursString = String(describing:Hours)
            }
            
            if min < 10{
                minsString = "0" + String(describing:min)
            }else{
                minsString = String(describing:min)
            }
            
            if secs < 10{
                secsString = "0" + String(describing:secs)
            }else{
                secsString = String(describing:secs)
            }
            
            return hoursString + ":" + minsString + ":" + secsString
        }
    }
    /**
     *  Get time interval between two times
     *
     *  @param dateFrom Datefrom
     *  @param dateTo   DateTo
     *
     *  @return Returns [01h:30m]
     */
    func getCreationTimeInt(expiryTimeStamp : Int) -> Int{
        let distanceTime = Date().timeIntervalSince1970
        return Int(distanceTime) - expiryTimeStamp
    }
    
    @IBAction func backToHome(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - slider to current position
    func resetSlider()
    {
        self.sliderView.reset()
    }
    
    //MARK: - update the driver status images and address according the
    func updateCustomerInfoStatus(){
        resetSlider()
        let sectionType : BookingStatus = BookingStatus(rawValue : status)!
        startTimeButton.isEnabled = false
        switch sectionType {
        case .bookingAccepted:
            sliderView.sliderText = "START"
            status = 8
            break
            
        case .onTheWayToPickup:
            sliderView.sliderText = "START"
            status = 8
            break
            
        case .arrivedAtPickup:
            sliderView.sliderText = "START"
            status = 8
            break
            
        case .tripStarted:
            sliderView.sliderText = "INVOICE" //"RAISE INVOICE"
            status = 9
            startTimeButton.isHidden = false
            startTimeButton.isEnabled = true
            
            break
        case .raiseInvoice:
            self.navigationController?.isNavigationBarHidden = true
            self.performSegue(withIdentifier: "toInvoiceVC", sender: nil)
            break
            
        default:
            break
        }
    }
    
    
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        switch segue.identifier! as String {
            
        case "toInvoiceVC":
            self.navigationController?.isNavigationBarHidden = true
            self.tabBarController?.tabBar.isHidden = true
            if let nextScene =  segue.destination as? InvoiceViewController
            {
         
                nextScene.bookingDict = bookingDict
               // nextScene.travelFee = self.travelfees
            }
            break
        case "timerCustReview":
            let nav = segue.destination as! UINavigationController
            if let review: CustReviewsVC = nav.viewControllers.first as! CustReviewsVC?
            {
                review.customerID = (bookingDict?.customerId)!
            }
            break
        default:
            break
        }
    }
    
    //MARK: - update driver status to server
    func updateBookingStatus(status:Int) {
        Helper.showPI(message:loading.load)
        var distance = 0.0
        var jobTime = 0.0
        if status == 9{
            let distanceData = DistanceCalculation().getTheSavedDistance(distanceDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
            if !distanceData.isEmpty{
                distance = Double((distanceData["dist"] as?  NSNumber)!)
            }
           jobTime = Double(self.remainingTime)
        }
        
        let ud = UserDefaults.standard
        
        let params : [String : Any] = ["status": status,
                                       "bookingId":bookingDict!.bookingId as Any,
                                       "latitude" :ud .value(forKey: "currentLat")! as Any,
                                       "longitude":ud .value(forKey: "currentLog")! as Any,
                                       "distance":distance,
                                       "second":jobTime]
        
        bookingMod.updateBookingStatus(dict: params ,completionHandler: { (succeeded, travelFees) in
            if succeeded{
                self.bookingDict?.statusCode = status
                if status == 8{
                    self.startTimeButton.isSelected = true
                      self.bookingDict?.bookingTime?.status = 1
                    self.updateTimeStatusToServer(status:1)
                }else if status == 9{  // delete the document for distance in couchDB
                    let deleteProfileDoc = Couchbase.sharedInstance
                    deleteProfileDoc.deleteDocument(withDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
                   // self.travelfees = travelFees
                    self.bookingDict?.travelFees = travelFees
                }
                self.updateCustomerInfoStatus()
            }else{
                self.resetSlider()
            }
        })
    }
}

extension TimerViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension TimerViewController:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 2
        case 2:
            return 3
        case 3:
            return 3 + (bookingDict?.services.count)!
        case 4:
            return 3
        case 5:
            if bookingDict?.jobDesc == "" {
                return 0
            }
            else{
                return 4
            }
            //return 0 //4 job desc
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableCell
        let mediumView = tableView.dequeueReusableCell(withIdentifier: "mediumView") as! MediumViewTableCell
        
        switch indexPath.section {
        case 0:
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                cell.selectionStyle = .none
                cell.custAddress.text = bookingDict?.addLine1
                return cell
            }
        case 1:
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "timerProfile") as! CustProfileBookingTableCell
                cell.selectionStyle = .none
                cell.custName.text = bookingDict?.firstName
                cell.categoryName.text = bookingDict?.categoryName
                
                return cell
            }
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
            switch indexPath.row{
            case 0 :
                return mediumView
            case 1:
                header.headerOfCell.text = "TOTAL BILL AMOUNT"
                return header
            default:
                let dateArray = Helper.getTheDateFromTimeStamp(timeStamp:bookingDict!.actuallGigTimeStart).components(separatedBy: "|")
                cell.dateOfAppointment.text = "Date: " + dateArray[0]
                cell.timeOfAppointment.text = "Time: " + dateArray[1]
                
                var total = bookingDict?.totalAmt
                cell.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                return cell
            }
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "timerEvents") as! BookingEventsTableCell
            cell.selectionStyle = .none
            
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text =  "PAYMENT BREAKDOWN"
                return header
//            case (bookingDict?.services.count)! + 2 :
//                cell.key.text = onBookingVC.Discount
//                cell.value.text =  Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.discount))
//                return cell
            case (bookingDict?.services.count)! + 2:
                let totalCel = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell
                
                var total = bookingDict?.totalAmt

                totalCel.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                
                return totalCel
            default:
                
                if bookingDict?.serviceType == 1{
                    
                    if (bookingDict?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingDict?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                       (bookingDict?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Discount"
                        {
                        cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)!
                    }
                    else {
                       cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)! + " * " + String(describing:bookingDict!.services[indexPath.row - 2].quantity)
                    }
                    cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.services[indexPath.row - 2].servicePrice))
                }else{
                    
                    let amt = bookingDict!.services[indexPath.row - 2].servicePrice / Double(bookingDict!.services[indexPath.row - 2].quantity)
                    
                    if (bookingDict?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingDict?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Discount"{
                        cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)!
                      cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.services[indexPath.row - 2].servicePrice))
                    }
                    else {
                       // cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)! +
//                            " (" + String(format:"%.2f",amt)  + " * " + String(describing:bookingDict!.services[indexPath.row - 2].quantity) + " hr)"
                        cell.key.text = Helper.timeInHourMin((bookingDict?.totalJobTime)!)
                        cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.totalHourlyFee))
                    }
                    
                    
                   
                }
                return cell
            }
            
        case 4:
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text =  "Payment Method"
                return header

            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentType") as! PaymentMethodTableCell
                if (bookingDict?.paidByWallet)! {
                    cell.paymentType.text = "Wallet" + " + " + (bookingDict?.paymentMethod)!
                }else{
                    cell.paymentType.text = bookingDict?.paymentMethod
                }
             
                cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                return cell
            }
            
            
            
        default:
            switch indexPath.row{
            case 0:
                return mediumView
            case 3:
                return mediumView
            case 1:
                header.headerOfCell.text =  "Job Description"
                return header
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "jobDesc") as! JobDescTableCell
                if bookingDict?.jobDesc == "" {
                    cell.jobDescription.text = "This booking does not have a description"
                }
                else {
                    cell.jobDescription.text = bookingDict!.jobDesc
                }
                return cell
            }
        }
    }
}


extension TimerViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

