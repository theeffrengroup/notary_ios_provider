
//
//  InvoiceViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView
import JaneSliderControl
import Kingfisher
import RxKeyboard

import RxSwift

class InvoiceViewController: UIViewController ,UINavigationControllerDelegate {
    
    @IBOutlet weak var bottomContraintForSlider: NSLayoutConstraint!
    @IBOutlet var invoiceTableView: UITableView!
    var noOfServices: Int = 0
    static var myImages = [UIImage]()
    @IBOutlet weak var sliderView: SliderControl!
    
    var temp = [UploadImage]()
    
    var lastLat:Double  = 0.00
    var lastLog:Double = 0.00
    var serviceTotal = 0.0
    var bookingDict:Accepted?
    var invoiceDetails = BookingModel()
    var servicesConfirmed = false
    let disposeBag = DisposeBag()
    var arrayOfExtraServices = [[String:Any]]()
    var travelFee = 0.0
    
    
    override func viewDidLoad() {
        InvoiceViewController.myImages = [UIImage]()
        super.viewDidLoad()
        invoiceTableView.estimatedRowHeight = 10
        invoiceTableView.rowHeight = UITableView.automaticDimension
        sliderView.sliderText = "Job Completed"
        let ud = UserDefaults.standard
        ud.set(false, forKey:"signatureMade")
        ud.synchronize()
        bottomContraintForSlider.constant = -73
        // Do any additional setup after loading the view.
        travelFee = (bookingDict?.travelFees)!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            invoiceTableView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.invoiceTableView.contentInset.bottom = keyboardVisibleHeight + 20
            })
            .disposed(by: disposeBag)
    }
    
    
    func resetSlider()
    {
        self.sliderView.reset()
    }
    
    
    
    @IBAction func gestureAction(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func needHelp(_ sender: Any) {
        self.performSegue(withIdentifier: "fromInvoice", sender:  nil)
    }
    
    @IBAction func sliderChanged(_ sender: Any) {
    }
    
    @IBAction func sliderCancel(_ sender: Any) {
        
    }
    
    @IBAction func sliderFinish(_ sender: Any) {
        self.updateTheJobCompletionStatus()
    }
    
    
    @IBAction func backToRootVC(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func confirmServices(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottomContraintForSlider.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
        
        self.view.endEditing(true)
        var counts = [Int]()
        for i in 0..<arrayOfExtraServices.count { // to get all the selected services
            if  let serviceName = arrayOfExtraServices[i]["serviceName"] as? String{
                if  let price = arrayOfExtraServices[i]["price"] as? String{
                    if serviceName.length > 0 && price.length > 0{
                        
                    }else{
                        counts.append(i)
                    }
                }
            }
        }
        counts = counts.sorted { $0 > $1 }
        for count in counts{
            arrayOfExtraServices.remove(at: count)
        }
        
        servicesConfirmed = true
        invoiceTableView.reloadData()
    }
    
    
    func updateTheJobCompletionStatus(){
        let ud = UserDefaults.standard
        if ud.bool(forKey: "signatureMade") {
            self.uploadSignatureAndDocimgToAmazon()
            self.updateBookingStatus(status: 10)
        }else{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Please take the signature from customer"), animated: true, completion: nil)
            self.resetSlider()
        }
    }
    
    
    func uploadSignatureAndDocimgToAmazon(){
        temp = [UploadImage]()
        
        let indexPath = IndexPath(row: 1, section: 5)
        if let cell: ICReceiverTableViewCell = invoiceTableView.cellForRow(at: indexPath) as? ICReceiverTableViewCell {
            var url = String()
            url = AMAZONUPLOAD.SIGNATURE + String(describing:bookingDict!.bookingId) + ".png"
            let image = self.image(from: cell.signatureView)
            temp.append(UploadImage.init(image: image, path: url))
        }
        
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
    }
    
    //MARK: - get the image from uiview
    func image(from view: SignatureView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    //MARK: - update driver status to server
    func updateBookingStatus(status:Int) {
        Helper.showPI(message:loading.load)
        let deleteChatDoc = Couchbase.sharedInstance
        let ud = UserDefaults.standard
        let signatureUrl = Utility.amazonUrl + AMAZONUPLOAD.SIGNATURE + String(describing:bookingDict!.bookingId) + ".png"
        
        let params : [String : Any] = ["status": status,
                                       "bookingId":bookingDict!.bookingId as Any,
                                       "latitude" :ud .value(forKey: "currentLat")! as Any,
                                       "longitude":ud .value(forKey: "currentLog")! as Any,
                                       "signatureUrl":signatureUrl,
                                       "additionalService":arrayOfExtraServices]
        
        invoiceDetails.updateBookingStatus(dict: params ,completionHandler: { (succeeded, travelFees) in
            if succeeded{
                self.bookingDict?.statusCode = status
                deleteChatDoc.deleteDocument(withDocID: String(describing:self.bookingDict!.bookingId))
                self.navigationController?.isNavigationBarHidden = true
                self.performSegue(withIdentifier: "toRateView", sender: nil)
            }else{
                self.resetSlider()
                Helper.hidePI()
            }
        })
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! as String {
            
        case "toRateView":
            let nav = segue.destination as! UINavigationController
            if let nextScene: SubmitReviewVC = nav.viewControllers.first as! SubmitReviewVC?
            {
                nextScene.bookingDict = bookingDict
                nextScene.signatureURL = Utility.amazonUrl + AMAZONUPLOAD.SIGNATURE + String(describing:bookingDict!.bookingId) + ".png"
                nextScene.delegate = self
                nextScene.extraServices = arrayOfExtraServices
                nextScene.travelFee = travelFee
            }
            break
        default:
            break
        }
    }
    
    @IBAction func addMoreService(_ sender: Any) {
        let dict:[String:Any] = ["serviceName":"",
                                 "price":"0.00"]
        arrayOfExtraServices.append(dict)
        
        invoiceTableView.beginUpdates()
        invoiceTableView.insertRows(at: [IndexPath(row: arrayOfExtraServices.count - 1, section: 3)], with: .automatic)
        invoiceTableView.endUpdates()
    }
    
    @objc func removedAddedItem(_ sender : UIButton){
        
        let indexPath = IndexPath(row:sender.tag, section: 3)
        let amount:String? = (arrayOfExtraServices[sender.tag]["price"] as! String)
        if amount?.length !=  0  {
            serviceTotal = serviceTotal - Double(amount!)!
        }
        arrayOfExtraServices.remove(at: sender.tag)
        self.invoiceTableView.deleteRows(at: [indexPath], with: .automatic)
        self.invoiceTableView.reloadSections([1,3,4], with: .automatic)
    }
}

extension InvoiceViewController : UIImagePickerControllerDelegate{
    /*The image is not getting dispalyed when when selected because this method is deprecated use th below method
    Owner : Vani
    Date : 27/02/20202
    */
    /*
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        InvoiceViewController.myImages.append(Helper.resizeImage(image: image, newWidth: 200)!)
        invoiceTableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    } */
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            InvoiceViewController.myImages.append(Helper.resizeImage(image: image, newWidth: 200)!)
            invoiceTableView.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension InvoiceViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension InvoiceViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 3
        case 2:
            return 3 + (bookingDict?.services.count)!
        case 3 :
            return arrayOfExtraServices.count
        case 4:
            if !servicesConfirmed{
                return 2
            }else{
                return 1
            }
            
        default:
            if !servicesConfirmed{
                return 1
            }else{
                return 2
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableCell
        let mediumView = tableView.dequeueReusableCell(withIdentifier: "mediumView") as! MediumViewTableCell
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "invoiceProfile") as! CustProfileBookingTableCell
            cell.custName.text = bookingDict?.firstName
            cell.categoryName.text = bookingDict?.categoryName
            let imageURL = bookingDict?.profilePic
            cell.custImage.kf.setImage(with: URL(string: imageURL!),
                                       placeholder:UIImage.init(named: "signup_profile_default_image"),
                                       options: [.transition(ImageTransition.fade(1))],
                                       progressBlock: { receivedSize, totalSize in
            },
                                       completionHandler: { image, error, cacheType, imageURL in
            })
            
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
            switch indexPath.row{
            case 0:
                return mediumView
            case 1 :
                header.headerOfCell.text = "TOTAL BILL AMOUNT"
                return header
            default:
                let dateArray = Helper.getTheDateFromTimeStamp(timeStamp:bookingDict!.actuallGigTimeStart).components(separatedBy: "|")
                cell.dateOfAppointment.text = "Date: " + dateArray[0]
                cell.timeOfAppointment.text = "Time: " + dateArray[1]
                
                var total = bookingDict?.totalAmt

                cell.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total! + serviceTotal + self.travelFee))
                return cell
            }
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "invoiceEvents") as! BookingEventsTableCell
            cell.selectionStyle = .none
            switch indexPath.row{
            case 0:
                return mediumView
                
            case 1:
                header.headerOfCell.text =  "PAYMENT BREAKDOWN"
                return header
                
            case (bookingDict?.services.count)! + 2 :
                cell.key.text = onBookingVC.Travel
                cell.value.text =  Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",(bookingDict?.travelFees)!))
                return cell
                
//            case 2 + (bookingDict?.services.count)! :
//                cell.key.text = onBookingVC.Discount
//                cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.discount))
//                return cell
                
            default:
                if bookingDict?.serviceType == 1{
                    if (bookingDict?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingDict?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Discount"{
                        cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)!
                    }
                    else {
                        cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)! + " * " + String(describing:bookingDict!.services[indexPath.row - 2].quantity)
                    }
                    
                    cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.services[indexPath.row - 2].servicePrice))
                }else{
                    
                    let amt = bookingDict!.services[indexPath.row - 2].servicePrice / Double(bookingDict!.services[indexPath.row - 2].quantity)
                    
                    if (bookingDict?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingDict?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Discount"{
                        cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)!
                        cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.services[indexPath.row - 2].servicePrice))
                    }
                    else{
                        
//                       //  cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)! +
//                            " (" + String(format:"%.2f",amt) + " * " + String(describing:bookingDict!.services[indexPath.row - 2].quantity) + " hr)"
                        cell.key.text = Helper.timeInHourMin((bookingDict?.totalJobTime)!)
                        cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.totalHourlyFee))
                    }
                   
                    
                }
                return cell
                
            }
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "extraServices") as! AddServiceTableCell
            cell.servicePriceField.layer.borderColor = COLOR.LIGHT_GRAY.cgColor
            cell.servicePriceField.layer.borderWidth = 1
            cell.servicePriceField.tag = indexPath.row
            
            cell.extraServiceName.layer.borderColor = COLOR.LIGHT_GRAY.cgColor
            cell.extraServiceName.layer.borderWidth = 1
            
            if servicesConfirmed{
                cell.removeAddedItem.isHidden = true
            }else{
                cell.removeAddedItem.isHidden = false
            }
            cell.removeAddedItem.tag = indexPath.row
            cell.removeAddedItem.addTarget(self, action: #selector(removedAddedItem(_:)), for:.touchUpInside)
            cell.extraServiceName.text = (arrayOfExtraServices[indexPath.row]["serviceName"] as! String)
            
            if let addServicePrice = arrayOfExtraServices[indexPath.row]["price"] as? String{
                cell.servicePriceField.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", Double(addServicePrice)!))
                //(arrayOfExtraServices[indexPath.row]["price"] as! String)
            }
            return cell
            
        case 4:
            if indexPath.row == 1{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell
                let total = bookingDict?.totalAmt
                cell.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total! + serviceTotal + travelFee ))
                return cell
                
                
            }else{
                if servicesConfirmed{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell
                    let total = bookingDict?.totalAmt
                    cell.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total! + serviceTotal + travelFee))
                    return cell
                    
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "addItems") as! AddMoreServicesTableCell
                    return cell
                }
            }
        case 5:
            
            if !servicesConfirmed{
                let cell = tableView.dequeueReusableCell(withIdentifier: "confirm") as! ICConfirmTableViewCell
                cell.selectionStyle = .none
                return cell
            }else{
                if indexPath.row == 0{
                    return mediumView
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "receiverDetails") as! ICReceiverTableViewCell
                    cell.selectionStyle = .none
                    return cell
                }
            }
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "invoiceEvents") as! BookingEventsTableCell
            cell.selectionStyle = .none
            return cell
        }
    }
}

extension InvoiceViewController:SubmitReviewVCDelegate{
    func submitReviewSuccessFull() {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        _ = navigationController?.popToRootViewController(animated: true)
    }
}

extension InvoiceViewController:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if servicesConfirmed{
            return false
        }
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.invoiceTableView.contentInset.bottom = keyboardVisibleHeight + 20
            })
            .disposed(by: disposeBag)
        for i in 0..<arrayOfExtraServices.count {
        let indexPath = IndexPath(row:i, section: 3)
        if let cell = invoiceTableView.cellForRow(at: indexPath) as? AddServiceTableCell{
            if textField == cell.servicePriceField {
                cell.servicePriceField.text = ""
            }
        }
        }
        
        textField.layer.borderColor = COLOR.APP_COLOR.cgColor
        textField.layer.borderWidth = 2
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        serviceTotal = 0.0
        
        
        
        for i in 0..<arrayOfExtraServices.count { // to get all the selected services
            let indexPath = IndexPath(row:i, section: 3)
            if let cell = invoiceTableView.cellForRow(at: indexPath) as? AddServiceTableCell{
                arrayOfExtraServices[i]["serviceName"] = cell.extraServiceName.text
                var amount = ""
                
                
                
                    if (cell.servicePriceField.text?.length)! > 0{
                        
                        amount = (cell.servicePriceField.text?.replacingOccurrences(of: Utility.currencySymbol + " ", with: "", options: NSString.CompareOptions.literal, range: nil))!
                        arrayOfExtraServices[i]["price"] =  String(describing: Double(amount)!) //Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",Double(amount)!))
                        
                        serviceTotal = serviceTotal + Double(amount)!
                        
                    }else{
                        amount = "0.00"
                        arrayOfExtraServices[i]["price"] =  String(describing: Double(amount)!)
                    }
           
                
                //                if amount.length !=  0 {
                //                    serviceTotal = serviceTotal + Double(amount)!
                //                }
        }
    }
        
        self.invoiceTableView.reloadSections([1,4], with: .automatic)
        textField.layer.borderColor = COLOR.LIGHT_GRAY.cgColor
        textField.layer.borderWidth = 2
        //invoiceTableView.reloadData()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }
        
        let newText = oldText.replacingCharacters(in: r, with: string)
        //let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1
        
        let numberOfDecimalDigits: Int
        if let dotIndex = newText.index(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
        return  numberOfDots <= 1 && numberOfDecimalDigits <= 2
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
    }
}
