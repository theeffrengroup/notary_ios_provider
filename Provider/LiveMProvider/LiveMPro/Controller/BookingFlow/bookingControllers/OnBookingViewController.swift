//
//  OnBookingViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 15/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import JaneSliderControl
import Kingfisher

//6: When the driver is on the way to pickup location
//7: when the driver arrived to pickup location
//8: when the driver started the trip after loading
//9: when the driver reached the drop location
//16: when the vehicle is unloaded

enum BookingStatus : Int {
    case bookingAccepted = 3
    case onTheWayToPickup = 6
    case arrivedAtPickup = 7
    case tripStarted = 8
    case raiseInvoice = 9
    case jobCompleted = 16
}


class OnBookingViewController: UIViewController,UIScrollViewDelegate {
    
    
    @IBOutlet var googleMaps: UIButton!
    @IBOutlet var getCurrentLocation: UIButton!
    @IBOutlet var wazeMaps: UIButton!
    @IBOutlet weak var updateViewButton: UIButton!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var leftSlider: SliderControl!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var cancelBookingButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var bookingTableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var updateViewContraint: NSLayoutConstraint!
    @IBOutlet weak var navigationButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var sliderTopView: UIView!
    
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    var animateView = UIView()
    var constantView = UIView()
    let locationManager = CLLocationManager()
    var location        = CLLocation()
    let source       = GMSMarker()
    let destination  = GMSMarker()
    var movingMarker = GMSMarker()
    var bookingMod = BookingModel()
    var bookingDict:Accepted?
    var status:Int        = 0
    var latitude:Double  = 0.00
    var longitude:Double = 0.00
    var travelledDistance:Double = 0.00
    var didFindMyLocation = false
    var isPathPlotted     = false
    var tableHeaderViewHeight = 240 as CGFloat
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView = self.bookingTableView.tableHeaderView as! GMSMapView
        self.bookingTableView.tableHeaderView = nil
        self.bookingTableView.addSubview(mapView)
        self.bookingTableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight,left: 0, bottom: 0, right: 0)
        self.bookingTableView.contentOffset = CGPoint(x:0,y:-tableHeaderViewHeight)
        bookingTableView.estimatedRowHeight = 10
        bookingTableView.rowHeight = UITableView.automaticDimension
        
        mapView.frame = CGRect(x:0, y: self.bookingTableView.contentOffset.y, width:self.bookingTableView.bounds.size.width, height: -self.bookingTableView.contentOffset.y)
        // animationForMarker()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.bookingTableView.contentOffset.y < -tableHeaderViewHeight {
            mapView.frame = CGRect(x:0, y: self.bookingTableView.contentOffset.y, width:self.bookingTableView.bounds.size.width, height: -self.bookingTableView.contentOffset.y)
        }
    }
    
    func updateTheOnbookingUI()  {
        initiateMap()
        self.mapView.bringSubviewToFront(self.topView)
        self.mapView.bringSubviewToFront(self.navigationView)
        self.mapView.bringSubviewToFront(self.getCurrentLocation)
        leftSlider.sliderText = ""
        titleLabel.text = "On the way to pickup"
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true        
        status = (bookingDict?.statusCode)!
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude) ,longitude: CLLocationDegrees(longitude) , zoom: 16)
        self.mapView.animate(to: camera)
        
        if status == 3{
            
            self.updateTableMapVieHeightInitially(status:false)
        }else{
            
            self.updateTableMapVieHeightInitially(status:true)
        }
        updateCustomerInfoStatus()
        self.bookingTableView.reloadData()
        self.mapView.bringSubviewToFront(self.backButton)
        self.mapView.bringSubviewToFront(self.cancelBookingButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        updateTheOnbookingUI()
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)
        setupGestureRecognizer()
        self.navigationController?.presentTransparentNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelHandling(_:)), name: Notification.Name("cancelledBooking"), object: nil)        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        do {
            // Set the map style by passing the URL of the local file. Make sure style.json is present in your project
            if let styleURL = Bundle.main.url(forResource: "Style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find style.json")
            }
        } catch {
            print("The style definition could not be loaded: \(error)")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if self.timer != nil{
            self.timer.invalidate()
        }
        // movingMarker.iconView = nil
    }
    
    ///********notifies when the booking has been cancelled*******//
    @objc func bookingCancelHandling(_ notification: NSNotification) {
        if String(describing:notification.userInfo!["bookingId"]!)  == String(describing:bookingDict!.bookingId) {
            self.tabBarController?.tabBar.isHidden = false
            _ = navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.hideTransparentNavigationBar()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
        
    }
    
    fileprivate func sliderName(_ slider:SliderControl) -> String {
        switch (slider) {
        case self.leftSlider: return "Middle Left Slider"
        default: return "Unknown Slider"
        }
    }
    
    @IBAction func updateTableView(_ sender: Any) {
        if status == 7{
            if self.updateViewButton.isSelected{
                self.updateTableMapVieHeight(status:true)
                
            }else{
                
                self.updateTableMapVieHeight(status:false)
            }
            bookingTableView.reloadData()
        }
    }
    
    func updateTheHeaderViewAnimation() {
        self.bookingTableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight,left: 0, bottom: 0, right: 0)
        self.bookingTableView.contentOffset = CGPoint(x:0,y:-tableHeaderViewHeight)
    }
    
    func updateTableMapVieHeightInitially(status:Bool){
        if status{
            
            self.updateViewButton.isSelected = false
            var newFrame: CGRect = self.mapView.frame
            newFrame.size.height = UIScreen.main.bounds.size.height - 220
            tableHeaderViewHeight  = UIScreen.main.bounds.size.height - 220
            self.mapView.frame = newFrame
            self.navigationButtonConstraint.constant = 0
            self.view.layoutIfNeeded()
        }else{
            self.updateViewButton.isSelected = true
            self.navigationButtonConstraint.constant = -65
            var newFrame: CGRect = self.mapView.frame
            newFrame.size.height = 240
            tableHeaderViewHeight  = 240
            self.mapView.frame = newFrame
            self.view.layoutIfNeeded()
        }
        updateTheHeaderViewAnimation()
    }
    
    func updateTableMapVieHeight(status:Bool){
        if status{
        
            UIView.animate(withDuration: 0.3,
                           delay: 0.1,
                           options: .curveEaseOut,
                           animations: {() -> Void in
                            self.updateViewButton.isSelected = false
                            var newFrame: CGRect = self.mapView.frame
                            newFrame.size.height = UIScreen.main.bounds.size.height - 220
                            self.tableHeaderViewHeight  = UIScreen.main.bounds.size.height - 220
                            self.mapView.frame = newFrame
                            self.navigationButtonConstraint.constant = 0
                            self.view.layoutIfNeeded()
                            
            }, completion: {(_ finished: Bool) -> Void in
                print("Completed")
            })
        }else{
            UIView.animate(withDuration: 0.3,
                           delay: 0.1,
                           options: .curveEaseOut,
                           animations: {() -> Void in
                            self.updateViewButton.isSelected = true
                            self.navigationButtonConstraint.constant = -65
                            var newFrame: CGRect = self.mapView.frame
                            newFrame.size.height = 240
                            self.tableHeaderViewHeight  = 240
                            self.mapView.frame = newFrame
                            self.view.layoutIfNeeded()
                            
            }, completion: {(_ finished: Bool) -> Void in
                print("Completed")
            })
        }
        updateTheHeaderViewAnimation()
    }
    
    @IBAction func backToHomeVC(_ sender: Any) {
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func callToCustomer(_ sender: Any) {
        let otherUserId = "9384524085ngef09t79204593"
        
        //      let otherUserId = "j89999887778999jjk999"
        
        guard let ownID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        MQTTCallManager.sendcallAvilibilityStatus(status: 0, topic: AppConstants.MQTT.callsAvailability + ownID)
        
        UserDefaults.standard.set(true, forKey: "iscallBtnCliked")
        let userID = otherUserId //Add other user 's userID
        let registerNum = "+918866815492"  //ADD:- Mobile phone number here
        
        let dict:[String:Any] = ["callerId": userID,    //ADD:- UserID here
            "callType" : AppConstants.CallTypes.audioCall,  //ADD:- call type  here
            "registerNum": registerNum,
            "callId": randomString(length: 100),
            "callerIdentifier": ""]
        
        UserDefaults.standard.set(dict, forKey: "storeIndexPath")
        MQTT.sharedInstance.subscribeTopic(withTopicName: AppConstants.MQTT.callsAvailability + userID , withDelivering: .atLeastOnce)
        
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        let audioView = AudioCallView(frame: CGRect(x:0, y:0, width: window.frame.width, height: window.frame.height))
        audioView.tag = 15
        audioView.userNameLbl.text = Utility.firstName
        audioView.callerID = userID
        audioView.setMessageData(messageData: dict)
        let imageURL = bookingDict?.profilePic
        audioView.userImageView.kf.setImage(with: URL(string: imageURL!),
                                            placeholder:UIImage.init(named: "signup_profile_default_image"),
                                            options: [.transition(ImageTransition.fade(1))],
                                            progressBlock: { receivedSize, totalSize in
        },
                                            completionHandler: { image, error, cacheType, imageURL in
        })
        window.addSubview(audioView);
        
        //        let dropPhone = "tel://" + (bookingDict?.mobileNum)!
        //        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
        //
        //            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        //        }
    }
    
    
    @IBAction func cancelBooking(_ sender: Any) {
        
        let cancelView = CancelView.instance
        cancelView.bookingID = (bookingDict?.bookingId)!
        cancelView.bookingType = (bookingDict?.bookingType)!
        if (bookingDict?.bookingType)! == 2{
            cancelView.startTime = (bookingDict?.bookingRequestedFor)!
            cancelView.entTime = (bookingDict?.bookingRequestEnds)!
        }
        cancelView.getTheCancelData()
        cancelView.delegate = self
        cancelView.show()
        
        
    }
    
    //MARK: - slider to current position
    
    
    @IBAction func getCurrentLocation(_ sender: Any) {
    
        getCurrentLocationPostion()
    }
    
    
    
    @IBAction func wayToGoogleMaps(_ sender: Any) {
        MapNavigation.navgigateTogoogleMaps(latit: (bookingDict?.latitude)! , logit: (bookingDict?.longitude)!)
    }
    
    
    //*****way pick r drop loc using waze maps*****//
    
    @IBAction func wayToWazeMaps(_ sender: Any) {
        viewWaze(location : location)
    }
    
    //MARK: - opens the waze maps for navigation
    func viewWaze(location : CLLocation) {
        MapNavigation.navgigateToWazeMaps(latit: (bookingDict?.latitude)!, logit: (bookingDict?.longitude)!)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }
    
    ///MARK:- Slider text data
    @IBAction func sliderFinished(_ sender: SliderControl) {
        self.updateBookingStatus(status:status)
    }
    
    func getCurrentLocationPostion(){
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude) ,longitude: CLLocationDegrees(longitude) , zoom: 16)
        self.mapView.animate(to: camera)
        
    }
    
    //MARK: - update the driver status images and address according the
    func updateCustomerInfoStatus(){
        resetSlider()
        let sectionType : BookingStatus = BookingStatus(rawValue : status)!
        switch sectionType {
        case .bookingAccepted:
            updateViewContraint.constant = 0
            leftSlider.sliderText = onBookingVC.onTheWay
            status = 6
            break
            
        case .onTheWayToPickup:
            updateViewContraint.constant = 21
            titleLabel.text = onBookingVC.tripStart
            leftSlider.sliderText = onBookingVC.arrived
            status = 7
            self.updateTableMapVieHeight(status:true)
            bookingTableView.reloadData()
            
            break
            
        case .arrivedAtPickup:
            self.performSegue(withIdentifier: "toTimerVC", sender: nil)
            
            break
        default:
            break
        }
    }
    
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        print("Cancelled")
        
    }
    
    
    @IBAction func messageAction(_ sender: Any) {
        
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatVC {
            controller.customerID = (bookingDict?.customerId)!
            controller.bookingID = String(describing:bookingDict!.bookingId)
            controller.custImage = (bookingDict?.profilePic)!
            controller.custName =  (bookingDict?.firstName)!
            if let navigator = navigationController {
                TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                          subType: CATransitionSubtype.fromTop.rawValue,
                                                                          for: (self.navigationController?.view)!,
                                                                          timeDuration: 0.3)
                navigator.pushViewController(controller, animated: false)
            }
        }
    }
    
    
    
    //MARK: - update driver status to server
    func updateBookingStatus(status:Int) {
        Helper.showPI(message:loading.load)
        let ud = UserDefaults.standard
        
        var distance = 0.0
        if status == 7{
            let distanceData = DistanceCalculation().getTheSavedDistance(distanceDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
            if !distanceData.isEmpty{
                distance = Double((distanceData["dist"] as?  NSNumber)!)
            }
        }
        
        if (ud .value(forKey: "currentLat") != nil) {
            let params : [String : Any] = ["status": status,
                                           "bookingId":bookingDict!.bookingId as Any,
                                           "latitude" :ud .value(forKey: "currentLat")! as Any,
                                           "longitude":ud .value(forKey: "currentLog")! as Any,
                                           "distance":distance]
            
            bookingMod.updateBookingStatus(dict: params ,completionHandler: { (succeeded, fees) in
                if succeeded{
                    self.bookingDict?.statusCode = status
                    if status == 7{
                        let deleteProfileDoc = Couchbase.sharedInstance
                        deleteProfileDoc.deleteDocument(withDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
                    }
                    self.updateCustomerInfoStatus()
                }else{
                    self.resetSlider()
                }
            })
        }else{
            let params : [String : Any] =  ["status": status,
                                            "bookingId":bookingDict!.bookingId as Any,
                                            "latitude" :latitude,
                                            "longitude":longitude,
                                            "distance":distance]
            
            bookingMod.updateBookingStatus(dict: params, completionHandler: { (succeeded, fees) in
                if succeeded{
                    
                    if status == 7{
                        let deleteProfileDoc = Couchbase.sharedInstance
                        deleteProfileDoc.deleteDocument(withDocID:String(describing:self.bookingDict!.bookingId) + "_Dist")
                    }
                    self.bookingDict?.statusCode = status
                    self.updateCustomerInfoStatus()
                }else{
                    self.resetSlider()
                }
            })
        }
    }
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        switch segue.identifier! as String {
            
        case "toTimerVC":
            if let nextScene =  segue.destination as? TimerViewController
            {
                nextScene.bookingDict = bookingDict
            }
            break
            
        case "onthewayCustReview":
            let nav = segue.destination as! UINavigationController
            if let review: CustReviewsVC = nav.viewControllers.first as! CustReviewsVC?
            {
                review.customerID = (bookingDict?.customerId)!
            }
            break
        case "toChatVC":
            let nav = segue.destination as! UINavigationController
            if let chat: ChatVC = nav.viewControllers.first as! ChatVC?
            {
                chat.customerID = (bookingDict?.customerId)!
                chat.bookingID = String(describing:bookingDict!.bookingId)
                chat.custImage = (bookingDict?.profilePic)!
                chat.custName =  (bookingDict?.firstName)!
            }
            break
        default:
            break
        }
    }
}
extension OnBookingViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension OnBookingViewController:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
       // return 6
        if status == 6 {
            return 6 //7
        }else{
            if self.updateViewButton.isSelected{
                return 6 //7
            }else{
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 2
        case 2:
            return 3
        case 3:
            return 3 + (bookingDict?.services.count)!
        case 4:
            return 3
        case 5:
            if bookingDict?.jobDesc == "" {
                return 0
            }
            else{
                return 3
            }
            //return 0 //3 description
        default:
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableCell
        let mediumView = tableView.dequeueReusableCell(withIdentifier: "mediumView") as! MediumViewTableCell
        
        switch indexPath.section {
        case 0:
            
            if status == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                cell.selectionStyle = .none
                cell.custAddress.text = bookingDict?.addLine1
                return cell
            }else{
                if self.updateViewButton.isSelected{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                    cell.selectionStyle = .none
                    cell.custAddress.text = bookingDict?.addLine1
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "custData") as! CustProfileBookingTableCell
                    cell.custName.text =  bookingDict?.firstName
                    cell.custAddress.text = bookingDict?.addLine1
                    cell.selectionStyle = .none
                    return cell
               
                }
            }
        case 1:
            
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddressProfile") as! AddressProfileBookingTableCell
                cell.selectionStyle = .none
                cell.custName.text = bookingDict?.firstName
                cell.categoryName.text =  bookingDict?.categoryName //"Category name"
                cell.bookingID.text = "Booking ID: " + String(describing:bookingDict!.bookingId)
                return cell
            }
       
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
            switch indexPath.row{
            case 0:
                return mediumView
            case 1 :
                header.headerOfCell.text = "Amount"
                return header
            default:
                let dateArray = Helper.getTheDateFromTimeStamp(timeStamp:bookingDict!.actuallGigTimeStart).components(separatedBy: "|")
                cell.dateOfAppointment.text = "Date: " + dateArray[0]
                cell.timeOfAppointment.text = "Time: " + dateArray[1]
             
                let total = bookingDict?.totalAmt

                cell.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                return cell
            }
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "bookingEvents") as! BookingEventsTableCell
            cell.selectionStyle = .none
            
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text =  "PAYMENT BREAKDOWN"
                return header
//            case (bookingDict?.services.count)! + 2 :
//                cell.key.text = onBookingVC.Discount
//                cell.value.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",bookingDict!.discount))
//                return cell
            case (bookingDict?.services.count)! + 2:
                let totalCell = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell
                
                let total = bookingDict?.totalAmt
                
                totalCell.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                
                return totalCell
            default:
                
                if bookingDict?.serviceType == 1{
                    if (bookingDict?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingDict?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                         (bookingDict?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Discount"{
                        cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)!
                    }
                    else {
                        cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)! + " * " + String(describing:bookingDict!.services[indexPath.row - 2].quantity)
                    }
                    
                    cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.services[indexPath.row - 2].servicePrice))
                }else{
                    
                    let amt = bookingDict!.services[indexPath.row - 2].servicePrice / Double(bookingDict!.services[indexPath.row - 2].quantity)
                    
                    if (bookingDict?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingDict?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                        (bookingDict?.services[indexPath.row - 2].serviceName)! == "Discount"{
                        cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)!
                        cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.services[indexPath.row - 2].servicePrice))
                    }
                    else{
//                        cell.key.text = (bookingDict?.services[indexPath.row - 2].serviceName)! + " (" + String(format:"%.2f",amt)  + " * " + String(describing:bookingDict!.services[indexPath.row - 2].quantity) + " hr)"
                        cell.key.text = Helper.timeInHourMin((bookingDict?.totalJobTime)!)
                        cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingDict!.totalHourlyFee))
                    }
                    
                    
                    
                }
                return cell
            }

        case 4:
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text =  "Payment Method"
                return header
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentType") as! PaymentMethodTableCell
                if (bookingDict?.paidByWallet)! {
                    cell.paymentType.text = "Wallet" + " + " + (bookingDict?.paymentMethod)!
                }else{
                    cell.paymentType.text = bookingDict?.paymentMethod
                }
                
                
                cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                  return cell
            }
        
        case 5:
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text =  "Job Description"
                return header
            default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "jobDesc") as! JobDescTableCell
            if bookingDict?.jobDesc == "" {
                cell.jobDescription.text = "This booking does not have a description"
            }
            else {
                cell.jobDescription.text = bookingDict!.jobDesc
            }
            return cell
            }
        default:
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text =  "Job Photos"
                return header
            default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "jobPhotos") as! JobPhotosTableCell
                return cell
            }
        }
    }
}

extension OnBookingViewController:cancelDeleteBooking{
    func cancelReason(){
 
        self.bookingDict?.statusCode = 10
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        _ = navigationController?.popToRootViewController(animated: true)
    }
}


extension OnBookingViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}
