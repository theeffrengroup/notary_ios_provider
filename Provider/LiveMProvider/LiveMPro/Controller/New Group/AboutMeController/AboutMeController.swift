//
//  AboutMeController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 19/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

protocol AboutMeDelegate {
    func updateUserStaus(status: String)
}

class AboutMeController: UIViewController {
    
    var delegate: AboutMeDelegate?
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var aboutMeText: UITextView!
    @IBOutlet weak var wordCount: UILabel!
    @IBOutlet weak var headerLabel: UILabel!

    var dataMeta:MetaData?
    var updateEventsModel = ProfileListModel()
    
    var aboutUser: String = ""
    var mainTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if aboutUser.length > 1{
            aboutMeText.textColor = UIColor.black
        }
        headerLabel.text = dataMeta?.fieldName
        aboutMeText.text = aboutUser
        updateCharacterCount()
        bottomConstraint.constant = 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// MARK: - This func updates the number of characters for every characters.
    func updateCharacterCount() {
        
        let charCount = self.aboutMeText.text!.length
        let maxLimit = 300
        let remainingChar = maxLimit - charCount
        if (self.aboutMeText.text! == dataMeta?.fieldDesc){
            aboutMeText.textColor = UIColor.lightGray
            self.wordCount.text = "300" + editProfileData.characters
        }else{
            self.wordCount.text = String(remainingChar) + editProfileData.characters
            aboutMeText.textColor = UIColor.black
            
        }
         //self.aboutMeText.becomeFirstResponder()
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        self.view.endEditing(true)
    }
    
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        if aboutMeText.text == aboutUser{
            dismiss(animated: true, completion: nil)
            return
        }else{
            let dict:[String : Any] = ["metaId":self.dataMeta?.fieldID as Any,
                                       "data":aboutMeText.text]
            let params:[String:Any] = ["metaData":dict]
            updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeded) in
                if succeded{
                    self.dataMeta?.description = self.aboutMeText.text
                    self.delegate?.updateUserStaus(status:self.aboutMeText.text)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    
                }
            })
        }
    }
}

extension AboutMeController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        bottomConstraint.constant = 280
        if textView.text == aboutUser{
            textView.textColor = UIColor.black
            if (self.aboutMeText.text! == dataMeta?.fieldDesc){
                   aboutMeText.text = ""
            }else{
             
            }
            
            let charCount = self.aboutMeText.text!.length
            let maxLimit = 300
            let remainingChar = maxLimit - charCount
            self.wordCount.text = String(remainingChar) + editProfileData.characters
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
         bottomConstraint.constant = 10
        if textView.text.isEmpty {
            textView.text = aboutUser
            textView.textColor = UIColor.lightGray
        }
    }
    
    ///MARK: - This func Count the Total characters for AboutMeController Text.
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        guard let text1 = textView.text else { return true }
        let length = text1.length + text.length - range.length
        self.updateCharacterCount()
        let count = 300 - length
        wordCount.text =  String(count) + editProfileData.characters
        return length <= 300 - 1
    }
}
