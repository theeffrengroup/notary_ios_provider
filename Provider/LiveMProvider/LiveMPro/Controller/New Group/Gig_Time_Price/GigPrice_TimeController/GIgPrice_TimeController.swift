//
//  GIgPrice_TimeController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 20/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxKeyboard
import RxSwift

protocol GigTimesDelegate {
    func updateTheServices(servicesList:[ServicesData])
}

class GIgPrice_TimeController: UITableViewController {
    var delegate: GigTimesDelegate?
    
    var servicesList = [ServicesData]()
    var updatedServiceList = [ServicesData]()
    var updateServiceList = ProfileListModel()
    let disposeBag = DisposeBag()
    
    @IBOutlet var serviceTableView: UITableView!
    @IBOutlet weak var editButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.serviceTableView.reloadData()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            serviceTableView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.serviceTableView.contentInset.bottom = keyboardVisibleHeight + 20
            })
            .disposed(by: disposeBag)
        
    }
    
    @IBAction func saveTheChanges(_ sender: Any) {
        self.view.endEditing(true)
        if !editButton.isSelected{
            editButton.isSelected = true
        }else{
            updatedServiceList = [ServicesData]()
            var serviceDict  = [[String:Any]]()
            for val in 0 ... servicesList.count - 1{
                let indexPath = IndexPath(row: val, section: 0)
                let cell: GigPrice_TimeCell = serviceTableView.cellForRow(at: indexPath) as! GigPrice_TimeCell
                if (cell.amountTf.text?.isEmpty)!{
                    let serviceMod = ServicesData()
                    
                    serviceMod.serviceId = servicesList[indexPath.row].serviceId
                    
                    serviceMod.serviceName = servicesList[indexPath.row].serviceName
                    
                    serviceMod.servicePrice = servicesList[indexPath.row].servicePrice
                    
                    serviceMod.serviceUnits = servicesList[indexPath.row].serviceUnits
                    
                    updatedServiceList.append(serviceMod)
                    
                    let dict : [String : Any] =  ["id" :servicesList[indexPath.row].serviceId,
                                                  "price":servicesList[indexPath.row].servicePrice]
                    serviceDict.append(dict)
                    
                }else{
                    let dict : [String : Any] =  ["id" :servicesList[indexPath.row].serviceId,
                                                  "price":cell.amountTf.text?.replacingOccurrences(of: Utility.currencySymbol, with: "") as Any]
                    serviceDict.append(dict)
                    
                    let serviceMod = ServicesData()
                    
                    serviceMod.serviceId = servicesList[indexPath.row].serviceId
                    
                    serviceMod.serviceName = servicesList[indexPath.row].serviceName
                    
                    //  serviceMod.servicePrice = Int(cell.amountTf.text!)!
                    serviceMod.servicePrice = Int((cell.amountTf.text?.replacingOccurrences(of: Utility.currencySymbol, with: ""))!)!
                    serviceMod.serviceUnits = servicesList[indexPath.row].serviceUnits
                    
                    updatedServiceList.append(serviceMod)
                }
            }
            let params : [String : Any] =  ["service" :serviceDict]
            
            updateServiceList.profileDataUpdateService( params: params,completionHandler: { (succeeded) in
                if succeeded{
                    self.editButton.isSelected = false
                    self.delegate?.updateTheServices(servicesList: self.updatedServiceList)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    Helper.hidePI()
                }
            })
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return servicesList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "time_priceCell", for: indexPath) as! GigPrice_TimeCell
        
        cell.amountTf.text = Helper.getTheAmtTextWithSymbol(amt:String(describing:servicesList[indexPath.row].servicePrice))
        cell.timeAndUnits.text = servicesList[indexPath.row].serviceName + " " + servicesList[indexPath.row].serviceUnits
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
          self.view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
}

extension GIgPrice_TimeController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if editButton.isSelected{
            return true
        }else{
            return false
        }
    }
}
