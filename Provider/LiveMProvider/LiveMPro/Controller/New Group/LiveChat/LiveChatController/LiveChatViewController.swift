//
//  LiveChatViewController.swift
//  CargoDriver
//
//  Created by Vengababu Maparthi on 28/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import WebKit

class LiveChatViewController: UIViewController,WKUIDelegate {
    
//    @IBOutlet weak var chatView: UIWebView!
    var activityIndicator = UIActivityIndicatorView()
    var liveChatModel = LiveChatModel()
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateTheView()
        // Do any additional setup after loading the view.
    }
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateTheView() {
        activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.webView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        self.requestUrls()
    }
    
    @IBAction func backToVc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // requesting the live chat url
    func requestUrls(){
        liveChatModel.getTheLiveChatURL { (success,chatUrl) in
            if success{
                let url = URL(string: chatUrl)
                let request = URLRequest(url: url!)
//                self.chatView.loadRequest(request)
                self.webView.load(request)
            }else{
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    //

}
/*
extension LiveChatViewController : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
       activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
         activityIndicator.stopAnimating()
    }
}*/

extension LiveChatViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        activityIndicator.stopAnimating()
    }
}
