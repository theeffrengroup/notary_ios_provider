
//
//  LiveChatModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxAlamofire
import RxCocoa
import RxSwift

class LiveChatModel:NSObject {
    let disposebag = DisposeBag()
    
    /// live chat api
    ///
    /// - Parameter completionHandler: return the live chat url on completion
    func getTheLiveChatURL(completionHandler: @escaping(Bool,String) ->()){
        
        let rxApiCall = LiveChatAPI()
        rxApiCall.liveChatAPI(method:API.LiveChat.Licence_url)
        rxApiCall.LiveChat_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .dataNotFound:
                    completionHandler(false,"")
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                case .SuccessResponse:
                    completionHandler(true,self.prepareUrl(responseModel.data["chat_url"] as! String))
                    break
                default:
                    completionHandler(false,"")
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    func prepareUrl(_ url: String) -> String {
        var string: String = "https://\(url)"
        string = string.replacingOccurrences(of: "{%license%}", with: API.LiveChat.Licence)
        string = string.replacingOccurrences(of: "{%group%}", with: "liveM")
        return string
    }
}

