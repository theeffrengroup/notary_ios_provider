//
//  TicketsViewController.swift
//  Zendesk
//
//  Created by Vengababu Maparthi on 13/12/17.
//  Copyright © 2017 Vengababu Maparthi. All rights reserved.
//

import UIKit

class TicketsViewController: UIViewController {
    @IBOutlet weak var emptyImage: UIImageView!
    
    @IBOutlet weak var helpCenterTableview: UITableView!
    var zendeskModel = ZendeskModel()
    var zendeskData = ModelClass()
    var value = 0
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.helpCenterTableview.addPullRefresh { [weak self] in
            // some code
            sleep(1)
            self?.getTheTicketsData()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        getTheTicketsData()
    }
    
    
    /// get the tickets data, if any
    func getTheTicketsData()  {
        zendeskModel.getTheTicketData(userID: String(describing:Utility.RequestID)) { (success,ticketsData) in
            if success{
                self.zendeskData = ticketsData
                self.helpCenterTableview.reloadData()
                self.helpCenterTableview.stopPullRefreshEver()
            }else{
                self.helpCenterTableview.stopPullRefreshEver()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func bactToVc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addNewTicket(_ sender: Any) {
        self.performSegue(withIdentifier: "toNewTicket", sender: 0) // 0 =  to create the new ticket
    }
    
    //navigate to the ticket details view controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nav = segue.destination as! UINavigationController
        if let nextScene: ZendeskVC = nav.viewControllers.first as! ZendeskVC?{
            nextScene.ticketID = sender as! Int
        }
    }
}



///****** tableview datasource*************//
extension TicketsViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.zendeskData.open.count != 0 && self.zendeskData.close.count != 0{
            self.emptyImage.isHidden = true
            return 2
        }else if self.zendeskData.open.count != 0 || self.zendeskData.close.count != 0{
            self.emptyImage.isHidden = true
            return 1
            
        }else{
            self.emptyImage.isHidden = false
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if self.zendeskData.open.count != 0{
                return self.zendeskData.open.count + 1
            }else{
                return self.zendeskData.close.count + 1
            }
        case 1:
            return self.zendeskData.close.count + 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier:"header") as! HeaderTableViewCell?
                if self.zendeskData.open.count != 0{
                    cell?.titleOfStatus.text = "STATUS : OPEN"
                }else{
                    cell?.titleOfStatus.text = "STATUS : CLOSED"
                }
                return cell!
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier:"status") as! StatusTableCell?
                if self.zendeskData.open.count != 0{
                    
                    cell?.subject.text = self.zendeskData.open[indexPath.row-1].subject
                    cell?.time.text = Helper.getTheScheduleViewTimeFormat(timeStamp: self.zendeskData.open[indexPath.row-1].time)
                    cell?.date.text = Helper.getTheOnlyDateFromTimeStamp(timeStamp: self.zendeskData.open[indexPath.row-1].time)
                    cell?.firstLetterSubject.text = self.zendeskData.open[indexPath.row-1].firstLetter
                    
                }else{
                    cell?.subject.text = self.zendeskData.close[indexPath.row-1].subject
                    cell?.time.text = Helper.getTheScheduleViewTimeFormat(timeStamp: self.zendeskData.close[indexPath.row-1].time)
                    cell?.date.text = Helper.getTheOnlyDateFromTimeStamp(timeStamp: self.zendeskData.close[indexPath.row-1].time)
                    cell?.firstLetterSubject.text =  self.zendeskData.close[indexPath.row-1].firstLetter
                    
                }
                return cell!
            }
        }else{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier:"header") as! HeaderTableViewCell?
                cell?.titleOfStatus.text = "STATUS : CLOSED"
                return cell!
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier:"status") as! StatusTableCell?
                cell?.subject.text = self.zendeskData.close[indexPath.row-1].subject
                cell?.time.text = Helper.getTheScheduleViewTimeFormat(timeStamp: self.zendeskData.close[indexPath.row-1].time)
                cell?.date.text = Helper.getTheOnlyDateFromTimeStamp(timeStamp: self.zendeskData.close[indexPath.row-1].time)
                cell?.firstLetterSubject.text = self.zendeskData.close[indexPath.row-1].firstLetter
                return cell!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0{
            return 40
        }else{
            return 70;
        }
    }
}

///***********tableview delegate methods**************//
extension TicketsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            return
        }
        
        if indexPath.section == 0{
            if self.zendeskData.open.count != 0{
                self.performSegue(withIdentifier: "toNewTicket", sender: self.zendeskData.open[indexPath.row - 1].id) //to view existing ticket id details
            }else{
                self.performSegue(withIdentifier: "toNewTicket", sender: self.zendeskData.close[indexPath.row - 1].id)
            }
        }else{
            self.performSegue(withIdentifier: "toNewTicket", sender: self.zendeskData.close[indexPath.row - 1].id)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

