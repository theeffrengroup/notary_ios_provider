//
//  NewTicketModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 27/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

class ticketHistory: NSObject {
    var firstLetter = ""
    var timeStamp:Int64 = 0
    var name = ""
    var comment = ""
}

// ticket history model
class historyModel: NSObject {
    var status = ""
    var priority = ""
    var subject = ""
    var comments = [ticketHistory]()
    
    func getTheParsedTicketsData(dict:[String:Any]) -> historyModel {
        let histModel = historyModel()
        comments = [ticketHistory]()
        
        if let subject = dict["subject"] as? String{
            histModel.subject = subject
        }
        
        if let status = dict["type"] as? String{
            histModel.status = status
        }
        
        if let priority = dict["priority"] as? String{
            histModel.priority = priority
        }
        
        for ticket in dict["events"] as! [[String:Any]] {
            let ticketData = ticketHistory()
            if let name = ticket["name"] as? String{
                ticketData.firstLetter = String(name.first!)
            }
            
            if let time = ticket["timeStamp"] as? NSNumber{
                ticketData.timeStamp = Int64(truncating: time)
            }
            
            if let name = ticket["name"] as? String{
                ticketData.name = name
            }
            
            if let comment = ticket["body"] as? String{
                ticketData.comment = comment
            }
            histModel.comments.append(ticketData)
        }
        return histModel
    }
}
