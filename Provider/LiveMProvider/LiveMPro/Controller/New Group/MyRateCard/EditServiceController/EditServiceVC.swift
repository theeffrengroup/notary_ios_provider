//
//  EditServiceVC.swift
//  LSP
//
//  Created by Vengababu Maparthi on 02/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import RxKeyboard
import RxSwift
import TGPControls

class EditServiceVC: UIViewController {
    @IBOutlet weak var priceTF: UITextField!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var serviceName: UILabel!
    
    @IBOutlet weak var timeTF: UITextField!

    @IBOutlet weak var descriptionButton: UIButton!
    @IBOutlet weak var priceButton: UIButton!
    let disposeBag = DisposeBag()
    var serviceAPI = RatecardViewModel()
    var service:Services?
    var price = ""
    var descriptionTag = ""
    var catName = ""
    
    @IBOutlet weak var editPriceScrollView: UIScrollView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var wordCount: UILabel!
    @IBOutlet weak var bottomContraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionTV: UITextView!
    
    @IBOutlet weak var sliderView: TGPDiscreteSlider!
    
    @IBOutlet weak var maxPrice: UILabel!
    @IBOutlet weak var minPrice: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryName.text = catName
        timeTF.layer.borderWidth = 1
        priceTF.layer.borderWidth = 1
        priceTF.layer.borderColor  = COLOR.textFieldBorderColor.cgColor
        timeTF.layer.borderColor  = COLOR.textFieldBorderColor.cgColor
        price = String(format:"%.2f",service!.serviceFee)
        timeTF.text = String(describing:service!.serviceCompletionTime)
        priceTF.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",service!.serviceFee))
        descriptionTag = (service?.serviceDescription)!
        if descriptionTag.length > 2{
            descriptionTV.textColor = UIColor.black
              descriptionTV.text = (service?.serviceDescription)!
        }
      self.serviceName.text = service?.serviceName
        updateCharacterCount()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        sliderView.ticksListener = self
        sliderView.incrementValue = Int(service!.maxFees/5 - service!.minFees/5)
        sliderView.minimumValue = CGFloat(service!.minFees)
        
        minPrice.text  = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",Int(service!.minFees)))
        maxPrice.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",Int(service!.maxFees)))
        if service!.serviceFee == 0.0{
            sliderView.value = 10
        }else{
            sliderView.value = CGFloat(service!.serviceFee)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /// MARK: - This func updates the number of characters for every characters.
    func updateCharacterCount() {
        
        let charCount = self.descriptionTV.text!.length
        let maxLimit = 250
        let remainingChar = maxLimit - charCount
        if service?.serviceDescription.length == 0{
            descriptionTV.textColor = UIColor.lightGray
            self.wordCount.text = "250" + editProfileData.characters
        }else{
            self.wordCount.text = String(remainingChar) + editProfileData.characters
            descriptionTV.textColor = UIColor.black
            
        }
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            editPriceScrollView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                
             self.editPriceScrollView.contentInset.bottom = keyboardVisibleHeight + 20
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func descriptionAction(_ sender: Any) {
        self.view.endEditing(true)
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.width ;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    @IBAction func priceAction(_ sender: Any) {
        self.view.endEditing(true)
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.origin.x;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    @IBAction func saveTheEnteredData(_ sender: Any) {
        self.view.endEditing(true)
        if Double(price)! > service!.maxFees ||  Double(price)! < service!.minFees{
            Helper.alertVC(errMSG: "The price should be between " + String(format:"%.2f",service!.minFees) + " and " + String(format:"%.2f",service!.maxFees))
            return
        }
        
        let dict:[String:Any] = ["service_category_Id":service!.serviceID as Any,
                                 "categoryType":2,
                                 "price":Double(self.price)!,
                                 "description":descriptionTag,
                                 "serviceCompletionTime": Double(timeTF.text!)! as Any]
        self.serviceAPI.updateServicePrice(serviceData: dict) { (succeeded) in
            if succeeded{
                self.service?.serviceFee = Double(self.price)!
                self.service?.serviceCompletionTime = Int(self.timeTF.text!)!
                self.service?.serviceDescription = self.descriptionTag
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
    
    func buttonEnable() {
        
        switch indicatorView.frame.origin.x {
        case 0:
            priceButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            descriptionButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        case self.view.frame.size.width / 2:
            descriptionButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            priceButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        default:
            priceButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            descriptionButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        }
    }
}

extension EditServiceVC:TGPControlsTicksProtocol{
    func tgpValueChanged(value: UInt) {
        priceTF.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",Double(value)))
        price = String(format:"%.2f",Double(value))
    }
    
    func tgpTicksDistanceChanged(ticksDistance:CGFloat, sender:AnyObject){
        
    }
}

extension EditServiceVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == mainScrollView {
            
            indicatorView.frame = CGRect(x: scrollView.contentOffset.x / 2, y: indicatorView.frame.origin.y, width: self.view.frame.size.width/2, height: 2)
            buttonEnable()
        }
    }
}


extension EditServiceVC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == priceTF{
            textField.text = ""
        }else{
            textField.text = ""
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let regex = try! NSRegularExpression(pattern: "\\..{3,}", options: [])
        
        let matches = regex.matches(in: newText, options:[], range:NSMakeRange(0, newText.length))
        guard matches.count == 0 else { return false }
        
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            return true
        case ".":
            let array = textField.text?.map { String($0) }
            var decimalCount = 0
            for character in array! {
                if character == "." {
                    decimalCount = decimalCount + 1
                }
            }
            if decimalCount > 0 {
                return false
            } else {
                return true
            }
        default:
            let array = string.map { String($0) }
            if array.count == 0 {
                return true
            }
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        if textField == priceTF{
            if !(textField.text?.isEmpty)!{
                price = textField.text!
                textField.text = Helper.getTheAmtTextWithSymbol(amt:textField.text!)
                sliderView.value = CGFloat(Double(price)!)
            }else{
                priceTF.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",service!.serviceFee))
                price =  String(format:"%.2f",service!.serviceFee)
            }
        }else{
            if !(textField.text?.isEmpty)!{
                textField.text = textField.text!
            }else{
                timeTF.text = String(describing:service!.serviceCompletionTime)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}


extension EditServiceVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        bottomContraint.constant = 280
        textView.textColor = UIColor.black
        if service?.serviceDescription.length == 0{
            descriptionTV.text = ""
        }else{
            
        }
        
        let charCount = self.descriptionTV.text!.length
        let maxLimit = 250
        let remainingChar = maxLimit - charCount
        self.wordCount.text = String(remainingChar) + editProfileData.characters
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        bottomContraint.constant = 10
        if textView.text.isEmpty {
            textView.text = self.service?.serviceDescription
            textView.textColor = UIColor.lightGray
            descriptionTag = (self.service?.serviceDescription)!
        }else{
            descriptionTag = textView.text
        }
    }
    
    ///MARK: - This func Count the Total characters for AboutMeController Text.
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        guard let text1 = textView.text else { return true }
        let length = text1.length + text.length - range.length
        self.updateCharacterCount()
        let count = 300 - length
        wordCount.text =  String(count) + editProfileData.characters
        return length <= 300 - 1
    }
}



