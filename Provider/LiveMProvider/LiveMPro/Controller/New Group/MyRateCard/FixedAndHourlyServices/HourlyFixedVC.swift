//
//  HourlyFixedVC.swift
//  LSP
//
//  Created by Vengababu Maparthi on 02/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import TGPControls

class HourlyFixedVC: UIViewController {
    @IBOutlet weak var fixedButton: UIButton!
    @IBOutlet weak var hourlyButton: UIButton!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var priceEditBy: UILabel!
    
    @IBOutlet weak var setHourlyPriceLabel: UILabel!
    @IBOutlet weak var vaeButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var hourlyPriceTF: UITextField!
    @IBOutlet weak var fixedTableViewCell: UITableView!
    
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var sliderAdjuster: TGPDiscreteSlider!
    @IBOutlet weak var price1: UILabel!
    @IBOutlet weak var price2: UILabel!
    @IBOutlet weak var price3: UILabel!
    @IBOutlet weak var price4: UILabel!
    @IBOutlet weak var price5: UILabel!
    @IBOutlet weak var price6: UILabel!
    
    
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var selectPriceRangeLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    
    var fromCat = false
    var singleCategory:ServiceCategory?
    var canEditHourFixed = 0  // 2 = fixed hourly cant & edit 6 = fixed hourly can edit
    var price = ""
    var serviceAPI = RatecardViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryName.text = singleCategory?.categoryName
        sliderAdjuster.ticksListener = self
        sliderAdjuster.incrementValue = Int(singleCategory!.hourMax/5 - singleCategory!.hourMin/5)
        sliderAdjuster.minimumValue = CGFloat(singleCategory!.hourMin)
        price1.text = Helper.getTheAmtTextWithSymbol(amt:String(describing:Int(singleCategory!.hourMin)))
        price6.text = Helper.getTheAmtTextWithSymbol(amt:String(describing:Int(singleCategory!.hourMax)))
        
        if singleCategory!.hourPrice == 0.0{
            sliderAdjuster.value = 10
        }else{
            sliderAdjuster.value = CGFloat(singleCategory!.hourPrice)
        }
        hourlyPriceTF.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",singleCategory!.hourPrice))
        price = String(format:"%.2f",singleCategory!.hourPrice)
        hourlyPriceTF.layer.borderWidth = 1
        hourlyPriceTF.layer.borderColor  = COLOR.textFieldBorderColor.cgColor
        
        fixedTableViewCell.estimatedRowHeight = 10
        fixedTableViewCell.rowHeight = UITableView.automaticDimension
        
        if canEditHourFixed == 6{
            vaeButton.isHidden = false
            priceEditBy.text = "Price set by provider"
            sliderAdjuster.isUserInteractionEnabled = true
            sliderAdjuster.isHidden = false
            hourlyPriceTF.isUserInteractionEnabled = true
        }else{
            setHourlyPriceLabel.text = "HOURLY PRICE"
            vaeButton.isHidden = true
            priceEditBy.text = "Price set by admin"
            sliderAdjuster.isHidden = true
            selectPriceRangeLabel.isHidden = true
            maxLabel.isHidden = true
            minLabel.isHidden = true
            price1.isHidden = true
            price6.isHidden = true
            sliderAdjuster.isUserInteractionEnabled = false
            hourlyPriceTF.isUserInteractionEnabled = false
        }
        selectAllButton.isSelected = true
        for serviceId in (singleCategory?.categoryServices)! {
            if serviceId.status == 0{
                selectAllButton.isSelected = false
            }
        }
        saveButton.isHidden = true
        selectAllButton.isHidden = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        fixedTableViewCell.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backToVC(_ sender: Any) {
        if fromCat{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    @IBAction func selectAllFixedService(_ sender: Any) {
        var serviceIds = ""
        for serviceId in (singleCategory?.categoryServices)! {
            if serviceIds.length > 0{
                serviceIds = serviceIds + "," + serviceId.serviceID
            }else{
                serviceIds = serviceId.serviceID
            }
        }
        
        
        for subCat in (singleCategory?.subCategoryData)! {
            for services in subCat.services{
                if serviceIds.length > 0{
                    serviceIds = serviceIds + "," + services.serviceID
                }else{
                    serviceIds = services.serviceID
                }
            }
        }
        
        
        if selectAllButton.isSelected ==  true
        {
            let dict:[String:Any] = ["service":serviceIds,
                                     "action":0]
            self.serviceAPI.updateTheSelectedUnselectedServices(serviceData: dict) { (succeeded) in
                if succeeded{
                    for serviceId in (self.singleCategory?.categoryServices)! {
                        serviceId.status = 0
                    }
                    
                    for subCat in (self.singleCategory?.subCategoryData)! {
                        for services in subCat.services{
                            services.status = 0
                        }
                    }
                    
                    self.selectAllButton.isSelected =  false
                    self.fixedTableViewCell.reloadData()
                }
            }
        }else{
            let dict:[String:Any] = ["service":serviceIds,
                                     "action":1]
            self.serviceAPI.updateTheSelectedUnselectedServices(serviceData: dict) { (succeeded) in
                if succeeded{
                    for serviceId in (self.singleCategory?.categoryServices)! {
                        serviceId.status = 1
                    }
                    
                    for subCat in (self.singleCategory?.subCategoryData)! {
                        for services in subCat.services{
                            services.status = 1
                        }
                    }
                    
                    self.selectAllButton.isSelected =  true
                    self.fixedTableViewCell.reloadData()
                }
            }
        }
    }
    
    @IBAction func saveTheHourlyPrice(_ sender: Any) {
        
        self.view.endEditing(true)
        if Double(price)! > singleCategory!.hourMax ||  Double(price)! < singleCategory!.hourMin{
            Helper.alertVC(errMSG: "The price should be between " + String(describing:Int(singleCategory!.hourMin)) + " and " + String(describing:Int(singleCategory!.hourMax)))
            return
        }
        let dict:[String:Any] = ["service_category_Id":singleCategory!.cat_ID,
                                 "categoryType":1,
                                 "price":Double(self.price)!,
                                 "description":"",
                                 "serviceCompletionTime":""]
        self.serviceAPI.updateServicePrice(serviceData: dict) { (succeeded) in
            if succeeded{
                self.singleCategory!.hourPrice = Double(self.price)!
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    @IBAction func fixedAction(_ sender: Any) {
        
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.origin.x;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    @IBAction func hourlyAction(_ sender: Any) {
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.width ;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    func buttonEnable() {
        
        switch indicatorView.frame.origin.x {
        case 0:
            
            saveButton.isHidden = true
            selectAllButton.isHidden = false
            fixedButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            hourlyButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        case self.view.frame.size.width / 2:
            if canEditHourFixed == 6{
                saveButton.isHidden = false
            }
            selectAllButton.isHidden = true
            hourlyButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            fixedButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        default:
            
            saveButton.isHidden = true
            selectAllButton.isHidden = false
            fixedButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            hourlyButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
        }
    }
    
    // delete target from cellForRowAtIndex
    @objc func selectRDeselectService(_ sender : UIButton){
        let section = sender.tag / 100
        let row = sender.tag % 100
        
        let indexPath = IndexPath(row: row, section: section)
        
        if let cel =  fixedTableViewCell.cellForRow(at: indexPath) as? FixedServiceTableCell {
            var dict = [String:Any]()
            if cel.CheckBoxButton.isSelected{
                if section == 0{
                    if self.singleCategory?.subCategoryData.count != 0 && self.singleCategory?.categoryServices.count == 0 {
                        dict = ["service":singleCategory?.subCategoryData[indexPath.section].services[indexPath.row-1].serviceID as Any,
                                "action":0]
                    }else{
                        dict = ["service":singleCategory?.categoryServices[indexPath.row].serviceID as Any,
                                "action":0]
                    }
                }else{
                    if self.singleCategory?.categoryServices.count != 0 {
                        dict = ["service":singleCategory?.subCategoryData[indexPath.section-1].services[indexPath.row-1].serviceID as Any,
                                "action":0]
                    }else{
                        dict = ["service":singleCategory?.subCategoryData[indexPath.section].services[indexPath.row-1].serviceID as Any,
                                "action":0]
                    }
                }
                
            }else{
                
                if section == 0{
                    if self.singleCategory?.subCategoryData.count != 0 && self.singleCategory?.categoryServices.count == 0 {
                        dict = ["service":singleCategory?.subCategoryData[indexPath.section].services[indexPath.row-1].serviceID as Any,
                                "action":1]
                    }else{
                        dict = ["service":singleCategory?.categoryServices[indexPath.row].serviceID as Any,
                                "action":1]
                    }
                }else{
                    if self.singleCategory?.categoryServices.count != 0 {
                        dict = ["service":singleCategory?.subCategoryData[indexPath.section-1].services[indexPath.row-1].serviceID as Any,
                                "action":1]
                    }else{
                        dict = ["service":singleCategory?.subCategoryData[indexPath.section].services[indexPath.row-1].serviceID as Any,
                                "action":1]
                    }
                }
            }
            
            serviceAPI.updateTheSelectedUnselectedServices(serviceData: dict) { (succeeded) in
                if succeeded{
                    if cel.CheckBoxButton.isSelected{
                        cel.CheckBoxButton.isSelected = false
                        if section == 0{
                            if self.singleCategory?.subCategoryData.count != 0 && self.singleCategory?.categoryServices.count == 0 {
                                self.singleCategory?.subCategoryData[indexPath.section].services[indexPath.row-1].status = 0
                            }else{
                                self.singleCategory?.categoryServices[indexPath.row].status = 0
                            }
                        }else{
                            if self.singleCategory?.categoryServices.count != 0 {
                                self.singleCategory?.subCategoryData[indexPath.section - 1].services[indexPath.row-1].status = 0
                            }else{
                                self.singleCategory?.subCategoryData[indexPath.section].services[indexPath.row-1].status = 0
                            }
                        }
                        
                    }else{
                        cel.CheckBoxButton.isSelected = true
                        
                        if section == 0{
                            if self.singleCategory?.subCategoryData.count != 0 && self.singleCategory?.categoryServices.count == 0 {
                                self.singleCategory?.subCategoryData[indexPath.section].services[indexPath.row-1].status = 1
                            }else{
                                self.singleCategory?.categoryServices[indexPath.row].status = 1
                            }
                        }else{
                            if self.singleCategory?.categoryServices.count != 0 {
                                self.singleCategory?.subCategoryData[indexPath.section - 1].services[indexPath.row-1].status = 1
                            }else{
                                self.singleCategory?.subCategoryData[indexPath.section].services[indexPath.row-1].status = 1
                            }
                        }
                    }
                    
                    self.selectAllButton.isSelected = true
                    
                    for serviceId in (self.singleCategory?.categoryServices)! {
                        if serviceId.status == 0{
                            self.selectAllButton.isSelected = false
                        }
                    }
                    
                    for subcat in (self.singleCategory?.subCategoryData)! {
                        for services in subcat.services{
                            if services.status == 0{
                                self.selectAllButton.isSelected = false
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let section = sender as! Int / 100
        let row = sender as! Int % 100
        
        if let nextScene = segue.destination as? EditServiceVC {
            
            if section == 0{
                if self.singleCategory?.subCategoryData.count != 0 && self.singleCategory?.categoryServices.count == 0 {
                    nextScene.service = self.singleCategory?.subCategoryData[section].services[row - 1]
                    nextScene.catName =  (self.singleCategory?.categoryName)!
                }else{
                    nextScene.service = self.singleCategory?.categoryServices[row]
                    nextScene.catName =  (self.singleCategory?.categoryName)!
                }
            }else{
                if self.singleCategory?.categoryServices.count != 0 {
                    nextScene.service = self.singleCategory?.subCategoryData[section - 1].services[row - 1]
                    nextScene.catName =  (self.singleCategory?.categoryName)!
                }else{
                    nextScene.service = self.singleCategory?.subCategoryData[section].services[row - 1]
                    nextScene.catName =  (self.singleCategory?.categoryName)!
                }
            }
        }
    }
    
    // delete target from cellForRowAtIndex
    @objc func editThisService(_ sender : UIButton){
        self.performSegue(withIdentifier: "editServices", sender: sender.tag)
    }
    
    
    @objc func viewTheDescription(_ sender : UIButton){
        
        let section = sender.tag / 100
        let row = sender.tag % 100
        
        if section == 0{
            if self.singleCategory?.subCategoryData.count != 0 && self.singleCategory?.categoryServices.count == 0 {
                let descriptionView = ViewMoreDescription.instance
                descriptionView.show(serviceData:self.singleCategory?.subCategoryData[section].services[row - 1])
            }else{
                let descriptionView = ViewMoreDescription.instance
                descriptionView.show(serviceData:singleCategory?.categoryServices[row])
            }
        }else{
            if self.singleCategory?.categoryServices.count != 0 {
                let descriptionView = ViewMoreDescription.instance
                descriptionView.show(serviceData:self.singleCategory?.subCategoryData[section-1].services[row - 1])
            }else{
                let descriptionView = ViewMoreDescription.instance
                descriptionView.show(serviceData:self.singleCategory?.subCategoryData[section].services[row - 1])
            }
        }
    }
}



extension HourlyFixedVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension HourlyFixedVC:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if singleCategory?.subCategoryData.count != 0 && singleCategory?.categoryServices.count != 0 {
            return singleCategory!.subCategoryData.count + 1
        }else  if singleCategory?.subCategoryData.count != 0 && singleCategory?.categoryServices.count == 0 {
            return singleCategory!.subCategoryData.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if singleCategory?.subCategoryData.count != 0 && singleCategory?.categoryServices.count != 0 {
            switch section {
            case 0:
                return (singleCategory?.categoryServices.count)!
            default:
                return singleCategory!.subCategoryData[section-1].services.count + 1
            }
            
        }else  if singleCategory?.subCategoryData.count != 0 && singleCategory?.categoryServices.count == 0 {
            return singleCategory!.subCategoryData[section].services.count + 1
        }else{
            return (singleCategory?.categoryServices.count)!
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "fixedService") as! FixedServiceTableCell
        if indexPath.section == 0{
            if singleCategory?.subCategoryData.count != 0 && singleCategory?.categoryServices.count == 0 {
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "servicesHeader") as! ServiceHeaderTableCell
                    cell.subCatLabel.text =  singleCategory?.subCategoryData[indexPath.section].subCatName
                    return cell
                }else{
                    if selectAllButton.isSelected ==  true{
                        cell.CheckBoxButton.isSelected = true
                    }else{
                        if singleCategory?.subCategoryData[indexPath.section].services[indexPath.row - 1].status == 0{
                            cell.CheckBoxButton.isSelected = false
                        }else{
                            cell.CheckBoxButton.isSelected = true
                        }
                    }
                    
                    cell.CheckBoxButton.tag = (indexPath.section*100)+indexPath.row
                    cell.CheckBoxButton.addTarget(self, action: #selector(selectRDeselectService(_:)), for:.touchUpInside)
                    
                    if canEditHourFixed == 6{
                        cell.editServicePrice.isHidden = false
                        cell.trailing25_47.constant = 47
                        cell.editServicePrice.tag = (indexPath.section*100)+indexPath.row
                        cell.editServicePrice.addTarget(self, action: #selector(editThisService(_:)), for:.touchUpInside)
                    }else{
                        cell.trailing25_47.constant = 25
                        cell.editServicePrice.isHidden = true
                    }
                    
                    cell.serviceDescription.text = singleCategory?.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceDescription
                    if Helper.countLabelLines(label:cell.serviceDescription) > 2{
                        cell.viewMoreButton.isHidden = false
                        cell.viewMoreButton.tag = (indexPath.section*100)+indexPath.row
                        cell.viewMoreButton.addTarget(self, action: #selector(viewTheDescription(_:)), for:.touchUpInside)
                    }else{
                        cell.viewMoreButton.isHidden = true
                    }
                    
                    cell.serviceName.text = singleCategory?.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceName
                    cell.servicePrice.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",singleCategory!.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceFee))
                    cell.serviceName.text = singleCategory?.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceName
                    
                     cell.serviceCompletionTime.text = String(describing:singleCategory!.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceCompletionTime) + "Mins"
                    cell.selectionStyle = .none
                }
            }else{
                if selectAllButton.isSelected ==  true{
                    cell.CheckBoxButton.isSelected = true
                }else{
                    if singleCategory?.categoryServices[indexPath.row].status == 0{
                        cell.CheckBoxButton.isSelected = false
                    }else{
                        cell.CheckBoxButton.isSelected = true
                    }
                }
                
                cell.CheckBoxButton.tag = (indexPath.section*100)+indexPath.row
                cell.CheckBoxButton.addTarget(self, action: #selector(selectRDeselectService(_:)), for:.touchUpInside)
                
                if canEditHourFixed == 6{
                    cell.editServicePrice.isHidden = false
                    cell.trailing25_47.constant = 47
                    cell.editServicePrice.tag = (indexPath.section*100)+indexPath.row
                    cell.editServicePrice.addTarget(self, action: #selector(editThisService(_:)), for:.touchUpInside)
                }else{
                    cell.trailing25_47.constant = 25
                    cell.editServicePrice.isHidden = true
                }
                
                cell.serviceDescription.text = singleCategory?.categoryServices[indexPath.row].serviceDescription
                if Helper.countLabelLines(label:cell.serviceDescription) > 2{
                    cell.viewMoreButton.isHidden = false
                    cell.viewMoreButton.tag = (indexPath.section*100) + indexPath.row
                    cell.viewMoreButton.addTarget(self, action: #selector(viewTheDescription(_:)), for:.touchUpInside)
                }else{
                    cell.viewMoreButton.isHidden = true
                }
                
                cell.serviceName.text = singleCategory?.categoryServices[indexPath.row].serviceName
                cell.servicePrice.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",singleCategory!.categoryServices[indexPath.row].serviceFee))
                
                cell.serviceName.text = singleCategory?.categoryServices[indexPath.row].serviceName
                
                cell.serviceCompletionTime.text = String(describing:singleCategory!.categoryServices[indexPath.row].serviceCompletionTime) + " Mins"
                cell.selectionStyle = .none
                
            }
        }else{
            
            if singleCategory?.categoryServices.count != 0 {
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "servicesHeader") as! ServiceHeaderTableCell
                    cell.subCatLabel.text = singleCategory?.subCategoryData[indexPath.section - 1].subCatName
                    return cell
                }else{
                    if selectAllButton.isSelected ==  true{
                        cell.CheckBoxButton.isSelected = true
                    }else{
                        if singleCategory?.subCategoryData[indexPath.section - 1].services[indexPath.row - 1].status == 0{
                            cell.CheckBoxButton.isSelected = false
                        }else{
                            cell.CheckBoxButton.isSelected = true
                        }
                    }
                    
                    cell.CheckBoxButton.tag = (indexPath.section*100)+indexPath.row
                    cell.CheckBoxButton.addTarget(self, action: #selector(selectRDeselectService(_:)), for:.touchUpInside)
                    
                    if canEditHourFixed == 6{
                        cell.editServicePrice.isHidden = false
                        cell.trailing25_47.constant = 47
                        cell.editServicePrice.tag = (indexPath.section*100)+indexPath.row
                        cell.editServicePrice.addTarget(self, action: #selector(editThisService(_:)), for:.touchUpInside)
                    }else{
                        cell.trailing25_47.constant = 25
                        cell.editServicePrice.isHidden = true
                    }
                    
                    cell.serviceDescription.text = singleCategory?.subCategoryData[indexPath.section - 1].services[indexPath.row - 1].serviceDescription
                    if Helper.countLabelLines(label:cell.serviceDescription) > 2{
                        cell.viewMoreButton.isHidden = false
                        cell.viewMoreButton.tag = (indexPath.section*100)+indexPath.row
                        cell.viewMoreButton.addTarget(self, action: #selector(viewTheDescription(_:)), for:.touchUpInside)
                    }else{
                        cell.viewMoreButton.isHidden = true
                    }
                    
                    cell.serviceName.text = singleCategory?.subCategoryData[indexPath.section - 1].services[indexPath.row - 1].serviceName
                    cell.servicePrice.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",singleCategory!.subCategoryData[indexPath.section - 1].services[indexPath.row - 1].serviceFee))
                    cell.serviceName.text = singleCategory?.subCategoryData[indexPath.section - 1].services[indexPath.row - 1].serviceName
                    
                    cell.serviceCompletionTime.text = String(describing:singleCategory!.subCategoryData[indexPath.section - 1].services[indexPath.row - 1].serviceCompletionTime) + "Mins"
                    cell.selectionStyle = .none
                }
            }else{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "servicesHeader") as! ServiceHeaderTableCell
                    cell.subCatLabel.text = singleCategory?.subCategoryData[indexPath.section].subCatName
                    return cell
                }else{
                    if selectAllButton.isSelected ==  true{
                        cell.CheckBoxButton.isSelected = true
                    }else{
                        if singleCategory?.subCategoryData[indexPath.section].services[indexPath.row - 1].status == 0{
                            cell.CheckBoxButton.isSelected = false
                        }else{
                            cell.CheckBoxButton.isSelected = true
                        }
                    }
                    
                    cell.CheckBoxButton.tag = (indexPath.section*100)+indexPath.row
                    cell.CheckBoxButton.addTarget(self, action: #selector(selectRDeselectService(_:)), for:.touchUpInside)
                    
                    if canEditHourFixed == 6{
                        cell.editServicePrice.isHidden = false
                        cell.trailing25_47.constant = 47
                        cell.editServicePrice.tag = (indexPath.section*100)+indexPath.row
                        cell.editServicePrice.addTarget(self, action: #selector(editThisService(_:)), for:.touchUpInside)
                    }else{
                        cell.trailing25_47.constant = 25
                        cell.editServicePrice.isHidden = true
                    }
                    
                    cell.serviceDescription.text = singleCategory?.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceDescription
                    if Helper.countLabelLines(label:cell.serviceDescription) > 2{
                        cell.viewMoreButton.isHidden = false
                        cell.viewMoreButton.tag = (indexPath.section*100)+indexPath.row
                        cell.viewMoreButton.addTarget(self, action: #selector(viewTheDescription(_:)), for:.touchUpInside)
                    }else{
                        cell.viewMoreButton.isHidden = true
                    }
                    
                    cell.serviceName.text = singleCategory?.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceName
                    cell.servicePrice.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",singleCategory!.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceFee))
                    cell.serviceName.text = singleCategory?.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceName
                    
                    cell.serviceCompletionTime.text = String(describing:singleCategory!.subCategoryData[indexPath.section].services[indexPath.row - 1].serviceCompletionTime) + "Mins"
                    cell.selectionStyle = .none
                }
            }
        }
        return cell
    }
}

extension HourlyFixedVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == mainScrollView {
            indicatorView.frame = CGRect(x: scrollView.contentOffset.x / 2, y: indicatorView.frame.origin.y, width: self.view.frame.size.width/2, height: 2)
            buttonEnable()
        }
    }
}

extension HourlyFixedVC:TGPControlsTicksProtocol{
    func tgpValueChanged(value: UInt) {
        price = String(format:"%.2f",Double(value))
        hourlyPriceTF.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",Double(value)))
    }
    
    func tgpTicksDistanceChanged(ticksDistance:CGFloat, sender:AnyObject){
        
    }
}


extension HourlyFixedVC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == hourlyPriceTF{
            textField.text = ""
        }else{
            textField.text = ""
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let regex = try! NSRegularExpression(pattern: "\\..{3,}", options: [])
        
        let matches = regex.matches(in: newText, options:[], range:NSMakeRange(0, newText.length))
        guard matches.count == 0 else { return false }
        
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            return true
        case ".":
            let array = textField.text?.map { String($0) }
            var decimalCount = 0
            for character in array! {
                if character == "." {
                    decimalCount = decimalCount + 1
                }
            }
            if decimalCount > 0 {
                return false
            } else {
                return true
            }
        default:
            let array = string.map { String($0) }
            if array.count == 0 {
                return true
            }
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        if textField == hourlyPriceTF{
            if !(textField.text?.isEmpty)!{
                
                price = textField.text!
                textField.text = Helper.getTheAmtTextWithSymbol(amt:textField.text!)
                sliderAdjuster.value = CGFloat(Double(price)!)
            }else{
                hourlyPriceTF.text  = Helper.getTheAmtTextWithSymbol(amt:textField.text!)
                price = String(format:"%.2f",singleCategory!.hourPrice)
            }
        }else{
            if !(textField.text?.isEmpty)!{
                textField.text = Helper.getTheAmtTextWithSymbol(amt:textField.text!)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}




