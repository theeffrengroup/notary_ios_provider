//
//  FixedServicesVC.swift
//  LSP
//
//  Created by Vengababu Maparthi on 02/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class FixedServicesVC: UIViewController {
    
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var priceEditBy: UILabel!
    @IBOutlet weak var selectAllServicesButton: UIButton!
    @IBOutlet weak var fixedServiceTableView: UITableView!
    var services = [Services]()
    var subcatData = [Subcat]()
    var canEditServices = 0  // 1 = fixed cant edit, 5 = fixed can edit
    var serviceAPI = RatecardViewModel()
    var fromCat = false
    var catName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryName.text = catName
        fixedServiceTableView.estimatedRowHeight = 10
        fixedServiceTableView.rowHeight = UITableView.automaticDimension
        selectAllServicesButton.isSelected = true
        for serviceId in services {
            if serviceId.status == 0{
                selectAllServicesButton.isSelected = false
            }
        }
        
        if canEditServices == 5{
            
            priceEditBy.text = "Price set by provider"
            
        }else{
            priceEditBy.text = "Price set by admin"
        }
        
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        fixedServiceTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectAllService(_ sender: Any) {
        var serviceIds = ""
        
        
        for serviceId in services {
            if serviceIds.length > 0{
                serviceIds = serviceIds + "," + serviceId.serviceID
            }else{
                serviceIds = serviceId.serviceID
            }
        }
        
        for subCat in self.subcatData{
            for services in subCat.services{
                if serviceIds.length > 0{
                    serviceIds = serviceIds + "," + services.serviceID
                }else{
                    serviceIds = services.serviceID
                }
            }
        }
        
        
        if selectAllServicesButton.isSelected ==  true
        {
            let dict:[String:Any] = ["service":serviceIds,
                                     "action":0]
            self.serviceAPI.updateTheSelectedUnselectedServices(serviceData: dict) { (succeeded) in
                if succeeded{
                    
                    for subCat in self.subcatData{
                        for services in subCat.services{
                            services.status = 0
                        }
                    }
                    
                    for serviceId in self.services {
                        serviceId.status = 0
                    }
                    
                    self.selectAllServicesButton.isSelected =  false
                    self.fixedServiceTableView.reloadData()
                }
            }
            
        }else{
            let dict:[String:Any] = ["service":serviceIds,
                                     "action":1]
            self.serviceAPI.updateTheSelectedUnselectedServices(serviceData: dict) { (succeeded) in
                if succeeded{
                    
                    for subCat in self.subcatData{
                        for services in subCat.services{
                            services.status = 1
                        }
                    }
                    
                    for serviceId in self.services {
                        serviceId.status = 1
                    }
                    
                    self.selectAllServicesButton.isSelected =  true
                    self.fixedServiceTableView.reloadData()
                }
            }
        }
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        if fromCat{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // delete target from cellForRowAtIndex
    @objc func selectRDeselectService(_ sender : UIButton){
        
        let section = sender.tag / 100
        let row = sender.tag % 100
        
        let indexPath = IndexPath(row: row, section: section)
        
        if let cel =  fixedServiceTableView.cellForRow(at: indexPath) as? FixedServiceTableCell {
            var dict = [String:Any]()
            
            if cel.CheckBoxButton.isSelected{
                if section == 0{
                    if subcatData.count != 0 && services.count == 0 {
                        dict = ["service":subcatData[section].services[indexPath.row-1].serviceID,
                                "action":0]
                    }else{
                        dict = ["service":services[indexPath.row].serviceID,
                                "action":0]
                    }
                }else{
                    if services.count != 0 {
                        dict = ["service":subcatData[section-1].services[indexPath.row-1].serviceID,
                                "action":0]
                    }else{
                        dict = ["service":subcatData[section].services[indexPath.row-1].serviceID,
                                "action":0]
                    }
                }
            }else{
                if section == 0{
                    if subcatData.count != 0 && services.count == 0 {
                        dict = ["service":subcatData[section].services[indexPath.row-1].serviceID,
                                "action":1]
                    }else{
                        dict = ["service":services[indexPath.row].serviceID,
                                "action":1]
                    }
                }else{
                    if services.count != 0 {
                        dict = ["service":subcatData[section-1].services[indexPath.row-1].serviceID,
                                "action":1]
                    }else{
                        dict = ["service":subcatData[section].services[indexPath.row-1].serviceID,
                                "action":1]
                    }
                }
            }
            
            
            self.serviceAPI.updateTheSelectedUnselectedServices(serviceData: dict) { (succeeded) in
                if succeeded{
                    
                    if cel.CheckBoxButton.isSelected{
                        cel.CheckBoxButton.isSelected = false
                        if section == 0{
                            if self.subcatData.count != 0 && self.services.count == 0 {
                                self.subcatData[section].services[indexPath.row-1].status = 0
                            }else{
                                self.services[indexPath.row].status = 0
                            }
                        }else{
                            if self.services.count != 0 {
                                self.subcatData[section-1].services[indexPath.row-1].status = 0
                            }else{
                                self.subcatData[section].services[indexPath.row-1].status = 0
                            }
                        }
                    }else{
                        cel.CheckBoxButton.isSelected = true
                        if section == 0{
                            if self.subcatData.count != 0 && self.services.count == 0 {
                                self.subcatData[section].services[indexPath.row-1].status = 1
                            }else{
                                self.services[indexPath.row].status = 1
                            }
                        }else{
                            if self.services.count != 0 {
                                self.subcatData[section-1].services[indexPath.row-1].status = 1
                            }else{
                                self.subcatData[section].services[indexPath.row-1].status = 1
                            }
                        }
                    }
                    
                    self.selectAllServicesButton.isSelected = true
                    
                    for subCat in self.subcatData{
                        for services in subCat.services{
                            if services.status == 0{
                                self.selectAllServicesButton.isSelected = false
                            }
                        }
                    }
                    
                    for serviceId in self.services {
                        if serviceId.status == 0{
                            self.selectAllServicesButton.isSelected = false
                        }
                    }
                    
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let section = sender as! Int / 100
        let row = sender as! Int % 100
        
        if let nextScene = segue.destination as? EditServiceVC {
            
            if section == 0{
                if self.subcatData.count != 0 && self.services.count == 0 {
                    nextScene.service = self.subcatData[section].services[row - 1]
                    nextScene.catName = categoryName.text!
                }else{
                    nextScene.service = self.services[row]
                    nextScene.catName = categoryName.text!
                }
            }else{
                if self.services.count != 0 {
                    nextScene.service = self.subcatData[section - 1].services[row - 1]
                    nextScene.catName = categoryName.text!
                }else{
                    nextScene.service = self.subcatData[section].services[row - 1]
                    nextScene.catName = categoryName.text!
                }
            }
        }
    }
    
    @objc func editThisService(_ sender : UIButton){
        
        self.performSegue(withIdentifier: "toEditPrice", sender: sender.tag)
    }
    
    
    
    @objc func viewTheDescription(_ sender : UIButton){
        
        let section = sender.tag / 100
        let row = sender.tag % 100
        
        if section == 0{
            if subcatData.count != 0 && services.count == 0 {
                let descriptionView = ViewMoreDescription.instance
                descriptionView.show(serviceData:subcatData[section].services[row - 1])
            }else {
                let descriptionView = ViewMoreDescription.instance
                descriptionView.show(serviceData:services[row])
            }
        }else{
            if services.count != 0 {
                let descriptionView = ViewMoreDescription.instance
                descriptionView.show(serviceData:subcatData[section - 1].services[row - 1])
            }else{
                let descriptionView = ViewMoreDescription.instance
                descriptionView.show(serviceData:subcatData[section].services[row - 1])
            }
            
        }
    }
}


extension FixedServicesVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension FixedServicesVC:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if subcatData.count != 0 && services.count != 0 {
            return subcatData.count + 1
        }else  if subcatData.count != 0 && services.count == 0 {
            return subcatData.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if subcatData.count != 0 && services.count != 0 {
            switch section {
            case 0:
                return services.count
            default:
                return subcatData[section-1].services.count + 1
            }
            
        }else  if subcatData.count != 0 && services.count == 0 {
            return subcatData[section].services.count + 1
        }else{
            return services.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "fixedServices") as! FixedServiceTableCell
        if indexPath.section == 0{
            if subcatData.count != 0 && services.count == 0 {
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "serviceHeader") as! ServiceHeaderTableCell
                    cell.subCatLabel.text = subcatData[indexPath.section].subCatName
                    return cell
                }else{
                    if selectAllServicesButton.isSelected ==  true{
                        cell.CheckBoxButton.isSelected = true
                    }else{
                        if subcatData[indexPath.section].services[indexPath.row - 1].status == 0{
                            cell.CheckBoxButton.isSelected = false
                        }else{
                            cell.CheckBoxButton.isSelected = true
                        }
                    }
                    
                    cell.CheckBoxButton.tag = (indexPath.section*100)+indexPath.row
                    cell.CheckBoxButton.addTarget(self, action: #selector(selectRDeselectService(_:)), for:.touchUpInside)
                    
                    if canEditServices == 5{
                        cell.editServicePrice.isHidden = false
                        cell.trailing25_47.constant = 47
                        cell.editServicePrice.tag = (indexPath.section*100)+indexPath.row
                        cell.editServicePrice.addTarget(self, action: #selector(editThisService(_:)), for:.touchUpInside)
                    }else{
                        cell.trailing25_47.constant = 25
                        cell.editServicePrice.isHidden = true
                    }
                    
                    cell.serviceDescription.text = subcatData[indexPath.section].services[indexPath.row - 1].serviceDescription
                    if Helper.countLabelLines(label:cell.serviceDescription) > 2{
                        cell.viewMoreButton.isHidden = false
                        cell.viewMoreButton.tag = (indexPath.section*100)+indexPath.row
                        cell.viewMoreButton.addTarget(self, action: #selector(viewTheDescription(_:)), for:.touchUpInside)
                    }else{
                        cell.viewMoreButton.isHidden = true
                    }
                    
                    cell.serviceName.text = subcatData[indexPath.section].services[indexPath.row - 1].serviceName
                    cell.servicePrice.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",subcatData[indexPath.section].services[indexPath.row - 1].serviceFee))
                    cell.serviceName.text = subcatData[indexPath.section].services[indexPath.row - 1].serviceName
                    cell.serviceCompletionTime.text = String(describing:subcatData[indexPath.section].services[indexPath.row - 1].serviceCompletionTime) + "Mins"
                    cell.selectionStyle = .none
                }
            }else{
                if selectAllServicesButton.isSelected ==  true{
                    cell.CheckBoxButton.isSelected = true
                }else{
                    if services[indexPath.row].status == 0{
                        cell.CheckBoxButton.isSelected = false
                    }else{
                        cell.CheckBoxButton.isSelected = true
                    }
                }
                
                cell.CheckBoxButton.tag = (indexPath.section*100)+indexPath.row
                cell.CheckBoxButton.addTarget(self, action: #selector(selectRDeselectService(_:)), for:.touchUpInside)
                
                if canEditServices == 5{
                    cell.editServicePrice.isHidden = false
                    cell.trailing25_47.constant = 47
                    cell.editServicePrice.tag = (indexPath.section*100)+indexPath.row
                    cell.editServicePrice.addTarget(self, action: #selector(editThisService(_:)), for:.touchUpInside)
                }else{
                    cell.trailing25_47.constant = 25
                    cell.editServicePrice.isHidden = true
                }
                
                cell.serviceDescription.text = services[indexPath.row].serviceDescription
                if Helper.countLabelLines(label:cell.serviceDescription) > 2{
                    cell.viewMoreButton.isHidden = false
                    cell.viewMoreButton.tag = (indexPath.section*100) + indexPath.row
                    cell.viewMoreButton.addTarget(self, action: #selector(viewTheDescription(_:)), for:.touchUpInside)
                }else{
                    cell.viewMoreButton.isHidden = true
                }
                
                cell.serviceName.text = services[indexPath.row].serviceName
                cell.servicePrice.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",services[indexPath.row].serviceFee))
                
                cell.serviceName.text = services[indexPath.row].serviceName
                cell.serviceCompletionTime.text = String(describing:services[indexPath.row].serviceCompletionTime) + "Mins"
                cell.selectionStyle = .none
                
            }
        }else{
            if services.count != 0 {
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "serviceHeader") as! ServiceHeaderTableCell
                    cell.subCatLabel.text = subcatData[indexPath.section-1].subCatName
                    return cell
                }else{
                    if selectAllServicesButton.isSelected ==  true{
                        cell.CheckBoxButton.isSelected = true
                    }else{
                        if subcatData[indexPath.section - 1].services[indexPath.row - 1].status == 0{
                            cell.CheckBoxButton.isSelected = false
                        }else{
                            cell.CheckBoxButton.isSelected = true
                        }
                    }
                    
                    cell.CheckBoxButton.tag = (indexPath.section*100)+indexPath.row
                    cell.CheckBoxButton.addTarget(self, action: #selector(selectRDeselectService(_:)), for:.touchUpInside)
                    
                    if canEditServices == 5{
                        cell.editServicePrice.isHidden = false
                        cell.trailing25_47.constant = 47
                        cell.editServicePrice.tag = (indexPath.section*100)+indexPath.row
                        cell.editServicePrice.addTarget(self, action: #selector(editThisService(_:)), for:.touchUpInside)
                    }else{
                        cell.trailing25_47.constant = 25
                        cell.editServicePrice.isHidden = true
                    }
                    
                    cell.serviceDescription.text = subcatData[indexPath.section - 1].services[indexPath.row - 1].serviceDescription
                    if Helper.countLabelLines(label:cell.serviceDescription) > 2{
                        cell.viewMoreButton.isHidden = false
                        cell.viewMoreButton.tag = (indexPath.section*100)+indexPath.row
                        cell.viewMoreButton.addTarget(self, action: #selector(viewTheDescription(_:)), for:.touchUpInside)
                    }else{
                        cell.viewMoreButton.isHidden = true
                    }
                    
                    cell.serviceName.text = subcatData[indexPath.section - 1].services[indexPath.row - 1].serviceName
                    cell.servicePrice.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",subcatData[indexPath.section - 1].services[indexPath.row - 1].serviceFee))
                    cell.serviceName.text = subcatData[indexPath.section - 1].services[indexPath.row - 1].serviceName
                    
                    cell.serviceCompletionTime.text = String(describing:subcatData[indexPath.section - 1].services[indexPath.row - 1].serviceCompletionTime) + "Mins"
                    cell.selectionStyle = .none
                }
            }else{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "serviceHeader") as! ServiceHeaderTableCell
                    cell.subCatLabel.text = subcatData[indexPath.section].subCatName
                    return cell
                }else{
                    if selectAllServicesButton.isSelected ==  true{
                        cell.CheckBoxButton.isSelected = true
                    }else{
                        if subcatData[indexPath.section].services[indexPath.row - 1].status == 0{
                            cell.CheckBoxButton.isSelected = false
                        }else{
                            cell.CheckBoxButton.isSelected = true
                        }
                    }
                    
                    cell.CheckBoxButton.tag = (indexPath.section*100)+indexPath.row
                    cell.CheckBoxButton.addTarget(self, action: #selector(selectRDeselectService(_:)), for:.touchUpInside)
                    
                    if canEditServices == 5{
                        cell.editServicePrice.isHidden = false
                        cell.trailing25_47.constant = 47
                        cell.editServicePrice.tag = (indexPath.section*100)+indexPath.row
                        cell.editServicePrice.addTarget(self, action: #selector(editThisService(_:)), for:.touchUpInside)
                    }else{
                        cell.trailing25_47.constant = 25
                        cell.editServicePrice.isHidden = true
                    }
                    
                    cell.serviceDescription.text = subcatData[indexPath.section].services[indexPath.row - 1].serviceDescription
                    if Helper.countLabelLines(label:cell.serviceDescription) > 2{
                        cell.viewMoreButton.isHidden = false
                        cell.viewMoreButton.tag = (indexPath.section*100)+indexPath.row
                        cell.viewMoreButton.addTarget(self, action: #selector(viewTheDescription(_:)), for:.touchUpInside)
                    }else{
                        cell.viewMoreButton.isHidden = true
                    }
                    
                    cell.serviceName.text = subcatData[indexPath.section].services[indexPath.row - 1].serviceName
                    cell.servicePrice.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",subcatData[indexPath.section].services[indexPath.row - 1].serviceFee))
                    cell.serviceName.text = subcatData[indexPath.section].services[indexPath.row - 1].serviceName
                    
                    cell.serviceCompletionTime.text = String(describing:subcatData[indexPath.section].services[indexPath.row - 1].serviceCompletionTime) + "Mins"
                    
                    cell.selectionStyle = .none
                }
            }
        }
        return cell
    }
}


