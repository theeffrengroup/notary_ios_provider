//
//  HourlyViewController.swift
//  LSP
//
//  Created by Vengababu Maparthi on 05/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import TGPControls
class HourlyViewController: UIViewController {
    @IBOutlet weak var savePrice: UIButton!
    
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var hourlyFees: UITextField!

    @IBOutlet weak var sliderView: TGPDiscreteSlider!
    @IBOutlet weak var priceEditBy: UILabel!
    
    @IBOutlet weak var setHourlyPriceLabel: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var sixthLabel: UILabel!
    @IBOutlet weak var selectPriceRangeLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    
    var singleCategory:ServiceCategory?
    var canEditHourFixed = 0  //3 = hourly not edit  4= hourly can edit
    var fromCat = false
    var serviceAPI = RatecardViewModel()
    var price = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        selectPriceRangeLabel.text = "SELECT YOUR PRICE"
        if canEditHourFixed == 4{
            savePrice.isHidden = false
            priceEditBy.text = "Price set by provider"
            sliderView.isHidden = false
            sliderView.isUserInteractionEnabled = true
            hourlyFees.isUserInteractionEnabled = true
        }else{
            setHourlyPriceLabel.text = "HOURLY PRICE"
            savePrice.isHidden = true
            priceEditBy.text = "Price set by admin"
            sliderView.isHidden = true
            selectPriceRangeLabel.isHidden = true
            maxLabel.isHidden = true
            minLabel.isHidden = true
            sixthLabel.isHidden = true
            firstLabel.isHidden = true
            sliderView.isUserInteractionEnabled = false
            hourlyFees.isUserInteractionEnabled = false
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        self.categoryName.text = singleCategory!.categoryName
        sliderView.ticksListener = self
        sliderView.incrementValue = Int(singleCategory!.hourMax/5 - singleCategory!.hourMin/5)
        sliderView.minimumValue = CGFloat(singleCategory!.hourMin)
       
        
        firstLabel.text  = Helper.getTheAmtTextWithSymbol(amt:String(describing:Int(singleCategory!.hourMin)))
        sixthLabel.text = Helper.getTheAmtTextWithSymbol(amt:String(describing:Int(singleCategory!.hourMax)))
        if singleCategory!.hourPrice == 0.0{
            sliderView.value = 10
        }else{
            sliderView.value = CGFloat(singleCategory!.hourPrice)
        }
        
        hourlyFees.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",singleCategory!.hourPrice))
        price = String(format:"%.2f",singleCategory!.hourPrice)
        hourlyFees.layer.borderWidth = 1
        hourlyFees.layer.borderColor  = COLOR.textFieldBorderColor.cgColor
        
    }
    
    @IBAction func saveThePrice(_ sender: Any) {
        self.view.endEditing(true)
        if Double(price)!  > singleCategory!.hourMax ||  Double(price)!  < singleCategory!.hourMin{
            Helper.alertVC(errMSG: "The price should be between " + String(describing:Int(singleCategory!.hourMin)) + " and " + String(describing:Int(singleCategory!.hourMax)))
            return
        }
        let dict:[String:Any] = ["service_category_Id":singleCategory!.cat_ID,
                                 "categoryType":1,
                                 "price":Double(self.price)!,
                                 "description":"",
                                 "serviceCompletionTime":""]
        self.serviceAPI.updateServicePrice(serviceData: dict) { (succeeded) in
            if succeeded{
                self.singleCategory!.hourPrice = Double(self.price)!
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
    
    
    @IBAction func backToView(_ sender: Any) {
        if fromCat{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension HourlyViewController:TGPControlsTicksProtocol{
    func tgpValueChanged(value: UInt) {
        hourlyFees.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",Double(value)))
        price = String(format:"%.2f",Double(value))
    }
    
    func tgpTicksDistanceChanged(ticksDistance:CGFloat, sender:AnyObject){
        
    }
}

extension HourlyViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == hourlyFees{
            textField.text = ""
        }else{
            textField.text = ""
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let regex = try! NSRegularExpression(pattern: "\\..{3,}", options: [])
        
        let matches = regex.matches(in: newText, options:[], range:NSMakeRange(0, newText.length))
        guard matches.count == 0 else { return false }
        
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            return true
        case ".":
            let array = textField.text?.map { String($0) }
            var decimalCount = 0
            for character in array! {
                if character == "." {
                    decimalCount = decimalCount + 1
                }
            }
            if decimalCount > 0 {
                return false
            } else {
                return true
            }
        default:
            let array = string.map { String($0) }
            if array.count == 0 {
                return true
            }
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        if textField == hourlyFees{
            if !(textField.text?.isEmpty)!{
                
                price = textField.text!
                textField.text = Helper.getTheAmtTextWithSymbol(amt:textField.text!)
                sliderView.value = CGFloat(Double(price)!)
            }else{
                hourlyFees.text  = Helper.getTheAmtTextWithSymbol(amt:textField.text!)
                price = String(format:"%.2f",singleCategory!.hourPrice)
            }
        }else{
            if !(textField.text?.isEmpty)!{
                textField.text = Helper.getTheAmtTextWithSymbol(amt:textField.text!)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}



