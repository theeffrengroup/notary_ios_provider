//
//  RateCardViewModel.swift
//  LSP
//
//  Created by Vengababu Maparthi on 03/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//


import Foundation

import RxAlamofire
import RxCocoa
import RxSwift


class RatecardViewModel:NSObject {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    var rateCardManager = RateCardDBManager()
    
    /// get the rate card details
    ///
    /// - Parameters:
    ///   - completionHandler: returns the ratecardModel model data on completion
    func getTheRateCardDetails(completionHandler:@escaping (Bool, [ServiceCategory] ) -> ()) {
        
        var hadRateCardData =  false
        self.rateCardManager.getAllRateCardData(forUserID: "RateCardData") { (rateCardData) in
            hadRateCardData = true
            completionHandler(true,ServiceCategory().getTheParsedRateCardDetails(categoriesData: rateCardData))
        }
        
        if !hadRateCardData{
            Helper.showPI(message:loading.load)
        }
        
        let rxApiCall = RateCardAPI()
        rxApiCall.getTheRateCardData()
        rxApiCall.Ratecard_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                       Helper.hidePI()
                    if  let data =  responseModel.data["data"] as? [[String:Any]]{
                        let rateDict  : [String : Any] = ["rateCardDict": data]
                        self.rateCardManager.saveTheRateCardData(forUserID: "RateCardData", dict: rateDict)
                        completionHandler(true,ServiceCategory().getTheParsedRateCardDetails(categoriesData: data))
                    }//@locate RateCardModel
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    
    /// updates the selecter or unselected service for the category
    ///
    /// - Parameters:
    ///   - serviceData: selected or unselected service id
    ///   - completionHandler: returns if the service updated or not
    func updateTheSelectedUnselectedServices(serviceData:[String:Any],completionHandler:@escaping(Bool)-> ())  {
        Helper.showPI(message:loading.load)
        let rxApiCall = RateCardAPI()
        rxApiCall.updateTheSelectedServices(parameters:serviceData)
        rxApiCall.Ratecard_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    
    /// updates the price and description for service, hourly
    ///
    /// - Parameters:
    ///   - serviceData: has service price and description
    ///   - completionHandler: returns true if success
    func updateServicePrice(serviceData:[String:Any],completionHandler:@escaping(Bool)-> ())  {
        Helper.showPI(message:loading.load)
        let rxApiCall = RateCardAPI()
        rxApiCall.updateServicePrice(parameters:serviceData)
        rxApiCall.Ratecard_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)  //bug ID:Card #15, desc: My ratecard_Price set by provider_no confirmation
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}

