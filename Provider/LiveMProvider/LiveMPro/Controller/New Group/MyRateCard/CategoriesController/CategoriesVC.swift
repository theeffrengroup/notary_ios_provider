//
//  CategoriesVC.swift
//  LSP
//
//  Created by Vengababu Maparthi on 02/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
//segue - toFixedHourly
//categories
class CategoriesVC: UIViewController {

    @IBOutlet weak var categoryTableView: UITableView!
    
    var categories = [ServiceCategory]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backToVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        switch segue.identifier! {
        case "toFixedHourly":
            if let nextScene = segue.destination as? HourlyFixedVC {
                nextScene.singleCategory = self.categories[sender as! Int]
                nextScene.canEditHourFixed = self.categories[sender as! Int].billingModel
                
                nextScene.fromCat = true
                
            }
            break
        case "toHourly":
            if let nextScene = segue.destination as? HourlyViewController {
                nextScene.singleCategory = self.categories[sender as! Int]
                nextScene.canEditHourFixed = self.categories[sender as! Int].billingModel
                
                nextScene.fromCat = true
            }
            break
        case "toFixed":
            if let nextScene = segue.destination as? FixedServicesVC {
                nextScene.services = self.categories[sender as! Int].categoryServices
                nextScene.subcatData = self.categories[sender as! Int].subCategoryData
                nextScene.canEditServices = self.categories[sender as! Int].billingModel
                nextScene.catName = self.categories[sender as! Int].categoryName
                nextScene.fromCat = true
            }
            break
        default:
            break
        }
    }
}


extension CategoriesVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch categories[indexPath.row].billingModel {
        case 2,6:
            performSegue(withIdentifier: "toFixedHourly", sender: indexPath.row)
            break
        case 1,5:
            performSegue(withIdentifier: "toFixed", sender: indexPath.row)
            break
        case 3,4:
            performSegue(withIdentifier: "toHourly", sender: indexPath.row)
            break
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CategoriesVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "categories") as! CategoryTableCell
        cell.categoryLabel.text = categories[indexPath.row].categoryName
        cell.selectionStyle = .none
        return cell
    }
}


