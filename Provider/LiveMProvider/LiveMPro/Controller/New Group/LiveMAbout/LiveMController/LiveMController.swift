//
//  LiveMController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class LiveMController: UIViewController {
    
    @IBOutlet weak var liveMWebsite: UILabel!
    @IBOutlet weak var liveMQuote: UILabel!
    @IBOutlet weak var appVersion: UILabel!
    var content = ["Rate us on App Store",
                   "Like us on facebook"]
    let  logo = [#imageLiteral(resourceName: "livem_star_icon"),
                 #imageLiteral(resourceName: "livem_thumb_icon"),
                 #imageLiteral(resourceName: "livem_legal_icon")]
    
    override func viewDidLoad() {
        appVersion.text = "App Version " + Utility.appVersion
        super.viewDidLoad()
        // Do any additional setup after loading the view
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func viewWebsite(_ sender: Any) {
        let url = URL(string: AppDetails.websiteName)
        UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
    }
}

extension LiveMController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return content.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "liveMcell", for: indexPath) as! LiveMCell
        cell.content.text = content[indexPath.row]
        cell.logo.image = self.logo[indexPath.row]
        return cell
    }
}

extension LiveMController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let url = URL(string: AppDetails.appstoreLink)
            UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
            break
        case 1:
            let url = URL(string: AppDetails.facebookurl)
            UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
            break
        case 2:
            let url = URL(string: AppDetails.youtubeChannelLiveM)
            UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
            break
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}
