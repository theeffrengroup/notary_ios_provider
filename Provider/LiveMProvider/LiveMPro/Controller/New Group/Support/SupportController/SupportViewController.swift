//
//  SupportViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SupportViewController: UIViewController {
    
    @IBOutlet var menuButton: UIButton!
    @IBOutlet var supportTableView: UITableView!
    var supportArray = [Support]()
    var supportModel =  Support()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        supportTableView.estimatedRowHeight = 10
        supportTableView.rowHeight = UITableView.automaticDimension
        self.updateSupportTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        super.viewWillAppear(animated)
        
    }
    
    // updathe support table data
    func updateSupportTableView() {
        supportModel.getTheSupportData { (succeeded, responseData) in
            if succeeded{
                self.supportArray = responseData
                self.supportTableView.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///**********opens the leftmenu********//
    @IBAction func menuAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        switch segue.identifier! as String {
        case "tosubSupport":
            if let supsuport =  segue.destination as? SupSupportViewController
            {
                supsuport.supportDict   = supportArray[sender as! Int]
            }
            break
        case "toWebVC1":
            if let searchAddress =  segue.destination as? WebViewController
            {
                let data = supportArray[sender as! Int]
                searchAddress.titleOfController = data.supportName
                searchAddress.urlFrom = data.supportUrl
                searchAddress.htmlFrom = data.supportDesc
            }
            break
        default:
            break
        }
        
    }
    
}

///*********** tableview datasource************///
extension SupportViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return supportArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SupportDetailListCell = (tableView.dequeueReusableCell(withIdentifier: "SupportDetailListCell") as! SupportDetailListCell?)!
        cell.suportLabel.text = supportArray[indexPath.row].supportName
        
        return cell
    }
}

///*********** tableview Delegate************///
extension SupportViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = supportArray[indexPath.row]
        if data.SubData.count == 0 {
            performSegue(withIdentifier: "toWebVC1", sender: indexPath.row)
            return
        }
        performSegue(withIdentifier: "tosubSupport", sender: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

