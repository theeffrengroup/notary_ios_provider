//
//  WebViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 20/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController,WKUIDelegate {

    @IBOutlet var titleLabel: UILabel!
//    @IBOutlet weak var webView: UIWebView!
    var webView: WKWebView!
    var urlFrom = ""
    var htmlFrom = ""
    var titleOfController = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        titleLabel.text = titleOfController
        if urlFrom.length != 0 {
            Helper.showPI(message: loading.load)
            webView.load(NSURLRequest(url: URL(string: urlFrom)!) as URLRequest)
        }else if htmlFrom.length != 0{
            Helper.showPI(message: loading.load)
            let htmlString:String! = htmlFrom
            webView.loadHTMLString(htmlString, baseURL: nil)
        }else{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"no proper link provided"), animated: true, completion: nil)
        }
        // Do any additional setup after loading the view.
    }

    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        
        view = webView
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}
/*
extension WebViewController : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Helper.hidePI()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Helper.hidePI()
    }
}
*/
extension WebViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Helper.hidePI()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Helper.hidePI()
    }
}
