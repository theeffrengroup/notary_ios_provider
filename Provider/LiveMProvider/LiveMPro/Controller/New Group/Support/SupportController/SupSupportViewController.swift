//
//  SupSupportViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 10/06/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SupSupportViewController: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var supportVC: UITableView!
    var supportDict = Support()
    var subSupportDict = [Support]()
    
    override func viewDidLoad() {
        self.titleLabel.text = supportDict.supportName
        super.viewDidLoad()
        subSupportDict = supportDict.SubData
        self.supportVC.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backToVC(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toWebVC" {
            let index  = sender as? Int
            let nextScene            = segue.destination as? WebViewController
            nextScene?.urlFrom   = subSupportDict[index!].supportUrl
            nextScene?.htmlFrom = subSupportDict[index!].supportDesc
            nextScene?.titleOfController = subSupportDict[index!].supportName
        }
    }
    
}

///*********** tableview datasource************///
extension SupSupportViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return subSupportDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SubSupportTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "subSupport") as! SubSupportTableViewCell?)!
        cell.subSupportLabel.text = subSupportDict[indexPath.row].supportName
        
        return cell
    }
}

///*********** tableview Delegate************///
extension SupSupportViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toWebVC", sender: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
