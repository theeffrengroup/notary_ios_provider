//
//  BankData.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 14/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxAlamofire
import RxCocoa
import RxSwift
import SwiftKeychainWrapper
enum bankResults {
    case success(BankDetails)
    case failure(Bool)
}

class BankDetails {
    var city        = ""
    var state       = ""
    var address     = ""
    var postalCode  = ""
    var dateOFBirth = ""
    var holderName  = ""
    var lastName    = ""
    var verifiedRNot = "'"
    let disposebag = DisposeBag()
    
    var bankAccountData = [BankAccountDataModel]()
    
    var apicall = APILibrary()
    
    
    
    /// get the bank details which is already added
    ///
    /// - Parameter completionHandler: returns bank details
    func getBankDate(completionHandler:@escaping(bankResults) -> ()){
        Helper.showPI(message: "loading...")
        let stripeDataModel = BankDetails()
        let rxApiCall = BankDataAPI()
        rxApiCall.getAddedBankData(method:API.METHOD.ADDBANKSTRIPE)
        rxApiCall.bank_Response
            .subscribe(onNext: {responseModel in
                Helper.hidePI()
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                switch responseCodes{
                case .dataNotFound:
                   // Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .UserLoggedOut:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    Session.expired()
                    break
                case .TokenExpired:
                    let defaults = KeychainWrapper.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        defaults.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apicall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                case .SuccessResponse:
                    Helper.hidePI()
                    if let bankDict = responseModel.data["data"] as? [String : Any]{
                        
                        if let stripeData = bankDict["legal_entity"] as? [String : Any]{
                            
                            if let dict = stripeData["verification"] as? [String : Any]{
                                if let verifyRNot = dict["status"] as? String{
                                    stripeDataModel.verifiedRNot = verifyRNot
                                }
                            }
                            
                            if let firstName = stripeData["first_name"] as? String{
                                stripeDataModel.holderName = firstName
                            }
                            
                            if let latName = stripeData["last_name"] as? String{
                                stripeDataModel.lastName = latName
                            }
                            
                            if let dateofBirth = stripeData["dob"] as? [String:Any]{
                                stripeDataModel.dateOFBirth =  String(describing:dateofBirth["month"]!) + "/" +  String(describing:dateofBirth["day"]!)
                                    + "/" + String(describing:dateofBirth["year"]!)
                            }
                            
                            if let addressDict =  stripeData["address"] as? [String:Any]{
                                stripeDataModel.city       = String(describing:addressDict["city"]!)
                                stripeDataModel.state       = String(describing:addressDict["state"]!)
                                stripeDataModel.address     = String(describing:addressDict["line1"]!)
                                stripeDataModel.postalCode = String(describing:addressDict["postal_code"]!)
                            }
                        }
                        
                        if  let dict = bankDict["external_accounts"] as? [String : Any]{
                            if let data =  dict["data"] as? [[String:Any]]{
                                stripeDataModel.bankAccountData = self.handlingTheBankData(accounts: data)
                            }
                        }
                    }
                    completionHandler(.success(stripeDataModel))
                    break
                default:
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    /// parse the bank details
    ///
    /// - Returns: return bank data in model
    func handlingTheBankData(accounts:[[String:Any]]) -> [BankAccountDataModel]{
        bankAccountData = []
        for dict in accounts {
            let bankModel = BankAccountDataModel()
            
            if let routing = dict["routing_number"] as? String{
                bankModel.routingNum = routing
            }
            
            if let holderNam = dict["account_holder_name"] as? String{
                bankModel.holderName = holderNam
            }
            
            if let account = dict["last4"] as? String{
                bankModel.accountNum = "XXXX " +  account
            }
            
            if let defaultBank = dict["default_for_currency"] as? NSNumber{
                bankModel.defaultBank = Int(defaultBank)
            }
            
            if let bankNam = dict["bank_name"] as? String{
                bankModel.bankName = bankNam
            }
            
            if let id = dict["id"] as? String{
                bankModel.iD = id
            }
            bankAccountData.append(bankModel)
        }
        
        return bankAccountData
    }
}


class BankAccountDataModel: NSObject {
    var iD  = ""
    var accountNum = ""
    var routingNum = ""
    var holderName = ""
    var bankName = ""
    var defaultBank = 0
}



class AddbankStripeModel: NSObject {
    
    var apicall = APILibrary()
    let disposebag = DisposeBag()
    
    
    /// add Stripe account
    ///
    /// - Parameters:
    ///   - params: stripe details
    ///   - completionHanlder: return true if the stripe added successfully
    func addStripeAccount(params:[String:Any],completionHanlder:@escaping(Bool)->()) { // - post
        Helper.showPI(message: "adding stripe...")
        let rxApiCall = BankDataAPI()
        rxApiCall.addBankAndMakeBankDefault(method:API.METHOD.ADDBANKSTRIPE,params:params)
        rxApiCall.bank_Response
            .subscribe(onNext: {responseModel in
                Helper.hidePI()
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                case .UserLoggedOut:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    Session.expired()
                    break
                case .TokenExpired:
                    let defaults = KeychainWrapper.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        defaults.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apicall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                case .SuccessResponse:
                    if let message = responseModel.data["message"] as? String{
                        Helper.alertVC(errMSG: message)
                    }
                    completionHanlder(true)
                    break
                default:
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}



class AddBankModel: NSObject {
    
    var apicall = APILibrary()
    let disposebag = DisposeBag()
    
    
    /// adds the bank after the stripe verified
    ///
    /// - Parameters:
    ///   - params: bank details like account number, name, personal numr
    ///   - completionHanlder: returns true when the bank added successfull
    func addBankAccountAfterStripeVerified(params:[String:Any],completionHanlder:@escaping(Bool)->()) { // - post
            Helper.showPI(message: "adding bank...")
        let rxApiCall = BankDataAPI()
        rxApiCall.addBankAndMakeBankDefault(method:API.METHOD.ADDBANKAFTERSTRIPE,params:params)
        rxApiCall.bank_Response
            .subscribe(onNext: {responseModel in
                Helper.hidePI()
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .UserLoggedOut:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    Session.expired()
                    break
                case .TokenExpired:
                    let defaults = KeychainWrapper.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        defaults.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apicall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                case .SuccessResponse:
                    if let message = responseModel.data["message"] as? String{
                        Helper.alertVC(errMSG: message)
                    }
                    completionHanlder(true)
                    break
                default:
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
        
        
    }
}


class DeleteBankModel: NSObject {
    
    var apicall = APILibrary()
    let disposebag = DisposeBag()
    
    //MARK: - delete the bank account using the account number
    func deleteBankAccount(params:[String:Any],completionHanlder:@escaping(Bool)->()) {  // - delete
        let rxApiCall = BankDataAPI()
        rxApiCall.deleteBankAccount(method:API.METHOD.DELETEBANK,params:params)
        rxApiCall.bank_Response
            .subscribe(onNext: {responseModel in
                Helper.hidePI()
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .UserLoggedOut:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    Session.expired()
                    break
                case .TokenExpired:
                    let defaults = KeychainWrapper.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        defaults.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apicall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                case .SuccessResponse:
                    if let message = responseModel.data["message"] as? String{
                        Helper.alertVC(errMSG: message)
                    }
                    completionHanlder(true)
                    break
                default:
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
        
    }
}


class DefaultBank:NSObject{
    
    var apicall = APILibrary()
    let disposebag = DisposeBag()
    
    
    // make the bank account default
    func defaultBankAccount(params:[String:Any],completionHanlder:@escaping(Bool)->()) {
        
        let rxApiCall = BankDataAPI()
        rxApiCall.defaultBankAccount(method:API.METHOD.DEFAULTBANK,params:params)
        rxApiCall.bank_Response
            .subscribe(onNext: {reponseModel in
                Helper.hidePI()
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: reponseModel.httpStatusCode)!
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: reponseModel.data["message"] as! String)
                    break
                    
                case .UserLoggedOut:
                    Helper.alertVC(errMSG: reponseModel.data["message"] as! String)
                    Session.expired()
                    break
                case .TokenExpired:
                    let defaults = KeychainWrapper.standard
                    if let sessionToken =  reponseModel.data["data"]   as? String  {
                        defaults.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apicall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                case .SuccessResponse:
                    if let message = reponseModel.data["message"] as? String{
                        Helper.alertVC(errMSG: message)
                    }
                    completionHanlder(true)
                    break
                default:
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}









