//
//  BankAccountDetailsExtension.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


// //MARK: - TableView Datasource method
extension BankAccountDetailsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2;
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            if bankServiceCallDone{
                return 3
            }else{
                return 3
            }
        default:
            if bankDate.bankAccountData.count != 0 {
                return bankDate.bankAccountData.count + 2
            }else if bankServiceCallDone{
                return 3
            }else{
                return 3
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                
                let sectionCell : PBSectionCell = tableView.dequeueReusableCell(withIdentifier: "sectionCell") as! PBSectionCell
                return sectionCell
            case 1:
                let header = tableView.dequeueReusableCell(withIdentifier:"bankheader") as! BankHeaderTableViewCell?
                header?.bankHeaderLabel.text = bankDetails.stripeAccount
                header?.bankHeaderLabel.adjustsFontSizeToFitWidth = true
                return header!
                
            default:
                
                if (self.bankDate.address.length > 0) {
                    let stripe = tableView.dequeueReusableCell(withIdentifier:"stripe") as! StripeTableViewCell?
                    
                    if verifiedorNot == bankDetails.verified {
                        stripe?.stripeStatusButton.isSelected =  true
                        stripe?.verifiedButton.isSelected = true
                        
                    }else{
                        stripe?.stripeStatusButton.isSelected =  false
                        stripe?.verifiedButton.isSelected = false
                    }
                    stripe?.stripeAccountNumber.text = "Stripe Account Name: " + bankDate.holderName + " " + bankDate.lastName
                    return stripe!
                }else{
                    
                    let stripe = tableView.dequeueReusableCell(withIdentifier:"addstripe") as! AddStripeTableCell?
                    stripe?.addStripeButton.layer.borderWidth = 2
                    stripe?.addStripeButton.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x2598ED).cgColor
                    return stripe!
                }
                
            }
            
        default:
            switch indexPath.row {
            case 0:
                
                let sectionCell : PBSectionCell = tableView.dequeueReusableCell(withIdentifier: "sectionCell") as! PBSectionCell
                return sectionCell
                
                
            case 1:
                let header = tableView.dequeueReusableCell(withIdentifier:"bankheader") as! BankHeaderTableViewCell?
                header?.bankHeaderLabel.text = bankDetails.bankAccount
                return header!
                
            default:
                
                if bankDate.bankAccountData.count != 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier:"bankDetails") as! BankTableViewCell?
                    
                    cell?.updateBankAccount(bankData: bankDate.bankAccountData[indexPath.row - 2])
                    
                    if bankDate.bankAccountData[indexPath.row - 2].defaultBank == 1{
                        cell?.defaultButton.isSelected = true
                    }else{
                        cell?.defaultButton.isSelected = false
                    }
                    return cell!
                }else{
                    let bank = tableView.dequeueReusableCell(withIdentifier:"addbank") as! AddBankTableCell?
                    if (self.bankDate.address.length > 0)  {
                        bank?.addBankButton.layer.borderWidth = 2
                        bank?.addBankButton.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x2598ED).cgColor
                    }else{
                        bank?.addBankButton.layer.borderWidth = 2
                        bank?.addBankButton.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0xB8B8B8).cgColor
                    }
                    return bank!
                }
            }
        }
    }
}

//MARK: - TableView Delegate method
extension BankAccountDetailsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 1 {
            if verifiedorNot != bankDetails.verified {
                toVerify = true
                self.performSegue(withIdentifier: "toAddStripe", sender: nil)
            }
        }
        
        if indexPath.section == 1 && indexPath.row != 0 {
//            let Vc:SelectDefaultBank = SelectDefaultBank.instance
//            Vc.delegate = self
//            Vc.show(bankData:bankDate.bankAccountData[indexPath.row - 2])
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
