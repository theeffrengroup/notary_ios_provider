//
//  AddBankDetailsExtensionVC.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension AddBankDetailsViewController{
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated:true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    //MARK: - Add Stripe account API
    func makeTheServiceCallToSaveTheBank(){
        if pickedImage.image == nil{
            self.present(Helper.alertVC(title:alertMsgCommom.Message, message:bankDetailsMsg.photoID), animated: true, completion: nil)
        }else if (holderName.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message, message:bankDetailsMsg.firstName), animated: true, completion: nil)
        }else if (lastName.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message, message:bankDetailsMsg.lastName), animated: true, completion: nil)
        }
        else if(personalIDTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message , message:bankDetailsMsg.uniqueNum), animated: true, completion: nil)
        }else if(addressTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message , message:bankDetailsMsg.address), animated: true, completion: nil)
        }else if(cityTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message, message:bankDetailsMsg.city), animated: true, completion: nil)
        }else if(stateTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message , message:bankDetailsMsg.state), animated: true, completion: nil)
        }else if(posyalCodeTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message , message:bankDetailsMsg.postal), animated: true, completion: nil)
        }else{
            self.sendRequestForSignup() //@Locate AddbankDetailsViewController
        }
    }
}
