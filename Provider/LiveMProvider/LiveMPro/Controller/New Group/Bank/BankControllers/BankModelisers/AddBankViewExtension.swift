//
//  AddBankViewExtension.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension AddBankViewController{
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated:true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
}

extension AddBankViewController:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch textField {
        case accountHolderName:
            view1.backgroundColor = COLOR.APP_COLOR
            break
            
        case accountNumber:
            view2.backgroundColor = COLOR.APP_COLOR
            
            break
        case routingNumber:
            view3.backgroundColor = COLOR.APP_COLOR
            break
        default:
            break
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        switch textField {
        case accountHolderName:
            if !(accountHolderName.text?.isEmpty)!{
                view1.backgroundColor = COLOR.APP_COLOR
            }else{
                view1.backgroundColor = COLOR.E1E1E1
            }
            break
            
        case accountNumber:
            if !(accountNumber.text?.isEmpty)!{
                view2.backgroundColor = COLOR.APP_COLOR
            }else{
                view2.backgroundColor = COLOR.E1E1E1
            }
            break
        case routingNumber:
            if !(routingNumber.text?.isEmpty)!{
                view3.backgroundColor = COLOR.APP_COLOR
            }else{
                view3.backgroundColor = COLOR.E1E1E1
            }
            break
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case accountHolderName:
            accountNumber.becomeFirstResponder()
            return false
        case accountNumber:
            routingNumber.becomeFirstResponder()
            return false
        default:
            dismisskeyBord()
            break
        }
        return true
    }
}
