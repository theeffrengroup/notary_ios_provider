//
//  AddBankDetailsTextField.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


// MARK: - Textfield delegate method
extension AddBankDetailsViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        activeTextField = textField
        switch textField {
        case holderName:
            view1.backgroundColor = COLOR.APP_COLOR
            break
            
        case lastName:
            view2.backgroundColor = COLOR.APP_COLOR
            
            break
        case DateOFB:
            view3.backgroundColor = COLOR.APP_COLOR
            break
        case personalIDTF:
            view4.backgroundColor = COLOR.APP_COLOR
            
            break
            
        case addressTF:
            view5.backgroundColor = COLOR.APP_COLOR
            
            break
        case cityTF:
            view6.backgroundColor = COLOR.APP_COLOR
            break
        case stateTF:
            view7.backgroundColor = COLOR.APP_COLOR
            
            break
        case posyalCodeTF:
            view8.backgroundColor = COLOR.APP_COLOR
            break
            
        default:
            
            break
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Format Date of Birth MM/DD/yyyy
        
        //initially identify your textfield
        
        if textField == DateOFB {
            
            // check the chars length dd -->2 at the same time calculate the dd-MM --> 5
            if (DateOFB?.text?.count == 2) || (DateOFB?.text?.count == 5) {
                //Handle backspace being pressed
                if !(string == "") {
                    // append the text
                    DateOFB?.text = (DateOFB?.text)! + "/"
                }
            }
            // check the condition not exceed 9 chars
            return !(textField.text!.count > 9 && (string.count ) > range.length)
        }
        else {
            return true
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        switch textField {
        case holderName:
            if !(holderName.text?.isEmpty)!{
                view1.backgroundColor = COLOR.APP_COLOR
            }else{
                view1.backgroundColor = COLOR.E1E1E1
            }
            break
            
        case lastName:
            if !(lastName.text?.isEmpty)!{
                view2.backgroundColor = COLOR.APP_COLOR
            }else{
                view2.backgroundColor = COLOR.E1E1E1
            }
            break
        case DateOFB:
            if !(DateOFB.text?.isEmpty)!{
                view3.backgroundColor = COLOR.APP_COLOR
            }else{
                view3.backgroundColor = COLOR.E1E1E1
            }
            break
            
        case personalIDTF:
            if !(personalIDTF.text?.isEmpty)!{
                view4.backgroundColor = COLOR.APP_COLOR
            }else{
                view4.backgroundColor = COLOR.E1E1E1
            }
            break
            
        case addressTF:
            if !(addressTF.text?.isEmpty)!{
                view5.backgroundColor = COLOR.APP_COLOR
            }else{
                view5.backgroundColor = COLOR.E1E1E1
            }
            break
            
        case cityTF:
            if !(cityTF.text?.isEmpty)!{
                view6.backgroundColor = COLOR.APP_COLOR
            }else{
                view6.backgroundColor = COLOR.E1E1E1
            }
            break
        case stateTF:
            if !(stateTF.text?.isEmpty)!{
                view7.backgroundColor = COLOR.APP_COLOR
            }else{
                view7.backgroundColor = COLOR.E1E1E1
            }
 
            break
        case posyalCodeTF:
            if !(posyalCodeTF.text?.isEmpty)!{
                view8.backgroundColor = COLOR.APP_COLOR
            }else{
                view8.backgroundColor = COLOR.E1E1E1
            }
            
            break
        default:
            
            break
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case holderName:
            
            lastName.becomeFirstResponder()
            return false
        case lastName:
            DateOFB.becomeFirstResponder()
            return false
        case DateOFB:
            personalIDTF.becomeFirstResponder()
            return false
        case personalIDTF:
            addressTF.becomeFirstResponder()
            return false
        case addressTF:
            cityTF.becomeFirstResponder()
            return false
        case cityTF:
            stateTF.becomeFirstResponder()
            return false
        case stateTF:
            posyalCodeTF.becomeFirstResponder()
            return false
        default:
            dismisskeyBord()
            return true
        }
    }
}

