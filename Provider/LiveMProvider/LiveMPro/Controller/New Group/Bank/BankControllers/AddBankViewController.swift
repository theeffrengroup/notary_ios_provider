//
//  AddBankViewController.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 18/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class AddBankViewController: UIViewController {
    @IBOutlet weak var routingNumber: UITextField!
    @IBOutlet weak var addAccountNumber: UIButton!
    @IBOutlet weak var accountNumber: UITextField!
    @IBOutlet weak var accountHolderName: UITextField!
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var view3: UIView!
    var addBankModel = AddBankModel()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountHolderName.autocapitalizationType = .words
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        setupGestureRecognizer()
    }
    
    
    @IBAction func backToVc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func AddAccountAction(_ sender: Any) {
        addBankAccount()
    }
    
    func addBankAccount(){
        if (accountHolderName.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message, message:bankDetailsMsg.holderName), animated: true, completion: nil)
        }
        else if(accountNumber.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message , message:bankDetailsMsg.accountNum), animated: true, completion: nil)
        }else if(routingNumber.text?.isEmpty)!{
            self.present(Helper.alertVC(title:alertMsgCommom.Message, message:bankDetailsMsg.routingNum), animated: true, completion: nil)
        }else{
            sendRequestForSignup()
        }
        
    }
    
    func sendRequestForSignup() {
        Helper.showPI(message: loading.addBank)
        var bankDetails = [String: Any]()
        
        bankDetails = [
            "country"       :"US",
            "routing_number":routingNumber.text! as Any,
            "account_number":accountNumber.text! as Any,
            "account_holder_name":accountHolderName.text! as Any,
            "email":Utility.userEmail]
        
        addBankModel.addBankAccountAfterStripeVerified(params: bankDetails, completionHanlder: { success in
        if success{
        _ = self.navigationController?.popToRootViewController(animated: true)
        }else{
        
        }
        })
    }
    
    func dismisskeyBord(){
        self.view.endEditing(true)
    }
}


extension AddBankViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
     //   navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}



