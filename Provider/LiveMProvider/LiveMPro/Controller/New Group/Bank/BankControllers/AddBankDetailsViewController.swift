//
//  AddBankDetailsViewController.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class AddBankDetailsViewController: UIViewController {
    
    @IBOutlet var stateTF: FloatLabelTextField!
    @IBOutlet var addressTF: FloatLabelTextField!
    @IBOutlet var holderName: FloatLabelTextField!
    @IBOutlet var DateOFB: FloatLabelTextField!
    @IBOutlet var personalIDTF: FloatLabelTextField!
    @IBOutlet var cityTF: FloatLabelTextField!
    @IBOutlet var posyalCodeTF: FloatLabelTextField!
    @IBOutlet var addPhotoID: UIImageView!
    
    
    @IBOutlet weak var lastName: FloatLabelTextField!
    
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var contentScrollView: UIView!
    
    @IBOutlet weak var addOrVerifyStripe: UIButton!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view8: UIView!
    
    @IBOutlet weak var titleView: UILabel!
    var toVerify:Bool = false
    
    var stripeSavedDict:BankDetails?
    var addBankStripeModel = AddbankStripeModel()
    
    var activeTextField = UITextField()
    var pickedImage = UIImageView()
    var temp = [UploadImage]()
    var bankPhotoUrl = ""
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickedImage.image = nil
        updateTheTextFileds()
        holderName.text = Utility.firstName
        lastName.text = Utility.lastName
        
        
        if toVerify == true {
            titleView.text = bankDetailsMsg.verifyStripeTitle
            addOrVerifyStripe.setTitle(bankDetailsMsg.verifyStripe, for: .normal)
            DateOFB.text = stripeSavedDict?.dateOFBirth
            holderName.text = stripeSavedDict?.holderName
            lastName.text = stripeSavedDict?.lastName
            cityTF.text = stripeSavedDict?.city
            stateTF.text = stripeSavedDict?.state
            addressTF.text = stripeSavedDict?.address
            posyalCodeTF.text = stripeSavedDict?.postalCode
            
        }else{
            titleView.text = bankDetailsMsg.addStripeTitle
            addOrVerifyStripe.setTitle(bankDetailsMsg.addStripe, for: .normal)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func updateTheTextFileds(){
        holderName.autocapitalizationType = .words
        lastName.autocapitalizationType = .words
        stateTF.autocapitalizationType = .words
        addressTF.autocapitalizationType = .words
    }
    
    func isEmptyLists(dict: [String: Any]) -> Bool {
        if dict.isEmpty {
            return false
        }else{
            return true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        setupGestureRecognizer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            mainScrollView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardSize = CGSize(width: 320, height: 240)
        let height: CGFloat = UIDevice.current.orientation.isPortrait ? keyboardSize.height : keyboardSize.width //UIDeviceOrientationIsPortrait(UIDevice.current.orientation) ? keyboardSize.height : keyboardSize.width
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            var edgeInsets: UIEdgeInsets = self.mainScrollView.contentInset
            edgeInsets.bottom = height
            self.mainScrollView.contentInset = edgeInsets
            edgeInsets = self.mainScrollView.scrollIndicatorInsets
            edgeInsets.bottom = height
            self.mainScrollView.scrollIndicatorInsets = edgeInsets
        })
    }
    
    
    @IBAction func tapGestureToDismissKeyBoard(_ sender: Any) {
        self.dismisskeyBord()
    }
    
    @IBAction func addPhotoID(_ sender: Any) {
        self.selectImage()
    }
    
    @IBAction func backButton(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func saveTheBankDetails(_ sender: Any) {
        self.makeTheServiceCallToSaveTheBank()  //@locate AddbankDetailsExtensionVC
    }
    
    //AddStripe Account API
    func sendRequestForSignup() {
        let name = DateOFB.text!.components(separatedBy: "/")
        Helper.showPI(message: loading.addStripe)
        var bankDetails = [String: Any]()
        //vani 13/11/2019
        var ipAddress = UserDefaults.standard.object(forKey: "ipAddress") as! String
//        var ipAddress = Helper.getIPAddress()
        
        bankDetails = [
            "city"          :cityTF.text!,
            "line1"         :addressTF.text!,
            "postal_code"   :posyalCodeTF.text!,
            "state"         :stateTF.text!,
            "month"          :name[0],
            "day"            :name[1],
            "year"           :name[2],
            "first_name"    :holderName.text!,
            "last_name"     :lastName.text!,
            "personal_id_number":personalIDTF.text!,
            "ip"            :ipAddress,
            "date"          :Helper.currentDateTime ,
            "country"       :"US",
            "document"      :bankPhotoUrl,
            "email":Utility.userEmail]
        
        addBankStripeModel.addStripeAccount(params: bankDetails, completionHanlder: { success in
            if success{
                _ = self.navigationController?.popToRootViewController(animated: true)
            }else{
                
            }
        })
    }
    
    func uploadProfileimgToAmazon(){
        
        var url = String()
        
        temp = [UploadImage]()
        let timeStamp = Helper.currentTimeStamp
        url = AMAZONUPLOAD.DOCUMENTS + timeStamp + "_" + "bankPhotoID" + ".png"
        
        temp.append(UploadImage.init(image: addPhotoID.image!, path: url))
        
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
        
        bankPhotoUrl = Utility.amazonUrl + AMAZONUPLOAD.DOCUMENTS + timeStamp +  "_" + "bankPhotoID" + ".png"
    }
    
    
    func dismisskeyBord(){
        self.view.endEditing(true)
    }
    
    /// MARK: - actionSheet
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: signup.selectImage as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: signup.cancel as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: signup.gallery as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: signup.camera as String?,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    // MARK: - photo gallery
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = true
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    // MARK: - Camera selection
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            imagePickerObj.allowsEditing = true
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
        }
    }
}


// MARK: - ImagePicker delegate method
extension AddBankDetailsViewController : UIImagePickerControllerDelegate {
    /*The image is not getting dispalyed when when selected because this method is deprecated use th below method
    Owner : Vani
    Date : 27/02/20202
    */
    /*
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        addPhotoID.image = Helper.resizeImage(image: image, newWidth: 200)
        pickedImage.image = Helper.resizeImage(image: image, newWidth: 200)
        bankPhotoUrl = ""
        self.uploadProfileimgToAmazon()
        self.dismiss(animated: true, completion: nil)
    } */
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            addPhotoID.image = Helper.resizeImage(image: image, newWidth: 200)
            pickedImage.image = Helper.resizeImage(image: image, newWidth: 200)
            bankPhotoUrl = ""
            self.uploadProfileimgToAmazon()
        }
        self.dismiss(animated: true, completion: nil)
    }
}


extension AddBankDetailsViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
      //  navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}





