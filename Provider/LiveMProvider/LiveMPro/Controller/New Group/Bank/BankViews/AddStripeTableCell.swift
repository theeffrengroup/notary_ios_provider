//
//  AddStripeTableCell.swift
//  RunnerDev
//
//  Created by Vengababu Maparthi on 19/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class AddStripeTableCell: UITableViewCell {

    @IBOutlet weak var addStripeButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
