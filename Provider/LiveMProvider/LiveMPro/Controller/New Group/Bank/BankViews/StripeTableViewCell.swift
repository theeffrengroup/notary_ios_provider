//
//  StripeTableViewCell.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 18/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class StripeTableViewCell: UITableViewCell {

    @IBOutlet weak var verifiedButton: UIButton!
    @IBOutlet weak var stripeAccountNumber: UILabel!

    @IBOutlet weak var stripeStatusButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
