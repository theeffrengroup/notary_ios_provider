
//
//  EditNumberModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 20/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//




import Foundation
import SwiftKeychainWrapper
class ChangeNumberModel:NSObject {
    
    var apiLibrary = APILibrary()
    
    
    /// changes the phone number
    ///
    /// - Parameters:
    ///   - params: new phone with country code
    ///   - completionHandler: return the id, which is required for validating time
    func changeNumber(params:[String:Any],completionHandler:@escaping (Bool,String) -> ()) {
        apiLibrary.updateNewPhoneNumber(params: params,completionHandler: { (responseModel) in
            let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
            Helper.hidePI()
            switch responseCodes{
            case .SuccessResponse:
                if let dict = responseModel.data["data"] as? [String:Any]{
                   completionHandler(true,dict["sid"] as! String)
                }
              //  Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                break
                
            case .UserLoggedOut:
                Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                Session.expired()
                break
            case .TokenExpired:
                let defaults = KeychainWrapper.standard
                if let sessionToken =  responseModel.data["data"]   as? String  {
                    defaults.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                    self.apiLibrary.getTheNewSessionToken(completionHandler: { (success) in
                        if success{
                            
                        }
                    })
                }
                break
            case .adminNotAccepted:
                Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                break
            default:
                completionHandler(false,"")
                Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                break
            }
            
        })
    }
}


