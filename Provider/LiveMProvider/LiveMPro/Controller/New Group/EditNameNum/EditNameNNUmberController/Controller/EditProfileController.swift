//
//  EditProfileController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 20/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit



protocol EditProfileDelegate {
    func updatedName(firstNam: String, lastName:String)
}


class EditProfileController: UIViewController {
    var delegate: EditProfileDelegate?
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var lastNameTf: FloatLabelTextField!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var buttonBottomConst: NSLayoutConstraint!
    var name : NSString = ""
    var text : NSString = ""
    var activeTextField = UITextField()
    @IBOutlet weak var textfield:FloatLabelTextField!
    var updateEventsModel = ProfileListModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveName(_ sender: Any) {
        if (textfield.text?.isEmpty)!  && (lastNameTf.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Field shouldn't be empty"), animated: true, completion: nil)
            
        }else{
            self.updateName()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func updateName(){
        var params = [String : Any]  ()
        
        if !(textfield.text?.isEmpty)!  && !(lastNameTf.text?.isEmpty)!{
            params =  ["firstName" :textfield.text! as Any,
                       "lastName": lastNameTf.text! as Any]
        }else if (textfield.text?.isEmpty)!  && !(lastNameTf.text?.isEmpty)!{
            params = ["lastName": lastNameTf.text! as Any]
        }
        else if !(textfield.text?.isEmpty)!  && (lastNameTf.text?.isEmpty)!{
            params = ["firstName" :textfield.text! as Any]
        }
        
        Helper.showPI(message:loading.load)
        
        
        updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeded) in
            if succeded{
                self.delegate?.updatedName(firstNam: self.textfield.text!, lastName: self.lastNameTf.text!)
                self.dismiss(animated: true, completion: nil)
            }else{
                
            }
        })
    }
    
    func alertVC(errMSG:String){
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1
        alertWindow.makeKeyAndVisible()
        
        alertWindow.rootViewController?.present(Helper.alertVC(title: alertMsgCommom.Message, message:errMSG), animated: true, completion: nil)
    }
    
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
}

extension EditProfileController:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case lastNameTf:
            view2.backgroundColor = COLOR.APP_COLOR
        default:
            view1.backgroundColor = COLOR.APP_COLOR
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case lastNameTf:
            if (lastNameTf.text?.isEmpty)!{
                view2.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }else{
                view2.backgroundColor = COLOR.APP_COLOR
            }
        default:
            if (textfield.text?.isEmpty)!{
                view1.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }else{
                view1.backgroundColor = COLOR.APP_COLOR
            }
        }
    }
}
