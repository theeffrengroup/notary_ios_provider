//
//  ChangeNumberViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
protocol ChangeNumberDelegate:class
{
    func updatedNewNumber(numberNew: String,countryCode:String,countrySymbol:String)
}



import UIKit



class ChangeNumberViewController: UIViewController,CountryPickerDelegate {
  open weak var delegate: ChangeNumberDelegate?
    
    @IBOutlet var countryImage: UIImageView!
    @IBOutlet var countryCode: UILabel!
    @IBOutlet var buttonContraint: NSLayoutConstraint!
    @IBOutlet var phoneTF: UITextField!
    
    @IBOutlet weak var view1: UIView!
    var changeNumMod = ChangeNumberModel()
    var countryCodeSymbol = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0x444444)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.setCountryCode()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    
    //MARK: - Keyboard Methods
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
          //  self.moveViewUp(keyboardHeight: keyboardSize.height)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
           // self.moveViewDown()
        }
    }

    
    @IBAction func pickTheCountryCode(_ sender: Any) {
        performSegue(withIdentifier:"pickCountryCodeFromFP", sender: nil)
    }
    
    
    @IBAction func verifyOTP(_ sender: Any) {
        //  self.moveViewDown()
        self.view.endEditing(true)
        if (phoneTF.text?.isEmpty)! {
                 self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Please enter the number"), animated: true, completion: nil)
        }else
        {
           self.changeWithVerification()
        }
    }
    
    // MARK: - setCountrycode
    func setCountryCode(){
        let country = NSLocale.current.regionCode
        let picker = CountryPicker.dialCode(code: country!)
        countryCode.text = picker.dialCode
        countryImage.image = picker.flag
        /*
        Get countrysymbolcode
        Owner :vani
        Date : 13/03/2020
        */
        countryCodeSymbol = picker.code
    }
    
    //vani 13/02/2020
    // MARK: - country delegate method
    internal func didPickedCountry(country: Country)
    {
        countryCode.text = country.dialCode
        countryImage.image = country.flag
         countryCodeSymbol   = country.code
    }
    
    
    @IBAction func backToVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "pickCountryCodeFromFP"
        {
            let nav = segue.destination as! UINavigationController
            if let picker: CountryPicker = nav.viewControllers.first as! CountryPicker?
            {
                picker.delegate = self
            }
        }else if segue.identifier == "toVerifyTheNumber" {
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.delegate = self
            nextScene?.mobileNumber = phoneTF.text!
            nextScene?.countryCode = countryCode.text!
            nextScene?.providerID = sender as! String
            nextScene?.defineTheOtp = 3
        }
    }
    
    
    func changeWithVerification() {
        
        Helper.showPI(message:loading.load)
        let params : [String : Any] =  ["phone"   :  phoneTF.text!,
                                        "countryCode": countryCode.text!,
                                        //vani 13/02/2020
                                        "countrySymbol":countryCodeSymbol,
                                        "userType" : "2"]  //1:Slave 2:Master
        
        print("getOtp parameters :",params)
        changeNumMod.changeNumber(params: params) { (succeeded,masterId) in
            if succeeded{
                   self.performSegue(withIdentifier:"toVerifyTheNumber", sender: masterId)
            }
        }
        // have to add service api
    }
}
/*
extension ChangeNumberViewController:CountryPickerDelegate{
    // MARK: - country delegate method
    internal func didPickedCountry(country: Country)
    {
        countryCode.text = country.dialCode
        countryImage.image = country.flag
    }
} */

extension ChangeNumberViewController:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.isEmpty)!{
            view1.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
        }else{
            view1.backgroundColor = COLOR.APP_COLOR
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        view1.backgroundColor = COLOR.APP_COLOR
        return true
    }
}

extension ChangeNumberViewController:verifyOtpDelegate{
    func phoneNumberVerified(){
        
    }
    func updatedNumber(numberNew: String,countryCode:String){
        delegate?.updatedNewNumber(numberNew: numberNew, countryCode: countryCode,countrySymbol:countryCodeSymbol)
        self.dismiss(animated: true, completion: nil)
    }
}


