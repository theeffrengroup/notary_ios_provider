//
//  APICalls.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 05/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UIKit
import RxAlamofire
import RxCocoa
import RxSwift


class EventModel{
    
    var eventID = ""
    var eventName = ""
    var eventStatus = 0
}

class MetaData {
    var description = ""
    var fieldID = ""
    var fieldType = 0
    var isMandatory = 0
    var fieldName = ""
    var fieldDesc = ""
    var predefinedData = [Predefined]()
}


class Predefined {
    var name = ""
    var image = ""
}

class ServicesData{
    
    var serviceId = ""
    var serviceName = ""
    var servicePrice = 0
    var serviceUnits = ""
}

class ProfileMod {
    var firstName = ""
    var lastName = ""
    var catName = ""
    var about = ""
    var address = ""
    var countryCode = ""
    var countryCodeSymbol = ""
    var dob = ""
    var email = ""
    var instruments = ""
    var link = ""
    var musicGeners = ""
    var profilePic = ""
    var profileStatus = 0
    var radius = 0
    var rules = ""
    var mobileNum = ""
    var servicesList = ""
    var maxRadius = 0
    var minRadius = 0
    var distanceMatrix = 0
    var gender = ""
    
    var images = [String]()
    var metaData = [MetaData]()
    var eventsArrayDict = [EventModel]()
    var servicesArrayDict = [ServicesData]()
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    var profileManager = ProfileDBManager()
    
    
    //MARK: - profile details service
    func profileDetailsService(completionHandler:@escaping (Bool,ProfileMod , [Categories]) -> ()) {
        var hadProfileData =  false
        self.profileManager.getAllProfileData(forUserID: Utility.userId) { (profileData) in
            hadProfileData = true
            if let docs = profileData["documents"] as? [[String: Any]] {
                completionHandler(true,self.getEventAndProfileData(dict: profileData) , Categories.parsingTheCategoryServiceResponse(responseData: docs))
            }
            else {
                completionHandler(true,self.getEventAndProfileData(dict: profileData) , [])
            }
           // completionHandler(true,self.getEventAndProfileData(dict: profileData) , Categories.parsingTheCategoryServiceResponse(responseData: profileData["documents"] as! [[String: Any]]))
        }
        
        if !hadProfileData{
            Helper.showPI(message:loading.load)
        }
        
        
        let rxApiCall = ProfileAPI()
        rxApiCall.profileDetailsService(method:API.METHOD.PROFILEDATA)
        rxApiCall.Profile_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .SuccessResponse:
                    
                    if let dict = responseModel.data["data"] as? [String : Any]{
                        let profileDict  : [String : Any] = ["profileDict": dict]
                        self.profileManager.saveTheMessage(forUserID: Utility.userId, dict: profileDict)
                        
                        if let docs = dict["documents"] as? [[String: Any]] {
                            completionHandler(true,self.getEventAndProfileData(dict: dict) , Categories.parsingTheCategoryServiceResponse(responseData: docs))
                        }
                        else {
                            completionHandler(true,self.getEventAndProfileData(dict: dict) , [])
                        }
                         //completionHandler(true,self.getEventAndProfileData(dict: dict) , Categories.parsingTheCategoryServiceResponse(responseData: dict["documents"] as! [[String: Any]]))
                    }
                    
                   
                    
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false, ProfileMod.init(),[] )
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    
    
    
    /// logout from profile action
    ///
    /// - Parameter completionHandler: return true if success
    func logoutActionService(completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.logout)
        let rxApiCall = ProfileAPI()
        rxApiCall.logoutActionService(method:API.METHOD.LOGOUT)
        rxApiCall.Profile_Response
            .subscribe(onNext: {responseModel in
                switch responseModel.httpStatusCode{
                case 200:
                    Helper.hidePI()
                    completionHandler(true)
                    break
                default:
                    Helper.hidePI()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    //MARK:-  parsing the  profile data
    func getEventAndProfileData(dict:[String:Any]) -> ProfileMod {
        let profileDictModel = ProfileMod()
        let ud = UserDefaults.standard
        if  let aboutYou = dict["about"] as? String{
            profileDictModel.about = aboutYou
        }
        
        if let currencySymbol = dict["currencySymbol"] as? String{
            ud.set(currencySymbol, forKey: USER_INFO.CURRENCYSYMBOL)
            ud.synchronize()
        }
        
        if let event = dict["address"] as? String{
            profileDictModel.address = event
        }
        
        if  let id = dict["countryCode"] as? String{
            profileDictModel.countryCode = id
        }
        
        
        if  let id = dict["countrySymbol"] as? String{
            profileDictModel.countryCodeSymbol = id
        }
        
        if let event = dict["dob"] as? String{
            profileDictModel.dob = event
        }
        
        
        if let event = dict["email"] as? String{
            profileDictModel.email = event
        }
        
        if  let id = dict["firstName"] as? String{
            profileDictModel.firstName = id
        }
        
        if  let id = dict["lastName"] as? String{
            profileDictModel.lastName = id
        }
        
//        if  let  catNam = dict["catName"] as? String{
//            profileDictModel.catName = catNam
//        }
        
        if let catArray = dict["catNameArr"] as? [String]{
            for catName in catArray{
                if profileDictModel.catName.length > 1{
                   profileDictModel.catName = profileDictModel.catName + ", " + catName
                }else{
                    profileDictModel.catName = catName
                }
            }
            
        }

        if  let id = dict["link"] as? String{
            profileDictModel.link = id
        }
        
        if let event = dict["mobile"] as? String{
            profileDictModel.mobileNum = event
        }
        
        if  let id = dict["musicGeners"] as? String{
            profileDictModel.musicGeners = id
        }
        
        if let gender = dict["gender"] as? Int {
            if gender == 1{
                profileDictModel.gender = "Male"
            }
            else if gender == 2 {
                 profileDictModel.gender = "Female"
            }
            else{
                profileDictModel.gender = "Others"
            }
        }
        
        if let event = dict["instruments"] as? String{
            profileDictModel.instruments = event
        }
        
        if let distanceMatrix = dict["distanceMatrix"] as? NSNumber{
            profileDictModel.distanceMatrix = Int(distanceMatrix)
        }
        
        if  let id = dict["profilePic"] as? String{
            profileDictModel.profilePic = id
        }
        
        if let event = dict["profileStatus"] as? NSNumber{
            profileDictModel.profileStatus = Int(event)
        }
        
        if  let id = dict["radius"] as? NSNumber{
            profileDictModel.radius = Int(id)
        }
        
        if  let maxRadius = dict["maxRadius"] as? NSNumber{
            profileDictModel.maxRadius = Int(maxRadius)
        }
        
        if  let minRadius = dict["minRadius"] as? NSNumber{
            profileDictModel.minRadius = Int(minRadius)
        }
        
        if let event = dict["rules"] as? String{
            profileDictModel.rules = event
        }
        
      
        for image in dict["workImage"] as! [String] {
            profileDictModel.images.append(image)
        }
        
        for items in (dict["events"] as? [[String: Any]])!  {
            let eventMod = EventModel()
            
            if  let id = items["_id"] as? String{
                eventMod.eventID = id
            }
            
            if let event = items["name"] as? String{
                eventMod.eventName = event
            }
            
            if  let eventStat = items["status"] as? NSNumber {
                eventMod.eventStatus = Int(eventStat)
                if eventStat == 1{
                    if  let serName = items["name"] as? String {
                        if profileDictModel.servicesList.length == 0{
                            profileDictModel.servicesList = serName
                        }else{
                            profileDictModel.servicesList = profileDictModel.servicesList + ", " + serName
                        }
                    }
                }
            }
            profileDictModel.eventsArrayDict.append(eventMod)
        }
        
        for metaArray in (dict["metaDataArr"] as? [[String: Any]])!  {
            let metData = MetaData()
            
            if  let fieldName = metaArray["fieldName"] as? String{
                metData.fieldName = fieldName
            }
            
            if  let fieldID = metaArray["_id"] as? String {
                metData.fieldID = fieldID
            }
            
            if  let fieldType = metaArray["fieldType"] as? NSNumber {
                metData.fieldType = Int(fieldType)
            }
            
            if  let fieldMandatory = metaArray["isManadatory"] as? NSNumber {
                metData.isMandatory = Int(fieldMandatory)
            }
            
            if let serUnits = metaArray["data"] as? String{
                metData.description = serUnits
            }
            
            if let fieldDesc = metaArray["Description"] as? String{
                metData.fieldDesc = fieldDesc
            }
            
            if let predefinedData = metaArray["preDefined"] as? [[String:Any]]{
                for predefined in predefinedData{
                    let existData = Predefined()
                    
                    if let name = predefined["name"] as? String{
                        existData.name = name
                    }
                    
                    if let image = predefined["icon"] as? String{
                        existData.image = image
                    }
                    
                    metData.predefinedData.append(existData)
                }
            }
            
            profileDictModel.metaData.append(metData)
        }
        
        for service in (dict["services"] as? [[String: Any]])!  {
            let serviceMod = ServicesData()
            
            if  let id = service["id"] as? String{
                serviceMod.serviceId = id
            }
            
            if  let serName = service["name"] as? String {
                serviceMod.serviceName = serName
            }
            
            if  let serPrice = service["price"] as? NSNumber {
                serviceMod.servicePrice = Int(serPrice)
            }
            
            if let serUnits = service["unit"] as? String{
                serviceMod.serviceUnits = serUnits
            }
            profileDictModel.servicesArrayDict.append(serviceMod)
        }
        return profileDictModel
    }
}

