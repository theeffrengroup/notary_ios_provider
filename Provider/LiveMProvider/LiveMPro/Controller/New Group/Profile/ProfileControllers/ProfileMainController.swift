//
//  ProfileMainController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 01/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileMainController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    var profileModelObject  = ProfileMod()
    var containerLayoutConstraint = NSLayoutConstraint()
    
    @IBOutlet weak var profileTableView: UITableView!
    
    @IBOutlet weak var blurrViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var blurrView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var navigationTopView: UIView!
    
    var categoryArray = [Categories]()
    @IBOutlet weak var artistName: UILabel!
    
    var categories = [myListingPlaceHolders.PayoutMethod,
                      myListingPlaceHolders.Mylisting,
                      myListingPlaceHolders.Myratecard,
                      myListingPlaceHolders.MyDocuments,
                      myListingPlaceHolders.Wallet,
                      myListingPlaceHolders.Support,
                      myListingPlaceHolders.Helpcenter,
                      myListingPlaceHolders.LiveChat,
                      myListingPlaceHolders.Share,
                      myListingPlaceHolders.GoTaskerPro,
                      myListingPlaceHolders.Reviews]
                      //myListingPlaceHolders.Portal]
    
    var categoryImages = ["payment_method_icon",
                          "my_listing_icon",
                          "ratecard",
                          "document",
                          "wallet",
                          "faq_icon",
                          "help_center_icon",
                          "live_chat_icon_off",
                          "share_icon",
                          "gt",
                          "reviews_icon_offmdpi"]
                          //"portal"]
    
    var documentIsEmpty = true
    var rateCardModel = RatecardViewModel()
    var serviceCategories =  [ServiceCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.layer.borderWidth = 2.0
        profileImage.layer.borderColor = COLOR.APP_COLOR.cgColor
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if self.profileTableView.contentOffset.y < -180 {
            headerView.frame = CGRect(x:0, y: self.profileTableView.contentOffset.y, width:self.profileTableView.bounds.size.width, height: -self.profileTableView.contentOffset.y)
        }
        
        let yOffset = scrollView.contentOffset.y //+ 64
        
        if yOffset > 0 {
            
            if yOffset < headerView.frame.size.height - 64 {
                
                //Scroll to Top
                if blurrView.frame.size.height <= 64 {
                    
                    //show Navigation
                    self.navigationTopView.isHidden = false
                    Helper.shadowView(sender: self.navigationTopView, width: UIScreen.main.bounds.size.width, height: 64)
                    //     self.navigationTitleView.isHidden = false
                    
                    blurrViewTopConstraint.constant = headerView.frame.size.height - 64 - 1
                    blurrView.alpha = 0
                    
                    // print("\nNo - 1, y = \(yOffset)")
                    
                } else {
                    
                    hideNavigation()
                    blurrViewTopConstraint.constant = yOffset
                    blurrView.alpha = yOffset/(headerView.frame.size.height - 64)
                    
                    //   print("\nNo - 2, y = \(yOffset)")
                    
                }
            } else {
                
                self.navigationTopView.isHidden = false
                Helper.shadowView(sender: self.navigationTopView, width: UIScreen.main.bounds.size.width, height: 64)
                //  self.navigationTitleView.isHidden = false
                
                blurrViewTopConstraint.constant = headerView.frame.size.height - 64 - 1
                blurrView.alpha = 0
                //  print("\nNo - 3, y = \(yOffset)")
                
            }
            
        } else {
            hideNavigation()
            blurrViewTopConstraint.constant = 0
            blurrView.alpha = 0
            //  print("\nNo - 4, y = \(yOffset)")
        }
    }
    
    func hideNavigation() {
        
        self.navigationTopView.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view?.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    func showNavigation() {
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.view?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(false)
        self.profileDetails()
        hideNavigation()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Get profile data API
    func profileDetails(){
        var isApiCalledIOnce = false
        self.profileModelObject.profileDetailsService( completionHandler: { (succeeded,ProfileMod,category) in
            self.categoryArray = category
            self.profileResponseData(succeeded: succeeded,profileData:ProfileMod)
            if isApiCalledIOnce == false{
                self.rateCardModel.getTheRateCardDetails() { (success, categoreis) in
                    isApiCalledIOnce = true
                    self.serviceCategories = categoreis
                    
                    for doc in self.categoryArray{
                        if doc.categoryDocs.count > 0{
                           self.documentIsEmpty = false
                        }
                    }
                    self.profileTableView.reloadData()
                }
            }
        })
    }
    
    /// Artist profile data
    ///
    /// - Parameters:
    ///   - succeeded: acknowledges service succeeded r not
    ///   - profileData: response data
    func profileResponseData(succeeded: Bool,profileData:ProfileMod) {
        if succeeded{
            profileModelObject = profileData
            profileName.text = profileModelObject.firstName + " " + profileModelObject.lastName
            artistName.text = profileModelObject.firstName + " " + profileModelObject.lastName
            let imageURL = profileModelObject.profilePic
            self.profileImage.kf.setImage(with: URL(string: imageURL),
                                          placeholder:UIImage.init(named: "signup_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: { image, error, cacheType, imageURL in
            })
        }else{
            Helper.hidePI()
        }
    }
    
    @IBAction func profileImageAction(_ sender: UIButton) {
        performSegue(withIdentifier: "toProfileDetails", sender: self)
    }
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        switch segue.identifier! {
        case "toProfileDetails":
            let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
            animatedTabBar.animationTabBarHidden(true)
            if let nextScene = segue.destination as? ProfileViewController {
                nextScene.profileDetails = profileModelObject
            }
            break
        case "toMyListing":
            let nav = segue.destination as! UINavigationController
            if let nextScene:MyListingController  = nav.viewControllers.first as! MyListingController? {
                nextScene.profileData = profileModelObject
            }
            break
            
        case "toMutlipleCat":
            let nav = segue.destination as! UINavigationController
            if let nextScene:CategoriesVC  = nav.viewControllers.first as! CategoriesVC? {
                nextScene.categories = self.serviceCategories
            }
            break
            
        case "fixedHourly":
            let nav = segue.destination as! UINavigationController
            if let nextScene:HourlyFixedVC  = nav.viewControllers.first as! HourlyFixedVC? {
                nextScene.singleCategory = self.serviceCategories[0]
                nextScene.canEditHourFixed = self.serviceCategories[0].billingModel
            }
            break
            
        case "onlyHourly":
            let nav = segue.destination as! UINavigationController
            if let nextScene:HourlyViewController  = nav.viewControllers.first as! HourlyViewController? {
                nextScene.singleCategory = self.serviceCategories[0]
                nextScene.canEditHourFixed = self.serviceCategories[0].billingModel
            }
            break
            
        case "toSingleFixedCat":
            let nav = segue.destination as! UINavigationController
            if let nextScene:FixedServicesVC  = nav.viewControllers.first as! FixedServicesVC? {
                nextScene.services = self.serviceCategories[0].categoryServices
                nextScene.subcatData = self.serviceCategories[0].subCategoryData
                nextScene.canEditServices = self.serviceCategories[0].billingModel
                nextScene.catName = self.serviceCategories[0].categoryName
            }
            break
            
        case "toUpdateDocs" :
            
            let nav = segue.destination as! UINavigationController
            if let nextScene:CategoryViewController  = nav.viewControllers.first as! CategoryViewController? {
                if self.categoryArray.count == 1{
                    nextScene.categoryID = self.categoryArray[0].catID
                    nextScene.categorytit = self.categoryArray[0 ].catName
                    nextScene.documents = self.categoryArray[0].categoryDocs
                    nextScene.fromWhichScreen = 2
                }
            }
            break
            
        case "documentsView":
            let nav = segue.destination as! UINavigationController
            if let nextScene:EditCategoryVC  = nav.viewControllers.first as! EditCategoryVC? {
                nextScene.categories = self.categoryArray
            }
            break
        default:
            break
        }
    }
}

extension ProfileMainController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !self.documentIsEmpty{
            return categories.count
        }else{
            return categories.count - 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileMainCell", for: indexPath) as! ProfileMainControllerCell
        if !self.documentIsEmpty{
            cell.updateTheNameImage(name:categories[indexPath.row] , image:UIImage(named: categoryImages[indexPath.row])!)
        }else{
            if indexPath.row > 2 {
                cell.updateTheNameImage(name:categories[indexPath.row + 1] , image:UIImage(named: categoryImages[indexPath.row + 1])!)
            } else {
                cell.updateTheNameImage(name:categories[indexPath.row] , image:UIImage(named: categoryImages[indexPath.row])!)
            }
//            if indexPath.row == 3{
//                cell.updateTheNameImage(name:categories[11] , image:UIImage(named: categoryImages[3])!)
//            }else{
//                cell.updateTheNameImage(name:categories[indexPath.row] , image:UIImage(named: categoryImages[indexPath.row])!)
//            }
        }
        return cell
    }
    
}

extension ProfileMainController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "toPayoutMethod", sender: self)
            break
        case 1:
            performSegue(withIdentifier: "toMyListing", sender: self)
            break
        case 2:
            if self.serviceCategories.count > 1{
                performSegue(withIdentifier: "toMutlipleCat", sender: self)
            }else if self.serviceCategories.count == 1{
                switch self.serviceCategories[0].billingModel {
                case 1,5:
                    performSegue(withIdentifier: "toSingleFixedCat", sender: self)
                    break
                case 2,6:
                    performSegue(withIdentifier: "fixedHourly", sender: self)
                    break
                case 3,4:
                    performSegue(withIdentifier: "onlyHourly", sender: self)
                    break
                default:
                    break
                }
            }
            break
         
            
        case 3:
            if !self.documentIsEmpty{
                if self.categoryArray.count == 1{
                    performSegue(withIdentifier: "toUpdateDocs", sender: self)
                }else{
                    performSegue(withIdentifier: "documentsView", sender: self)
                }
                break
            }else{
               // performSegue(withIdentifier: "viewPortal", sender: self)
                performSegue(withIdentifier: "toWallet", sender: self)
                break
            }
        case 4:
            if !self.documentIsEmpty{
                performSegue(withIdentifier: "toWallet", sender: self)
                break
            }
            else{
                performSegue(withIdentifier: "toFAQ", sender: self)
                break
            }
        case 5:
            
            if !self.documentIsEmpty{
               performSegue(withIdentifier: "toFAQ", sender: self)
                break
            }
            else{
                performSegue(withIdentifier: "toHelpCenter", sender: self)
                break
            }
            
        case 6:
            if !self.documentIsEmpty{
               performSegue(withIdentifier: "toHelpCenter", sender: self)
                break
            }
            else{
                 performSegue(withIdentifier: "openLiveChat", sender: self)
                break
            }
            
        case 7:
            if !self.documentIsEmpty{
                performSegue(withIdentifier: "openLiveChat", sender: self)
                break
            }
            else{
                performSegue(withIdentifier: "toShare", sender: self)
                break
            }
          
        case 8:
            if !self.documentIsEmpty{
                 performSegue(withIdentifier: "toShare", sender: self)
                break
            }
            else{
                performSegue(withIdentifier: "toLiveM", sender: self)
                break
            }
            
        case 9 :
            
            if !self.documentIsEmpty{
                performSegue(withIdentifier: "toLiveM", sender: self)
                break
            }
            else{
                performSegue(withIdentifier: "toReviews", sender: self)
                break
            }
            
         
        case 10:
            if !self.documentIsEmpty{
               performSegue(withIdentifier: "toReviews", sender: self)
                break
            }
            else{
                
                break
            }
            
            break
//        case 11:
//            performSegue(withIdentifier: "viewPortal", sender: self)
//            break
        default:
            break
        }
        
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

