//
//  ProfileViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher


class ProfileViewController: UIViewController ,CountryPickerDelegate{
    var profilePicUrl = String()
    var activeTextField = UITextField()
    
    
    @IBOutlet weak var typeOfService: UILabel!
    
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var dateOFBirth: UITextField!
    @IBOutlet weak var emailID: UITextField!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var mobileNumber: UITextField!
    @IBOutlet var firstName: UITextField!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryCode: UILabel!
    
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var genderLabel: UILabel!
    
    
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var dateOfBirthButton: UIButton!
    
    @IBOutlet weak var bottomPickerView: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    var profileDetails = ProfileMod()
    var pickedImage = UIImageView()
    var email = String()
    var isCameraOpened = false
    
    @IBOutlet var profileImage: UIImageView!
    var profileUrl = String()
    
    var temp = [UploadImage]()
    var updateEventsModel = ProfileListModel()
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profilePicUrl = ""
        makeTextfieldsDisable()
        
        self.handlingTheProfileData(data:profileDetails)
        profileImage.layer.borderWidth = 2.0
        profileImage.layer.borderColor = COLOR.APP_COLOR.cgColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
        assign data from server
        Owner :vani
        Date : 13/03/2020
        */
        self.handlingTheProfileData(data:profileDetails)
        self.navigationController?.isNavigationBarHidden = false
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)
        setupGestureRecognizer()
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissView(){
        self.view.endEditing(true)
    }
    
    //MARK: - Removed the user interation to all textfields
    func makeTextfieldsDisable(){
        lastNameTF.isUserInteractionEnabled = false
        firstName.isUserInteractionEnabled = false
        mobileNumber.isUserInteractionEnabled = false
        emailID.isUserInteractionEnabled = false
        dateOFBirth.isUserInteractionEnabled = false
        view1.isHidden = true
        view2.isHidden = true
        view3.isHidden = true
        view4.isHidden = true
        view5.isHidden = true
        lastNameView.isHidden = true
    }
    
    //MARK: - Handling The Profile Data
    func handlingTheProfileData(data:ProfileMod){
        self.firstName.text    = data.firstName
        self.mobileNumber.text = data.mobileNum
        self.countryCode.text  = data.countryCode
        self.typeOfService.text = data.catName
        self.lastNameTF.text = data.lastName
        /*
         Get country image from country symbol
         Owner :vani
         Date : 13/03/2020
         */
//        let picker = CountryPicker.dail_code(code: data.countryCode)
        let picker = CountryPicker.dialCode(code: data.countryCodeSymbol)
        countryImage.image = picker.flag
        self.email = data.email
        self.emailID.text = data.email
        self.dateOFBirth.text = data.dob
        
        self.genderLabel.text = data.gender
        let imageURL = data.profilePic
        self.profileImage.kf.setImage(with: URL(string: imageURL),
                                      placeholder:UIImage.init(named: "signup_profile_default_image"),
                                      options: [.transition(ImageTransition.fade(1))],
                                      progressBlock: { receivedSize, totalSize in
        },
                                      completionHandler: { image, error, cacheType, imageURL in
        })
        
        let image = data.profilePic
        let name = data.firstName
        let ud = UserDefaults.standard
        ud.set(image, forKey:  USER_INFO.USERIMAGE)
        ud.set(name, forKey:  USER_INFO.USER_NAME)
        ud.synchronize()
    }
    
    //MARK: - Storing updated profileImage to Amazon
    func uploadProfileimgToAmazon(){
        
        var url = String()
        temp = [UploadImage]()
        let timeStamp  = Helper.currentTimeStamp
        url =  AMAZONUPLOAD.PROFILEIMAGE + timeStamp + ".png"
        temp.append(UploadImage.init(image: profileImage.image!, path: url))
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
        
        Helper.showPI(message:loading.load)
        profileUrl = Utility.amazonUrl  + AMAZONUPLOAD.PROFILEIMAGE + timeStamp + ".png"
        self.updateimage(proUrl: profileUrl)
        let ud = UserDefaults.standard
        ud.set(profileUrl, forKey:  USER_INFO.USERIMAGE)
        ud.synchronize()
    }
    
    // MARK: - Selet profile Image
    func selectImage() {
        isCameraOpened = true
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image",
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel",
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera",
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery",
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    //MARK: - Remove profile pic
    func removeProfilePic(){
        profileImage.image = UIImage.init(named: "signup_profile_default_image")
        profilePicUrl = ""
    }
    
    //MARK: - open Photo Gallary
    func chooseFromPhotoGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    //MARK: - Open Camera
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    //MARK: - Update profileImage
    func updateimage(proUrl:String){
        
        let params : [String : Any] =  ["profilePic" : proUrl,
                                        ]
        
        updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeded) in
            if succeded{
                print("successfully updated profile image")
            }else{
                
            }
        })
    }
    
    //MARK: - dismissViewAction
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    //MARK: - change profile image action
    @IBAction func changeProfilePic(_ sender: Any) {
        self.cancelDOBAction(self)
        selectImage()
    }
    
    //MARK: back to Main profileVC
    @IBAction func backControllerAction(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = true
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeEmailId(_ sender: Any) {
        if  editButton.isSelected{
            self.performSegue(withIdentifier: "toUpdateEmail", sender: nil)
        }
    }
    
    @IBAction func changePhoneNumber(_ sender: Any) {
        if  editButton.isSelected{
            self.cancelDOBAction(self)
            self.performSegue(withIdentifier: "changePhone", sender: nil)
        }
    }
    
    @IBAction func countryPickerAction(_ sender: UIButton) {
        self.cancelDOBAction(self)
        if editButton.isSelected {
            self.performSegue(withIdentifier: "changeCountryCode", sender: nil)
        }
    }
    
    @IBAction func LogoutAction(_ sender: UIButton) {
        self.alertForLogout()
        
    }
    
    @IBAction func cancelDOBAction(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottomPickerView.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    @IBAction func selectDOBAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottomPickerView.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd MMMM yyyy"
        dateOFBirth.text = dateFormatter.string(from: datePicker.date)
        
        
        let dateFormatter1 = DateFormatter.initTimeZoneDateFormat()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        self.updateDateofBirth(date: dateFormatter1.string(from: datePicker.date))
        
    }
    
    @IBAction func changeName(_ sender: Any) {
        performSegue(withIdentifier: "changeName", sender: nil)
    }
    
    //MARK: - change password action
    @IBAction func passwordChangeAction(_ sender: UIButton) {
        
        CheckOldPassword.instance.show()
        CheckOldPassword.instance.delegate = self
    }
    
    @IBAction func selectDataOFBirthAction(_ sender: Any) {
        if  editButton.isSelected{
            UIView.animate(withDuration: 0.5,
                           delay: 0.1,
                           options: .curveEaseOut,
                           animations: {() -> Void in
                            self.bottomPickerView.constant = 0
                            self.view.layoutIfNeeded()
            }, completion: {(_ finished: Bool) -> Void in
                print("Completed")
            })
        }
    }
    
    
    @IBAction func editAction(_ sender: UIButton) {
        if  editButton.isSelected{
            editButton.isSelected = false
            firstName.isUserInteractionEnabled = false
            lastNameTF.isUserInteractionEnabled = false
            mobileNumber.isUserInteractionEnabled = false
            emailID.isUserInteractionEnabled = false
            view1.isHidden = true
            view2.isHidden = true
            view3.isHidden = true
            view4.isHidden = true
            view5.isHidden = true
            lastNameView.isHidden = true
            dateOfBirthButton.isHidden = true
            changePasswordButton.isHidden = false
            logoutButton.isHidden = false
            self.updateName()
        }else{
            editButton.isSelected = true
            firstName.isUserInteractionEnabled = true
            lastNameTF.isUserInteractionEnabled = true
            mobileNumber.isUserInteractionEnabled = true
            emailID.isUserInteractionEnabled = true
            view1.isHidden = false
            view2.isHidden = false
            view3.isHidden = false
            view4.isHidden = false
            view5.isHidden = false
            lastNameView.isHidden = false
            dateOfBirthButton.isHidden = false
            changePasswordButton.isHidden = true
            logoutButton.isHidden = true
        }
        self.cancelDOBAction(self)
    }
    
    func updateName(){
        
        var params = [String : Any]  ()
        
        if !(firstName.text?.isEmpty)!  && !(lastNameTF.text?.isEmpty)!{
            params =  ["firstName" :firstName.text! as Any,
                       "lastName": lastNameTF.text! as Any]
        }else if (firstName.text?.isEmpty)!  && !(lastNameTF.text?.isEmpty)!{
            params = ["lastName": lastNameTF.text! as Any]
        }
        else if !(firstName.text?.isEmpty)!  && (lastNameTF.text?.isEmpty)!{
            params = ["firstName" :firstName.text! as Any]
        }
        
        Helper.showPI(message:loading.load)
        
        updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeded) in
            if succeded{
                self.profileDetails.firstName = self.firstName.text!
                self.profileDetails.lastName = self.lastNameTF.text!
            }else{
                
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changeCountryCode"
        {
            let nav = segue.destination as! UINavigationController
            if let picker: CountryPicker = nav.viewControllers.first as! CountryPicker?
            {
                picker.delegate = self
            }
        }else if segue.identifier == "changeName"{
            let nav = segue.destination as! UINavigationController
            if let nextScene:EditProfileController = nav.viewControllers.first as? EditProfileController{
                nextScene.delegate = self
            }
        }else if segue.identifier ==  "changePhone"{
            let nav = segue.destination as! UINavigationController
            if let nextScene:ChangeNumberViewController = nav.viewControllers.first as? ChangeNumberViewController{
                nextScene.delegate = self
            }
        }else if segue.identifier == "toUpdateEmail"{
            let nav = segue.destination as! UINavigationController
            if let nextScene:ChangeEmailVC = nav.viewControllers.first as? ChangeEmailVC{
                nextScene.delegate = self
            }
        }
    }
    
    // MARK: - country delegate method
    internal func didPickedCountry(country: Country)
    {
        countryCode.text = country.dialCode
        countryImage.image = country.flag
    }
    
    func alertForLogout(){
        let alert = UIAlertController(title: "Alert", message: "Are you sure, you want to logout?", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.profileDetails.logoutActionService( completionHandler: { (response) in
                if  response{
                    Helper.hidePI()
                    Session.expired()
                }
            })
        })
        let noButton = UIAlertAction(title: "No", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            print("you pressed No, thanks button")
            // call method whatever u need
        })
        
        alert.addAction(yesButton)
        alert.addAction(noButton)
        present(alert, animated: true) //{ _ in }
    }
    
    
    //MARK: - Update profileImage
    func updateDateofBirth(date:String){
        
        let params : [String : Any] =  ["dob" : date,
                                        ]
        
        updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeded) in
            if succeded{
                print("successfully updated date of birth")
            }else{
                
            }
        })
    }
    
    //MARK:-  Crop imageview
    func cropTheImageVC()  {
        guard let image = pickedImage.image else {
            return
        }
        let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)
    }
}

//MARK: - Imagepicker delegate
extension ProfileViewController:UIImagePickerControllerDelegate{
    /*The image is not getting dispalyed when when selected because this method is deprecated use th below method
    Owner : Vani
    Date : 27/02/20202
    */
    /*
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        pickedImage.image  = image;
        self.dismiss(animated: true, completion: nil)
        self.cropTheImageVC()
        
        //        profileImage.image = Helper.resizeImage(image: image, newWidth: 200)
        //        self.uploadProfileimgToAmazon()
        //        profilePicUrl = "profile_profile_default_image"
        //        self.dismiss(animated: true, completion: nil)
    }*/
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            pickedImage.image  = image
        }
        self.dismiss(animated: true, completion: nil)
        self.cropTheImageVC()
    }
}

extension ProfileViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        dismissView()
        return true
    }
}

//MARK: - ChangePassword delegate
extension ProfileViewController:CheckPasswordDelegate{
    func AuthorisedToChangeThePassword() {//
        performSegue(withIdentifier: "toChangePassword", sender: nil)
    }
}

extension ProfileViewController : EditProfileDelegate{
    func updatedName(firstNam: String, lastName: String) {
        firstName.text = firstNam
        lastNameTF.text = lastName
    }
}

extension ProfileViewController : EditEmailIdDelegate{
    func updatedNewEmailID(emailID: String) {
        self.emailID.text = emailID
    }
}

extension ProfileViewController:ChangeNumberDelegate{
    func updatedNewNumber(numberNew: String, countryCode: String,countrySymbol:String) {
        /*
        Get country image from country symbol
        Owner :vani
        Date : 13/03/2020
        */
//        let picker = CountryPicker.dail_code(code: countryCode)
        let picker = CountryPicker.dialCode(code: countrySymbol)
        countryImage.image = picker.flag
        self.countryCode.text = countryCode
        self.mobileNumber.text = numberNew
    }
}

extension ProfileViewController:UINavigationControllerDelegate{
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        // navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}


extension ProfileViewController :CropViewControllerDelegate{
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        pickedImage.image = image
        profileImage.image = image
        controller.dismiss(animated: true, completion: nil)
        self.uploadProfileimgToAmazon()
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        
        controller.dismiss(animated: true, completion: nil)
    }
}



