//
//  MyListingController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 09/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxCocoa
import RxAlamofire
import RxSwift

class MyListingController: UIViewController,UIScrollViewDelegate,UINavigationControllerDelegate{
    @IBOutlet weak var addPreviousImagesLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var removeImage: UIButton!
    @IBOutlet weak var listingTableView: UITableView!
    
    @IBOutlet weak var bottomContrainDatePicker: NSLayoutConstraint!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    var index: IndexPath? = nil
    var titles: [String] = [ myListingPlaceHolders.aboutTitle,
                             myListingPlaceHolders.locationTitle,
                             myListingPlaceHolders.radiusTitle,
                             ]
    var profileStat = 0
    var artistAddress = ""
    var gigTimeService = [ServicesData]()
    var profileData:ProfileMod?
    var updateEventsModel = ProfileListModel()
    var pageVC: ListingPageVC?
    var maxRadVal = 0
    var minRadVal = 0
    var exactRadiusVal = 0
    var indexSection = -1
    var indexRow = -1
    
    var model =  listingImage.sharedInstance
    
    override func viewDidLoad() {
        model.images = (profileData?.images)!
        super.viewDidLoad()
        updateTheTableViewData()
        updatePage()
        
        addPreviousImagesLabel.isHidden = true
        self.removeImage.isHidden = true
        if self.model.images.count > 0{
            self.removeImage.isHidden = false
        }else if self.model.images.count == 0{
            self.removeImage.isHidden = true
            addPreviousImagesLabel.isHidden = false
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        listingTableView.reloadData()
    }
    
    /// Configure Page
    func updatePage(){
        self.pageControl.defersCurrentPageDisplay = true
        self.pageControl.numberOfPages            = self.model.images.count
        self.pageControl.currentPage              = 0
        Helper.hidePI()
    }
    
    func updateTheTableViewData()  {
        profileStat = (profileData?.profileStatus)!
        artistAddress = (profileData?.address)!
        maxRadVal = (profileData?.maxRadius)!
        minRadVal = (profileData?.minRadius)!
        exactRadiusVal = (profileData?.radius)!
        gigTimeService = (profileData?.servicesArrayDict)!
    }
    
    func updateStatusToServer(status:Int) {
        var params = [String : Any]()
        params =  [
            "profileStatus" :status]
        self.updateTheProviderUpdatedData(params: params , fromMain:1)
    }
    
    func updateTheProviderUpdatedData (params:[String:Any] , fromMain:Int){
        updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeded) in
            if succeded{
                if fromMain == 1{
                    Helper.hidePI()
                }else if fromMain == 2{
                    self.addTheWorkImage()
                }else if fromMain == 3{
                    self.deleteTheWorkImage()
                }
            }else{
                Helper.hidePI()
            }
        })
        
    }
    
    func addTheWorkImage()  {
        if self.model.images.count > 1{
            DispatchQueue.main.async {
                self.removeImage.isHidden = false
            }
        }
        
        if let view = self.children.first as? ListingPageVC {
            view.images = self.model.images
            DispatchQueue.main.async {
                self.removeImage.isHidden = false
                self.addPreviousImagesLabel.isHidden = true
                view.setViewControllers([view.getViewControllerAtIndex(self.pageControl.currentPage)],
                                        direction: UIPageViewController.NavigationDirection.forward,
                                        animated: true,
                                        completion: nil)
                self.updatePage()
            }
        }
        
    }
    
    func deleteTheWorkImage()  {
        if let view = self.children.first as? ListingPageVC {
            view.images.remove(at: self.pageControl.currentPage)
            if self.model.images.count != self.pageControl.currentPage{
                DispatchQueue.main.async {
                    view.setViewControllers([view.getViewControllerAtIndex(self.pageControl.currentPage)],
                                            direction: UIPageViewController.NavigationDirection.forward,
                                            animated: true,
                                            completion: nil)
                    self.updatePage()
                }
                
                
            } else if self.model.images.count >= 1{
                DispatchQueue.main.async {
                    view.setViewControllers([view.getViewControllerAtIndex(self.pageControl.currentPage - 1)],
                                            direction: UIPageViewController.NavigationDirection.forward,
                                            animated: true,
                                            completion: nil)
                    self.updatePage()
                }
            }else{
                DispatchQueue.main.async {
                    view.setViewControllers([view.getViewControllerAtIndex(0)], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                    self.removeImage.isHidden = true
                    self.addPreviousImagesLabel.isHidden = false
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func statusUpdateSwitch(_ sender: Any) {
        if profileStat == 0 {
            profileStat = 1
            self.updateStatusToServer(status:1)
        }else{
            profileStat = 0
            self.updateStatusToServer(status:0)
        }
    }
    
    
    @IBAction func removeVisibleImage(_ sender: Any) {
        alertToDeleteTheImage()
    }
    
    func alertToDeleteTheImage(){
        let alert = UIAlertController(title: "Alert", message: "Are you sure, you want to Delete??", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.model.images.remove(at: self.pageControl.currentPage)
            self.profileData?.images.remove(at: self.pageControl.currentPage)
            if self.model.images.count == 0{
                self.removeImage.isHidden = true
            }
            let paramDict:[String:Any] = ["workImage":self.model.images]
            self.updateTheProviderUpdatedData(params: paramDict , fromMain:3)
            
        })
        let noButton = UIAlertAction(title: "No", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            print("you pressed No, thanks button")
            // call method whatever u need
        })
        
        alert.addAction(yesButton)
        alert.addAction(noButton)
        present(alert, animated: true) //{ _ in }
    }
    
    @IBAction func addMoreImagesToContainer(_ sender: Any) {
        selectImage()
    }
    
    // MARK: - Selet profile Image
    func selectImage() {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image",
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel",
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera",
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery",
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK: - open Photo Gallary
    func chooseFromPhotoGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = true
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    //MARK: - Open Camera
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            imagePickerObj.allowsEditing = true
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    @IBAction func hideDatePicker(_ sender: Any) {
        
    }
    
    @IBAction func selectTheDate(_ sender: Any) {
        
    }
    
    @IBAction func listingAddAction(_ sender: UIButton) {
        
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.listingTableView)
        index = self.listingTableView.indexPathForRow(at: buttonPosition)
        let Newbutton: UIButton = sender
        if index?.section == 0{
            if (profileData?.metaData[Newbutton.tag].fieldType == 2) {
                performSegue(withIdentifier: "toSelectFields", sender: Newbutton.tag)
            }
            else if (profileData?.metaData[Newbutton.tag].fieldType == 1) {
                performSegue(withIdentifier: "toAboutMe", sender: Newbutton.tag)
            }else  {
                performSegue(withIdentifier: "metaData", sender: Newbutton.tag)
            }
        }else if index?.section == 1{
            
            performSegue(withIdentifier: "toLocation", sender: self)
        }else if index?.section == 2{
            
            performSegue(withIdentifier: "toRadius", sender: self)
        }
    }
    
    
    ///This function calculates the height of content Label  ListingCell row's and returns the height of the label.
    func heightForView(text: String, width: CGFloat) -> CGFloat {
        
        let label: UILabel = UILabel(frame: CGRect(x: 0,
                                                   y: 0,
                                                   width: width,
                                                   height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier! {
        case "toAboutMe":
            let nav = segue.destination as! UINavigationController
            
            if let aboutMeVC:AboutMeController  = nav.viewControllers.first as? AboutMeController {
                if let cell = listingTableView.cellForRow(at: index!) as? ListingCell {
                    aboutMeVC.aboutUser = cell.content.text!
                }
                aboutMeVC.dataMeta =  profileData?.metaData[(index?.row)!]
                aboutMeVC.delegate = self
            }
            break
            
        case "toRadius":
            let nav = segue.destination as! UINavigationController
            if let nextScene:RadiusController  = nav.viewControllers.first as!
                RadiusController? {
                nextScene.delegate = self
                nextScene.maxVal = maxRadVal
                nextScene.minVal = minRadVal
                nextScene.exactRadius = exactRadiusVal
                
                    nextScene.distanceUnit = Utility.DistanceUnits
           
            }
            break
        case "toLocation":
            let nav = segue.destination as! UINavigationController
            if let nextScene:ArtistAddressVC  = nav.viewControllers.first as!
                ArtistAddressVC? {
                nextScene.delegate = self
            }
            break
        case "embedListing":
            self.pageVC = segue.destination as? ListingPageVC
            model.images = (profileData?.images)!
            self.pageVC?.images = (profileData?.images)!
            self.pageVC?.pageContentDelegate = self
            break
        case "toSelectFields":
            let nav = segue.destination as! UINavigationController
            
            if let addFields:AddFieldsVC  = nav.viewControllers.first as? AddFieldsVC {
                addFields.dataMeta =  profileData?.metaData[sender as! Int]
                
            }
            break
        case "metaData":
            let nav = segue.destination as! UINavigationController
            
            if let addFields:MetaDataController  = nav.viewControllers.first as? MetaDataController {
                addFields.dataMeta =  profileData?.metaData[sender as! Int]
                
                
            }
            break
        default:
            break
        }
    }
    
    @objc func readMoreSelected(_ sender : UIButton){
        if indexSection == sender.tag / 100 && indexRow == sender.tag % 100{
            indexSection = -1
            indexRow = -1
        }else{
            indexSection = sender.tag / 100
            indexRow = sender.tag % 100
        }
        listingTableView.reloadData()
    }
    
    func updateTheContainerImages(imageUrl:String) {
        profileData?.images.append(imageUrl)
        self.model.images.append(imageUrl)
        
        let paramDict:[String:Any] = ["workImage":self.model.images]
        self.updateTheProviderUpdatedData(params: paramDict, fromMain:2)
        
    }
}

//MARK: - Imagepicker delegate
extension MyListingController:UIImagePickerControllerDelegate{
    /*The image is not getting dispalyed when when selected because this method is deprecated use th below method
    Owner : Vani
    Date : 27/02/20202
    */
    /*
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        Helper.showPI(message: loading.load)
        UploadImageModel().updateTheImageToAamzon(image: image, imagePath: AMAZONUPLOAD.WORKIMAGES +  Helper.currentTimeStamp  + ".png") { (success, imageURl) in
            self.updateTheContainerImages(imageUrl: imageURl)
            
            
        }
        self.dismiss(animated: true, completion: nil)
    } */
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            Helper.showPI(message: loading.load)
            UploadImageModel().updateTheImageToAamzon(image: image, imagePath: AMAZONUPLOAD.WORKIMAGES +  Helper.currentTimeStamp  + ".png") { (success, imageURl) in
                self.updateTheContainerImages(imageUrl: imageURl)
                
                
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
}


extension MyListingController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return profileData!.metaData.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell") as! ListingCell
            cell.selectionStyle = .none
            
            cell.addButton.tag = indexPath.row
            
            if profileData?.metaData[indexPath.row].description.length == 0 {
                cell.readMore.isHidden = true
                cell.addButton.setTitle(myListingPlaceHolders.add, for: .normal)
                cell.content.text = profileData?.metaData[indexPath.row].fieldDesc
                cell.content.textColor = UIColor.lightGray
                cell.title.text = profileData?.metaData[indexPath.row].fieldName
                cell.addButton.tag = indexPath.row
            }else{
                cell.addButton.tag = indexPath.row
                cell.content.textColor = COLOR.MYLISTINGTEXTCOLOR
                cell.addButton.setTitle(myListingPlaceHolders.edit, for: .normal)
                cell.content.text = profileData?.metaData[indexPath.row].description
                cell.title.text = profileData?.metaData[indexPath.row].fieldName
                let words = Helper.countLabelLines(label:cell.content)
                if words > 1{
                    
                    cell.readMore.tag =  (indexPath.section*100)+indexPath.row
                    cell.readMore.addTarget(self, action: #selector(readMoreSelected(_:)), for:.touchUpInside)
                    
                    if self.indexSection == indexPath.section && self.indexRow == indexPath.row{
                        cell.readMore.isSelected = true
                    }else{
                        cell.readMore.isSelected = false
                    }
                }else{
                    cell.readMore.isHidden = true
                }
            }
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell") as! ListingCell
            cell.title.text = "Location"
            cell.addButton.tag = indexPath.section
            if artistAddress.length == 0 {
                cell.readMore.isHidden = true
                cell.addButton.setTitle(myListingPlaceHolders.add, for: .normal)
                cell.content.text = ListingConstants.contents[indexPath.row]
                cell.content.textColor = UIColor.lightGray
            }else{
                cell.readMore.isHidden = true
                cell.content.text = artistAddress
                cell.addButton.setTitle(myListingPlaceHolders.edit, for: .normal)
                cell.content.textColor = COLOR.MYLISTINGTEXTCOLOR
            }
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell") as! ListingCell
            cell.title.text = "Radius"
            
            cell.addButton.tag = indexPath.section
            
            if exactRadiusVal == 0 {
                cell.readMore.isHidden = true
                cell.addButton.setTitle(myListingPlaceHolders.add, for: .normal)
                cell.content.text = ListingConstants.contents[indexPath.row]
                cell.content.textColor = UIColor.lightGray
            }else{
                cell.readMore.isHidden = true
                
                    cell.content.text = String(exactRadiusVal) + Utility.DistanceUnits
            
                
                cell.addButton.setTitle(myListingPlaceHolders.edit, for: .normal)
                cell.content.textColor = COLOR.MYLISTINGTEXTCOLOR
            }
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "statusCell") as! StatusCell
            cell.content.text = ListingConstants.Status
            if profileStat == 0 {
                cell.statusSwitch.setOn(false, animated: true)
            }else{
                cell.statusSwitch .setOn(true, animated: true)
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension MyListingController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var newIndex = indexPath
        switch newIndex.section {
            
        case 0:
            var width: CGFloat = 0.0
            var height: CGFloat = 0.0
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell") as! ListingCell
            
            width = cell.content.frame.width
            width = width + 35.0
            if profileData?.metaData[newIndex.row].description.length == 0 {
                height =  heightForView(text: (profileData?.metaData[newIndex.row].fieldDesc)! , width: width)
            }else{
                height =  heightForView(text: (profileData?.metaData[newIndex.row].description)! , width: width)
                if indexSection != indexPath.section || indexRow != indexPath.row{
                    if height > 30  {
                        height = 30
                    }
                }
            }
            height = height + 54.0
            return height
            
        case 1:
            var width: CGFloat = 0.0
            
            var height: CGFloat = 0.0
            let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell") as! ListingCell
            width = cell.content.frame.width
            width = width + 35.0
            if artistAddress.length == 0 {
                height =  heightForView(text: ListingConstants.contents[newIndex.row] , width: width)
            }else{
                height =  heightForView(text: artistAddress , width: width)
            }
            height = height + 54.0
            return height
        case 2:
            var width: CGFloat = 0.0
            var height: CGFloat = 0.0
            let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell") as! ListingCell
            width = cell.content.frame.width
            width = width + 35.0
            if exactRadiusVal == 0 {
                height =  heightForView(text: ListingConstants.contents[newIndex.row] , width: width)
            }else{
                height =  heightForView(text: String(exactRadiusVal) , width: width)
            }
            height = height + 54.0
            return height
            
        case 3:
            return 100
            
        default:
            return 44
        }
    }
}

extension MyListingController: AboutMeDelegate {
    
    func updateUserStaus(status: String) {
        listingTableView.reloadData()
    }
}

extension MyListingController:MyRadiusDelegate{
    func updateTheNewRadius(radius:Int){
        exactRadiusVal = radius
        listingTableView.reloadData()
    }
}

extension MyListingController:ArtistAddressDelegate{
    func updateArtistDelegate(address: String,addressID:String) {
        artistAddress = address
        listingTableView.reloadData()
    }
}


extension MyListingController: LiveMHelpPageVCDelegate {
    
    func didSelect(index: Int) {
        pageControl.currentPage = index
    }
}

