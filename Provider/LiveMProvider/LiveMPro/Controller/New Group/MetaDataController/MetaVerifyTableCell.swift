//
//  MetaVerifyTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 03/04/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class MetaVerifyTableCell: UITableViewCell {
    @IBOutlet weak var metaLabel: UILabel!
    
    @IBOutlet weak var docImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
