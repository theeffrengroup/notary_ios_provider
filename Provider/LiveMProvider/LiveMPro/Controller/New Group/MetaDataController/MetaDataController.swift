//
//  MetaDataController.swift
//  LSP
//
//  Created by Vengababu Maparthi on 03/04/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class MetaDataController: UIViewController {
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var metaDataTableView: UITableView!
    @IBOutlet weak var controllerTitle: UILabel!
    @IBOutlet weak var bottomDatePickerConstraint: NSLayoutConstraint!
    @IBOutlet weak var metaDatePicker: UIDatePicker!
    
    var selectedDate      = ""
    var updateEventsModel = ProfileListModel()
    var dataMeta:MetaData?
    var typeofMeta        = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.typeofMeta = (self.dataMeta?.fieldType)!
        
        
        self.controllerTitle.text = self.dataMeta?.fieldName
        

        if dataMeta!.description.length > 1 {
            selectedDate = (self.dataMeta?.description)!
        }else{
            selectedDate = (self.dataMeta?.fieldDesc)!
        }
        
        if typeofMeta == 3{
            metaDatePicker.minimumDate = Date.dateWithDifferentTimeInterval()
        }
        
        metaDataTableView.reloadData()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelDatePicker(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottomDatePickerConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    @IBAction func doneDatePicker(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottomDatePickerConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd MMMM yyyy"
        selectedDate = dateFormatter.string(from: metaDatePicker.date)
        metaDataTableView.reloadData()
    }
    
    
    @IBAction func editDataAction(_ sender: Any) {
        var dict = [String : Any]()
        
        switch typeofMeta {
        case 3,4:
            dict = ["metaId":self.dataMeta?.fieldID as Any,
                    "data":selectedDate]
            self.dataMeta?.description = selectedDate
        default:
            var data = ""
            for index in 0..<(self.dataMeta?.predefinedData.count)!{
                let indexPath = IndexPath(row:index, section: 0)
                if let selectedRow = metaDataTableView.cellForRow(at: indexPath) {
                    if selectedRow.accessoryType == .checkmark {
                        if data.isEmpty{
                            data = self.dataMeta!.predefinedData[index].name
                        }else{
                            data = data + "," + self.dataMeta!.predefinedData[index].name
                        }
                    }
                }
            }
            self.dataMeta?.description = data
            dict = ["metaId":self.dataMeta?.fieldID as Any,
                    "data":data]
        }
        
        let params:[String:Any] = ["metaData":dict]
        
        updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeded) in
            if succeded{
                self.dismiss(animated: true, completion: nil)
            }else{
                
            }
        })
    }
    
    
    
    @IBAction func selectDatePicker(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottomDatePickerConstraint.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
}

extension MetaDataController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if typeofMeta == 5 || typeofMeta == 6 {
            if tableView.cellForRow(at: indexPath)?.accessoryType == .checkmark{
                tableView.cellForRow(at: indexPath)?.accessoryType = .none
            }else{
                tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension MetaDataController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if typeofMeta == 3 || typeofMeta == 4{
            return 1
        }else if typeofMeta == 5{
            return (self.dataMeta?.predefinedData.count)!
        }else if typeofMeta == 6{
            return (self.dataMeta?.predefinedData.count)!
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch typeofMeta {
        case 3,4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "metDatePicker") as! DatePickerTableCell
            cell.dateLabel.text = selectedDate
            return cell
            
        case 5:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "metaVerify") as! MetaVerifyTableCell
            cell.metaLabel.text = self.dataMeta?.predefinedData[indexPath.row].name
            cell.docImage.kf.setImage(with: URL(string: (self.dataMeta?.predefinedData[indexPath.row].image)!),
                                      placeholder:UIImage.init(named: "upload_image_two"),
                                      options: [.transition(ImageTransition.fade(1))],
                                      progressBlock: { receivedSize, totalSize in
            },
                                      completionHandler: { image, error, cacheType, imageURL in
            })
            
            return cell
            
        case 6:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "metaData") as! MetaTableCell
            cell.metaLabel.text = self.dataMeta?.predefinedData[indexPath.row].name
            
            return cell
        default:
            return UITableViewCell()
        }
    }
}
