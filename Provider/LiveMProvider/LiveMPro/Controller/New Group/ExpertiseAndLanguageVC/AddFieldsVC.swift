//
//  AddFieldsVC.swift
//  LSP
//
//  Created by Vengababu Maparthi on 08/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class AddFieldsVC: UIViewController {
    
    @IBOutlet weak var topContraintTableView: NSLayoutConstraint!
    @IBOutlet weak var addMoreButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fieldsTableView: UITableView!
    var count = 0
    var updateEventsModel = ProfileListModel()
    var dataMeta:MetaData?
    var desc = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = self.dataMeta?.fieldName
        if dataMeta!.description.length > 1 {
            desc = (dataMeta?.description.components(separatedBy: ","))!
            count = desc.count + count
        }
        
        addMoreButton.setTitle("Add " + (self.dataMeta?.fieldName)! , for: .normal)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveTheAddedData(_ sender: Any) {
        var data = ""
        for i in 0..<desc.count + count { // to get all the selected services
            let indexPath = IndexPath(row:i, section: 0)
            if let cell = fieldsTableView.cellForRow(at: indexPath) as? FieldsTableCell{
                if data.length > 0{
                    data = data + "," + cell.fieldsTextField.text!
                }else{
                    data = cell.fieldsTextField.text!
                }
            }
        }
        let dict:[String : Any] = ["metaId":self.dataMeta!.fieldID as Any,
                                     "data":data]
        
        let params:[String:Any] = ["metaData":dict]
        updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (succeded) in
            if succeded{
                self.dataMeta?.description = data
                self.dismiss(animated: true, completion: nil)
            }else{
                
            }
        })
    }
    
    @IBAction func backToVc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func addMoreFields(_ sender: Any) {
        count = count + 1
        fieldsTableView.reloadData()
    }
}

extension AddFieldsVC:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        textField.leftView = paddingView
        textField.leftViewMode = .always
        
        textField.layer.borderColor = COLOR.APP_COLOR.cgColor
        textField.layer.borderWidth = 2
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = COLOR.E1E1E1.cgColor
        textField.layer.borderWidth = 2
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension AddFieldsVC:UITableViewDelegate{
    
}


extension AddFieldsVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if count == 0{
            topContraintTableView.constant = 0
        }else{
            topContraintTableView.constant = 20
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            count = count - 1
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "field") as! FieldsTableCell
        if indexPath.row  < desc.count {
            cell.fieldsTextField.text =  desc[indexPath.row]
        }
        cell.fieldsTextField.tag = indexPath.row
        cell.fieldsTextField.placeholder = (self.dataMeta?.fieldName)!
        cell.fieldsTextField.layer.borderColor = COLOR.E1E1E1.cgColor
        cell.fieldsTextField.layer.borderWidth = 1
        cell.selectionStyle = .none
        return cell
    }
}


