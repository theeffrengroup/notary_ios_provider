//
//  LiveMPageHelpModel.swift
//  RunnerLive
//
//  Created by Rahul Sharma on 24/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

struct LiveMHelpModelLibrary {
    /// Data display on  help container controller
    
    var pages: [[String]] = [["Show your talent",
                            "LiveM is an instant and effective promotional channel."],
                             ["Play when you want",
                              "You have the freedom and flexibility to play whenever you have time."],
                             ["Make more money",
                              "Create your musical profile and make extra money."],
                             ["Awesome community",
                              "LiveM community is full of awesome friendly people who love live music."]]

}


struct listingImage {
    
    /// Data display on  help container controller
    static let sharedInstance = listingImage()
    
    var images = [String]()
    
//        = [
//        "https://s3.amazonaws.com/buggyapp/Drivers/DriverLincence/1516885361181_0_07.png",
//        "https://s3.amazonaws.com/buggyapp/Drivers/DriverLincence/1516885813433_0_07.png",
//        "https://s3.amazonaws.com/buggyapp/Drivers/DriverLincence/1516887407498_0_07.png",
//        "https://s3.amazonaws.com/buggyapp/Drivers/DriverLincence/1516974714898_0_07.png"]
}

