//
//  LiveMHelpPageVC.swift
//  RunnerLive
//
//  Created by Rahul Sharma on 24/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class LiveMHelpPageVC: UIPageViewController {
    
    var model = LiveMHelpModelLibrary()
    var pageContentDelegate: LiveMHelpPageVCDelegate? = nil
    var pageIndex = 0
      var index = 0
    var timer       = Timer()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.dataSource = self
        self.setViewControllers([getViewControllerAtIndex(0)],
                                direction: UIPageViewController.NavigationDirection.forward,
                                animated: true,
                                completion: nil)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
     //    self.handleTimer(interval:5)
    }
    
    // move the paging view for every 5 seconds
    
    func handleTimer(interval:Double) {
        self.timer.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval:interval,
                                          target: self,
                                          selector: #selector(updatePageView),
                                          userInfo: nil,
                                          repeats: true)
        
    }
    
    @objc func updatePageView()  {
        
        self.setViewControllers([getViewControllerAtIndex(index)],
                                direction: UIPageViewController.NavigationDirection.forward,
                                animated: true,
                                completion: nil)
        if index == 3{
            index = -1
        }
        index = index + 1
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer.invalidate()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension LiveMHelpPageVC: UIPageViewControllerDataSource {
    
    // this method gives ViewController which is befor of the current page
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        
        let ipContentVC: LiveMPageContainerController = viewController as! LiveMPageContainerController
        var index = ipContentVC.pageIndex
        
        //calculate
        if ((index == 0) || (index == NSNotFound)) {
            return nil
        }
        
        index -= 1
        return getViewControllerAtIndex(index)
    }
    
    // this method gives ViewController which is After of the current page
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        
        let ipContentVC: LiveMPageContainerController = viewController as! LiveMPageContainerController
        var index = ipContentVC.pageIndex
        
        //calculate next viewcontroller index
        if (index == NSNotFound) {
            return nil
        }
        
        index += 1
        
        if (index == model.pages.count) {
            return nil
        }
        return getViewControllerAtIndex(index)
    }
    
    public func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return model.pages.count
    }
    
    /// Get Current View Controller of Page
    ///
    /// - parameter index: Current Index
    ///
    /// - returns: UIController
    func getViewControllerAtIndex(_ index: Int) -> LiveMPageContainerController {
        
        let liveMHelpContent = self.storyboard?.instantiateViewController(withIdentifier: "LiveMHelpPageVC") as! LiveMPageContainerController
        liveMHelpContent.pageIndex = index
        liveMHelpContent.pageContentDelegate = self
        return liveMHelpContent
    }
}


extension LiveMHelpPageVC: LiveMHelpContentDelegate {
    
    //MARK: - InitialPageContentVCDelegate Method
    func didSelect1(index: Int) {
        pageIndex = index
        pageContentDelegate?.didSelect(index: pageIndex)
    }
}

