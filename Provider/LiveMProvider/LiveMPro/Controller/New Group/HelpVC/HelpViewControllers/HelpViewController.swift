//
//  HelpViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit


class HelpViewController: UIViewController {
    
    //These vars are used to play background video in the view

    @IBOutlet weak var handlingImage: UIImageView!
    
    
    
    override func viewDidLoad() {
        if(iPHONE.IS_iPHONE_5) //640x 1136
        {
            handlingImage.image = UIImage(named:"landin_640 x 1136.png")
        }else if(iPHONE.IS_iPHONE_4s) //640x960
        {
            handlingImage.image = UIImage(named:"landin_640 x 960.png")
        }else if(iPHONE.IS_iPHONE_6) //750x1334
        {
            handlingImage.image = UIImage(named:"landin_750 x 1334.png")
        }else if(iPHONE.IS_iPHONE_6_Plus) //1242x2208
        {
            handlingImage.image = UIImage(named:"landin_1242 x 2208.png")
        }else{
            handlingImage.image = UIImage(named:"landin_1125 x 2436.png")
        }
        
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
   
    }
    
    
    override func viewDidAppear(_ animated: Bool) {  
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
 
    }
    
    //MARK:- navigate to register screen
    @IBAction func signupButton(_ sender: Any) {
        performSegue(withIdentifier: "toSignUp", sender: nil)
    }
    
    //MARK:- navigate to login screen
    @IBAction func loginButton(_ sender: Any) {
    //vani  06/04/2020
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let nextVC = storyboard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
//        self.navigationController?.pushViewController(nextVC, animated: false)
//        present(nextVC, animated: true, completion: nil)
        performSegue(withIdentifier: "toSignIn", sender: nil)
    }
  
}

