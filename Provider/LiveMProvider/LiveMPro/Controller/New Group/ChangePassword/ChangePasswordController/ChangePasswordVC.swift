//
//  ChangePasswordVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 19/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
class ChangePasswordVC: UIViewController {

    @IBOutlet var reEnterPassword: FloatLabelTextField!
    @IBOutlet var newPassword: FloatLabelTextField!
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    
    var passwordModel = ChangePasswordModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    //************* hide the keyboard************//
    @IBAction func hideKeyboard(_ sender: Any) {
         self.view.endEditing(true)
        
    }
    
    ///Save the new password*******
    @IBAction func saveTheNewpassword(_ sender: Any) {
        if (newPassword.text?.isEmpty)! {
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Please enter new password"), animated: true, completion: nil)
        }else if (reEnterPassword.text?.isEmpty)! {
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Please Re-enter  password"), animated: true, completion: nil)
        }else if newPassword.text  != reEnterPassword.text{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Password mismatched"), animated: true, completion: nil)
        }else{
            self.updatePassword()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backToVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func updatePassword(){
        Helper.showPI(message:loading.update)
        let param : [String : Any] =  ["oldPassword":Utility.savedPassword,
                                       "newPassword": newPassword.text!]
        
        passwordModel.changePassword(params: param) { (succeeded) in
            if succeeded{
                KeychainWrapper.standard.set(self.newPassword.text!, forKey: USER_INFO.SAVEDPASSWORD)
                self.dismiss(animated: true, completion: nil)
            }
        }
        // add service api
    }
    

    func dismissView(){
        self.view.endEditing(true)
    }
}



// MARK: - Textfield delegate method
extension ChangePasswordVC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == newPassword{
           view1.backgroundColor = COLOR.APP_COLOR
        }else{
            view2.backgroundColor = COLOR.APP_COLOR
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == reEnterPassword && (newPassword.text?.length)! > 2{
            if Helper.isValidPassword(password:(newPassword.text)!){
                reEnterPassword.becomeFirstResponder()
            }else{
                newPassword.becomeFirstResponder()
                newPassword.text = ""
                self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.PasswordInvalid), animated: true, completion: nil)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == newPassword{
            if (textField.text?.isEmpty)!{
                view1.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }else{
                view1.backgroundColor = COLOR.APP_COLOR
            }
        }else{
            if (textField.text?.isEmpty)!{
                view2.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }else{
                view2.backgroundColor = COLOR.APP_COLOR
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case newPassword:
            reEnterPassword.becomeFirstResponder()
            break

        default:
     
            dismissView()
            break
        }
        
        return true
    }
}




