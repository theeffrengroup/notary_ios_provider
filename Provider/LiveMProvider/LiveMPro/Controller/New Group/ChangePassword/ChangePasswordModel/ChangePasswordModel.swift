
//
//  EditNumberModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 20/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import Foundation
import RxAlamofire
import RxCocoa
import RxSwift


class ChangePasswordModel:NSObject {

    let disposebag = DisposeBag()
    
    
    /// changing password for
    ///
    /// - Parameters:
    ///   - params: old password and new password
    ///   - completionHandler: returns if new password updated
    func changePassword(params:[String:Any],completionHandler:@escaping (Bool) -> ()) {
        Helper.showPI(message:loading.update)
        let rxApiCall = ChangePasswordAPI()
        rxApiCall.changePassword(paramDict:params ,method:API.METHOD.CHANGEPASSWORD)
        rxApiCall.changePassword_Response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    completionHandler(true)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
      
                case .adminNotAccepted:
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
                
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}

