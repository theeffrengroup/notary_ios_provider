//
//  ReviewsModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 07/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import Kingfisher
import SwiftKeychainWrapper
class InnerReviewData: NSObject {
    
    var reviewText = ""
    var rating = 0.0
    var reviewBy = ""
    var profilePic = ""
    var timeStamp = 0
}

class ReviewModel: NSObject {
   
    var averageRating = 0.0
    
    var reviewCount = 0
    
    var reviewInnerData = [InnerReviewData]()
    
    var apiCall = APILibrary()
    
    
    /// get the reviews of mine(artist) which given by customers
    ///
    /// - Parameters:
    ///   - index: each index (0...n) has 5 reviews
    ///   - completionHandler: returns the reviews in a model data
    func getTheReviewByIndexWise(index:String,completionHandler:@escaping(Bool,ReviewModel)->()) {
        apiCall.getMethodServiceCall(method: API.METHOD.SUBMITREVIEW + "/" + index) { (responseModel) in
            let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
            Helper.hidePI()
            
            switch responseCodes{
            case .UserLoggedOut:
                Session.expired()
                Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                break
                
            case .TokenExpired:
                let defaults = KeychainWrapper.standard
                if let sessionToken =  responseModel.data["data"]   as? String  {
                    defaults.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                    self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                        if success{
                            
                        }
                    })
                }
                break
                
            case .SuccessResponse:
                if let reviewData = responseModel.data["data"] as? [String:Any]{
                    completionHandler(true,self.parseTheReviewData(dict:reviewData))
                }
                break
                
            default:
                Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                
                break
                
            }
        }
    }
    
    //MARK: - parse the review data
    func parseTheReviewData(dict:[String:Any]) -> ReviewModel {
        let reviewData = ReviewModel()
        if let avgRating = dict["averageRating"] as? NSNumber {
            reviewData.averageRating = Double(Float(avgRating))
        }
        if let revCount = dict["reviewCount"] as? Int {
            reviewData.reviewCount = revCount
        }
        if let reviewDictArray = dict["reviews"] as? [[String:Any]]{
            for reviewDict in reviewDictArray{
                let reviews = InnerReviewData()
                if let revText = reviewDict["review"] as? String{
                    reviews.reviewText = revText
                }
                if let rating = reviewDict["rating"] as? NSNumber{
                    reviews.rating = Double(Float(rating))
                }
                if let revBy = reviewDict["reviewBy"] as? String{
                    reviews.reviewBy = revBy
                }
                if let proPic = reviewDict["profilePic"] as? String{
                    reviews.profilePic = proPic
                }
                
                if let rewTime = reviewDict["reviewAt"] as? Int{
                    reviews.timeStamp = rewTime
                }
                reviewInnerData.append(reviews)
            }
        }
        reviewData.reviewInnerData = reviewInnerData
        return reviewData
    }
}
