//
//  ReviewViewController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 02/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import FloatRatingView


class ReviewViewController: UIViewController {
    
    @IBOutlet weak var heightOfTopview: NSLayoutConstraint!
    @IBOutlet weak var noReviewLabel: UILabel!
    @IBOutlet weak var emptyReviewImage: UIImageView!
    
    @IBOutlet weak var ratingVal: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet weak var numberOfReviews: UILabel!
    var value = 0
    var refreshControl = UIRefreshControl()
    var reviewMod = ReviewModel()
    var reviewDataModel = ReviewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshTableView()
        self.getTheReview(indeVal: "0")
        reviewTableView.estimatedRowHeight = 10
        reviewTableView.rowHeight = UITableView.automaticDimension
        self.heightOfTopview.constant = 0
        self.noReviewLabel.isHidden = false
        self.emptyReviewImage.isHidden = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshTableView() {
        refreshControl = UIRefreshControl()
        refreshControl.triggerVerticalOffset = 100.0
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        self.refreshControl.beginRefreshing()
        self.reviewTableView.bottomRefreshControl = refreshControl
    }
    
    @objc func refresh() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.value += 1
            self.getTheReview(indeVal: String(describing:self.value))
        }
    }
    
    
    
    func getTheReview(indeVal:String) {
        reviewMod.getTheReviewByIndexWise(index: indeVal) { (succeeded, reviewData) in
            self.reviewDataModel = reviewData
            var reviewText = ""
            if self.reviewDataModel.reviewInnerData.count == 0{
                self.heightOfTopview.constant = 0
                self.noReviewLabel.isHidden = false
                self.emptyReviewImage.isHidden = false
            }else{
                self.heightOfTopview.constant = 65
                self.noReviewLabel.isHidden = true
                self.emptyReviewImage.isHidden = true
                if self.reviewDataModel.reviewCount != 0{
                    if self.reviewDataModel.reviewCount == 1 || self.reviewDataModel.reviewCount == 0{
                        reviewText = " Review"
                    }else{
                        reviewText = " Reviews"
                    }
                    
                    self.ratingView.rating = self.reviewDataModel.averageRating
                    self.ratingVal.text = String(format: "%.1f", self.reviewDataModel.averageRating)
                    self.numberOfReviews.text = String(describing:self.reviewDataModel.reviewCount)  + reviewText
                }
            }
           
            
            self.reviewTableView.reloadData()
            self.reviewTableView.bottomRefreshControl?.endRefreshing()
        }
    }
    
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
   
    
}

extension ReviewViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return self.reviewDataModel.reviewInnerData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! ReviewCell
        cell.updateTheReviewCell(reviewData:self.reviewDataModel.reviewInnerData[indexPath.row])
        
        
        return cell
    }
}

extension ReviewViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

