//
//  ReviewCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 02/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView
import Kingfisher

class ReviewCell: UITableViewCell {
    
    @IBOutlet weak var reviewerImage: UIImageView!
    @IBOutlet weak var reviewerName: UILabel!
    @IBOutlet weak var reviewTime: UILabel!
    @IBOutlet weak var review: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateTheReviewCell(reviewData:InnerReviewData)  {
        reviewerName.text = reviewData.reviewBy
        review.text = reviewData.reviewText
        ratingView.rating = reviewData.rating
        
        reviewerImage.kf.setImage(with: URL(string: reviewData.profilePic),
                                       placeholder:UIImage.init(named: "signup_profile_default_image"),
                                       options: [.transition(ImageTransition.fade(1))],
                                       progressBlock: { receivedSize, totalSize in
        },
                                       completionHandler: { image, error, cacheType, imageURL in
        })
        
        reviewTime.text = self.getCreationTimeInt(expiryTimeStamp: reviewData.timeStamp)
    }
    
    func updateCustReviewData(reviewData:CustReviewData)  {
        reviewerName.text = reviewData.reviewBy
        review.text = reviewData.reviewText
        ratingView.rating = reviewData.rating
        
        reviewerImage.kf.setImage(with: URL(string: reviewData.profilePic),
                                  placeholder:UIImage.init(named: "signup_profile_default_image"),
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
        },
                                  completionHandler: { image, error, cacheType, imageURL in
        })
        
        reviewTime.text = self.getCreationTimeInt(expiryTimeStamp: reviewData.timeStamp)
    }
    
    func getCreationTimeInt(expiryTimeStamp : Int) -> String{
        let currentTime = Date().timeIntervalSince1970
        let remainingTime = Int(currentTime) - expiryTimeStamp
       
        if remainingTime/86400 < 1{
            
            return "Today"
        
        }else if remainingTime/86400 < 2{
            
            return "Yesterday"
            
        }else if  remainingTime/86400 < 30{
            
            return String(describing:Int(remainingTime/86400)) + " days ago"
            
        }else if remainingTime/86400 < 360 {
            
             return String(describing:Int(remainingTime/86400)) + " Months ago"
            
        }else{
            
             return String(describing:Int(remainingTime/86400)) + " years ago"
            
        }
    }
}
