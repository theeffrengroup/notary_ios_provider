//
//  InviteViewController.swift
//  LSP
//
//  Created by Rahul Sharma on 15/01/18.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Social
import MessageUI

import MobileCoreServices
import FBSDKShareKit
import FacebookShare



class InviteViewController: UIViewController {
    
    @IBOutlet var referralCode: UILabel!
    var shareText = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        shareText = "Download the app and join the Notary family.Use my referral code \(Utility.referralCode)."
        
         referralCode.text = Utility.referralCode
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func shareCodeWithMessage(_ sender: Any) {
        if (MFMessageComposeViewController.canSendText()) {
            let messageVC = MFMessageComposeViewController()
            messageVC.body = shareText + AppDetails.appstoreLink
            messageVC.recipients =  ["Enter tel-nr"]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }else{
            Helper.alertVC(errMSG: "No messaging option available")
        }
    }
    
    
    @IBAction func shareCodeWithEmail(_ sender: Any) {
        let composeVC = MFMailComposeViewController()
        if !(MFMailComposeViewController.canSendMail()) {
            print("Mail services are not available")
            self.showSendMailErrorAlert()
            return
        }else{
            composeVC.mailComposeDelegate = self
            let toRecipents = [""]
            let referralText = AppDetails.appName
            
            composeVC.navigationBar.isTranslucent = false
            composeVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : COLOR.APP_COLOR , NSAttributedString.Key.backgroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
            composeVC.navigationBar.tintColor = COLOR.APP_COLOR
            composeVC.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            composeVC.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            composeVC.setSubject(referralText)
            composeVC.setMessageBody(shareText + AppDetails.appstoreLink , isHTML: false)
            composeVC.setToRecipients(toRecipents)
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func shareCodeWithTwitter(_ sender: UIButton) {
        let tweetUrl = AppDetails.appstoreLink
        
        let shareString = "https://twitter.com/intent/tweet?text=\(String(format: shareText))&url=\(tweetUrl)"
        
        // encode a space to %20 for example
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // cast to an url
        let url = URL(string: escapedShareString)
        
        // open in safari
        UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func shareCodeWithWhatsapp(_ sender: UIButton) {
        let whatsAppsBrowserURLString = "https://api.whatsapp.com/send?text=\(String(format: shareText,AppDetails.appstoreLink))"
        let escapedShareString = whatsAppsBrowserURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // cast to an url
        let url = URL(string: escapedShareString)
        
        UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
    }
    
    
    @IBAction func shareCodeWithFB(_ sender: Any) {

       /*
        let content = LinkShareContent.init(url: URL(string:AppDetails.appstoreLink)!,
                                            quote: String(format: shareText, AppDetails.appstoreLink))
        
        
        let dialog = ShareDialog.init(content: content)
        dialog.mode = ShareDialogMode.automatic
        
        do {
            
            try dialog.show()
            
        } catch let error {
            
            print(error)
        }*/
        //vani 21/02/2020
        let shareContent = ShareLinkContent()
        shareContent.contentURL = URL.init(string: AppDetails.appstoreLink)!
        shareContent.quote = AppDetails.appstoreLink
        
        let dialog = ShareDialog()
        dialog.shareContent = shareContent
        
        dialog.show()
        
    }
    //vani 21/02/2020
    /*
    func showShareDialog<C: ContentProtocol>(_ content: C, mode: ShareDialogMode = .automatic) {
        let dialog = ShareDialog(content: content)
        dialog.presentingViewController = self
        dialog.mode = mode
        do {
            try dialog.show()
        } catch (let error) {
            Helper.alertVC(errMSG: "Failed to present share dialog with error\(error)")
        }
    }*/
    
    
    @IBAction func menuButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: .alert)
        sendMailErrorAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(sendMailErrorAlert, animated: true, completion:nil)
    }
}

extension InviteViewController :MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension InviteViewController : MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
}

