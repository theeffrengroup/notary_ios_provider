//
//  FAQController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 09/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class FAQController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var index: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func expandAction(_ sender: UIButton) {
        

        
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tableView)
        index = self.tableView.indexPathForRow(at: buttonPosition)
        tableView.reloadData()
        let cell = tableView.cellForRow(at: index!) as! FAQCell
        cell.answerLabel.isHidden = false
        
        sender.setBackgroundImage(#imageLiteral(resourceName: "faq_minus_icon_off"), for:  UIControl.State.normal)
        
    }

    @IBAction func backAction(_ sender: UIButton) {
        
         dismiss(animated: true, completion: nil)
    }
}

extension FAQController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "faqCell", for: indexPath) as! FAQCell
        cell.answerLabel.isHidden = true
        cell.expandButton.setBackgroundImage(#imageLiteral(resourceName: "faq_add_icon_off"), for:  UIControl.State.normal)
        cell.selectionStyle = .none
        return cell
    }
}

extension FAQController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath == index {
                return 120
        }
        else {
        return 65

        }
    }

}
