//
//  EditCategoryVC.swift
//  LSP
//
//  Created by Vengababu Maparthi on 26/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class EditCategoryVC: UIViewController {
    
    @IBOutlet weak var categoryTableView: UITableView!
    
    var categories = [Categories]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backToView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let nextScene = segue.destination as? CategoryViewController
        nextScene?.categoryID = self.categories[sender as! Int].catID
        nextScene?.categorytit = self.categories[sender as! Int].catName
        nextScene?.documents = self.categories[sender as! Int].categoryDocs
        nextScene?.fromWhichScreen = 1
        
    }
}


extension EditCategoryVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 45;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "editCategory", for: indexPath) as! CategoryEditTableCell
        cell.categoryLabel.text = categories[indexPath.row].catName
        return cell
    }
}

extension EditCategoryVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "editCat", sender: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

