//
//  CategoryEditTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 26/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class CategoryEditTableCell: UITableViewCell {
    @IBOutlet weak var categoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
