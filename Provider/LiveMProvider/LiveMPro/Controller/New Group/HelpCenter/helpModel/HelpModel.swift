//
//  HelpModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 25/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper
class HelpModelClass: NSObject {
    
    var apiCall = APILibrary()
    
    func updateTheHelpData(params:[String:Any]) {
        
        apiCall.updateDriverStatus(dict: params) { (responseModel) in
            let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
            Helper.hidePI()
            switch responseCodes{
            case .SuccessResponse:
          
                break
                
            case .TokenExpired:
                let defaults = KeychainWrapper.standard
                if let sessionToken =  responseModel.data["data"]   as? String  {
                    defaults.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                    self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                        if success{
                            
                        }
                    })
                }
                break
            case .UserLoggedOut:
                Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                Session.expired()
                break
            default:
                Helper.alertVC(errMSG: responseModel.data["message"] as! String)
    
                break
            }
        }
    }
    
}
