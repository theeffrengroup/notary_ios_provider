//
//  HelpCenterController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 15/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class HelpCenterController: UIViewController {
    
    
    @IBOutlet weak var name: FloatLabelTextField!
    @IBOutlet weak var email: FloatLabelTextField!
    @IBOutlet weak var subject: FloatLabelTextField!
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //  amazonWrapper.delegate = nil
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            mainScrollView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardSize = CGSize(width: 320, height: 240)
        let height: CGFloat = UIDevice.current.orientation.isPortrait ? keyboardSize.height : keyboardSize.width//UIDeviceOrientationIsPortrait(UIDevice.current.orientation) ? keyboardSize.height : keyboardSize.width
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            var edgeInsets: UIEdgeInsets = self.mainScrollView.contentInset
            edgeInsets.bottom = height
            self.mainScrollView.contentInset = edgeInsets
            edgeInsets = self.mainScrollView.scrollIndicatorInsets
            edgeInsets.bottom = height
            self.mainScrollView.scrollIndicatorInsets = edgeInsets
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension HelpCenterController : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Message"{
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Message"
            textView.textColor = UIColor.lightGray
        }
    }
    
    ///MARK: - This func Count the Total characters for AboutMeController Text.
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

extension HelpCenterController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == name{
          email.becomeFirstResponder()
        }else if textField == email{
            subject.becomeFirstResponder()
        }else if textField == subject{
            messageTextView.becomeFirstResponder()
        }
        return true
    }
}










