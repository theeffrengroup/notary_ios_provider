//
//  WalletCardCell.swift
//  DayRunner
//
//  Created by apple on 9/6/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class WalletCardCell: UITableViewCell {
    @IBOutlet weak var selectCard: UIButton!
    @IBOutlet weak var currencySymbol: UILabel!
    
    @IBOutlet weak var amountTF: UITextField!
    
    @IBOutlet weak var cardNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
