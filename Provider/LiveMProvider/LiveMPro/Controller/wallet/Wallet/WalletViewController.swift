//
//  WalletViewController.swift
//  Provider
//
//  Created by apple on 9/6/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

enum WalletSectionType: Int {
    case currentCredit = 0
    case cardDetails = 1
}

enum WalletRowType: Int {
    case seperator = 0
    case Default = 1
}

class WalletViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var activeTextField = UITextField()
    var enterAmt = ""
    var cardID = ""
    var cardBrand = ""
    var last4 = ""
    var walletModel = WalletModel()
    
    @IBOutlet weak var walletMsg: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Wallet"

        self.getTheUpdatedData(fromApi:false)
        if Utility.walletAmt > 0{
            walletMsg.text = "GoTasker app owes you " + Helper.getTheAmtTextWithSymbol(amt:String(describing:Utility.walletAmt)) + " Amount"
        }else{
            walletMsg.text = "You owe the GoTasker app " + Helper.getTheAmtTextWithSymbol(amt:String(describing:Utility.walletAmt)) + " Amount"
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadSections([1], with: .none)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tapGesture(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func confirmAndPayAction(_ sender: Any) {
        let indexPath = IndexPath(row: WalletRowType.Default.rawValue, section: WalletSectionType.cardDetails.rawValue)
        if let cell = self.tableView.cellForRow(at: indexPath) as? WalletCardCell{
            if cell.amountTF.text?.length == 0 {
                Helper.alertVC(errMSG: "Please enter amount")
            }
            else {
                self.sendRequestForPayment()
            }
        }
    }
    
    func sendRequestForPayment() {
        
        if Utility.cardLast4 == "Select Card" {
            Helper.alertVC(errMSG: "Please select the card")
        }
        else {
            cardID = Utility.CardID
            last4 = Utility.cardLast4
            cardBrand = Utility.CardBrand
            showWarning()
        }
    }
    
    func serviceForPay() {
        Helper.showPI(message: "Loading..")
        let params : [String : Any] = ["cardId"  :cardID,
                                       "amount"  :self.enterAmt] //  "card" :last4, "cardType":cardBrand,
       
        walletModel.updateTheRechargeAmount(dict: params) { (succeeded) in
            if succeeded{
                 self.getTheUpdatedData(fromApi:true)
            }
        }
    }
    
    
    func getTheUpdatedData(fromApi:Bool) {
        
        walletModel.getTheWalletData { (succeeded) in
            if succeeded{
                if fromApi{
                    Helper.alertVC(errMSG: "Wohooo...You have successfully recharged your wallet with " + Helper.getTheAmtTextWithSymbol(amt:self.enterAmt) + " balance.!")
                    let indexPath = IndexPath(row: WalletRowType.Default.rawValue, section: WalletSectionType.cardDetails.rawValue)
                    if let cell = self.tableView.cellForRow(at: indexPath) as? WalletCardCell{
                        cell.amountTF.text = ""
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        switch segue.identifier! as String  {
        case "cardSegue":
            if let cardVC = segue.destination as? CardViewController {
                cardVC.delegate = self
            }
        default:
            break
        }
    }
    
    @objc func cardButtonClicked() {
        self.performSegue(withIdentifier: "cardSegue", sender: self)
    }
    
    @objc func recentTransactionButton() {
        self.performSegue(withIdentifier:"recentTransactionSegue", sender: self)
    }
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        
        self.tableView.contentOffset = CGPoint(x: 0, y: 0)
    }
    /*****************************************************************/
    //MARK: - Keyboard Methods
    /*****************************************************************/
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self .moveViewUp(activeView: activeTextField, keyboardHeight: keyboardSize.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.tableView.contentOffset = CGPoint(x: 0, y: -remainder)
                            print("remainder.........", remainder)
            })
        }
        // Set ContentSize of ScrollView
        //        var contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        //        contentSizeOfContent.height += keyboardHeight
        //        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    
    
    /// Show Default Alert message
    private func showWarning() {
        alertForConfirmation()
    }
    
    func alertForConfirmation(){
        let alert = UIAlertController(title: "Confirm", message: "Are you sure you want to recharge your wallet with " + Helper.getTheAmtTextWithSymbol(amt:self.enterAmt) + "?", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.serviceForPay()
        })
        let noButton = UIAlertAction(title: "No", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
        })
        alert.addAction(noButton)
        alert.addAction(yesButton)
        present(alert, animated: true) //{ _ in }
    }
}

extension WalletViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension WalletViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionCell = tableView.dequeueReusableCell(withIdentifier: "WalletSectionCell") as! WalletSectionCell
        
        switch WalletSectionType(rawValue: indexPath.section)! {
        case .currentCredit:
            
            let cell = tableView.dequeueReusableCell(withIdentifier:"WalletCurrentCreditCell") as! WalletCurrentCreditCell
            cell.recentTrasactionBtn.addTarget(self,
                                               action: #selector(recentTransactionButton), for: UIControl.Event.touchUpInside)
            cell.hardLimitLbl.text = Helper.getTheAmtTextWithSymbol(amt:String(Utility.hardLimit))
            cell.softLimitLbl.text = Helper.getTheAmtTextWithSymbol(amt:String(Utility.softLimit))
            cell.currentCreditLbl.text = Helper.getTheAmtTextWithSymbol(amt:String(describing:Utility.walletAmt)) + "*"
            return cell
            
        case.cardDetails:
            
            switch WalletRowType(rawValue: indexPath.row)! {
            case .seperator:
                
                return sectionCell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier:"WalletCardCell") as! WalletCardCell
                cell.selectCard.addTarget(self,
                                          action: #selector(cardButtonClicked),
                                          for: UIControl.Event.touchUpInside)
                
                cell.currencySymbol.text = Utility.currencySymbol
                cell.cardNo.text = "**** **** ****" + Utility.cardLast4
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch WalletSectionType(rawValue: indexPath.section)! {
        case .currentCredit:
            return 148
            
        case .cardDetails:
            
            switch WalletRowType(rawValue: indexPath.row)! {
            case .seperator:
                return 20
            default:
                return 200
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch WalletSectionType(rawValue: section)! {
        case .currentCredit:
            
            return 1
            
        case .cardDetails:
            
            return 2
        }
    }
}

extension WalletViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.activeTextField = textField
        self.moveViewUp(activeView: activeTextField, keyboardHeight: 250)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.enterAmt = textField.text!
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.enterAmt = textField.text!
        self.moveViewDown()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}

extension WalletViewController: CardViewControllerDelegate {
    
    func didSelect(card: CardsType) {
        let ud = UserDefaults.standard
        ud.set(card.last4, forKey: USER_INFO.LAST4)
        ud.set(card.brand, forKey: USER_INFO.CARDBRAND)
        ud.set(card.id, forKey: USER_INFO.CARDID)
        ud.synchronize()
        cardID = card.id
        cardBrand = card.brand
        last4 = card.last4
        
    }
}

