//
//  AddCardPopup.swift
//  DayRunner
//
//  Created by Rahul Sharma on 27/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class AddCardPopup: UIView, UITextFieldDelegate {

    @IBOutlet weak var contentView: UIView!

    static var shared: AddCardPopup {
        let view = Bundle(for: self).loadNibNamed("AddCardPopup",
                                                  owner: nil,
                                                  options: nil)?.first as! AddCardPopup
        return view
    }
    
    func showPopup(window: UIWindow) {
        self.frame = window.frame;
        window.addSubview(self);
        self.layoutIfNeeded()
        
        showAnimate()
    }
    
    //MARK: - Animation
    func showAnimate() {
        contentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        contentView.alpha = 0.0;
        UIView.animate(withDuration: 0.25,
                       animations: {
                        self.contentView.alpha = 1.0
                        self.contentView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        });
    }
    
    //MARK: - Hide Animation
    func removeAnimate() {
        
        UIView.animate(withDuration: 0.25,
                       animations: {
                        self.contentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        self.contentView.alpha = 0.0;
            },
                       completion:{(finished : Bool)  in
                        if (finished) {
                            self.removeFromSuperview()
                        }
        });
    }
    
    @IBAction func AddCardButtonAction(_ sender: AnyObject) {
        removeAnimate()
    }
    
    @IBAction func tapClicked(_ sender: Any) {
        self.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }


}
