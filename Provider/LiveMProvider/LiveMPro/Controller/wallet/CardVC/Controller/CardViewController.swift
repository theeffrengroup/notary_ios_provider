//
//  CardViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 27/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

protocol CardViewControllerDelegate {
    func didSelect(card: CardsType)
}

class CardViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    fileprivate var cards = [CardsType]()
    var delegate: CardViewControllerDelegate?
    var cardModel = CardModel()
    fileprivate let defaults = UserDefaults.standard
    fileprivate var timer = Timer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        getCards()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCardButtonClicked(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "addCardSegue", sender: self)
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let stripe =  segue.destination as? StripeViewController
        {
            stripe.delegate = self
        }
    }
}


extension CardViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "addCardTableViewCell") as! AddCardTableViewCell?
        
        cell?.cardNumber.text = "Card Ending with " + self.cards[indexPath.row].last4
        if self.cards[indexPath.row].defaultCard == "1"{
            cell?.tickMark.isHidden = false
            cell?.cardNumber?.textColor = COLOR.APP_COLOR
        }
        else {
            cell?.tickMark.isHidden = true
            cell?.cardNumber?.textColor = #colorLiteral(red: 0.4078176022, green: 0.407827884, blue: 0.4078223705, alpha: 1)
        }
        
        cell?.cardImage.image = Helper.cardImage(with: self.cards[indexPath.row].brand)
        
        return cell!
    }
}

extension CardViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.cards[indexPath.row].defaultCard == "1"{
            delegate?.didSelect(card: cards[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        }else{
            self.MakeCardDefault(cardId: self.cards[indexPath.row].id, completion: { (success) in
                if success{
                    self.delegate?.didSelect(card: self.cards[indexPath.row])
                    self.navigationController?.popViewController(animated: true)
                }
            })
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            deleteCard(cardId: cards[indexPath.row].id,
                       completion: { (success) in
                        if (success) {
                            self.cards.remove(at: indexPath.row)
                            tableView.reloadData()
                        }
            })
        }
    }
}

extension CardViewController : StripeViewControllerDelegate {
    
    func didCardAdded() {
        getCards()
    }
}

extension CardViewController {
    
    fileprivate func getCards() {
        
        Helper.showPI(message: "loading..")
        
        cardModel.getTheCardData { (succeeded, dict) in
            if succeeded{
                self.cards = []
                for cardDict in dict {
                    var card = CardsType()
                    
                    card.brand     = String(describing: cardDict["brand"]!)
                    card.exp_month = String(describing: cardDict["expMonth"]!)
                    card.exp_year  = String(describing: cardDict["expYear"]!)
                    card.id        = String(describing: cardDict["id"]!)
                    card.last4     = String(describing: cardDict["last4"]!)
                    card.funding   = String(describing: cardDict["funding"]!)
                    card.defaultCard = String(describing: cardDict["isDefault"]!)
                    
                    if card.defaultCard == "1"{
                        let ud = UserDefaults.standard
                        ud.set(card.last4, forKey: USER_INFO.LAST4)
                        ud.set(card.brand, forKey: USER_INFO.CARDBRAND)
                        ud.set(card.id, forKey: USER_INFO.CARDID)
                        ud.synchronize()
                    }
                    
                    self.cards.append(card)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    
    fileprivate func deleteCard(cardId: String, completion: @escaping (Bool) -> Void) {
        
        Helper.showPI(message: "loading..")
        
        let params: [String: Any] = ["cardId": cardId]
        
        cardModel.deleteTheCard(dict: params) { (success) in
            if success{
                completion(true)
            }else{
                completion(false)
            }
        }
    }
    
    
    fileprivate func MakeCardDefault(cardId: String, completion: @escaping (Bool) -> Void) {
        
        Helper.showPI(message: "loading..")
        
        let params: [String: Any] = ["cardId": cardId]
        
        cardModel.makeCardDefault(dict: params) { (succeeded) in
            if succeeded{
                completion(true)
            }else{
               completion(false)
            }
        }
        
    }
}

struct CardsType {
    
    var exp_year = ""
    var last4 = ""
    var id = ""
    var brand = ""
    var exp_month = ""
    var funding = ""
    var defaultCard = ""
    
    init(exp_year: String, last4: String, id: String, brand: String, exp_month: String, funding: String,cardDefa:String) {
        self.exp_year = exp_year
        self.last4 = last4
        self.id = id
        self.brand = brand
        self.exp_month = exp_month
        self.funding = funding
        self.defaultCard = cardDefa
    }
    
    init() {
        
    }
}
