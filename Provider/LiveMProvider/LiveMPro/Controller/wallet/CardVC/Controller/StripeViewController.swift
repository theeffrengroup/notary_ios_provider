//
//  StripeViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Stripe

protocol StripeViewControllerDelegate {
    func didCardAdded()
}

class StripeViewController: UIViewController, STPPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var paymentBackgroundView: UIView!
    
    let screenSize = UIScreen.main.bounds
    var paymentTextField: STPPaymentCardTextField?
    var delegate: StripeViewControllerDelegate?
    fileprivate var didClicked: Bool = false
    
    var cardModel = CardModel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Stripe.setDefaultPublishableKey(Utility.stripeKey)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.title = "Add Card"
        
        setUp()
        paymentBackgroundView.addSubview(paymentTextField!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        paymentTextField?.becomeFirstResponder()
    }
    
    func setUp() {
        
        paymentTextField = STPPaymentCardTextField()
        
        paymentTextField?.delegate = self
        paymentTextField?.cursorColor = UIColor.blue
        paymentTextField?.borderColor = UIColor.clear
        paymentTextField?.borderWidth = 0
        paymentTextField?.font = UIFont.boldSystemFont(ofSize: 12)
        paymentTextField?.textColor = UIColor.black;
        paymentTextField?.cornerRadius = 2.0;
        
        paymentTextField?.frame = CGRect(x: 0,
                                         y: 0,
                                         width: CGFloat(screenSize.size.width - 90),
                                         height: 50)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        
        Helper.showPI(message: "Saving..")
        if !(paymentTextField?.isValid)! {
            Helper.alertVC(errMSG: "Enter Valid card")
            Helper.hidePI()
        }
        else if (Stripe.defaultPublishableKey() == nil) || Stripe.defaultPublishableKey()?.count == 0 {
            Helper.alertVC(errMSG: "Enter Valid Details")
            Helper.hidePI()
        }
        else {
            
            guard didClicked == false else {
                Helper.hidePI()
                return
            }
            didClicked = true
            //vani 24.02/2020
            guard let cardParams = paymentTextField?.cardParams else { return }
            let cardParameters = STPCardParams()
            
            cardParameters.number = cardParams.number
            cardParameters.expMonth = cardParams.expMonth as! UInt
            cardParameters.expYear = cardParams.expYear as! UInt
            cardParameters.cvc = cardParams.cvc
            STPAPIClient.shared().createToken(withCard: cardParameters,
                                              completion: { (token:STPToken?, error:Error?) in
                                                
                                                if (error != nil) {
                                                    Helper.alertVC(errMSG: "Error")
                                                    
                                                    Helper.hidePI()
                                                }
                                                else {
                                                    self.send(token: (token?.tokenId)!)
                                                    print("Card Token: \((token?.tokenId)!)")
                                                }
            })
        }
    }
    
    @IBAction func scanCardAction(_ sender: Any) {
        
        if let scanCardViewController = CardIOPaymentViewController(paymentDelegate: self) {
            
            scanCardViewController.hideCardIOLogo = true
            scanCardViewController.disableManualEntryButtons = true
            scanCardViewController.modalPresentationStyle = UIModalPresentationStyle.formSheet
            scanCardViewController.title = "Scan Card"
            scanCardViewController.navigationController?.isNavigationBarHidden = false
            present(scanCardViewController, animated: true, completion: nil)
        }
    }
    
    func send(token: String){
        let params : [String : Any] = ["cardToken"   : String(describing: token),
                                         "email"        :Utility.userEmail]
        
        cardModel.addNewCard(dict: params) { (succeeded) in
            if succeeded{
                DispatchQueue.main.async(execute: {
                    _ = self.navigationController?.popViewController(animated: false)
                    self.delegate?.didCardAdded()
                })
            }
        }
    }
}


//MARK: - Card IO Deleagte -
extension StripeViewController: CardIOPaymentViewControllerDelegate {
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        self.dismiss(animated: true, completion: nil)
        
        let cardParameters = STPCardParams()
        
        cardParameters.number =  cardInfo.cardNumber
        cardParameters.expMonth = cardInfo.expiryMonth
        cardParameters.expYear = cardInfo.expiryYear
        cardParameters.cvc = cardInfo.cvv
        
        if STPCardValidator.validationState(forCard: cardParameters) == STPCardValidationState.valid {
            //vani 24/02/2020
            paymentTextField?.cardParams = STPPaymentMethodCardParams(cardSourceParams: cardParameters) //cardParameters;
            doneButtonAction((Any).self)
        }
        else{
            Helper.alertVC(errMSG: "Enter Valid card")
        }
    }
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}

