//
//  WalletModel.swift
//  LSP
//
//  Created by Vengababu Maparthi on 24/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

import RxAlamofire
import RxCocoa
import RxSwift


class CardModel:NSObject {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    
    
    func addNewCard(dict:[String:Any],completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.load)
        let rxApiCall = CardAPI()
        rxApiCall.addNewCard(paramDict:dict)
        rxApiCall.Card_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    func getTheCardData(completionHandler:@escaping (Bool , [[String:Any]]) -> ()) {
        Helper.showPI(message:loading.load)
        let rxApiCall = CardAPI()
        rxApiCall.getTheCards()
        rxApiCall.Card_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    
                    if let dict = responseModel.data["data"] as? [[String:Any]]{
                     completionHandler(true,dict)
                    }
                 
                    break
                default:
                    completionHandler(false, [])
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    func deleteTheCard(dict:[String:Any],completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.load)
        let rxApiCall = CardAPI()
        rxApiCall.deleteCard(paramDict:dict)
        rxApiCall.Card_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    func makeCardDefault(dict:[String:Any],completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.load)
        let rxApiCall = CardAPI()
        rxApiCall.makeCardDefault(paramDict:dict)
        rxApiCall.Card_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}

