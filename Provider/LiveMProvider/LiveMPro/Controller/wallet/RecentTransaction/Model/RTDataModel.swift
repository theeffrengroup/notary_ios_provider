//
//  RTDataModel.swift
//  DayRunner
//
//  Created by apple on 9/14/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class RTDataModel: NSObject {

    var amount = ""
    var closingBal = ""
    var comment = ""
    var currency = ""
    var intiatedBy = ""
    var openingBal = ""
    var paymentTxnId = ""
    var paymentType = ""
    var timestamp:Int64 = 0
    var trigger = ""
    var txnId = ""
    var txnType = ""
    var tripId = ""
    
    override init() {
        super.init()
    }
    
    init(dictionary: [String: Any]) {
        super.init()
        
        self.amount = String(describing: dictionary["amount"] ?? 0)
        self.closingBal = dictionary["closingBal"] as? String ?? ""
        self.comment = dictionary["comment"] as? String ?? ""
        self.currency = dictionary["currency"] as? String ?? ""
        self.intiatedBy = dictionary["intiatedBy"] as? String ?? ""
        self.openingBal = dictionary["openingBal"] as? String ?? ""
        self.paymentTxnId = dictionary["paymentTxnId"] as? String ?? ""
        self.paymentType = dictionary["paymentType"] as? String ?? ""
        if let time = dictionary["timestamp"] as? NSNumber{
            self.timestamp = Int64(time)
        }
        self.trigger = dictionary["trigger"] as? String ?? ""
        self.txnId = dictionary["txnId"] as? String ?? ""
        self.txnType = dictionary["txnType"] as? String ?? ""
        self.tripId = String(describing:dictionary["tripId"] ?? 0)
    }
}

struct RTDataType {
    
    var credits: [RTDataModel] = []
    var debits: [RTDataModel] = []
    var credits_debits: [RTDataModel] = []
    
    init(data: [String: Any]) {
        
        if let creditArr = data["creditArr"] as? [[String: Any]] {
            for credit in creditArr {
                credits.append(RTDataModel(dictionary: credit))
            }
        }
        
        if let debitArr = data["debitArr"] as? [[String: Any]] {
            for debit in debitArr {
                debits.append(RTDataModel(dictionary: debit))
            }
        }
        
        if let creditDebitArr = data["creditDebitArr"] as? [[String: Any]] {
            for creditDebit in creditDebitArr {
                credits_debits.append(RTDataModel(dictionary: creditDebit))
            }
        }
    }
}
