//
//  RTDebitCell.swift
//  DayRunner
//
//  Created by apple on 9/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class RTDebitCell: UITableViewCell {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var transIDLbl: UILabel!
    @IBOutlet weak var paymentBy: UILabel!
    @IBOutlet weak var paidTime: UILabel!
    @IBOutlet weak var bookingID: UILabel!
    @IBOutlet weak var paidAmt: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        if highlighted {
            view.backgroundColor = UIColor(hexString: "f8f8f8")
        }
        else {
            view.backgroundColor = .white
        }
    }
}
