//
//  RecentTransactionsController.swift
//  DayRunner
//
//  Created by apple on 9/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class RecentTransactionsController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var allTableView: UITableView!
    @IBOutlet weak var debitTableView: UITableView!
    @IBOutlet weak var creditTableView: UITableView!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var debitButton: UIButton!
    @IBOutlet weak var creditButton: UIButton!
    
    fileprivate var allTransactions: [RTDataModel] = []
    fileprivate var creditTransactions: [RTDataModel] = []
    fileprivate var debitTransactions: [RTDataModel] = []
    
    var recentTracModel = RTModel()
    var indexVal = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allButton.isSelected = true
        debitButton.isSelected = false
        creditButton.isSelected = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       /*
        There was some blank space below the navigationBar
        Owner : Vani
        Date : 06/04/2020
        */
       super.viewWillAppear(animated)
       if #available(iOS 13.0, *) {
            navigationController?.navigationBar.setNeedsLayout()
       }
        allTransactions = []
        creditTransactions = []
        debitTransactions = []
        
        
        recentTracModel.recentTractionsData(index: indexVal) {(recentTrans) in
            if(recentTrans != nil){
                
                self.allTransactions = (recentTrans?.credits_debits)!
                self.creditTransactions = (recentTrans?.credits)!
                self.debitTransactions = (recentTrans?.debits)!
                
                self.allTableView.reloadData()
                self.debitTableView.reloadData()
                self.creditTableView.reloadData()
            }
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        allTableView.addPullToRefresh { (finished) in
            if finished{
                self.indexVal = self.indexVal + 1
            }
        }

        creditTableView.addPullToRefresh { (finished) in
            if finished{
                self.indexVal = self.indexVal + 1
            }
        }

        debitTableView.addPullToRefresh { (finished) in
            if finished{
                self.indexVal = self.indexVal + 1
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self,
                                                  name: UIApplication.didBecomeActiveNotification,
                                                  object: nil)
    }
    
    deinit {
        allTableView.dg_removePullToRefresh()
        creditTableView.dg_removePullToRefresh()
        debitTableView.dg_removePullToRefresh()
    }

    
    // MARK: - UIButton Actions
    
    @IBAction func allButtonAction(_ sender: Any) {
        
        var frame = scrollView.bounds
        frame.origin.x = 0 * frame.size.width
        scrollView.scrollRectToVisible(frame, animated: true)
    }
    
    @IBAction func debitButtonAction(_ sender: Any) {
        
        var frame = scrollView.bounds
        frame.origin.x = 1 * frame.size.width
        scrollView.scrollRectToVisible(frame, animated: true)
    }
    
    @IBAction func creditButtonAction(_ sender: Any) {
        
        var frame = scrollView.bounds
        frame.origin.x = 2 * frame.size.width
        scrollView.scrollRectToVisible(frame, animated: true)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
}

extension RecentTransactionsController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension RecentTransactionsController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case allTableView:
            return allTransactions.count
            
        case creditTableView:
            return creditTransactions.count
            
        case debitTableView:
            return debitTransactions.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case allTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RTAllCell") as! RTAllCell
            Helper.shadowView(sender: (cell.view)!, width:UIScreen.main.bounds.width - 20, height: 81)
            
            cell.bookingID?.text = "BID: " + allTransactions[indexPath.row].tripId
            
            
            cell.paidAmt?.text = Helper.getTheAmtTextWithSymbol(amt:self.allTransactions[indexPath.row].amount)
            cell.paidTime?.text = Helper.yearMonthDateTime(timeStamp:self.allTransactions[indexPath.row].timestamp)
            cell.paymentBy?.text = self.allTransactions[indexPath.row].trigger
            cell.transIDLbl?.text = "TRANS_ID: " + self.allTransactions[indexPath.row].txnId
            if self.allTransactions[indexPath.row].txnType == "DEBIT" {
                //    cell.debitImg.isHidden = false
                //     cell.imageLbl.isHidden = true
                cell.imageLbl.image = #imageLiteral(resourceName: "Shape 2 copy 6")
            }else {
                cell.imageLbl.image = #imageLiteral(resourceName: "Shape 2 copy 7")
                //                cell.debitImg.isHidden = true
                //                cell.imageLbl.isHidden = false
            }
            print("All \(indexPath.row): \(allTransactions[indexPath.row])")
            return cell
            
        case creditTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RTCreditCell") as! RTCreditCell
            Helper.shadowView(sender: (cell.view)!, width:UIScreen.main.bounds.width - 20, height: 81)
            
            cell.bookingID?.text =  "BID: " + self.creditTransactions[indexPath.row].tripId
            cell.paidAmt?.text = Helper.getTheAmtTextWithSymbol(amt:self.creditTransactions[indexPath.row].amount)
            
            cell.paidTime?.text = Helper.yearMonthDateTime(timeStamp:self.creditTransactions[indexPath.row].timestamp)
        
            cell.paymentBy?.text = self.creditTransactions[indexPath.row].trigger
            cell.transIDLbl?.text =  "TRANS_ID: " + self.creditTransactions[indexPath.row].txnId

            return cell
            
        case debitTableView:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "RTDebitCell") as! RTDebitCell
            Helper.shadowView(sender: (cell.view)!, width:UIScreen.main.bounds.width - 20, height: 81)
            
            cell.bookingID?.text =   "BID: " + self.debitTransactions[indexPath.row].tripId
            cell.paidAmt?.text = Helper.getTheAmtTextWithSymbol(amt:self.debitTransactions[indexPath.row].amount)
            cell.paidTime?.text =  Helper.yearMonthDateTime(timeStamp:self.debitTransactions[indexPath.row].timestamp)
            cell.paymentBy?.text = self.debitTransactions[indexPath.row].trigger
            cell.transIDLbl?.text = "TRANS_ID: " + self.debitTransactions[indexPath.row].txnId

            return cell

        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case allTableView:
            return 100
            
        case creditTableView:
            return 100
            
        case debitTableView:
            return 100
        default:
            return 0
        }
    }
}


extension RecentTransactionsController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            
            let offsetX = scrollView.contentOffset.x
            let widthOfView = scrollView.bounds.size.width
            
            if offsetX < widthOfView / 2 {
                self.allButton.isSelected = true
                self.debitButton.isSelected = false
                self.creditButton.isSelected = false
            }
            else if offsetX < widthOfView * 3 / 2 && offsetX >= widthOfView / 2 {
                self.allButton.isSelected = false
                self.debitButton.isSelected = true
                self.creditButton.isSelected = false
            }
            else {
                self.allButton.isSelected = false
                self.debitButton.isSelected = false
                self.creditButton.isSelected = true
            }
        }
    }
}
