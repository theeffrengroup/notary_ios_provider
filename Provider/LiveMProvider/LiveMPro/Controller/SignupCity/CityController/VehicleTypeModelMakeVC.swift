//
//  VehicleTypeModelMakeVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 26/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit


////////*******************shows cities***************************///

protocol CityAndCategoryDelegate:class
{
    func didSelectMakeModelType(name:String,cityID:String,index:Int)
}

// City controller 

class VehicleTypeModelMakeVC: UIViewController {
    
    open weak var delegate: CityAndCategoryDelegate?
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var vehicleModelTV: UITableView!
    var vechArray: [[String: AnyObject]] = []
    
    var idMakeSeleted =  String()

    var cityArray         = [CityModel]()
    var cityModel         = Cities()
    
    var citySelectedID = String()

    var alreadySelectedIndex:Int = -1
    var alreadySelectedIndexes = [Int]()
    
    var type:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        titleLabel.text = "select city"
        getTypeMakeNModel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Call the cities API
    func getTypeMakeNModel(){
        Helper.showPI(message:loading.load)
        
        cityModel.getCitiesAPI { (succeeded,cities) in
            if succeeded{
                self.cityArray = cities
                self.vehicleModelTV.reloadData()
            }
        }
    }

   
    @IBAction func backToVC(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}

//MARK:- tableview datasource
extension VehicleTypeModelMakeVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cityArray.count;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"typeModelMake") as! VTMMTableViewCell?
        
        cell?.updateTheText(city:cityArray[indexPath.row].cityName)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 50;
    }
}

//MARK:- Tableview delegate methods
extension VehicleTypeModelMakeVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? VTMMTableViewCell {
            cell.checkMarkButton.isSelected = true
            cell.textLabelForVehicle.textColor = COLOR.APP_COLOR
        }
        delegate?.didSelectMakeModelType(name: cityArray[indexPath.row].cityName, cityID: cityArray[indexPath.row].cityID, index: indexPath.row)
        _ = navigationController?.popViewController(animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? VTMMTableViewCell {
            cell.checkMarkButton.isSelected = false
            cell.textLabelForVehicle.textColor = Helper.colorFromHexString(hexCode: "000000")
        }
    }
}



