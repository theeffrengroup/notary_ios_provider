//
//  SignInModel.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SwiftKeychainWrapper


class Signin:NSObject {
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    var emailAddress = Variable<String>(""){
        didSet {
            print("Assign email address")
        }
    }
    
    var password = Variable<String>("")
    
    
    /// signin API
    ///
    /// - Parameter completionHandler: return boolean value on completion
    func signinApi(completionHandler:@escaping (Bool) -> ()) {
        
        let signInApi = SignInAPI()
        let value = SigninModel.init(email: emailAddress.value, password: password.value)
        
        signInApi.makeApiCallForSignIn (method:API.METHOD.LOGIN ,parameters: value)
        signInApi.signIN_Response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                    
                case .uauthoriedUser:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .profileReject:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .adminNotAccepted:
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                case .SuccessResponse:
                    Helper.hidePI()
                    UserDefaults.standard.set(self.emailAddress.value, forKey: USER_INFO.USER_EMAIL)
                    UserDefaults.standard.synchronize()
                    completionHandler(self.savingTheData(dict: response.data["data"] as! [String : Any]))
                
                    
                    break
                default:
                    Helper.hidePI()
                    completionHandler(false)
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    
    /// saves the data in nsuserdefaults
    ///
    /// - Parameter dict: signin response data
    /// - Returns: return true when the saving got complete
    func savingTheData(dict:[String:Any]) -> Bool{
        let defaults = UserDefaults.standard
        
        if let emailID = dict["email"]  as? String  {
            defaults.set(emailID , forKey: USER_INFO.USER_EMAIL)
        }
        
        if let firstName = dict["firstName"]  as? String  {
            defaults.set(firstName , forKey: USER_INFO.FIRSTNAME)
            if let last = dict["lastName"]  as? String  {
                defaults.set(firstName + " " + last , forKey: USER_INFO.USER_NAME)
            }
        }
        
        
        
        if let lastName = dict["lastName"]  as? String  {
            defaults.set(lastName , forKey: USER_INFO.LASTNAME)
        }
        
        if let requestID = dict["requester_id"] as? NSNumber {
           defaults.set(requestID, forKey: USER_INFO.REQUEST_ID)
        }
        
        if let bidType = dict["bid"] as? NSNumber {
            defaults.set(String(describing:bidType), forKey: USER_INFO.WHICHPROVIDERTYPE)
        }
        
        if let masterID  = dict["id"]  as? String  {
            defaults.set(masterID , forKey: USER_INFO.USER_ID)
        }
        
        if let sessionToken =  dict["token"]   as? String  {
            KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
//            UserDefaults(suiteName: API.groupIdentifier)!.set( sessionToken, forKey: "sessionToken")
            KeychainWrapper(serviceName: API.groupIdentifier).set( sessionToken, forKey: "sessionToken")

        }
        
        if let fcmTopci =  dict["fcmTopic"]   as? String  {
            defaults.set(fcmTopci, forKey: USER_INFO.FCMTOPIC)
        }
        
        if let pPic =  dict["profilePic"]   as? String  {
            defaults.set(pPic, forKey: USER_INFO.USERIMAGE)
        }
        
        if let referral =  dict["referralCode"]   as? String  {
            defaults.set(referral, forKey: USER_INFO.REFERRAL_CODE)
        }
        defaults.synchronize()
        
        let configModel = ConfigModelClass()
        configModel.makeServiceCallForConfigData()
        
        return true
    }
}
