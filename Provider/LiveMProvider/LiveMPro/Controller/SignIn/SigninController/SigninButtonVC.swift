//
//  SigninButtonVC.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper
extension SignInViewController {
    
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    //******** Moves to retrive password*********//
    @IBAction func forgotPasswordAction(_ sender: Any) {
        performSegue(withIdentifier: "toForgotPassword", sender: nil)
    }
    
    @IBAction func signInAction(_ sender: Any) {
        dismisskeyBord()
        signinMethod()
    }
    
    @IBAction func rememberMe(_ sender: Any) {
        if self.rememberMeButton.isSelected{
            rememberMeButton.isSelected = false
            UserDefaults.standard.set("", forKey: USER_INFO.SAVEDID)
            KeychainWrapper.standard.set("", forKey: USER_INFO.SAVEDPASSWORD)
            
            UserDefaults.standard.synchronize()
        }else{
            rememberMeButton.isSelected = true
            UserDefaults.standard.set(self.emailTextField.text, forKey: USER_INFO.SAVEDID)
            KeychainWrapper.standard.set(self.passwrodTextField.text!, forKey: USER_INFO.SAVEDPASSWORD)
            UserDefaults.standard.synchronize()
        }
        
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        performSegue(withIdentifier: "toSignup", sender: nil)
    }
    
    ///*************checking the textfield data*******//
    func signinMethod(){
        if (emailTextField.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signin.emailAddress), animated: true, completion: nil)
        }
        else if (passwrodTextField.text?.isEmpty)!{
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signin.password), animated: true, completion: nil)
        }
        else{
            sendRequestForSignIn()
        }
    }
    
    // Signin APi
    func sendRequestForSignIn() {
      
        Helper.showPI(message:loading.signin)
        
        signinModel.signinApi() { (succeeded) in
            if succeeded{
                if self.rememberMeButton.isSelected{
                    UserDefaults.standard.set(self.emailTextField.text, forKey: USER_INFO.SAVEDID)
                    KeychainWrapper.standard.set(self.passwrodTextField.text!, forKey: USER_INFO.SAVEDPASSWORD)
                    UserDefaults.standard.synchronize()
                }
                
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainViewController  = storyboard.instantiateViewController(withIdentifier: "HomeTabBarController")
                mainViewController.navigationController?.isNavigationBarHidden = true
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.viewControllers.append(mainViewController)
                
            }else{
                self.shake(textField:self.emailTextField)
                self.shake(textField:self.passwrodTextField)
            }
        }
    }
}

