//
//  SignInViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SignInViewController: UIViewController {
    
    var activeTextField = UITextField()
    
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var passwrodTextField: FloatLabelTextField!
    @IBOutlet var emailTextField: FloatLabelTextField!
    @IBOutlet weak var view1: UIView!
        @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var view2: UIView!
    var signinModel = Signin()
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    @IBOutlet weak var rememberMeButton: UIButton!
    var sessionTkn = String()
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainScrollView.bringSubviewToFront(signupButton)
        Helper.shadowView(sender: bottomView, width:UIScreen.main.bounds.size.width, height:bottomView.frame.size.height)
//        locationtracker.startLocationTracking()
        if CLLocationManager.locationServicesEnabled() {
                    switch CLLocationManager.authorizationStatus() {
                        
                    case .notDetermined, .restricted, .denied:
                        print("No access")
                    case .authorizedAlways, .authorizedWhenInUse:
                        print("Access")
                        locationtracker.startLocationTracking()
                    @unknown default:
                        break
                    }
                    
                }
        else {
                print("Location services are not enabled")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        self.navigationController?.isNavigationBarHidden = false
        //Binding textfield and viewmodel data
        passwrodTextField.rx.text
            .orEmpty
            .bind(to: signinModel.password)
            .disposed(by: disposeBag)
        
        emailTextField.rx.text
            .orEmpty
            .bind(to: signinModel.emailAddress)
            .disposed(by: disposeBag)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.assignSavedData()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
    
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
    
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {

    }

    //*****hide keyboard*****//
    func dismisskeyBord() {
        view.endEditing(true)
    }
    

    
    func shake(textField:UITextField) {
        textField.shake(10, withDelta: 7, speed: 0.06, shakeDirection: .horizontal)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = true
        //vani 06/04/2020
//        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension UIViewController {
  func presentInFullScreen(_ viewController: UIViewController,
                           animated: Bool,
                           completion: (() -> Void)? = nil) {
    viewController.modalPresentationStyle = .fullScreen
    present(viewController, animated: animated, completion: completion)
  }
}

