//
//  SignIn.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 21/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
// signup request params

struct SigninModel {
    var emailRPhoneNum:String
    var passwordField:String
    
    init(email:String,password:String) {
        self.emailRPhoneNum = email
        self.passwordField = password
    }
}
