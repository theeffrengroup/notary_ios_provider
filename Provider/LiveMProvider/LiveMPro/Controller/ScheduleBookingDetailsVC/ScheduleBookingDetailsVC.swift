//
//  ScheduleBookingDetailsVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 10/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class ScheduleBookingDetailsVC: UIViewController {
    @IBOutlet weak var eventID: UILabel!
    
    @IBOutlet weak var scheduleBookingTableView: UITableView!
    var bookingData:CompletedBookings?
    var scheduleModel = ScheduleBookingViewModel()
    var serviceAPI = false
    var bookingId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getThebookingData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 08/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeTheBookingDetails(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openChatAction(_ sender: Any) {
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatVC {
            controller.customerID = (bookingData?.customerID)!
            controller.bookingID = String(describing:bookingData!.bookingId)
            controller.custImage = (bookingData?.profilePic)!
            controller.custName =  (bookingData?.firstName)!
            if let navigator = navigationController {
                TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                          subType: CATransitionSubtype.fromTop.rawValue,
                                                                          for: (self.navigationController?.view)!,
                                                                          timeDuration: 0.3)
                navigator.pushViewController(controller, animated: false)
            }
        }
    }
    
    @IBAction func cancelTheBooking(_ sender: Any) {
        let cancelView = CancelView.instance
        cancelView.bookingID = (bookingData?.bookingId)!
        cancelView.bookingType = (bookingData?.bookingType)!
        if (bookingData?.bookingType)! == 2{
            cancelView.startTime = (bookingData?.bookingRequestedFor)!
            cancelView.entTime = (bookingData?.bookingRequestedEndTime)!
        }
        cancelView.getTheCancelData()
        cancelView.delegate = self
        cancelView.show()
    }
    
    @IBAction func callToCustomerAction(_ sender: Any) {
        
        let dropPhone = "tel://" + (bookingData?.mobileNum)!
        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    func getThebookingData() {
        scheduleModel.getBookingSchedulebookingData(bookingID: bookingId) { (success, scheduledata) in
            if success{
                self.serviceAPI = true
                self.bookingData = scheduledata.bookingsData
                self.scheduleBookingTableView.reloadData()
                self.eventID.text =  "BID: " + String(describing:self.bookingData!.bookingId)
            }
        }
    }
}


extension ScheduleBookingDetailsVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ScheduleBookingDetailsVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.serviceAPI {
            return 5
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return 2
        case 2:
            return 3
            
        case 3:
            return 3 + (bookingData?.services.count)!
        case 4:
            return 4
            
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableCell
        let mediumView = tableView.dequeueReusableCell(withIdentifier: "mediumView") as! MediumViewTableCell
        
        switch indexPath.section {
        case 0:
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text = "ADDRESS"
                return header
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "bookingAddress") as! AddressBookingTableCell
                cell.selectionStyle = .none
                cell.custAddress.text = bookingData?.addLine1
                return cell
            }
        case 1:
            switch indexPath.row{
            case 0:
                return mediumView
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "timerProfile") as! CustProfileBookingTableCell
                cell.selectionStyle = .none
                cell.custName.text = bookingData?.firstName
                cell.categoryName.text =  bookingData?.categoryName
                
                return cell
            }
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "totalAmt") as! TotalAmtTableCell
            switch indexPath.row{
            case 0 :
                return mediumView
            case 1:
                header.headerOfCell.text = "TOTAL BILL AMOUNT"
                return header
            default:
                let dateArray = Helper.getTheDateFromTimeStamp(timeStamp:bookingData!.actuallGigTimeStart).components(separatedBy: "|")
                cell.dateOfAppointment.text = "Date: " + dateArray[0]
                cell.timeOfAppointment.text = "Time: " + dateArray[1]
                
                var total = bookingData?.price
                cell.totalAmt.text =  Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))
                return cell
            }
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "timerEvents") as! BookingEventsTableCell
            cell.selectionStyle = .none
            
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text =  "PAYMENT BREAKDOWN"
                return header
             
            case (bookingData?.services.count)! + 2:
                let totalCel = tableView.dequeueReusableCell(withIdentifier: "finalAmt") as! FinalAmtTableCell

                var total = bookingData?.price

                totalCel.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",total!))

                return totalCel
            default:
                
                if bookingData?.serviceType == 1{
                    
                    if (bookingData?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingData?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                        (bookingData?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                        (bookingData?.services[indexPath.row - 2].serviceName)! == "Discount"
                    {
                        cell.key.text = (bookingData?.services[indexPath.row - 2].serviceName)!
                    }
                    else {
                        cell.key.text = (bookingData?.services[indexPath.row - 2].serviceName)! + " * " + String(describing:bookingData!.services[indexPath.row - 2].quantity)
                    }
                    cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.services[indexPath.row - 2].servicePrice))
                }else{
                    
                    
                    
                    if (bookingData?.services[indexPath.row - 2].serviceName)! == "Visit Fee" || (bookingData?.services[indexPath.row - 2].serviceName)! == "Travel Fee" ||
                        (bookingData?.services[indexPath.row - 2].serviceName)! == "Last Due" ||
                        (bookingData?.services[indexPath.row - 2].serviceName)! == "Discount"{
                        cell.key.text = (bookingData?.services[indexPath.row - 2].serviceName)!
                        cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.services[indexPath.row - 2].servicePrice))
                    }
                    else {
                        // cell.key.text = (bookingData?.services[indexPath.row - 2].serviceName)! +
                        //                            " (" + String(format:"%.2f",amt)  + " * " + String(describing:bookingData!.services[indexPath.row - 2].quantity) + " hr)"
                        cell.key.text = Helper.timeInHourMin((bookingData?.totalJobTime)!)
                        cell.value.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",bookingData!.totalHourlyFee))
                    }
                    
                    
                    
                }
                return cell
            }
            
        case 4:
            switch indexPath.row{
            case 0:
                return mediumView
            case 1:
                header.headerOfCell.text =  "Payment Method"
                return header
                
            case 2:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentType") as! PaymentMethodTableCell
                if (bookingData?.paidByWallet)! {
                    cell.paymentType.text = "Wallet" + " + " + (bookingData?.paymentMethod2)!
                }else{
                    cell.paymentType.text = bookingData?.paymentMethod2
                }
                
                cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                return cell
                
            default:
                return mediumView
            }
            
            
            
        default:
            return mediumView
        }
        }
    }


extension ScheduleBookingDetailsVC:cancelDeleteBooking{
    func cancelReason(){
       self.dismiss(animated: true, completion: nil)
    }
}
