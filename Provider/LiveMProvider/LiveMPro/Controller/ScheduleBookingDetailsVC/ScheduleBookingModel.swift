//
//  ScheduleBookingModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 10/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation




class ScheduleBookModel: NSObject {
    
    var bookingsData = CompletedBookings()
    
    
    /// parse the schedule booking data for to show schedule data
    ///
    /// - Parameter acceptData: schedule booking details having the
    /// - Returns: returns the schedule model data
    func parsingTheServiceResponse(acceptData:[String: Any]) -> ScheduleBookModel{
        let bookingData = ScheduleBookModel()
        
        let model = CompletedBookings()
        if  let address1   =  acceptData["addLine1"] as? String {
            model.addLine1 = address1
        }
        
        if  let bookingType   =  acceptData["bookingType"] as? NSNumber {
            model.bookingType = Int(bookingType)
        }
        if  let custID   =  acceptData["customerId"] as? String {
            model.customerID = custID
        }
        
        if  let cancelReason  =  acceptData["cancellationReason"] as? String {
            model.cancellationReason = cancelReason
        }
        
        if  let category      =  acceptData["categoryName"] as? String {
            model.categoryName = category
        }
        
        if   let eventStartTime     = acceptData["eventStartTime"] as? NSNumber {
            model.actuallGigTimeStart = Int64(eventStartTime)
        }
        
        if  let currType  = acceptData["currency"] as? String {
            model.currencyType = currType
        }
        
        if  let currSymbol  = acceptData["currencySymbol"] as? String {
            model.currencySymbol = currSymbol
        }
        
        if  let address2    = acceptData["addLine2"] as? String {
            model.addLine2 = address2
        }
        
        
        if   let expiryTime     = acceptData["bookingExpireTime"] as? NSNumber {
            model.bookingExpireTime = Int64(expiryTime)
        }
        
        if let bookId     = acceptData["bookingId"] as? NSNumber {
            model.bookingId = Int64(bookId)
        }
        
        if  let city = acceptData["city"] as? String {
            model.city = city
        }
        
        if   let country     = acceptData["country"] as? String {
            model.country = country
        }
        
        if let customerData = acceptData["customerData"] as? [String:Any]{
            if let firstName     = customerData["firstName"] as? String {
                model.firstName = firstName
            }
            if  let lastname = customerData["lastName"] as? String {
                model.lastName = lastname
            }
            
            if  let profileImg = customerData["profilePic"] as? String {
                model.profilePic = profileImg
            }
            
            if let mobile  = customerData["phone"] as? String{
                model.mobileNum = mobile
            }
            if let status = acceptData["status"] as? NSNumber{
                model.statusCode = Int(status)
                if status != 10{
                    if  let averageRat  = customerData["averageRating"] as? NSNumber {
                        model.averageRating = Double(Float(averageRat))
                    }
                }else{
                    if let reviewData = acceptData["reviewByProvider"] as? [String:Any]{
                        if  let averageRat  = reviewData["rating"] as? NSNumber {
                            model.averageRating = Double(Float(averageRat))
                        }
                    }
                }
            }
        }
        
        
        if   let lat     = acceptData["latitude"] as? NSNumber {
            model.latitude = Double(lat)
        }
        
        if let longt     = acceptData["longitude"] as? NSNumber {
            model.longitude = Double(longt)
        }
        
        if  let paymentType = acceptData["paymentMethod"] as? NSNumber {
            model.paymentMethod = Int(paymentType)
        }
        
        if let pincode     = acceptData["pincode"] as? String {
            model.pincode = pincode
        }
   
        if let dict = acceptData["event"] as? [String:Any]{
            if  let eventType = dict["name"] as? String {
                model.typeofEvent = eventType
            }
        }
     
        
        if  let bookingRequestedFor  = acceptData["bookingRequestedFor"] as? NSNumber {
            model.bookingRequestedFor = Int64(bookingRequestedFor)
        }
        
        if  let bookingRequestedAt  = acceptData["bookingRequestedAt"] as? NSNumber {
            model.bookingRequestedAt = Int64(bookingRequestedAt)
        }
        
        if  let bookingEndtime  = acceptData["bookingEndtime"] as? NSNumber {
            model.bookingRequestedEndTime = Int64(bookingEndtime)
        }
        
        
        if let statMsg = acceptData["statusMsg"] as? String{
            model.statusMsg = statMsg
        }
        
        if let signatureUrl = acceptData["signatureUrl"] as? String{
            model.signature = signatureUrl
        }
        

        
        if let account = acceptData["accounting"] as? [String:Any]{
            if  let paymentType = acceptData["paymentMethodText"] as? String {
                model.paymentMethod2 = paymentType
            }
            
            if let totalHourlyFee = account["totalActualHourFee"] as? NSNumber{
                model.totalHourlyFee =  Double(totalHourlyFee)
            }
            
            if let totalTime = account["totalActualJobTimeMinutes"] as? NSNumber{
                model.totalJobTime =  Double(totalTime)
            }
            
            
            
            if let cancelAmount = account["cancellationFee"] as? NSNumber{
                model.cancellationFees = Double(cancelAmount)
            }
            
            if let myEarning = account["providerEarning"] as? NSNumber{
                model.myEarnings = Double(myEarning)
            }
            
            if let appEarning = account["appEarningPgComm"] as? NSNumber{
                model.appEarnings = Double(appEarning)
            }
            
            if let totalFee = account["total"] as? NSNumber{
                model.price =  Double(totalFee)
            }
            
            if let discount = account["discount"] as? NSNumber{
                model.discount = Double(discount)
            }
            if let discount = account["discount"] as? Double{
                if discount != 0 {
                    let serviceMod = BookingServices()
                    serviceMod.servicePrice = discount
                    serviceMod.serviceName = "Discount"
                    model.services.append(serviceMod)
                }
                
            }
            
            if let visitFees = account["visitFee"] as? Double{
                if visitFees > 0{
                    let serviceMod = BookingServices()
                    serviceMod.servicePrice = visitFees
                    serviceMod.serviceName = "Visit Fee"
                    model.services.append(serviceMod)
                }
            }
            
            if let travelFree = account["travelFee"] as? Double {
                if travelFree > 0 {
                    let serviceMod = BookingServices()
                    serviceMod.servicePrice = travelFree
                    serviceMod.serviceName = "Travel Fee"
                    model.services.append(serviceMod)
                }
            }
            if let dues = account["lastDues"] as? Double {
                if dues != 0 {
                    let serviceMod = BookingServices()
                    serviceMod.servicePrice = dues
                    serviceMod.serviceName = "Last Due"
                    model.services.append(serviceMod)
                }
            }
            
            if let paidByWal = account["paidByWallet"] as? Int {
                if paidByWal == 1 {
                    model.paidByWallet = true
                }
           }
        
        }
        
        if let dict = acceptData["service"] as? [String:Any]{
            let gigDuration = gigTime()
            if let name = dict["name"] as? String{
                gigDuration.name = name
                if let unit = dict["unit"] as? String{
                    model.gigTime = name + unit
                }
            }
            
            if let unit = dict["unit"] as? String{
                gigDuration.unit = unit
            }
            
            if let price = dict["price"] as? NSNumber{
                gigDuration.price = Int(price)
            }
            
            if let second = dict["second"] as? NSNumber{
                gigDuration.second = Int(second)
            }
            model.GEGTime = gigDuration
           
        }
         bookingData.bookingsData = model
        return bookingData
    }
}
