//
//  SubmitReviewVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 04/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import FloatRatingView
import RxKeyboard
import RxSwift

protocol SubmitReviewVCDelegate:class
{
    func submitReviewSuccessFull()
}

class SubmitReviewVC: UIViewController {
    open weak var delegate: SubmitReviewVCDelegate?
    @IBOutlet weak var eventDate: UILabel!  // 7 jul 2017
    @IBOutlet weak var reviewTableView: UITableView!
    
    var signatureURL = ""
    var bookingID:Int = 0
    var bookingDict:Accepted?
    var submitRevModel = SubmitReviewModel()
    var  ratingBycust = 0.0
    var reviewTextView = ""
    var activeTextView = UITextView()
    let disposeBag = DisposeBag()
    var extraServices = [[String:Any]]()
    var total = 0.00
    var travelFee = 0.0
    
    override func viewDidLoad() {
        total = bookingDict!.totalAmt + travelFee
        eventDate.text = Helper.getTheScheduleViewDataFormat(timeStamp:bookingDict!.bookingRequestedFor)
        
        for extraService in extraServices {
            if let serviceAmt = extraService["price"] as? String{
                total = total + Double(serviceAmt)!
            }
        }
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 08/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            self.moveViewUp(activeView: activeTextView, keyboardHeight: keyboardSize.height)
//        }
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.reviewTableView.contentInset.bottom = keyboardVisibleHeight + 20
            })
            .disposed(by: disposeBag)
    }
    
    
    @IBAction func showReceiptView(_ sender: Any) {
        let Vc:ReceiptPageView = ReceiptPageView.instance
        Vc.signUrl = signatureURL
      //  Vc.show(bookingData:bookingDict)
    }
    
    
    @IBAction func submitReview(_ sender: Any) {

        let dict : [String : Any] =  ["bookingId"      : bookingDict?.bookingId as Any,
                                      "rating"         : ratingBycust,
                                      "review"    : reviewTextView]
        self.submitRevModel.submitReview(params:dict,completionHandler: { (succeeded) in
            
            if succeeded{
                self.delegate?.submitReviewSuccessFull()
                self.tabBarController?.tabBar.isHidden = false
                self.navigationController?.isNavigationBarHidden = false
                
                self.dismiss(animated: true, completion: nil)
                self.view.endEditing(true)
            }else{
                
            }
        })
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        // Resign back to Normal Position having set Content Size as Initial
        //        self.mainScrollView.contentSize = self.contentScrollView.frame.size
        self.reviewTableView.contentOffset = CGPoint(x: 0, y: 0)
    }
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.reviewTableView.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.reviewTableView.contentOffset = CGPoint(x: 0, y: -remainder)
                            print("remainder.........", remainder)
            })
        }
    }
    
}

extension SubmitReviewVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SubmitReviewVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "billAmount") as! BillAmountTableCell
            
            cell.billAmount.text = Helper.getTheAmtTextWithSymbol(amt: String(format:"%.2f",total))
            
            cell.selectionStyle = .none
            return cell
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "reviewProfile") as! ReviewProfileTableCell
            cell.custName.text = bookingDict?.firstName
            let imageURL = bookingDict?.profilePic
            cell.custImage.kf.setImage(with: URL(string: imageURL!),
                                       placeholder:UIImage.init(named: "signup_profile_default_image"),
                                       options: [.transition(ImageTransition.fade(1))],
                                       progressBlock: { receivedSize, totalSize in
            },
                                       completionHandler: { image, error, cacheType, imageURL in
            })
             cell.selectionStyle = .none
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "reviewText") as! RatingReviewTableCell
            cell.ratingView.rating = 4.0
            cell.ratingView.delegate = self
            ratingBycust = 4.0
             cell.selectionStyle = .none
            return cell
        }
    }
}

extension SubmitReviewVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        activeTextView = textView
        if textView.text == "How was the customer behaviour?"{
            textView.text = nil
            textView.textColor = UIColor.black
            
        }
        self.moveViewUp(activeView: activeTextView, keyboardHeight: 250)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "How was the customer behaviour?"
            textView.textColor = UIColor.lightGray
        }else{
            if textView.text  != "How was the customer behaviour?"{
                reviewTextView = textView.text
            }
        }
    }
    
    ///MARK: - This func Count the Total characters for AboutMeController Text.
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }

        return true
    }
}


//MARK: - Rating view delegate

extension SubmitReviewVC: FloatRatingViewDelegate{
    /**
     Returns the rating value when touch events end
     */

    public func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        ratingBycust = Double(rating)
    }
}

