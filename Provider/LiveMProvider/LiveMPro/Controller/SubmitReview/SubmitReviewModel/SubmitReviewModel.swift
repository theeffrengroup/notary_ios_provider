//
//  ReviewModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 04/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import SwiftKeychainWrapper
class SubmitReviewModel {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    
    /// submit review once the booking got completed
    ///
    /// - Parameters:
    ///   - params: rating and details of bookings
    ///   - completionHandler: returns true when the api got succeeded
    func submitReview(params: [String : Any],completionHandler:@escaping (Bool) -> ()) {
        Helper.showPI(message: loading.load) 
        
        let acceptReject = SubmitReviewAPI()
        
        acceptReject.submitReview(method: API.METHOD.SUBMITREVIEW, parameters: params)
        acceptReject.SubmitReview_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    completionHandler(true)
                    break
                case .UserLoggedOut:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    Session.expired()
                    break
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false)  //Message should present
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}
