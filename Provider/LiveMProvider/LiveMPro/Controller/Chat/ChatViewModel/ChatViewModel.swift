//
//  ChatModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 09/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa


class ChatViewModel: NSObject {
    
    let disposebag = DisposeBag()
    var chatActive = [ChatData]()
    var chatPast = [ChatData]()
    

    func getThebookingChat(completionHandler:@escaping (Bool,ChatViewModel) -> ()) {

        let rxApiCall = ChatAPI()
        rxApiCall.getTheBookingsChat()
        rxApiCall.chat_Response
            .subscribe(onNext: {response in
                switch response.httpStatusCode{
                case 200:
                    if let dict = response.data["data"] as? [String:Any]  {
                        completionHandler(true,self.updateTheBookingChatData(bookingChat: dict ))
                    }
                    break
                default:
                    break
                }
            }, onError: { error in
                Helper.alertVC(errMSG: error.localizedDescription)
                print(error)
            }).disposed(by: disposebag)
    }
    
    func updateTheBookingChatData(bookingChat:[String:Any]) -> ChatViewModel {
        let bookingModel = ChatViewModel()
        if let bookingData = bookingChat["past"] as? [[String:Any]]{
            for pastChat in bookingData {
                let pastData = ChatData()
                if let bookingID = pastChat["bookingId"] as? NSNumber{
                    pastData.bookingID = Int64(bookingID)
                }
                
                if let custName = pastChat["firstName"] as? String{
                    pastData.custName = custName
                }
                
                if let custImage = pastChat["profilePic"] as? String{
                    pastData.custImage = custImage
                }
                
                if let bookingDate = pastChat["bookingRequestedAt"] as? NSNumber{
                    pastData.date = Int64(bookingDate)
                }
                
                if let categoryName = pastChat["catName"] as? String{
                    pastData.categoryName = categoryName
                }
                
                if let customerId = pastChat["customerId"] as? String{
                    pastData.customerID = customerId
                }
                
                bookingModel.chatPast.append(pastData)
            }
        }
        
        if let bookingData = bookingChat["accepted"] as? [[String:Any]]{
            for activeChat in bookingData {
                let activeData = ChatData()
                if let bookingID = activeChat["bookingId"] as? NSNumber{
                    activeData.bookingID = Int64(bookingID)
                }
                
                if let custName = activeChat["firstName"] as? String{
                    activeData.custName = custName
                }
                
                if let custImage = activeChat["profilePic"] as? String{
                    activeData.custImage = custImage
                }
                
                if let bookingDate = activeChat["bookingRequestedAt"] as? NSNumber{
                    activeData.date = Int64(bookingDate)
                }
                
                if let categoryName = activeChat["catName"] as? String{
                    activeData.categoryName = categoryName
                }
                
                if let customerId = activeChat["customerId"] as? String{
                    activeData.customerID = customerId
                }
                
                bookingModel.chatActive.append(activeData)
            }
        }
        
        return bookingModel
    }
}
