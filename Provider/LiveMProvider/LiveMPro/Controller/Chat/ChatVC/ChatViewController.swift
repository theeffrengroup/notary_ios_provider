//
//  ChatViewController.swift
//  LSP
//
//  Created by Vengababu Maparthi on 28/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var activeCountLabel: UILabel!
    @IBOutlet weak var pastCountLabel: UILabel!
    
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var pastChatButton: UIButton!
    @IBOutlet weak var activeChatButton: UIButton!
    
    @IBOutlet weak var pastTableView: UITableView!
    @IBOutlet weak var activeTableView: UITableView!
    
    var chatData = ChatViewModel()
    
    var searchActive = [ChatData]()
    var searchPast = [ChatData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.shadowView(sender: self.topView, width: UIScreen.main.bounds.size.width , height: 52)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 06/04/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(false)
        self.tabBarController?.tabBar.isHidden = false
        self.getTheChatOnGoingBookings()
    }
    
    
    func getTheChatOnGoingBookings() {
        chatData.getThebookingChat { (success, chatDict) in
            if success{
                self.chatData = chatDict
                self.activeCountLabel.text = String(describing:self.chatData.chatActive.count)
                self.pastCountLabel.text = String(describing:self.chatData.chatPast.count)
                self.activeTableView.reloadData()
                self.pastTableView.reloadData()
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pastChatAction(_ sender: Any) {
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.width ;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    @IBAction func activeChatAction(_ sender: Any) {
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.origin.x;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    
    func buttonEnable() {
        
        switch indicatorView.frame.origin.x {
        case 0:
            activeChatButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            pastChatButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        case self.view.frame.size.width / 2:
            pastChatButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            activeChatButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        default:
            activeChatButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            pastChatButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        }
    }
}

extension ChatViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1{
            if searchActive.count != 0 {
                self.activeCountLabel.text = String(describing:self.searchActive.count)
                return searchActive.count
            }
            self.activeCountLabel.text = String(describing:self.chatData.chatActive.count)
            return self.chatData.chatActive.count
        }else  if tableView.tag == 2{
            
            if searchPast.count != 0 {
                self.pastCountLabel.text = String(describing:self.searchPast.count)
                return searchPast.count
            }
            self.pastCountLabel.text = String(describing:self.chatData.chatPast.count)
            return self.chatData.chatPast.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "active") as! ActivePastChatTableCell
            if searchActive.count != 0 {
                cell.updateThechatDataOnView(data:self.searchActive[indexPath.row])
            }else{
                cell.updateThechatDataOnView(data:self.chatData.chatActive[indexPath.row])
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "past") as! ActivePastChatTableCell
            if searchPast.count != 0 {
                cell.updateThechatDataOnView(data:self.searchPast[indexPath.row])
            }else{
                cell.updateThechatDataOnView(data:self.chatData.chatPast[indexPath.row])
            }
            return cell
        }
    }
}

extension ChatViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == mainScrollView {
            
            indicatorView.frame = CGRect(x: scrollView.contentOffset.x / 2, y: indicatorView.frame.origin.y, width: self.view.frame.size.width/2, height: 2)
            buttonEnable()
        }
    }
}

extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedChat = ChatData()
        var pastBooking = false
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)
        self.tabBarController?.tabBar.isHidden = true
        
        if tableView.tag == 1{
            if searchActive.count != 0 {
                selectedChat = searchActive[indexPath.row]
            }else{
                selectedChat = self.chatData.chatActive[indexPath.row]
            }
            
        }else{
            pastBooking = true
            if searchPast.count != 0 {
                selectedChat = searchPast[indexPath.row]
            }else{
                selectedChat = self.chatData.chatPast[indexPath.row]
            }
        }
        
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatVC {
            controller.customerID = selectedChat.customerID
            controller.bookingID = String(describing:selectedChat.bookingID)
            controller.custImage = selectedChat.custImage
            controller.custName = selectedChat.custName
            controller.pastBooking = pastBooking
            if let navigator = navigationController {
                TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                          subType: CATransitionSubtype.fromTop.rawValue,
                                                                          for: (self.navigationController?.view)!,
                                                                          timeDuration: 0.3)
                navigator.pushViewController(controller, animated: false)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension ChatViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContent(searchBar.text!)
    }
    
    /// Filter Content
    ///
    /// - Parameter searchText: Search String
    fileprivate func filterContent(_ searchText: String) {
        searchActive.removeAll()
        searchPast.removeAll()
        // Populate the results
        if activeChatButton.currentTitleColor == COLOR.APP_COLOR{
            for chat: ChatData in chatData.chatActive {
                
                let custName = chat.custName.lowercased()
                if (custName.range(of: searchText.lowercased()) != nil) {
                    searchActive.append(chat)
                }
            }
        }else{
            
            for chat: ChatData in chatData.chatPast {
                
                let custName = chat.custName.lowercased()
                if (custName.range(of: searchText.lowercased()) != nil) {
                    searchPast.append(chat)
                }
            }
        }
        self.pastTableView.reloadData()
        self.activeTableView.reloadData()
    }
}
