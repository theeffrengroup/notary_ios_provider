//
//  ChatListModel.swift
//  LSP
//
//  Created by Vengababu Maparthi on 28/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

class ChatData: NSObject {
    
    var custImage = ""
    var custName = ""
    var categoryName = ""
    
    var date:Int64  = 0
    var bookingID:Int64  = 0
    var customerID = ""
    
}
