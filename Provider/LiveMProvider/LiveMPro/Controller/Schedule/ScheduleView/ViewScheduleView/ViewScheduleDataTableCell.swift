//
//  ViewScheduleDataTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 31/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit  //viewSchedule

class ViewScheduleDataTableCell: UITableViewCell {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var slotDayViewLabel: UILabel!
    @IBOutlet weak var slotPrice: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var deleteSchedulebutton: UIButton!
    @IBOutlet weak var slotTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        Helper.shadowView(sender: topView, width:UIScreen.main.bounds.size.width-30, height:topView.frame.size.height)
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateTheActiveScheduleData(viewData:ActiveSchedule?)  {
        startTime.text =  Helper.getTheScheduleViewDataFormat(timeStamp:(viewData?.startDate)!)
        endTime.text =  Helper.getTheScheduleViewDataFormat(timeStamp:(viewData?.endDate)!)
        slotDayViewLabel.text = viewData?.days
        slotTime.text = Helper.getTheScheduleViewTimeFormat(timeStamp:(viewData?.startDate)!) + " to " +  Helper.getTheScheduleViewTimeFormat(timeStamp:(viewData?.endDate)!)
    }
    
    func updateThePastScheduleData(viewData:PastSchedule?)  {
        startTime.text =  Helper.getTheScheduleViewDataFormat(timeStamp:(viewData?.startDate)!)
        endTime.text =  Helper.getTheScheduleViewDataFormat(timeStamp:(viewData?.endDate)!)
        slotDayViewLabel.text = viewData?.days
        slotTime.text = Helper.getTheScheduleViewTimeFormat(timeStamp:(viewData?.startDate)!) + " to " +  Helper.getTheScheduleViewTimeFormat(timeStamp:(viewData?.endDate)!)
    }
}
