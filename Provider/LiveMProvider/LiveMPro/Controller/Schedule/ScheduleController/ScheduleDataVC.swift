//
//  ScheduleDataVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 31/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

protocol scheduleDataDelegate {
    
    func updateTheScheduledData( days: String,indexPath:IndexPath)
    func updateTheWeekData(indexes:[IndexPath])
    func updateTheDuration( duration: String,indexPath:IndexPath)
    func updateTheStartDateEndDate( startDate: String, endDate: String)
}



class ScheduleDataVC: UIViewController {
    
    var delegate: scheduleDataDelegate?

    @IBOutlet weak var bottoMConstraint: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scheduleDataTableview: UITableView!
    var  scheduleData = ScheduleDataMod()
    var viewData = [String]()
    var typeOfView = 0
    var repeatScheduleSelected = ""
    var startDate = ""
    var endDate = ""
    
    var viewStartDate = ""
    var viewEndDate = ""
    
    var dateType = -1
    var indexesArray = [IndexPath]() // selected weeks array
    var selectedIndex = IndexPath()
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.date = Date.dateWithDifferentTimeInterval()
        datePicker.minimumDate = Date.dateWithDifferentTimeInterval()
        self.updateTheAccoridngToTheType()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was some blank space below the navigationBar
         Owner : Vani
         Date : 13/03/2020
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        setupGestureRecognizer() 
    }
    
    func updateTheAccoridngToTheType() {
        switch typeOfView {
        case 0:
            doneButton.isHidden = true
            titleLabel.text = scheduleMsgs.selectDays
            viewData = scheduleData.repeatDays
            scheduleDataTableview.reloadData()
            if selectedIndex.count != 0{
                if let cell = scheduleDataTableview.cellForRow(at: selectedIndex) as? WeekDaysTableCell {
                    cell.selectButton.isSelected = true
                }
            }
            break
        case 1:
            doneButton.isHidden = false
            scheduleDataTableview.allowsMultipleSelection = true
            titleLabel.text = scheduleMsgs.repeatDays
            if repeatScheduleSelected == "Everyday"  || repeatScheduleSelected ==  "SelectDays"{
                viewData = scheduleData.everyday
            }else if repeatScheduleSelected == "WeekDays"{
                viewData = scheduleData.weekDays
            }else {
                viewData = scheduleData.weekEnd
            }
            
            scheduleDataTableview.reloadData()
            
            for index in indexesArray{
                if let cell = scheduleDataTableview.cellForRow(at: index) as? WeekDaysTableCell {
                    cell.selectButton.isSelected = true
                }
            }
            
            break
        case 2:
            
            titleLabel.text = scheduleMsgs.duration
            viewData = scheduleData.duration
            scheduleDataTableview.reloadData()
            if selectedIndex.count != 0{
                if let cell = scheduleDataTableview.cellForRow(at: selectedIndex) as? MonthTableCell {
                    cell.selectButton.isSelected = true
                }
            }
            break
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func selectDate(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottoMConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
        
        let dateFormatter1 = DateFormatter.initTimeZoneDateFormat()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        
        let dateFormatter2 = DateFormatter.initTimeZoneDateFormat()
        dateFormatter2.dateStyle = .medium
        dateFormatter2.timeStyle = .none
        dateFormatter2.dateFormat = "dd MMMM yyyy"
    
        if dateType == 0{
            viewStartDate = dateFormatter2.string(from: datePicker.date)
            startDate = dateFormatter1.string(from: datePicker.date)
            
        }else{
            viewEndDate = dateFormatter2.string(from: datePicker.date)
            endDate = dateFormatter1.string(from: datePicker.date)
        }
        scheduleDataTableview.reloadData()
    }
    
    @IBAction func cancelDatepicker(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottoMConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    
    @IBAction func backToVC(_ sender: Any) {
        _ = navigationController?.popViewController(animated:true)
    }
    
  
    
    @IBAction func selectTheSelectedData(_ sender: Any) {
        if typeOfView == 2{
            if startDate.length != 0 && endDate.length != 0{
                delegate?.updateTheStartDateEndDate(startDate: startDate, endDate: endDate)
                _ = navigationController?.popViewController(animated:true)
            }else{
                if selectedIndex.count != 0{
                    _ = navigationController?.popViewController(animated:true)
                }else{
                    Helper.alertVC(errMSG: scheduleMsgs.selectstartEndDate)
                }
            }
        }else{
            var selectedIndexPath = [IndexPath]()
            if scheduleDataTableview.indexPathsForSelectedRows != nil{
                selectedIndexPath = scheduleDataTableview.indexPathsForSelectedRows!
            }
            
            /// if there is  selected rows or  already selected rows
            if selectedIndexPath.count != 0 || indexesArray.count != 0{
                if indexesArray.count != 0 {
                    for index in indexesArray{
                        if let cell = scheduleDataTableview.cellForRow(at: index) as? WeekDaysTableCell {
                            if cell.selectButton.isSelected{
                                selectedIndexPath.append(index)
                            }
                        }
                    }
                    if selectedIndexPath.count != 0{
                        delegate?.updateTheWeekData(indexes: selectedIndexPath.removeDuplicates()) // removes the duplicate elements
                    }else{
                        Helper.alertVC(errMSG: scheduleMsgs.selectDuration)
                        return
                    }
                }else{
                    delegate?.updateTheWeekData(indexes: selectedIndexPath)
                }
                _ = navigationController?.popViewController(animated:true)
                
            }else{
                Helper.alertVC(errMSG: scheduleMsgs.selectDuration)
            }
        }
    }
    
    @objc func addTheStartEndDate(_ sender : UIButton){
        doneButton.isHidden = false
        if sender.tag == 5{
            dateType = 0
            if startDate.length != 0{
                let dateFormatter = DateFormatter.initTimeZoneDateFormat()
                dateFormatter.dateFormat = "yyyy-MM-dd" //"dd MMMM yyyy"
                let date = dateFormatter.date(from: viewStartDate)
                datePicker.date = date!
            }
        }else{
            dateType = 1
            if endDate.length != 0{
                let dateFormatter = DateFormatter.initTimeZoneDateFormat()
                dateFormatter.dateFormat = "yyyy-MM-dd" //"dd MMMM yyyy"
                let date = dateFormatter.date(from: viewEndDate)
                datePicker.date = date!
            }
        }
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottoMConstraint.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
}

extension ScheduleDataVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if typeOfView == 2 {
            return viewData.count 
        }else{
            return viewData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch typeOfView {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeekDays") as! WeekDaysTableCell
            cell.weekDayLabel.text = viewData[indexPath.row]
            cell.selectionStyle = .none
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeekDays") as! WeekDaysTableCell
            cell.weekDayLabel.text = viewData[indexPath.row]
            cell.selectionStyle = .none
            return cell
            
        default:
            switch indexPath.row{
            case 5:
                let dateCell = tableView.dequeueReusableCell(withIdentifier: "StartEndDate") as! StartEndDateTableCell
                dateCell.startEndDateLabel.text = viewData[indexPath.row]
                dateCell.selectionStyle = .none
                if startDate.length != 0{
                    dateCell.selectDateButton.setTitle(startDate, for: .normal)
                }else{
                    dateCell.selectDateButton.setTitle(scheduleMsgs.selectDate, for: .normal)
                }
                dateCell.selectDateButton.tag = indexPath.row
                dateCell.selectDateButton.addTarget(self, action: #selector(addTheStartEndDate(_:)), for:.touchUpInside)
                return dateCell
                
            case 6:
                let dateCell = tableView.dequeueReusableCell(withIdentifier: "StartEndDate") as! StartEndDateTableCell
                dateCell.startEndDateLabel.text = viewData[indexPath.row]
                dateCell.selectionStyle = .none
                if endDate.length != 0{
                    dateCell.selectDateButton.setTitle(endDate, for: .normal)
                }else{
                    dateCell.selectDateButton.setTitle(scheduleMsgs.selectDate, for: .normal)
                }
                dateCell.selectDateButton.tag = indexPath.row
                dateCell.selectDateButton.addTarget(self, action: #selector(addTheStartEndDate(_:)), for:.touchUpInside)
                return dateCell
                
            case 4:
                let cellHeader = tableView.dequeueReusableCell(withIdentifier: "ScheduleDataHeader") as! AddScheduleHeaderTableCell
                cellHeader.headerLabel.text = viewData[indexPath.row]
                cellHeader.selectionStyle = .none
                return cellHeader
            default:
                let dateCell = tableView.dequeueReusableCell(withIdentifier: "Month") as! MonthTableCell
                   dateCell.selectionStyle = .none
                dateCell.selectButton.isSelected = false
                dateCell.monthLabel.text = viewData[indexPath.row]
                return dateCell
            }
        }
    }
}

extension ScheduleDataVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 4{
            return 40
        }else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch typeOfView {
        case 0:
            if let cell = tableView.cellForRow(at: indexPath) as? WeekDaysTableCell {
                cell.selectButton.isSelected = true
            }
            delegate?.updateTheScheduledData(days: viewData[indexPath.row], indexPath: indexPath)
            _ = navigationController?.popViewController(animated:true)
            break
        case 1:
            if let cell = tableView.cellForRow(at: indexPath) as? WeekDaysTableCell {
                cell.selectButton.isSelected = true
            }
            break
        default:
            doneButton.isHidden = true
            if let cell = tableView.cellForRow(at: indexPath) as? MonthTableCell {
                cell.selectButton.isSelected = true
                delegate?.updateTheDuration( duration: viewData[indexPath.row], indexPath: indexPath)
                _ = navigationController?.popViewController(animated:true)
            }
            break
        }
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if typeOfView == 0 || typeOfView == 1 {
            if let cell = tableView.cellForRow(at: indexPath) as? WeekDaysTableCell {
                cell.selectButton.isSelected = false
            }
        }
        else if typeOfView == 2{
            if let cell = tableView.cellForRow(at: indexPath) as? MonthTableCell {
                cell.selectButton.isSelected = false
            }
        }
    }
}


extension ScheduleDataVC: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}


extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}




