//
//  AddScheduleVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 31/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class AddScheduleVC: UIViewController {
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var datePickerTitle: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var addScheduleTableView: UITableView!
    var addScheduleMod = AddSchedModel()
    var scheduleData = [[String:String]]()
    var repeatSchedule = ""
    var repeatScheduleType = -1
    
    var repeatOnDays = ""
    
    var startTime = ""
    var endTime = ""
    var viewStartTime = ""
    var viewEndTime = ""
    
    var selectStartDate = ""
    var selectEndDate = ""
    
    var addressSelected = ""
    var addressSelectedID = ""
    
    var timeType = -1
    var viewData = [String]()
    var addScheduleData = ScheduleDataMod()
    
    var indexPath1 = IndexPath()
    var indexPath2 = IndexPath()
    var indexPathArray = [IndexPath]()
    override func viewDidLoad() {
        super.viewDidLoad()
        scheduleData = addScheduleMod.addSchedule
        addScheduleTableView.reloadData()

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }    
    
    @IBAction func cancelDatePicker(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottomConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    @IBAction func selectDate(_ sender: Any) {
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottomConstraint.constant = -220
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
        
        let dateFormatter1 = DateFormatter.initTimeZoneDateFormat()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        dateFormatter1.dateFormat = "HH:mm"
        
        let dateFormatter2 = DateFormatter.initTimeZoneDateFormat()
        dateFormatter2.dateStyle = .medium
        dateFormatter2.timeStyle = .none
        dateFormatter2.dateFormat = "hh:mm a"
    
        
        
        if timeType == 0{
           self.viewStartTime =  dateFormatter2.string(from: datePicker.date)
            self.startTime = dateFormatter1.string(from: datePicker.date)
        }else if timeType == 1{
            self.viewEndTime =  dateFormatter2.string(from: datePicker.date)
            self.endTime = dateFormatter1.string(from: datePicker.date)
        }
        addScheduleTableView.reloadData()
    }
    
    
    @IBAction func confirmTheData(_ sender: Any) {
        
        if repeatSchedule.length == 0{
            Helper.alertVC(errMSG: scheduleMsgs.selectRepeatSched)
        }else if repeatOnDays.length == 0{
            Helper.alertVC(errMSG: scheduleMsgs.selectRepeatDays)
        }else if selectStartDate.length == 0{
            Helper.alertVC(errMSG: scheduleMsgs.selectStartData)
        }else if selectEndDate.length == 0{
            Helper.alertVC(errMSG: scheduleMsgs.selectEndData)
        }else if startTime.length == 0{
            Helper.alertVC(errMSG: scheduleMsgs.selectStartTime)
        }else if endTime.length == 0{
            Helper.alertVC(errMSG: scheduleMsgs.selectEndTime)
        }else if addressSelected.length == 0{
            Helper.alertVC(errMSG: scheduleMsgs.selectAddress)
        }else{
            var selectDays = [String]()
            if repeatSchedule == "SelectDays"{
                for index in indexPathArray{
                    
                    switch viewData[index.row]{
                    case "Sunday":
                        selectDays.append("Sun")
                        break
                    case "Monday":
                        selectDays.append("Mon")
                        break
                    case "Tuesday":
                        selectDays.append("Tue")
                        break
                    case "Wednessday":
                        selectDays.append("Wed")
                        break
                    case "Thursday":
                        selectDays.append("Thu")
                        break
                    case "Friday":
                        selectDays.append("Fri")
                        break
                    case "Saturday":
                        selectDays.append("Sat")
                        break
                    default:
                        break
                    }
                }
            }
            
            let paramObject =  AddScheduleParamObject.init(idAddress: addressSelectedID, timeStart: startTime, timeEnd: endTime, dateStart: selectStartDate, dateEnd: selectEndDate, dayRepeat: repeatScheduleType, selectDays: selectDays)
            
            addScheduleMod.postTheScheduleData(params: paramObject, completionHandler: { (succeeded) in
                if succeeded{
                    
                    self.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
    
    @IBAction func backToSchedule(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func addTheStartEndTime(_ sender : UIButton){
        
        let dateFormatter2 = DateFormatter.initTimeZoneDateFormat()
        dateFormatter2.dateStyle = .medium
        dateFormatter2.timeStyle = .none
        dateFormatter2.dateFormat = "hh:mm a"
        
        self.datePicker.setDate(dateFormatter2.date(from: dateFormatter2.string(from: Helper.roundTheTime()))!, animated: true)

        if sender.tag == 4{
            timeType = 0
            if viewStartTime.length > 0{
                let dateFormatter = DateFormatter.initTimeZoneDateFormat()
                dateFormatter.dateFormat =  "hh:mm a"
                let date = dateFormatter.date(from: viewStartTime)
                datePicker.date = date!
            }
            
        }else{
            timeType = 1
            if viewEndTime.length > 0{
                let dateFormatter = DateFormatter.initTimeZoneDateFormat()
                dateFormatter.dateFormat =  "hh:mm a"
                let date = dateFormatter.date(from: viewEndTime)
                datePicker.date = date!
            }
        }
        
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        self.bottomConstraint.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    
    func startOfMonth() -> Date {
        let date = Date.dateWithDifferentTimeInterval()
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: date)
        let startOfMonth = Calendar.current.date(from: comp)!
        return startOfMonth
    }
    
    func startMonth() -> String {
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let startDate = dateFormatter.string(from: Date())
        return startDate
    }
    
    func endOfMonth(val:Int) -> String {
        var comps2 = DateComponents()
        comps2.month = val
        comps2.day = -1
        let endOfMonth = Calendar.current.date(byAdding: comps2, to: startOfMonth())
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let endMonth = dateFormatter.string(from: endOfMonth!)
        return endMonth
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {  //toAddDetailsForSchedule
        let index = sender as! Int
        switch index {
        case 0:
            if segue.identifier == "toAddDetailsForSchedule"{
                if let nextScene = segue.destination as?  ScheduleDataVC{
                    nextScene.typeOfView =  sender as! Int
                    nextScene.delegate = self
                    nextScene.selectedIndex = indexPath1
                }
            }
            
            break
        case 1:
            if segue.identifier == "toAddDetailsForSchedule"{
                if let nextScene = segue.destination as?  ScheduleDataVC{
                    nextScene.typeOfView =  sender as! Int
                    nextScene.repeatScheduleSelected = repeatSchedule
                    nextScene.delegate = self
                    nextScene.indexesArray = indexPathArray
                }
            }
            
            break
        case 2:
            if segue.identifier == "toAddDetailsForSchedule"{
                if let nextScene = segue.destination as?  ScheduleDataVC{
                    nextScene.typeOfView =  sender as! Int
                    nextScene.delegate = self
                    if indexPath2.count != 0{
                        nextScene.selectedIndex = indexPath2
                    }else{
                        if selectStartDate.length != 0{
                            nextScene.startDate = selectStartDate
                            nextScene.endDate = selectEndDate
                            
                            nextScene.viewStartDate = selectStartDate
                            nextScene.viewEndDate = selectEndDate
                          
                        }
                    }
                }
            }
            
            break
        case 7:
            if segue.identifier == "scheduleAddress"{
                if let nextScene = segue.destination as?  ArtistAddressVC{
                    nextScene.fromSchedule = true
                    nextScene.delegate = self
                }
            }
            break
        default:
            
            break
        }
    }
    
    ///This function calculates the height of content Label  ListingCell row's and returns the height of the label.
    func heightForView(text: String, width: CGFloat) -> CGFloat {
        
        let label: UILabel = UILabel(frame: CGRect(x: 0,
                                                   y: 0,
                                                   width: width,
                                                   height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
}

extension AddScheduleVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 || indexPath.row == 6{
            return 40
        }else if indexPath.row == 1{
            var width: CGFloat = 0.0
            var height: CGFloat = 0.0
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleDetails") as! AddScheduleDetailsTableCell
            if repeatOnDays.length != 0 {
                cell.valueLabel.text = repeatOnDays
            }else{
                cell.valueLabel.text = scheduleData[indexPath.row]["val"]
            }
            width = cell.contentView.frame.width
            width = width - 60.0
            height =  heightForView(text: cell.valueLabel.text! , width: width)
            height = height + 30.0
            return height
        }
        else if indexPath.row == 7{
            var width: CGFloat = 0.0
            var height: CGFloat = 0.0
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleAddress") as! AddScheduleAddressTableCell
            if addressSelected.length != 0{
                cell.viewAddress.text = addressSelected
            }else{
                cell.viewAddress.text = scheduleMsgs.addAddress
            }
            width = cell.contentView.frame.width
            width = width - 60.0
            height =  heightForView(text: cell.viewAddress.text! , width: width)
            height = height + 30.0
            return height
        }else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row { //repeatSchedule
            
        case 0:
            self.performSegue(withIdentifier: "toAddDetailsForSchedule", sender: indexPath.row)
            break
        case 1:
            if repeatSchedule.length != 0 {
                if repeatSchedule == "SelectDays"{
                    self.performSegue(withIdentifier: "toAddDetailsForSchedule", sender: indexPath.row)
                }
            }else{
                Helper.alertVC(errMSG: scheduleMsgs.selectSched)
            }
            break
        case 2:
            self.performSegue(withIdentifier: "toAddDetailsForSchedule", sender: indexPath.row)
            break
        case 7:
            self.performSegue(withIdentifier: "scheduleAddress", sender: indexPath.row)
            break
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension AddScheduleVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleDetails") as! AddScheduleDetailsTableCell
        
        switch indexPath.row {
        case 0:
            cell.keyLabel.text = scheduleData[indexPath.row]["key"]    // everyday, weekends, weekdays
            if repeatSchedule.length != 0{
                cell.valueLabel.text = repeatSchedule
            }else{
                cell.valueLabel.text = scheduleData[indexPath.row]["val"]
            }
            cell.selectTimeButton.isHidden = true
            return cell
            
        case 1:
            cell.keyLabel.text = scheduleData[indexPath.row]["key"]   // this monday, sunday, tuesday ....
            if repeatOnDays.length != 0 {
                cell.valueLabel.text = repeatOnDays
            }else{
                cell.valueLabel.text = scheduleData[indexPath.row]["val"]
            }
            cell.selectTimeButton.isHidden = true
            return cell
            
        case 2:
            cell.keyLabel.text = scheduleData[indexPath.row]["key"]   // duration this month, 2months, 3months, 4months
            if selectStartDate.length != 0{
                cell.valueLabel.text = selectStartDate + " to " + selectEndDate
            }else{
                cell.valueLabel.text = scheduleData[indexPath.row]["val"]
            }
            cell.selectTimeButton.isHidden = true
            return cell
            
        case 3:
            let cellHeader = tableView.dequeueReusableCell(withIdentifier: "ScheduleHeader") as! AddScheduleHeaderTableCell
            return cellHeader
            
        case 4:
            cell.keyLabel.text = scheduleData[indexPath.row-1]["key"]
            if startTime.length != 0 {
                cell.valueLabel.text = viewStartTime
            }else{
                cell.valueLabel.text = scheduleData[indexPath.row-1]["val"]
            }
            cell.selectTimeButton.isHidden = false
            cell.selectTimeButton.tag = indexPath.row
            cell.selectTimeButton.addTarget(self, action: #selector(addTheStartEndTime(_:)), for:.touchUpInside)
            return cell
            
        case 5:
            cell.keyLabel.text =  scheduleData[indexPath.row-1]["key"]
            if endTime.length != 0 {
                cell.valueLabel.text = viewEndTime
            }else{
                cell.valueLabel.text = scheduleData[indexPath.row-1]["val"]
            }
            cell.selectTimeButton.isHidden = false
            cell.selectTimeButton.tag = indexPath.row
            cell.selectTimeButton.addTarget(self, action: #selector(addTheStartEndTime(_:)), for:.touchUpInside)
            return cell
            
        case 6:
            let cellHeader = tableView.dequeueReusableCell(withIdentifier: "ScheduleHeader") as! AddScheduleHeaderTableCell
            cellHeader.headerLabel.text = scheduleMsgs.location
            return cellHeader
            
        default:
            let cellAddress = tableView.dequeueReusableCell(withIdentifier: "ScheduleAddress") as! AddScheduleAddressTableCell
            if addressSelected.length != 0{
                cellAddress.viewAddress.text = addressSelected
            }else{
                cellAddress.viewAddress.text = scheduleMsgs.addAddress
            }
            return cellAddress
            
        }
    }
}

extension AddScheduleVC:scheduleDataDelegate{
    func updateTheDuration(duration: String, indexPath: IndexPath) {  // 1month , 2month , this month
        indexPath2 = indexPath
        switch duration {
        case "This Month":
            selectStartDate = self.startMonth()
            selectEndDate = self.endOfMonth(val: 1)
            break
        case "2 Months":
            selectStartDate = self.startMonth()
            selectEndDate = self.endOfMonth(val: 2)
            break
        case "3 Months":
            selectStartDate = self.startMonth()
            selectEndDate = self.endOfMonth(val: 3)
            break
        case "4 Months":
            selectStartDate = self.startMonth()
            selectEndDate = self.endOfMonth(val: 4)
            break
        default:
            break
        }
        addScheduleTableView.reloadData()
    }
    
    func updateTheStartDateEndDate( startDate: String, endDate: String) { // start date , end date
        selectStartDate = startDate
        selectEndDate = endDate
        indexPath2 = IndexPath()
        addScheduleTableView.reloadData()
    }
    
    func updateTheScheduledData( days: String, indexPath: IndexPath) {  // everyday,weekends, weekdays
        indexPathArray = [IndexPath]()
        repeatSchedule = days
        indexPath1 = indexPath
        repeatOnDays = ""
        switch repeatSchedule {
        case "Everyday":
            repeatScheduleType = 1
            for day in addScheduleData.everyday{
                if repeatOnDays.length != 0{
                    repeatOnDays = repeatOnDays + "," + day
                }else{
                    repeatOnDays = day
                }
            }
            viewData = addScheduleData.everyday
            break
        case "WeekDays":
            repeatScheduleType = 2
            for day in addScheduleData.weekDays{
                if repeatOnDays.length != 0{
                    repeatOnDays = repeatOnDays + "," + day
                }else{
                    repeatOnDays = day
                }
            }
            viewData = addScheduleData.weekDays
            break
        case "WeekEnd":
            repeatScheduleType = 3
            for day in addScheduleData.weekEnd{
                if repeatOnDays.length != 0{
                    repeatOnDays = repeatOnDays + "," + day
                }else{
                    repeatOnDays = day
                }
            }
            break
        case "SelectDays":
            repeatScheduleType = 4
            viewData = addScheduleData.everyday
        default:
            break
        }
        addScheduleTableView.reloadData()
    }
    
    func updateTheWeekData(indexes: [IndexPath]) { // week days mon,sun,tues,
        indexPathArray = indexes
        repeatOnDays = ""
        for indexpath in indexes{
            if repeatOnDays.length != 0{
                repeatOnDays = repeatOnDays + "," + viewData[indexpath.row]
            }else{
                repeatOnDays = viewData[indexpath.row]
            }
        }
        addScheduleTableView.reloadData()
    }
}

extension AddScheduleVC : ArtistAddressDelegate{
    func updateArtistDelegate(address: String,addressID:String){
        addressSelected = address
        addressSelectedID = addressID
        addScheduleTableView.reloadData()
    }
}

