//
//  ViewScheduleVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 31/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ViewScheduleVC: UIViewController {

    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var activeButton: UIButton!
    @IBOutlet weak var pastButton: UIButton!
    @IBOutlet weak var viewScheduleTableView: UITableView!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var pastScheduleTableView: UITableView!
    @IBOutlet weak var noPastScheduleView: UIView!
      @IBOutlet weak var noScheduleView: UIView!
    
    var viewModel = ViewScheduleModel()
    var viewModelData :ViewScheduleModel?
    var indexDeleted = 0
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getTheScheduleData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func activeBtnTapped(_ sender: Any) {
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.origin.x;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    @IBAction func pastBtnTapped(_ sender: Any) {
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.width ;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    func getTheScheduleData()  {
        viewModel.getTheScheduleData { (succeeded, viewScheduleData) in
            if succeeded{
                self.viewModelData = viewScheduleData
                self.viewScheduleTableView.reloadData()
                self.pastScheduleTableView.reloadData()
            }
        }
    }
    
   
    
    @IBAction func backToSchedule(_ sender: Any) {
           self.dismiss(animated: true, completion: nil)
    }
    
    @objc func deleteTheSchedule(_ sender : UIButton){
        indexDeleted = sender.tag
        self.alertForDeleteSchedule()
    }
    
    func alertForDeleteSchedule(){
        let params: [String : Any] =  [
            "scheduleId" :self.viewModelData?.activeData[indexDeleted].schedID]
            //viewModelData[indexDeleted].schedID]
        let alert = UIAlertController(title: "Schedule", message: "Are you sure, you want to Delete??", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            self.viewModel.deleteTheSchedule(dictParam:params, completionHandler: { (succeeded) in
                if  succeeded{
                    self.dismiss(animated: true, completion: nil)
                  //  self.getTheScheduleData()
                }
            })
        })
        let noButton = UIAlertAction(title: "No", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            print("you pressed No, thanks button")
            // call method whatever u need
        })
        alert.addAction(yesButton)
        alert.addAction(noButton)
        present(alert, animated: true) //{ _ in }
    }
}

extension ViewScheduleVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 185
    }
}

extension ViewScheduleVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch tableView {
            
        case viewScheduleTableView:
            if viewModelData?.activeData.count != 0{
                noScheduleView.isHidden  = true
                return 1
            }else{
                noScheduleView.isHidden  = false
                return 0
            }
            
        case pastScheduleTableView:
            if viewModelData?.pastData.count != 0{
                noPastScheduleView.isHidden  = true
                return 1
            }else{
                noPastScheduleView.isHidden  = false
                return 0
            }
        default:
            
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case viewScheduleTableView:
            
            switch section {
            case 0:
                if viewModelData?.activeData.count != 0 &&   viewModelData?.activeData.count != nil {
                    return  (self.viewModelData?.activeData.count)!
                }
                else{
                    return 0
                }
            default:
                return 0
            }
            
        case pastScheduleTableView:
            
            switch section {
            case 0:
                if viewModelData?.pastData.count != 0 && viewModelData?.pastData.count != nil {
                    return (self.viewModelData?.pastData.count)!
                }
                else{
                    return 0
                }
            default:
                return 0
            }
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        switch tableView {
        case viewScheduleTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "viewSchedule") as! ViewScheduleDataTableCell
            cell.updateTheActiveScheduleData(viewData: self.viewModelData?.activeData[indexPath.row])
           // cell.updateTheViewScheduleData(viewData:self.viewModelData?.activeData[indexPath.row])
            cell.deleteSchedulebutton.tag = indexPath.row
            cell.deleteSchedulebutton.addTarget(self, action: #selector(deleteTheSchedule(_:)), for:.touchUpInside)
            cell.selectionStyle = .none
            return cell
            
        case pastScheduleTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "viewSchedule") as! ViewScheduleDataTableCell
            cell.updateThePastScheduleData(viewData: self.viewModelData?.pastData[indexPath.row])
           //cell.updateTheViewScheduleData(viewData:self.viewModelData?.pastData[indexPath.row])
            cell.selectionStyle = .none
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension ViewScheduleVC: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == mainScrollView {
            
            indicatorView.frame = CGRect(x: scrollView.contentOffset.x / 2, y: indicatorView.frame.origin.y, width: self.view.frame.size.width/2, height: 2)
            buttonEnable()
        }
    }
    
    func buttonEnable() {
        
        switch indicatorView.frame.origin.x {
        case 0:
            activeButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            pastButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        case self.view.frame.size.width / 2:
            
            pastButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            activeButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        default:
            activeButton.setTitleColor(COLOR.APP_COLOR, for: .normal)
            pastButton.setTitleColor(Helper.colorFromHexString(hexCode: "3b3b3b"), for: .normal)
            
        }
    }
}
