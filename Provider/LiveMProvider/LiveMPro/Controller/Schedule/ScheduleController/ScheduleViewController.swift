//
//  ScheduleViewController.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 09/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import JTCalendar


class ScheduleViewController: UIViewController,JTCalendarDelegate {
    @IBOutlet weak var calendarMenuView: JTCalendarMenuView!
    
    @IBOutlet weak var noScheduleDataLabel: UILabel!
    @IBOutlet weak var emptyScheduleImage: UIImageView!
    @IBOutlet weak var calendarView: JTHorizontalCalendarView!
    @IBOutlet weak var scheduleTableview: UITableView!
    var calendarManager = JTCalendarManager()
    var dateSelected =  Date.dateWithDifferentTimeInterval()
    var todayDate: Date?
    var minDate: Date?
    var maxDate: Date?
    var eventsByDate = [[String:Any]]()
    var floaty = Floaty()
    var scheduleMod = ScheduleModel()
    var scheudleModData = [String:Any]()
    
    override func viewDidLoad() {
         self.layoutFAB()
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      
    }
    
    override func viewWillAppear(_ animated: Bool){
        todayDate = Date.dateWithDifferentTimeInterval()
        calendarManager.delegate = self
        floaty.plusColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        createMinAndMaxDate()
        calendarManager.menuView = calendarMenuView
        calendarManager.contentView = calendarView
        calendarManager.setDate(todayDate)
        self.getSlotDetailsAccording(toMonths: todayDate!)
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(false)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func getSlotDetailsAccording(toMonths dateString: Date) {
        let formatter = DateFormatter.initTimeZoneDateFormat()
        formatter.dateFormat = "MM-yyyy"
        let monthSelected = formatter.string(from: dateString)
        scheduleMod.getTheScheduledDataMonthWise(month: monthSelected) { (scheduledData) in
            self.scheudleModData = scheduledData
            self.calendarManager.reload()
            self.scheduleTableview.reloadData()
        }
    }


    /// floating button to create schedule and to view the created schedule
    func layoutFAB() {
        
        floaty.hasShadow = true
        
        floaty.addItem(scheduleMsgs.addSched, icon: UIImage(named: "add")) { item in
            self.performSegue(withIdentifier: "addSchedule", sender: nil)
        }
        
        floaty.addItem(scheduleMsgs.viewSched, icon: UIImage(named: "visible")) { item in
            self.performSegue(withIdentifier: "viewSchedule", sender: nil)
        }
        
        floaty.paddingX = 10
        floaty.paddingY = 80
        floaty.fabDelegate = self
        self.view.addSubview(floaty)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func preCalendar(_ sender: Any) {
        DispatchQueue.main.async {
            self.calendarView.loadPreviousPageWithAnimation()
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func postCalendar(_ sender: Any) {
        DispatchQueue.main.async {
              self.calendarView.loadNextPageWithAnimation()
             self.view.layoutIfNeeded()
        }
      
    }
    
    // Used only to have a key for _eventsByDate
    func dateFormatter() -> DateFormatter {
        var dateFormatter: DateFormatter?
        if dateFormatter == nil {
            dateFormatter = DateFormatter.initTimeZoneDateFormat()
            //dateFormatter?.timeZone = TimeZone.current
            dateFormatter?.dateFormat = "yyyy-MM-dd"
            //dd-MM-yyyy//"yyyy-MM-dd"
        }
        return dateFormatter ?? DateFormatter()
    }
  
    
    func createMinAndMaxDate() {
        todayDate = Date.dateWithDifferentTimeInterval()
        // Min date will be 9 month before today
        minDate = calendarManager.dateHelper!.add(to: todayDate, months: -9)
        maxDate = calendarManager.dateHelper!.add(to: todayDate, months: 9)
    }
    
    func haveEvent(forDay date: Date) -> Bool {
        let key: String = dateFormatter().string(from:date)
        if let dictArray = scheudleModData[key] as? [[String:Any]]{
            if dictArray.count > 0{
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    
    // Example of implementation of prepareDayView method
    
    
    func calendar(_ calendar: JTCalendarManager!, prepareDayView dayView: UIView!) {
        
        if   let dayview1 = dayView as? JTCalendarDayView{

            DispatchQueue.global().async{
                sleep(1);
                DispatchQueue.main.async{
                    if self.calendarManager.dateHelper!.date(Date(), isTheSameDayThan: dayview1.date) {
                    dayview1.circleView.isHidden = false
                    dayview1.circleView.backgroundColor = COLOR.APP_COLOR
                    dayview1.dotView.backgroundColor = UIColor.white
                    dayview1.textLabel?.textColor = UIColor.white
                }
                    else if  self.calendarManager.dateHelper!.date(self.todayDate, isTheSameDayThan: dayview1.date) { //!dateSelected.isEmpty &&
                    dayview1.circleView.isHidden = false
                    dayview1.circleView.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x808080)
                    dayview1.dotView.backgroundColor = COLOR.APP_COLOR
                    dayview1.textLabel?.textColor = UIColor.white
                }
                    else if !(self.calendarManager.dateHelper!.date(self.calendarView.date, isTheSameMonthThan: dayview1.date)) {
                    dayview1.circleView.isHidden = true
                    dayview1.dotView.backgroundColor = COLOR.APP_COLOR
                    dayview1.textLabel?.textColor = UIColor.lightGray
                }
                else {
                    dayview1.circleView.isHidden = true
                    dayview1.dotView.backgroundColor = COLOR.APP_COLOR
                    dayview1.textLabel?.textColor = UIColor.black
                }

                // Your method to test if a date have an event for example
                    if self.haveEvent(forDay: dayview1.date) {
                    dayview1.dotView.isHidden = false
                }
                else {
                    dayview1.dotView.isHidden = true
                }
            }
          }
        }
    }
    func calendar(_ calendar: JTCalendarManager!, didTouchDayView dayView: UIView!) {
        
        if let dayview1 = dayView as? JTCalendarDayView{
            todayDate = dayview1.date
           dayview1.circleView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            UIView.transition(with: dayview1 as UIView , duration: 0.3, options: [], animations: {() -> Void in
                dayview1.circleView.transform = CGAffineTransform.identity
                self.calendarManager.reload()
                self.scheduleTableview.reloadData()
            }) //{ _ in }
            if !(calendarManager.dateHelper!.date(calendarView.date, isTheSameMonthThan: dayview1.date)) {
                if calendarView.date.compare(dayview1.date) == .orderedAscending {
                    calendarView.loadNextPageWithAnimation()
                }
                else {
                    calendarView.loadPreviousPageWithAnimation()
                }
            }
        }
    }
    
    // Used to limit the date for the calendar, optional
    func calendar(_ calendar: JTCalendarManager?, canDisplayPageWith date: Date?) -> Bool {
        return calendarManager.dateHelper!.date(date, isEqualOrAfter: minDate, andEqualOrBefore: maxDate)
    }
    
    
    func calendarDidLoadNextPage(_ calendar: JTCalendarManager?) {
        DispatchQueue.global().async{
            sleep(1);
            DispatchQueue.main.async{
                self.getSlotDetailsAccording(toMonths: calendar!.date())
            }
        }
     
    }
    
    func calendarDidLoadPreviousPage(_ calendar: JTCalendarManager?) {
        DispatchQueue.global().async{
            sleep(1);
            DispatchQueue.main.async{
                self.getSlotDetailsAccording(toMonths: calendar!.date())
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "completedBookingDetails":
            let nav = segue.destination as! UINavigationController
            if let nextScene:PastBookingController  = nav.viewControllers.first as! PastBookingController? {
                nextScene.bookingId = String(describing:sender!)
            }
            break
        case "onGoingScheduleBooking":
            let nav = segue.destination as! UINavigationController
            if let nextScene:ScheduleBookingDetailsVC  = nav.viewControllers.first as! ScheduleBookingDetailsVC? {
                nextScene.bookingId = String(describing:sender!)
            }
            break
        default:
            break
        }
    }
            
       
}

extension ScheduleViewController:FloatyDelegate{
    // MARK: - Floaty Delegate Methods
    func floatyWillOpen(_ floaty: Floaty) {
        print("Floaty Will Open")
    }
    
    func floatyDidOpen(_ floaty: Floaty) {
        print("Floaty Did Open")
    }
    
    func floatyWillClose(_ floaty: Floaty) {
        print("Floaty Will Close")
    }
    
    func floatyDidClose(_ floaty: Floaty) {
        print("Floaty Did Close")
    }
}

extension ScheduleViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let bookingDict = eventsByDate[indexPath.section]["booked"] as? [[String:Any]]{
            
            if bookingDict.count > 0 {
                return 120
            }
        
            else {
                 return 75
            }
        }
       return CGFloat()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let bookingDict = eventsByDate[indexPath.section]["booked"] as? [[String:Any]]{
            if bookingDict.count + 1 == indexPath.row || indexPath.row == 0{
                tableView.deselectRow(at: indexPath, animated: true)
                return
            }
            
            if let status = bookingDict[indexPath.row - 1]["status"] as? NSNumber{
                if status == 10{
                    if let bookingID = bookingDict[indexPath.row - 1]["bookingId"] as? NSNumber{
                        self.performSegue(withIdentifier: "completedBookingDetails", sender: bookingID)
                    }
                }else{
                    if let bookingID = bookingDict[indexPath.row - 1]["bookingId"] as? NSNumber{
                        self.performSegue(withIdentifier: "onGoingScheduleBooking", sender: bookingID)
                    }
                }
            }
            
        }
    }
}

extension ScheduleViewController:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        let key: String = dateFormatter().string(from:todayDate!)
        if let dict = scheudleModData[key] as? [[String:Any]]{
            eventsByDate = dict
            
            if eventsByDate.count > 0 {
                noScheduleDataLabel.isHidden = true
                emptyScheduleImage.isHidden = true
            }else{
                noScheduleDataLabel.isHidden = false
                emptyScheduleImage.isHidden = false
            }
            return eventsByDate.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let bookingDict = eventsByDate[section]["booked"] as? [[String:Any]]{
              return bookingDict.count + 2
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scheduleTableCell") as! ScheduleTableViewCell
        
        var count = 0
        if let dict = eventsByDate[indexPath.section]["booked"] as? [[String:Any]]{
            count = dict.count
        }
        
        switch indexPath.row {
        case 0:
            let innerSchedule =  eventsByDate[indexPath.section]
            if let startDate = innerSchedule["startTime"] as? NSNumber{
               // cell.scheduledTime.text =       Helper.getTheScheduleTimeFormat(timeStamp:Int64(startDate))
                cell.scheduledTime.text =       Helper.getTheScheduleViewTimeFormat(timeStamp:Int64(startDate))
                cell.amORPmLabel.text =   Helper.getTheScheduleTimeAmorPM(timeStamp:Int64(startDate))
                cell.eventTime.text = ""
                cell.eventNameAndCustomerName.text = ""
            }
            
            cell.contentView.backgroundColor = COLOR.WHITE
            break
        case count + 1:
            let innerSchedule =  eventsByDate[indexPath.section]
            if let endDate = innerSchedule["endTime"] as? NSNumber{
                //cell.scheduledTime.text = Helper.getTheScheduleTimeFormat(timeStamp:Int64(endDate))
                cell.scheduledTime.text = Helper.getTheScheduleViewTimeFormat(timeStamp:Int64(endDate))
                cell.amORPmLabel.text =   Helper.getTheScheduleTimeAmorPM(timeStamp:Int64(endDate))
                cell.eventTime.text = ""
                cell.eventNameAndCustomerName.text = ""
            }
            
            cell.contentView.backgroundColor = COLOR.WHITE
            break
        default:
            if let bookingDict = eventsByDate[indexPath.section]["booked"] as? [[String:Any]]{
                if let startDate = bookingDict[indexPath.row - 1]["start"] as? NSNumber{
                    //cell.scheduledTime.text = Helper.getTheScheduleTimeFormat(timeStamp:Int64(startDate))
                    let schStart = Helper.getTheScheduleViewTimeFormat(timeStamp:Int64(startDate))
                    cell.amORPmLabel.text =   Helper.getTheScheduleTimeAmorPM(timeStamp:Int64(startDate))
                    if let endDate =  bookingDict[indexPath.row - 1]["end"] as? NSNumber{
                        let sEnd = Helper.getTheScheduleViewTimeFormat(timeStamp:Int64(endDate))
                        cell.scheduledTime.text = "\(schStart)\nto\n\(sEnd)"
                        
                        
                        
                            //schStart + " to " +
                        cell.eventTime.text = "( " + Helper.getTheScheduleViewTimeFormat(timeStamp:Int64(startDate)) + " - " +       Helper.getTheScheduleViewTimeFormat(timeStamp:Int64(endDate)) + " )"
                    }
                }
                
                if let custName = bookingDict[indexPath.row - 1]["firstName"] as? String{
                    if let event = bookingDict[indexPath.row - 1]["event"] as? String{
                        cell.eventNameAndCustomerName.text = custName + " - " + event
                    }
                }
                cell.contentView.backgroundColor = COLOR.WHITE
                if let status = bookingDict[indexPath.row - 1]["status"] as? NSNumber{
                    if status == 10{
                        cell.contentView.backgroundColor = COLOR.ONGOINGSCHED
                    }else{
                        cell.contentView.backgroundColor = COLOR.SCHEDULE
                    }
                }
            }
            break
        }
        return cell
    }
}



