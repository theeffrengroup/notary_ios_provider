//
//  ScheduleDataModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 01/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
class ScheduleDataMod : NSObject{
    var repeatDays: [String] = ["Everyday","WeekDays","WeekEnd","SelectDays"]
    var everyday: [String] = ["Sunday","Monday","Tuesday","Wednessday","Thursday","Friday","Saturday"]
    var weekDays: [String] = ["Monday","Tuesday","Wednessday","Thursday","Friday"]
    var weekEnd: [String] = ["Sunday","Saturday"]
    var duration:[String] = ["This Month","2 Months","3 Months","4 Months","CUSTOM","Start date",
                             "End date"]
}
