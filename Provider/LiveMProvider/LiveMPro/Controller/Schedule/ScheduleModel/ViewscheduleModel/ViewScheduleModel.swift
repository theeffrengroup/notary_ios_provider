//
//  ViewScheduleModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 02/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import SwiftKeychainWrapper
class ActiveSchedule {
    var schedID = ""
    var addressID = ""
    var days = ""
    var endDate:Int64 = 0
    var startDate:Int64 = 0
    var price = 0.00
}

class PastSchedule {
    var schedID = ""
    var addressID = ""
    var days = ""
    var endDate:Int64 = 0
    var startDate:Int64 = 0
    var price = 0.00
}

class ViewScheduleModel: NSObject {
    var apiCall = APILibrary()
    let disposebag = DisposeBag()

    var activeData = [ActiveSchedule]()
    var pastData = [PastSchedule]()
 
    var scheduleManager = ScheduleDBManager()
    
    
    /// get the schedule data
    ///
    /// - Parameter completionHanlder: return schedule data
    func getTheScheduleData(completionHanlder:@escaping(Bool,ViewScheduleModel) ->())  {
          var viewScheduleModel = ViewScheduleModel()
        
        scheduleManager.getScheduleData(forUserID: "scheduleData") { (scheduleData) in
            if scheduleData.count == 0{
                Helper.showPI(message: loading.gettingData)
            }
            completionHanlder(true,self.getTheScheduleParsedData(dict:scheduleData))
        }
        
        let rxApiCall = ScheduleAPI()
        rxApiCall.getMethodServiceCall(method:API.METHOD.SCHEDULEAPI)
        rxApiCall.schedule_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                
                switch responseCodes{
                case .UserLoggedOut:
                    Session.expired()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                    
                case .SuccessResponse:
                    //self.viewScheduleModel = ViewScheduleModel()
                    if let scheduleDict = responseModel.data["data"] as? [String:Any]{
                        let schedule  : [String : Any] = ["scheduleDict": scheduleDict]
                        self.scheduleManager.saveTheScheduleData(forUserID: "scheduleData", dict: schedule)
                        completionHanlder(true,self.getTheScheduleParsedData(dict:scheduleDict))
                    }
                    break
                    
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    
    /// deletes the schedule
    ///
    /// - Parameters:
    ///   - dictParam: parameters contains schedule id
    ///   - completionHandler: returns true once api got succeeded
    func deleteTheSchedule(dictParam:[String:Any],completionHandler:@escaping(Bool)->()) {
        //delete
        Helper.showPI(message: loading.deleteSched)
        let rxApiCall = ScheduleAPI()
        rxApiCall.deleteScheduleApiCall(method:API.METHOD.SCHEDULEAPI,params:dictParam)
        rxApiCall.schedule_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                
                switch responseCodes{
                case .UserLoggedOut:
                    Session.expired()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                    
                case .SuccessResponse:
                    completionHandler(true)
                    break
                    
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    func getTheScheduleParsedData(dict:[String:Any]) -> ViewScheduleModel {
         activeData = [ActiveSchedule]()
         pastData = [PastSchedule]()
        
        var viewScheduleModel = ViewScheduleModel()
        
    
        for schedDict in  (dict["active"] as? [[String: Any]])! {
            
            let activeModel = ActiveSchedule()
            if let scheduleID = schedDict["_id"] as? String{
                activeModel.schedID = scheduleID
                //scheduleData.schedID = scheduleID
            }
            if let addID = schedDict["addresssId"] as? String{
                activeModel.addressID = addID
            }
            if let dateStart = schedDict["startDate"] as? NSNumber{
                activeModel.startDate = Int64(dateStart)
            }
            if let dataEnd = schedDict["endDate"] as? NSNumber{
                activeModel.endDate = Int64(dataEnd)
            }
            var daysString = ""
            for day in schedDict["days"] as! [String] {
                if daysString.length > 0{
                    daysString = daysString + "," + day
                }else{
                    daysString =  day
                }
            }
            activeModel.days = daysString
            
            viewScheduleModel.activeData.append(activeModel)
        }
        
        for schedDict in  (dict["past"] as? [[String: Any]])!  {
            let scheduleData = ViewScheduleModel()
            let pastModel = PastSchedule()
            if let scheduleID = schedDict["_id"] as? String{
                pastModel.schedID = scheduleID
                //scheduleData.schedID = scheduleID
            }
            if let addID = schedDict["addresssId"] as? String{
                pastModel.addressID = addID
            }
            if let dateStart = schedDict["startDate"] as? NSNumber{
                pastModel.startDate = Int64(dateStart)
            }
            if let dataEnd = schedDict["endDate"] as? NSNumber{
                pastModel.endDate = Int64(dataEnd)
            }
            var daysString = ""
            for day in schedDict["days"] as! [String] {
                if daysString.length > 0{
                    daysString = daysString + "," + day
                }else{
                    daysString =  day
                }
            }
            pastModel.days = daysString
           
            viewScheduleModel.pastData.append(pastModel)
        }
        
        return viewScheduleModel
    }
}
