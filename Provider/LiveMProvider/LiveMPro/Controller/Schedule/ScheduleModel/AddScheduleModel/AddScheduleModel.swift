//
//  AddScheduleModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 31/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import SwiftKeychainWrapper
class AddScheduleParamObject: NSObject {
    let addresssId :String!
    let startTime:String!
    let endTime:String!
    let startDate:String!
    let endDate:String!
    let repeatDay:Int!
    let days:[String]!
    init(idAddress:String,timeStart:String,timeEnd:String,dateStart:String,dateEnd:String,dayRepeat:Int,selectDays:[String]) {
        self.addresssId = idAddress
        self.startTime = timeStart
        self.endTime = timeEnd
        self.startDate = dateStart
        self.endDate = dateEnd
        self.repeatDay = dayRepeat
        self.days = selectDays
        
    }
}

class AddSchedModel: NSObject {
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    var addSchedule: [[String:String]] = [["key":"Repeat Schedule",
                                           "val":"Add schedule"],
                                          ["key":"Repeat on days",
                                           "val":"Add days"],
                                          ["key":"Duration",
                                           "val":"Select duration"],
                                          ["key":"Start time",
                                           "val":"Select start time"],
                                          ["key":"End time",
                                           "val":"Select end time"],
                                          ]
    
    /// creates the scheduledata
    ///
    /// - Parameters:
    ///   - params: scheduled data
    ///   - completionHandler: returns true when api got succeeded
    func postTheScheduleData(params:AddScheduleParamObject,completionHandler: @escaping(Bool)->()){
        Helper.showPI(message: loading.createSched)

        let paramDict:[String:Any] = ["addresssId":params.addresssId,
                                      "startTime":params.startTime,
                                      "endTime":params.endTime,
                                      "startDate":params.startDate,
                                      "endDate":params.endDate,
                                      "repeatDay":params.repeatDay,
                                      "days":params.days,
                                      "deviceTime":Helper.currentDateTime]
        
        
        let rxApiCall = ScheduleAPI()
        rxApiCall.makeApiCallForSchedule(method:API.METHOD.SCHEDULEAPI,params: paramDict)
        rxApiCall.schedule_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .UserLoggedOut:
                    Session.expired()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                case .SuccessResponse:
                    completionHandler(true)
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}

