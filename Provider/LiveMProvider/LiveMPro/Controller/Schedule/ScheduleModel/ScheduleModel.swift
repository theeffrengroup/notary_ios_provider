//
//  ScheduleModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 09/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import SwiftKeychainWrapper
class scheduleData: NSObject {
    var proID = ""
    var scheduleID = ""
    var addressID = ""
    var startTime = ""
    var endTime = ""
}

class ScheduleModel: NSObject {
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    var scheduleID = ""
    var date = ""
    var innerSchedule = [scheduleData]()
    var scheduleResponse = [ScheduleModel]()
    var scheduleDBManager = ScheduleDBManager()
    
    
    
    /// get the schedule data, which is already created
    ///
    /// - Parameters:
    ///   - month: get the schedule data month wise
    ///   - completionHandler: return the scheduledata dictionary
    func getTheScheduledDataMonthWise(month:String,completionHandler: @escaping([String:Any]) ->())  {
        scheduleResponse = [ScheduleModel]()
        var hadScheduleData =  false
        self.scheduleDBManager.getMonthWiseScheduleData(forUserID: "schedulemonthlywise") { (scheduleData) in
            hadScheduleData = true
            completionHandler(self.getTheScheduleParsedData(Dict:scheduleData))
        }
        if !hadScheduleData{
            Helper.showPI(message:loading.load)
        }
        
        let rxApiCall = ScheduleAPI()
        rxApiCall.getMethodServiceCall(method:API.METHOD.SCHEDULEAPI + "/" + month)
        rxApiCall.schedule_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                
                switch responseCodes{
                case .UserLoggedOut:
                    Session.expired()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                case .TokenExpired:
                    let defaults = UserDefaults.standard
                    if let sessionToken =  responseModel.data["data"]   as? String  {
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                            if success{
                                
                            }
                        })
                    }
                    break
                    
                case .SuccessResponse:
                    if let scheduleDict = responseModel.data["data"] as? [[String:Any]]{
                        let schedule  : [String : Any] = ["scheduleDict": scheduleDict]
                        self.scheduleDBManager.saveTheScheduleDataMonthWise(forUserID: "schedulemonthlywise", dict: schedule)
                        completionHandler(self.getTheScheduleParsedData(Dict:scheduleDict))
                    }
                    break
                    
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    func getTheScheduleParsedData(Dict:[[String:Any]]) ->[String:Any]{
        //   let scheduleMain = ScheduleModel()
        var scheudleData = [String:Any]()
        for scheduleDict in Dict {
            if let scheduleID =  scheduleDict["date"] as? Int{
                let date = NSDate(timeIntervalSince1970: TimeInterval(scheduleID))
                let dateFormatter = DateFormatter.initTimeZoneDateFormat()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let scheduleDate = dateFormatter.string(from: date as Date)
                if scheudleData[scheduleDate] == nil{
                    if  let appDetailArr = scheduleDict["schedule"] as?  [[String:Any]]{
                        scheudleData[scheduleDate] = appDetailArr
                    }
                }
            }
        }
        return scheudleData
    }
}
