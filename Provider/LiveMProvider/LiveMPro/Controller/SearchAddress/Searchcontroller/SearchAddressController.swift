//
//  SearchAddressController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 15/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import RxSwift
import RxCocoa




protocol SearchAddressControllerDelegate {
    func didSelectAddress(lat: Float, long: Float, address1 : String, address2: String)
}

struct Address {
    
    var line1 = ""
    var line2 = ""
    var latitude = 0.0
    var longitude = 0.0
    var placeID = ""
    var referenceID = ""
    var type = ""
    var postalCode = ""
}


class SearchAddressController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var searchBar: UISearchBar!

    
    var searchActive : Bool = false
    
    var arrayOfLocation: [Any] = []
    var arrayOfRecent: [Any] = []
    let disposeBag = DisposeBag()
    
    var delegate: SearchAddressControllerDelegate? = nil
    
    //    let address = SearchAddressModel()
    var indexSelected: Int = 0
    
    var dropLatitude: Float = 0.00
    var dropLongitude: Float = 0.00
    var dropAddress = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.estimatedRowHeight = 10
        tableView.rowHeight = UITableView.automaticDimension
        updateUI()
        searchBar.becomeFirstResponder()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        /*
         There was gap between the navigationBar and view
         */
       
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Update UI
    fileprivate func updateUI() {
        
        navigationItem.title = "Pickup Location"
    }
    
    
    //MARK: - Google Auto Complete
    
    /// Call Google Auto Complete API
    ///
    /// - Parameter searchText: Words to be Searched
    func search(searchText: String) {
        
        // Remove Blank space from word
        let text = searchText.replacingOccurrences(of: " ", with: "")
        if text.length == 0 {
            self.arrayOfLocation = []
            self.tableView.reloadData()
        }
        // Get Params
        
        let latitude = NewLocationManager.shared.latitute
        let longitude = NewLocationManager.shared.longitude
        let language = "en"
        let key = "AIzaSyBlvz3SXKry5AS7A_BHGixOqijuGjlBkhM" //Key
        
        let placeAPI = String(format: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&location=%f,%f&radius=500&amplanguage=%@&key=%@" ,text,latitude,longitude,language,key)
        
        // Call Service
        NetworkHelper.requestPOST(urlString: placeAPI,
                                  success: { (response) in
                                    
                                    if response.isEmpty == false {
                                        
                                        if let array = response["predictions"].arrayObject {
                                            self.arrayOfLocation = array
                                        }
                                    }
                                    self.tableView.reloadData()
            },
                                  failure: { (Error) in
                                    // self.showErrorMessage(message: Error.localizedDescription)
        })
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func setLocationOnMap(_ sender: Any) {
        //        self.performSegue(withIdentifier: "selectPinSegue", sender:self)
    }
    
    /// Modify the Details from Google
    ///
    /// - Parameter array: Array of Google Search
    /// - Returns: Array Of Address
    
    func modify(addressDict: [String: AnyObject]) -> Address {
        
        // Take an temp Dict
        var addTitle: String = ""
        var addAddress: String = ""
        var addLatitude: Float = 0.0
        var addLongitude: Float = 0.0
        let addPlaceID: String = addressDict["place_id"] as! String
        let addReferenceID: String = addressDict["reference"] as! String
        
        
        
        let dict: [String: AnyObject] = addressDict["structured_formatting"] as! [String : AnyObject]
        
        // Get Address Line 1 and Matching offset and Length
        if let name = dict["main_text"] as! String? {
            
            addTitle = name
        }
        
        self.getAddressDetails(with: addPlaceID) { (true, lat, long) in
            addLatitude = lat
            addLongitude = long
        }
        
        
        // Get Address Line 2
        if let add = dict["secondary_text"] as! String? {
            addAddress = add
        }
        
        return Address(line1: addTitle,
                       line2: addAddress,
                       latitude: Double(addLatitude),
                       longitude: Double(addLongitude),
                       placeID: addPlaceID,
                       referenceID: addReferenceID,
                       type: "123435",
                       postalCode:"123456")
    }
    
    
    
    /// Invoke Google API to get Address Details
    ///
    /// - Parameters:
    ///   - placeID: Place Id of Selected address
    ///   - completion: Result with Lat and Long
    func getAddressDetails(with placeID: String,
                           completion: @escaping(_ success: Bool,  _ latitude: Float, _ longitude: Float) -> Void) {
        
        let key = "AIzaSyCEseJ5-b9bWvyaWMU-VuUqoXdzqHEOYFw"
        
        let placeAPI = String(format: "https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@",placeID,key)
        
        //   Helper.showPI()
        // Call Service
        NetworkHelper.requestPOST(urlString: placeAPI,
                                  success: { (response) in
                                    
                                    //    Helper.hidePI()
                                    if response.isEmpty == false {
                                        
                                        if let dictionary = response["result"].dictionaryObject {
                                            
                                            let locationDict: [String: AnyObject] = dictionary["geometry"] as! [String : AnyObject]
                                            let obj = locationDict["location"] as! [String:Any]
                                            if let latNum = obj["lat"] as? NSNumber, let longNum =  obj["lng"] as? NSNumber {
                                                
                                                let lat = latNum.floatValue
                                                
                                                let lng = longNum.floatValue
                                                
                                                completion(true, lat, lng)
                                                
                                            }
                                        }
                                        else {
                                            completion(false, 0.0, 0.0)
                                        }
                                    }
                                    else {
                                        completion(false, 0.0, 0.0)
                                    }
            },
                                  failure: { (Error) in
                                    completion(false, 0.0, 0.0)
        })
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchAddressController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(searchActive)
        {
            if arrayOfLocation.count != 0
            {
                return arrayOfLocation.count
            }
            else
            {
                return 0
            }
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: SearchAddressCell = (tableView.dequeueReusableCell(withIdentifier: "SearchAddressCell") as! SearchAddressCell?)!
        if(searchActive)
        {
            if arrayOfLocation.count != 0
            {
                
                let addAddres = modify(addressDict: arrayOfLocation[indexPath.row] as! [String : AnyObject])
                cell.addressLine1.text = addAddres.line1
                cell.addressLine2.text = addAddres.line2
                return cell
            }
        }
        else
        {
            cell.addressLine1.text = ""
            cell.addressLine2.text = ""
            return cell
        }
        
        return cell
    }
}


extension SearchAddressController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var addAddres = modify(addressDict: arrayOfLocation[indexPath.row] as! [String : AnyObject])
        
        getAddressDetails(with: addAddres.placeID) { (success, lat, long) in
            
            if success {
                addAddres.latitude = Double(lat)
                addAddres.longitude = Double(long)
                
                
                self.delegate?.didSelectAddress(lat: Float(addAddres.latitude),
                                           long: Float(addAddres.longitude),
                                           address1: addAddres.line1,
                                           address2: addAddres.line2)
                
                self.dismiss(animated: true, completion: nil)

            }
        }
    }
}

    extension SearchAddressController: UISearchBarDelegate
    {
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            
        }
        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            
            searchBar.text = ""
            searchBar.showsCancelButton = true
            searchActive = true;
        }
        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            
            searchActive = false;
            searchBar.showsCancelButton = false
        }
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            
            searchBar.text = ""
            searchActive = false;
            searchBar.endEditing(true)
        }
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
            search(searchText: searchBar.text!)
            searchBar.showsCancelButton = true
        }
}


