//
//  ForgotPasswordModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 11/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import RxCocoa
import RxSwift

class ForgotPasswordModel:NSObject {
    var apiLibrary = APILibrary()
    let disposebag = DisposeBag()
    
    
    
    /// if artist forgot password can retrive from email or password
    ///
    /// - Parameters:
    ///   - params: params contains email id or phone number
    ///   - completionHandler: return the true and id for phone number, return true and "" for email id
    func forgotPasswordAPI(params:[String:Any],completionHandler:@escaping (Bool,String) -> ()) {
        
        let rxApiCall = ForgotPasswordAPI()
        rxApiCall.forgotPasswordWithEmailAndPhoneAPi(paramDict: params,method:API.METHOD.FORGOTPASSWORD)
        rxApiCall.ForgotPassword_Response
            .subscribe(onNext: {response in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                
                Helper.hidePI()
                
                switch responseCodes{
                case .SuccessResponse:
                    if let dict = response.data["data"] as? [String:Any]{
                        if let providerID = dict["sid"] as? String{
                            completionHandler(true,providerID)
                            //Helper.alertVC(errMSG: response.data["message"] as! String)
                        }
                    }else{
                        completionHandler(true,"")
                        //Helper.alertVC(errMSG: response.data["message"] as! String)
                    }
                    break
                    
                default:
                    completionHandler(false,"")
                    Helper.alertVC(errMSG: response.data["message"] as! String)
                    break
                }
                
            }, onError: {error in
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}

