//
//  ForgotPasswordWithEmailVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 09/01/18.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit


class ForgotPasswordWithEmailVC: UIViewController {
    
    @IBOutlet var countryCode: UILabel!
    @IBOutlet var countryImage: UIImageView!
    
    
    @IBOutlet weak var view1: UIView!
    
    
    @IBOutlet weak var phoneButton: UIButton!
    
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var phoneTF: FloatLabelTextField!
    @IBOutlet var widthConstrain: NSLayoutConstraint!
    var typeOfRecovery:Int = 0
    var forgotPasswordModel = ForgotPasswordModel()
    var artistID = ""
    
    @IBOutlet weak var emailUnderView: UIView!
    @IBOutlet weak var phoneUnderVie: UIView!
    @IBOutlet weak var countryCodeView: UIView!
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCountryCode()
        self.navigationController?.isNavigationBarHidden = false
        
        phoneButton.isSelected = true
        phoneTF.keyboardType = .phonePad
        phoneTF.becomeFirstResponder()
        typeOfRecovery = 1
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    ///********dismissing the vc*******//
    @IBAction func backToController(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = true
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard))
        view.addGestureRecognizer(tap)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        /*
         There was gap between the navigationBar and view
         */
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
             navigationController?.navigationBar.setNeedsLayout()
        }
        setupGestureRecognizer()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            mainScrollView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardSize = CGSize(width: 320, height: 240)
        let height: CGFloat = UIDevice.current.orientation.isPortrait ? keyboardSize.height : keyboardSize.width //UIDeviceOrientationIsPortrait(UIDevice.current.orientation) ? keyboardSize.height : keyboardSize.width
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            var edgeInsets: UIEdgeInsets = self.mainScrollView.contentInset
            edgeInsets.bottom = height
            self.mainScrollView.contentInset = edgeInsets
            edgeInsets = self.mainScrollView.scrollIndicatorInsets
            edgeInsets.bottom = height
            self.mainScrollView.scrollIndicatorInsets = edgeInsets
        })
    }
    
    @IBAction func generateOTP(_ sender: Any) {
        if (phoneTF.text?.isEmpty)! {
            if typeOfRecovery == 1{
                self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Please enter Mobile Number"), animated: true, completion: nil)
            }else{
                self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Please enter Email ID"), animated: true, completion: nil)
            }
        }else{
            self.forgotPasswordRecovery()
        }
    }
    
    //*******hide keyboard***********//
    @IBAction func hideKeyBoard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    // MARK: - setCountrycode
    func setCountryCode(){
        let country = NSLocale.current.regionCode
        let picker = CountryPicker.dialCode(code: country!)
        countryCode.text = picker.dialCode
        countryImage.image = picker.flag
    }
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "pickCountryCodeFromFP"
        {
            let nav = segue.destination as! UINavigationController
            if let picker: CountryPicker = nav.viewControllers.first as! CountryPicker?
            {
                picker.delegate = self
            }
        }else if segue.identifier == "toVerifyOTP" {
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.mobileNumber = phoneTF.text!
            nextScene?.defineTheOtp = 2
            nextScene?.providerID = artistID
            nextScene?.countryCode = countryCode.text!
        }
    }
    
    
    func forgotPasswordRecovery(){
        
        Helper.showPI(message:loading.load)
        var params  = [String : Any]()
        if typeOfRecovery == 1{
            params  =  [ FORGOTPASSWORD_REQUEST.EMAIL_PHONE:phoneTF.text!,
                         FORGOTPASSWORD_REQUEST.USER_TYPE:2,
                         FORGOTPASSWORD_REQUEST.TYPE:typeOfRecovery,
                         FORGOTPASSWORD_REQUEST.COUNTRY_CODE:countryCode.text!]
        }else{
            params  =  [FORGOTPASSWORD_REQUEST.EMAIL_PHONE:phoneTF.text!,
                        FORGOTPASSWORD_REQUEST.USER_TYPE:2,
                        FORGOTPASSWORD_REQUEST.TYPE:typeOfRecovery,
            ]
        }
        
        forgotPasswordModel.forgotPasswordAPI(params: params) { (succeeded,providerID) in
            if succeeded{
                if  self.typeOfRecovery == 1{
                    
                    self.artistID = providerID
                    self.performSegue(withIdentifier: "toVerifyOTP", sender: nil)
                    
                }else{
                    self.navigationController?.popViewController(animated: true)
                    Helper.alertVC(errMSG: "The reset password link has been sent to the mentioned email ID")
//                    self.view.endEditing(true)
//                    let alertView = AlertViewEmailSent.instance
//                    alertView.delegate = self
//                    alertView.show()
                }
            }
        }
    }
    
    
    @IBAction func selectTheCountryCode(_ sender: UIButton) {
        
        performSegue(withIdentifier:"pickCountryCodeFromFP", sender: nil)
    }
    
    @IBAction func phoneAction(_ sender: Any) {
        self.view.endEditing(true)
        if phoneTF.keyboardType != .phonePad
        {
            phoneTF.text = ""
        }
        nextButton.setTitle("NEXT", for: .normal)
        phoneTF.placeholder = "Your phone number"
        noteLabel.text = "Enter your mobile number and you will get a verification code to reset password"
        emailButton.isSelected = false
        phoneButton.isSelected = true
        self.updateTheTextField(width: 70)
        typeOfRecovery = 1
        countryCodeView.isHidden = false
        emailUnderView.backgroundColor = COLOR.E1E1E1
        phoneUnderVie.backgroundColor = COLOR.APP_COLOR
        phoneTF.keyboardType = .phonePad
        phoneTF.becomeFirstResponder()
        
        
        
    }
    
    
    @IBAction func emailAction(_ sender: Any) {
        self.view.endEditing(true)
        if phoneTF.keyboardType == .phonePad
        {
            phoneTF.text = ""
        }
        widthConstrain.constant = 0
        nextButton.setTitle("OK", for: .normal)
        phoneTF.placeholder = "Your Email"
        noteLabel.text = "Enter your email address and you will get a link to reset password"
        emailButton.isSelected = true
        phoneButton.isSelected = false
        self.updateTheTextField(width: 0)
        typeOfRecovery = 2
        countryCodeView.isHidden = true
        emailUnderView.backgroundColor = COLOR.APP_COLOR
        phoneUnderVie.backgroundColor = COLOR.E1E1E1
        phoneTF.keyboardType = .emailAddress
        phoneTF.becomeFirstResponder()
      
    }
    
    func updateTheTextField(width:Int){
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {() -> Void in
                        
                        self.widthConstrain.constant = CGFloat(width)
                        self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            print("Completed")
        })
    }
    
    func buttonEnable(selected: CustomRadioButton, unselected: CustomRadioButton){
        
        phoneButton.contentHorizontalAlignment = .left
        emailButton.contentHorizontalAlignment = .left
        selected.isSelected = true
        unselected.isSelected = false
        selected.outerCircleColor = COLOR.APP_COLOR
        selected.innerCircleCircleColor = COLOR.APP_COLOR
        unselected.outerCircleColor = Helper.colorFromHexString(hexCode: "cccccc")
        
        if selected == phoneButton {
            phoneTF.text = ""
            hideKeyBoard(phoneButton)
            phoneTF.keyboardType = .decimalPad
            nextButton.setTitle("NEXT", for: .normal)
            phoneTF.placeholder = "Your phone number ?"
            noteLabel.text = "Enter your mobile number and you will get a verification code to reset password"
        }
        else
        {
            phoneTF.text = ""
            hideKeyBoard(emailButton)
            phoneTF.keyboardType = .emailAddress
            nextButton.setTitle("OK", for: .normal)
            phoneTF.placeholder = "Your Email ?"
            noteLabel.text = "Enter your email address and you will get a link to reset password"
        }
    }
    
}

extension ForgotPasswordWithEmailVC:CountryPickerDelegate{
    // MARK: - country delegate method
    internal func didPickedCountry(country: Country)
    {
        countryCode.text = country.dialCode
        countryImage.image = country.flag
    }
}

extension ForgotPasswordWithEmailVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (phoneTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: alertMsgCommom.Message, message:"Please enter Mobile or Email"), animated: true, completion: nil)
        }else{
            self.forgotPasswordRecovery()
        }
        self.view.endEditing(true)
        return true
    }
}

extension ForgotPasswordWithEmailVC : AlertViewCancelDelegate{
    func backToSignin(){
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
}


extension ForgotPasswordWithEmailVC: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}


