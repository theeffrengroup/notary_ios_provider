//
//  MyeventModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 22/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


class bookingTime {
    var  status = 0
    var second = 0
    var startTimeStamp = 0
}


class gigTime {
    var name = ""
    var unit = ""
    var price = 0
    var second = 0
}

class BookingServices {
    
    var serviceName = ""
    var servicePrice = 0.0
    var quantity = 0
}


class Request {
    var addLine1 = ""
    var addLine2 = ""
    var averageRating = 0.0
    var bookingExpireTime:Int64  = 0
    var bookingRequestedFor:Int64 = 0
    var actuallGigTimeStart:Int64 = 0
    var bookingRequestEnds:Int64 = 0
    var bookingRequestedat:Int64  = 0
    var bookingId:Int64  = 0
    var city = ""
    var country = ""
    var firstName = ""
    var gigTime = ""
    var lastName = ""
    var latitude = 0.0
    var longitude = 0.0
    var paymentMethod = ""
    var pincode = ""
    var placeId = ""
    var price = 0.0
    var profilePic = ""
    var typeofEvent = ""
    var statusCode = 0
    var bookingType = 0
    var mobileNum = ""
    var distance = 0.0
    var customerId = ""
    var GEGTime:gigTime?
    var currencyType = ""
    var currencySymbol = ""
    var discount = 0.0
    var totalAmt = 0.0
    var distanceMatrix = 0
    var serviceType = 0
    var categoryName = ""
    var statusMsg = ""
    var jobDesc = ""
    var totalHourlyFee = 0.0
    var totalJobTime = 0.0
    var paidByWallet = false
    var services = [BookingServices]()
}

class Accepted {
    var addLine1 = ""
    var addLine2 = ""
    var averageRating = 0.0
    var bookingExpireTime:Int64  = 0
    var actuallGigTimeStart:Int64 = 0
    var bookingRequestedFor:Int64 = 0
    var bookingRequestEnds:Int64 = 0
    var bookingRequestedat:Int64  = 0
    var bookingId:Int64  = 0
    var city = ""
    var country = ""
    var firstName = ""
    var gigTime = ""
    var lastName = ""
    var latitude = 0.0
    var longitude = 0.0
    var paymentMethod = ""
    var pincode = ""
    var placeId = ""
    var price = 0.0
    var bookingType = 0
    var profilePic = ""
    var typeofEvent = ""
    var statusCode = 0
    var mobileNum = ""
    var distance = 0.0
    var customerId = ""
    var bookingTime:bookingTime?
    var GEGTime:gigTime?
    var currencyType = ""
    var currencySymbol = ""
    var discount = 0.0
    var totalAmt = 0.0
    var distanceMatrix = 0
    var serviceType = 0
    var categoryName = ""
    var statusMsg = ""
    var services = [BookingServices]()
    var jobDesc = ""
    var totalHourlyFee = 0.0
    var totalJobTime = 0.0
    var paidByWallet = false
    var travelFees = 0.0
}

class MyeventModel: NSObject {
    var acceptedData = [Accepted]()
    var  requestData    = [Request]()
    
    func parsingTheServiceResponse(responseData:[String: Any]) -> MyeventModel{
        acceptedData = [Accepted]()
        requestData    = [Request]()
        Helper.hidePI()
        let bookingsData = MyeventModel()
        
        for acceptData in (responseData["accepted"] as? [[String: Any]])!  {
            let model = Accepted()
            if let bookingType = acceptData["bookingType"] as? NSNumber {
                
                model.bookingType = Int(bookingType)
            }
            if  let category      =  acceptData["category"] as? String {
                model.categoryName = category
            }
            
            if  let statusMsg      =  acceptData["statusMsg"] as? String {
                model.statusMsg = statusMsg
            }
            
            if  let address1      =  acceptData["addLine1"] as? String {
                model.addLine1 = address1
            }
            
            if  let currType  = acceptData["currency"] as? String {
                model.currencyType = currType
            }
            
            
            if  let currSymbol  = acceptData["currencySymbol"] as? String {
                model.currencySymbol = currSymbol
            }
            
            if  let distanceAway  = acceptData["distance"] as? NSNumber {
                model.distance = Double(Float(distanceAway))
            }
            
            if  let distanceMatrix  = acceptData["distanceMatrix"] as? NSNumber {
                model.distanceMatrix = Int(distanceMatrix)
            }
            
            if  let address2       = acceptData["addLine2"] as? String {
                model.addLine2 = address2
            }
            
            if  let averageRat  = acceptData["averageRating"] as? NSNumber {
                model.averageRating = Double(Float(averageRat))
            }
            
            if   let expiryTime     = acceptData["bookingExpireTime"] as? NSNumber {
                model.bookingExpireTime = Int64(expiryTime)
            }
            
            if   let eventStartTime     = acceptData["eventStartTime"] as? NSNumber {
                model.actuallGigTimeStart = Int64(eventStartTime)
            }
            
            
            if let bookId     = acceptData["bookingId"] as? NSNumber {
                model.bookingId = Int64(bookId)
            }
            
            if  let city = acceptData["city"] as? String {
                model.city = city
            }
            if   let country     = acceptData["country"] as? String {
                model.country = country
            }
            
            if let firstName     = acceptData["firstName"] as? String {
                model.firstName = firstName
            }
            
            if  let lastname = acceptData["lastName"] as? String {
                model.lastName = lastname
            }
            
            if  let serviceType = acceptData["serviceType"] as? NSNumber {
                model.serviceType = Int(serviceType)
            }
            
            if   let lat     = acceptData["latitude"] as? NSNumber {
                model.latitude = Double(lat)
            }
            
            if let longt     = acceptData["longitude"] as? NSNumber {
                model.longitude = Double(longt)
            }
            
            if  let paymentType = acceptData["paymentMethod"] as? String {
                model.paymentMethod = paymentType
            }
            
            if let pincode     = acceptData["pincode"] as? String {
                model.pincode = pincode
            }
            
            if  let profileImg = acceptData["profilePic"] as? String {
                model.profilePic = profileImg
            }
            
            if  let eventType = acceptData["typeofEvent"] as? String {
                model.typeofEvent = eventType
            }
            
            if let status = acceptData["status"] as? NSNumber{
                model.statusCode = Int(status)
            }
            
            if  let price     = acceptData["price"] as? NSNumber {
                model.price =  Double(price)
            }
            
            if  let bookingRequestedFor  = acceptData["bookingRequestedFor"] as? NSNumber {
                model.bookingRequestedFor = Int64(bookingRequestedFor)
            }
            
            if  let bookingRequestedAt  = acceptData["bookingRequestedAt"] as? NSNumber {
                model.bookingRequestedat = Int64(bookingRequestedAt)
            }
            
            if let endTime = acceptData["bookingEndtime"] as? NSNumber {
                model.bookingRequestEnds = Int64(endTime)
            }
            
            if  let custID  = acceptData["customerId"] as? String  {
                model.customerId = custID
            }
            
            if let mobile  = acceptData["phone"] as? String{
                model.mobileNum = mobile
            }
            
            if let desc = acceptData["jobDescription"] as? String {
                model.jobDesc = desc
            }
            
            if let dict = acceptData["gigTime"] as? [String:Any]{
                let gigDuration = gigTime()
                if let name = dict["name"] as? String{
                    gigDuration.name = name
                    if let unit = dict["unit"] as? String{
                        model.gigTime = name + " " + unit
                    }
                }
                if let unit = dict["unit"] as? String{
                    gigDuration.unit = unit
                }
                if let price = dict["price"] as? NSNumber{
                    gigDuration.price = Int(price)
                    model.price = Double(price)
                }
                if let second = dict["second"] as? NSNumber{
                    gigDuration.second = Int(second)
                }
                model.GEGTime = gigDuration
            }
            
            if let timer = acceptData["bookingTimer"] as? [String:Any]{
                let bookingTimer = bookingTime()
                if let status = timer["status"] as? NSNumber{
                    bookingTimer.status = Int(status)
                }
                
                if let second = timer["second"] as? NSNumber{
                    bookingTimer.second = Int(second)
                }
                
                if let startTimeStamp = timer["startTimeStamp"] as? NSNumber{
                    bookingTimer.startTimeStamp = Int(startTimeStamp)
                }
                model.bookingTime = bookingTimer
            }
            
          
            
            for service in (acceptData["item"] as? [[String:Any]])!{
                
                let serviceMod = BookingServices()
                if let quant = service["quntity"] as? NSNumber{
                    serviceMod.quantity =  Int(quant)
                }
                
                if let serviceAmt = service["amount"] as? NSNumber{
                    serviceMod.servicePrice = Double(serviceAmt)
                }
                
                if let serviceName = service["serviceName"] as? String{
                    serviceMod.serviceName = serviceName
                }
                
                model.services.append(serviceMod)
            }
            
            if let account = acceptData["accounting"] as? [String:Any]{
                
                if let totalHourlyFee = account["totalActualHourFee"] as? NSNumber{
                    model.totalHourlyFee =  Double(totalHourlyFee)
                }
                
                if let totalTime = account["totalActualJobTimeMinutes"] as? NSNumber{
                    model.totalJobTime =  Double(totalTime)
                }
                
                if let totalFee = account["total"] as? NSNumber{
                    model.totalAmt =  Double(totalFee)
                }
                
                
                if let discount = account["discount"] as? Double{
                    if discount != 0 {
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = discount
                        serviceMod.serviceName = "Discount"
                        model.services.append(serviceMod)
                    }
                    
                }
                
                if let visitFees = account["visitFee"] as? Double{
                    if visitFees > 0{
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = visitFees
                        serviceMod.serviceName = "Visit Fee"
                        model.services.append(serviceMod)
                    }
                }
                
//                if let travelFree = account["travelFee"] as? Double {
//                    if travelFree > 0 {
//                        let serviceMod = BookingServices()
//                        serviceMod.servicePrice = travelFree
//                        serviceMod.serviceName = "Travel Fee"
//                        model.services.append(serviceMod)
//                    }
//                }
                
                if let travelFee = account["travelFee"] as? Double {
                   model.travelFees = travelFee
                }
                
                if let dues = account["lastDues"] as? Double {
                    if dues != 0 {
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = dues
                        serviceMod.serviceName = "Last Due"
                        model.services.append(serviceMod)
                    }
                }
                
                if let paidByWal = account["paidByWallet"] as? Int {
                    if paidByWal == 1 {
                        model.paidByWallet = true
                    }
                }
            }
          
            
            bookingsData.acceptedData.append(model)
        }
        
        for bookingData in (responseData["request"] as? [[String: Any]])!  {
            let model = Request()
            if let bookingType = bookingData["bookingType"] as? NSNumber {
                
                model.bookingType = Int(bookingType)
            }
            
            if  let statusMsg      =  bookingData["statusMsg"] as? String {
                model.statusMsg = statusMsg
            }
            
            if  let address1      =  bookingData["addLine1"] as? String {
                model.addLine1   = address1
            }
            
            if  let category      =  bookingData["category"] as? String {
                model.categoryName = category
            }
            
            if  let currType  = bookingData["currency"] as? String {
                model.currencyType = currType
            }
            
            if  let currSymbol  = bookingData["currencySymbol"] as? String {
                model.currencySymbol = currSymbol
            }
            
            if  let address2       = bookingData["addLine2"] as? String {
                model.addLine2 = address2
            }
            
            if  let distanceAway       = bookingData["distance"] as? NSNumber {
                model.distance       =  Double(Float(distanceAway))
            }
            
            if  let distanceMatrix  = bookingData["distanceMatrix"] as? NSNumber {
                model.distanceMatrix = Int(distanceMatrix)
            }
            
            if  let bookingRequestedFor  = bookingData["bookingRequestedFor"] as? NSNumber {
                model.bookingRequestedFor = Int64(bookingRequestedFor)
            }
            
            if  let bookingRequestedAt  = bookingData["bookingRequestedForProvider"] as? NSNumber {
                model.bookingRequestedat = Int64(bookingRequestedAt) //bookingRequestedAt
            }
            
            if let endTime = bookingData["bookingEndtime"] as? NSNumber {
                model.bookingRequestEnds = Int64(endTime)
            }
            
            if   let eventStartTime     = bookingData["eventStartTime"] as? NSNumber {
                model.actuallGigTimeStart = Int64(eventStartTime)
            }
            
            if  let custID  = bookingData["customerId"] as? String  {
                model.customerId = custID
            }
            
            
            if  let averageRat  = bookingData["averageRating"] as? NSNumber {
                model.averageRating = Double(Float(averageRat))
            }
            
            if   let expiryTime     = bookingData["bookingExpireForProvider"] as? NSNumber {
                model.bookingExpireTime = Int64(expiryTime) //bookingExpireTime
            }
            
            if let bookId     = bookingData["bookingId"] as? NSNumber {
                model.bookingId = Int64(bookId)
            }
            
            if  let city = bookingData["city"] as? String {
                model.city = city
            }
            if   let country     = bookingData["country"] as? String {
                model.country = country
            }
            
            if let firstName     = bookingData["firstName"] as? String {
                model.firstName = firstName
            }
            
            if  let lastname = bookingData["lastName"] as? String {
                model.lastName = lastname
            }
            
            if  let serviceType = bookingData["serviceType"] as? NSNumber {
                model.serviceType = Int(serviceType)
            }
            
            if   let lat     = bookingData["latitude"] as? NSNumber {
                model.latitude = Double(lat)
            }
            
            if let longt     = bookingData["longitude"] as? NSNumber {
                model.longitude = Double(longt)
            }
            
            if  let paymentType = bookingData["paymentMethod"] as? String {
                model.paymentMethod = paymentType
            }
            if let pincode     = bookingData["pincode"] as? String {
                model.pincode = pincode
            }
            
            if  let profileImg = bookingData["profilePic"] as? String {
                model.profilePic = profileImg
            }
            
            if  let eventType = bookingData["typeofEvent"] as? String {
                model.typeofEvent = eventType
            }
            
            if let status = bookingData["status"] as? NSNumber{
                model.statusCode = Int(status)
            }
            
            if let mobile  = bookingData["phone"] as? String{
                model.mobileNum = mobile
            }
            
            if let desc = bookingData["jobDescription"] as? String {
                model.jobDesc = desc
            }
           
            if let dict = bookingData["gigTime"] as? [String:Any]{
                let gigDuration = gigTime()
                if let name = dict["name"] as? String{
                    gigDuration.name = name
                    if let unit = dict["unit"] as? String{
                        model.gigTime = name + " " + unit
                    }
                }
                if let unit = dict["unit"] as? String{
                    gigDuration.unit = unit
                }
                if let price = dict["price"] as? NSNumber{
                    gigDuration.price = Int(price)
                    model.price = Double(price)
                }
                if let second = dict["second"] as? NSNumber{
                    gigDuration.second = Int(second)
                }
                model.GEGTime = gigDuration
            }
            
            for service in (bookingData["item"] as? [[String:Any]])!{
                
                let serviceMod = BookingServices()
                if let quant = service["quntity"] as? NSNumber{
                    serviceMod.quantity =  Int(quant)
                }
                
                if let serviceAmt = service["amount"] as? NSNumber{
                    serviceMod.servicePrice = Double(serviceAmt)
                }
                
                if let serviceName = service["serviceName"] as? String{
                    serviceMod.serviceName = serviceName
                }
                model.services.append(serviceMod)
            }
            
            if let account = bookingData["accounting"] as? [String:Any]{
                
                if let totalFee = account["total"] as? NSNumber{
                    model.totalAmt =  Double(totalFee)
                }
                
                
                if let totalHourlyFee = account["totalActualHourFee"] as? NSNumber{
                    model.totalHourlyFee =  Double(totalHourlyFee)
                }
                
                if let totalTime = account["totalActualJobTimeMinutes"] as? NSNumber{
                    model.totalJobTime =  Double(totalTime)
                }

                if let discount = account["discount"] as? Double{
                     if discount > 0{
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = discount
                        serviceMod.serviceName = "Discount"
                        model.services.append(serviceMod)
                    }
                }
                if let visitFees = account["visitFee"] as? Double{
                    if visitFees > 0{
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = visitFees
                        serviceMod.serviceName = "Visit Fee"
                        model.services.append(serviceMod)
                    }
                }
                
                if let travelFree = account["travelFee"] as? Double {
                    if travelFree > 0 {
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = travelFree
                        serviceMod.serviceName = "Travel Fee"
                        model.services.append(serviceMod)
                    }
                }
                
                if let dues = account["lastDues"] as? Double {
                    if dues > 0 {
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = dues
                        serviceMod.serviceName = "Last Due"
                        model.services.append(serviceMod)
                    }
                }
                if let paidByWal = account["paidByWallet"] as? Int {
                    if paidByWal == 1 {
                        model.paidByWallet = true
                    }
                }
                
            }
            
           
            bookingsData.requestData.append(model)
        }
        
        self.handlingThePublishData(bookings:bookingsData)
        return bookingsData
    }
    
    
    ///******on going bookings bid's and status *************//
    func handlingThePublishData(bookings:MyeventModel){
        var bookingSumStr = String()
        
        if bookings.acceptedData.count == 0  && bookings.requestData.count == 0 {
            let ud = UserDefaults.standard
            ud.removeObject(forKey: USER_INFO.SELBID)
            UserDefaults(suiteName: API.groupIdentifier)!.removeObject(forKey: "bookingStr")
            UserDefaults.standard.synchronize()
            ud.synchronize()
            return
        }else{
            
            for accData  in bookings.acceptedData {
                if bookingSumStr.isEmpty {
                    bookingSumStr = String(describing:accData.bookingId) + "|" + String(describing:accData.statusCode)
                }
                else{
                    bookingSumStr = bookingSumStr + "," + String(describing:accData.bookingId) + "|"  + String(describing:accData.statusCode)
                }
            }
            
            for reqData  in bookings.requestData {
                if bookingSumStr.isEmpty {
                    bookingSumStr = String(describing:reqData.bookingId) + "|" + String(describing:reqData.statusCode)
                }
                else{
                    bookingSumStr = bookingSumStr + "," + String(describing:reqData.bookingId) + "|"  + String(describing:reqData.statusCode)
                }
            }
            
            let ud = UserDefaults.standard
            ud.set(bookingSumStr, forKey: USER_INFO.SELBID)
            UserDefaults(suiteName: API.groupIdentifier)!.set(bookingSumStr, forKey: "bookingStr")
            UserDefaults.standard.synchronize()
            ud.synchronize()
        }
    }
}
