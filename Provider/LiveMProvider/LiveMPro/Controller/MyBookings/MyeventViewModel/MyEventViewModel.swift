//
//  MainModel.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 18/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SwiftKeychainWrapper
protocol EventsModelDelegate{
    
    func checkTheDriverStatus(data: Int , statusRev:Int)
    
    func updateTheDriverStatus(status: String, success:Bool)
}



class Events:NSObject {
    
    var Delegate: EventsModelDelegate! = nil
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    var timer       = Timer()
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    /// changes the driver availability, offline r online
    ///
    /// - Parameter status: only the status string 3->online// 4->offilne
    func updateMasterStatus(status:String) {
        
        //Status: 3 driver online
        // status: 4 driver offline
                Helper.showPI(message:loading.load)
        let ud = UserDefaults.standard
        let paramDict : [String : Any] =  [
            "status": status,
            "latitude":ud.object(forKey: "currentLat") as Any,
            "longitude": ud.object(forKey: "currentLog") as Any]
        
        let rxApiCall = MyEventApi()
        rxApiCall.updateDriverStatus(params:paramDict ,method: API.METHOD.UPDATEDRIVERSTATUS)
        rxApiCall.booking_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    if (self.Delegate != nil) {
                        self.Delegate.updateTheDriverStatus(status: status, success:true)
                    }
                    if status == "3"// online status
                    {
                        UserDefaults.standard.set("3", forKey: USER_INFO.BOOKSTATUS)
                        self.handleTimer(interval: Double(Utility.apiInterval))
                        
                    }else{
                        self.handleTimer(interval: Double(Utility.apiInterval))
                        UserDefaults.standard.set("4", forKey: USER_INFO.BOOKSTATUS)
                        self.stopLocationUpdates()
                    }
                    break
                    
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    if (self.Delegate != nil) {
                        self.Delegate.updateTheDriverStatus(status: status, success: false)
                    }
                    if status == "4"
                    {
                        self.handleTimer(interval: Double(Utility.apiInterval))
                        
                    }else{
                        self.handleTimer(interval: Double(Utility.apiInterval))
                        self.stopLocationUpdates()
                    }
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    // stops the location update when the artist went offline and if there is no bookings
    func stopLocationUpdates()  {
        let ud = UserDefaults.standard
        if !ud.bool(forKey: "hasBookings"){
            self.locationtracker.stopLocationTracking()
            self.timer.invalidate()
        }
    }
    
    
    /// checks if there is not session timer will invalidate else updates location to server
    @objc func updateLcocationToServerChannel()  {
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            self.timer.invalidate()
        }else{
            self.locationtracker.updateLocationToServer()
        }
    }

    
    /// starts the location update timer when the artist in online
    ///
    /// - Parameter interval: dynamic from app configuration
    func handleTimer(interval:Double) {
        self.timer.invalidate()
        self.locationtracker.stopLocationTracking()
        self.locationtracker.startLocationTracking()
        self.timer = Timer.scheduledTimer(timeInterval:interval,
                                          target: self,
                                          selector: #selector(updateLcocationToServerChannel),
                                          userInfo: nil,
                                          repeats: true)
        
    }
    
    func createConnection(){
        let mqttModel = MQTT.sharedInstance
        if !mqttModel.isConnected {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                mqttModel.createConnection()
            }
        }
    }
    
    
    /// new bookings and on going booking response
    ///
    /// - Parameter completionHandler: return myeventmodel on completion
    func assignedBookingAPI(completionHandler:@escaping (MyeventModel) -> ()) {
        self.createConnection()
        
        let eventModel = MyeventModel()
        
        
        let rxApiCall = MyEventApi()
        rxApiCall.getBookingsDataAPi(method: API.METHOD.ASSIGNEDTRIPS)
        rxApiCall.booking_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    
                    let ud = UserDefaults.standard
                    if  let data =  responseModel.data["data"] as? [String:Any]{
                        if let accepted = data["accepted"] as? [[String:Any]]{
                            if let requested = data["request"] as? [[String:Any]]{
                                if (!requested.isEmpty || !accepted.isEmpty) &&  (requested.count != 0 || accepted.count != 0 )  {
                                    ud.set(true, forKey: "hasBookings") // update the hasbookings true,
                                }else{
                                    ud.set(false, forKey: "hasBookings") // update the hasbookings false,
                                }
                                ud.synchronize()
                            }
                        }
                        
                        
                        if let activateStatus:Int = data["profileStatus"] as? Int{
                            if let proStatus:Int = data["profileActivationStatus"] as? Int{
                                if activateStatus == 1{
                                    if let status:Int = data["status"] as? Int{
                                        if status == 3{
                                            UserDefaults.standard.set("3", forKey: USER_INFO.BOOKSTATUS)
                                            self.handleTimer(interval: Double(Utility.apiInterval))
                                            
                                        }else{
                                            self.handleTimer(interval: Double(Utility.apiInterval))
                                            UserDefaults.standard.set("4", forKey: USER_INFO.BOOKSTATUS)
                                            self.stopLocationUpdates()
                                            
                                        }
                                        if (self.Delegate != nil) {
                                            self.Delegate.checkTheDriverStatus(data: status, statusRev:proStatus)
                                        }
                                    }
                                }else{
                                    if (self.Delegate != nil) {
                                        self.Delegate.checkTheDriverStatus(data: activateStatus, statusRev:proStatus)
                                    }
                                }
                            }
                        }
                        completionHandler(eventModel.parsingTheServiceResponse(responseData:data))
                    }
                    break
                    
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
}
