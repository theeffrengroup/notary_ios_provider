//
//  AcceptedTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 30/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class AcceptedTableCell: UITableViewCell {
    @IBOutlet weak var priceAmount: UILabel!
    @IBOutlet weak var toBidLabel: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var progressNStatus: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bookingType: UILabel!
    @IBOutlet weak var bookingTime: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var distanceAway: UILabel!
    override func awakeFromNib() {
        Helper.shadowView(sender: self.topView, width: UIScreen.main.bounds.size.width - 20, height: 118)

        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateTheAcceptData(data: Accepted) {
        self.customerName.text = data.firstName
        
        if data.distanceMatrix == 1{
            self.distanceAway.text =  String(format: "%.02f", data.distance/1609.34) +  DistanceUnits.Miles
        }else{
            self.distanceAway.text = String(format: "%.02f", data.distance/1000) + DistanceUnits.Kms
        }
        
        toBidLabel.isHidden = true
        
        if data.bookingType == 1{
            self.bookingType.text = "ASAP"
        }else{
            self.bookingType.text = "SCHEDULE"
            
        }
        
        self.progressNStatus.text = data.statusMsg
        self.categoryName.text = data.categoryName
        self.priceAmount.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
        
        self.bookingTime.text = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestedFor)
    }
}
