//
//  RequestsCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 07/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class RequestsCell: UITableViewCell {
    @IBOutlet weak var progressTime: UILabel!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var toBidLabel: UILabel!
    @IBOutlet weak var bookingType: UILabel!
    @IBOutlet weak var bookingTime: UILabel!
    @IBOutlet weak var custName: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    
    @IBOutlet weak var priceAmt: UILabel!
    @IBOutlet weak var distanceAway: UILabel!
    var width: Int = 0
    var expiryTime:Float = 0.0
    var fullTime:Float = 0.0
    var remainingTime:Float = 0.0
    
    var acceptTimer  = Timer()
    
    override func awakeFromNib() {
    Helper.shadowView(sender: self.topView, width: UIScreen.main.bounds.size.width - 20, height: 118)
        
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateTheRequestData(data: Request) {
        self.custName.text = data.firstName
        
        if data.distanceMatrix == 1{
            self.distanceAway.text =  String(format: "%.02f", data.distance/1609.34) + DistanceUnits.Miles
        }else{
            //self.distanceAway.text = String(format: "%.02f", data.distance) + DistanceUnits.Kms
            self.distanceAway.text = String(format: "%.02f", data.distance/1000) + DistanceUnits.Kms
        }
        toBidLabel.isHidden = true
        self.categoryName.text = data.categoryName
        self.priceAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format: "%.2f", data.totalAmt))
        
        if data.bookingType == 1{
            self.bookingType.text = "ASAP"
        }else{
            self.bookingType.text = "SCHEDULE"
        }
        
        self.bookingTime.text = Helper.getTheDateFromTimeStamp(timeStamp:data.bookingRequestedFor)
        
        
        fullTime = Float(data.bookingExpireTime - data.bookingRequestedat)
        expiryTime = Float(self.getCreationTimeInt(expiryTimeStamp: data.bookingExpireTime))
        
        if !acceptTimer.isValid {
            acceptTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
        }
    }
    
    
    /**
     *  Calculate time for Remaining
     */
    @objc func timerTick() {
        let remainingTime: Float = expiryTime - 1
        
        progressTime.text = timeFormate(Int(remainingTime))
        expiryTime = remainingTime
        
        if expiryTime <= 0.0 {
            let notificationName = Notification.Name("gotNewBooking")
            NotificationCenter.default.post(name: notificationName, object: nil)
            acceptTimer.invalidate()
        }
    }
    
    /**
     *  Check for Time formate
     *
     *  @param remainingTime Remaining time
     *
     *  @return Return must 00:00/00
     */
    func timeFormate(_ remainingTime: Int) -> String {
        let min: Int = remainingTime / 60
        let secs: Int = remainingTime % 60
        
        var minsString = ""
        var secsString = ""
        
        if min < 10{
            minsString = "0" + String(describing:min)
        }else{
            minsString = String(describing:min)
        }
        
        if secs < 10{
            secsString = "0" + String(describing:secs)
        }else{
            secsString = String(describing:secs)
        }
        
        return minsString + ":" + secsString
    }
    /**
     *  Get time interval between two times
     *
     *  @param dateFrom Datefrom
     *  @param dateTo   DateTo
     *
     *  @return Returns [01h:30m]
     */
    func getCreationTimeInt(expiryTimeStamp : Int64) -> Int64{
        let distanceTime = Date().timeIntervalSince1970
        return expiryTimeStamp - Int64(distanceTime)
    }
}
