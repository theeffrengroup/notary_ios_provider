//
//  MyEventsViewController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 04/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FirebaseMessaging
//import AICustomViewControllerTransition

class MyEventsViewController: UIViewController {

    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var notificationCount: UILabel!
    @IBOutlet weak var walletCount: UILabel!
    @IBOutlet weak var walletButton: UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var upcomingTable: UITableView!
    @IBOutlet weak var requestsTable: UITableView!
    
    @IBOutlet weak var upcomingBookingCount: UILabel!
    @IBOutlet weak var requestBookingCount: UILabel!
    @IBOutlet weak var profileReviewLabel: UILabel!
    @IBOutlet weak var requestsButton: UIButton!
    @IBOutlet weak var upcomingButton: UIButton!
    @IBOutlet weak var offlineOnlineButton: UIButton!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var noAcceptedView: UIView!
    @IBOutlet weak var noRequestView: UIView!
    @IBOutlet weak var indicatorView: UIView!
    
    var fromNormalRequest:Bool = false
    var fromUpcomingRequest:Bool = false
    var distanceTime = Timer()
    var model   = Events() //Events model
    var bookingData:MyeventModel?
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileReviewLabel.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        
       
//        if UserDefaults.standard.bool(forKey: "NotificationDisabled") == true {
//          showNotificationAlert()
//        }
        let url:String = "/topics/" + Utility.fcmTopic
        Messaging.messaging().subscribe(toTopic:url)
        locationtracker.startLocationTracking()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        NotificationCenter.default.addObserver(self, selector: #selector(makeTheHomeServiceCall), name: Notification.Name("gotNewBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeTheHomeServiceCall), name: Notification.Name("cancelledBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeTheDriverOffline), name: Notification.Name("makeOfflineFromAdmin"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeTheHomeServiceCall), name: Notification.Name("accountAccepted"), object: nil)
        
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(false)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        
        /// updating model if the booking cancelled,ACCEPTED,DECLINED OR COMPLETED
        if UserDefaults.standard.value(forKey: "selectedIndex") != nil && self.bookingData != nil{
            let val = UserDefaults.standard.value(forKey: "selectedIndex") as! Int
            if (self.bookingData?.acceptedData.count)!  > 0 ||  (self.bookingData?.requestData.count)!  > 0{
                if (self.bookingData?.acceptedData.count)! > 0 {
                    if (self.bookingData?.acceptedData.indices.contains(val))!{
                        if self.bookingData!.acceptedData[val].statusCode == 10{
                            self.bookingData?.acceptedData.remove(at: val)
                        }
                    }
                }
                if (self.bookingData?.requestData.count)! > 0{
                    if (self.bookingData?.requestData.indices.contains(val))!{
                        if self.bookingData!.requestData[val].statusCode == 3 || self.bookingData!.requestData[val].statusCode == 4{
                            self.bookingData?.requestData.remove(at: val)
                        }
                    }
                }
                self.upcomingTable.reloadData()
                self.requestsTable.reloadData()
            }
        }
    }
    
   
    
    
    
    @IBAction func walletBtnTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "openWallet", sender: nil)
    }
    
    
   
    
    @IBAction func notificationViewAction(_ sender: Any) {
    }
    
    @objc func makeTheDriverOffline() {
        self.offlineOnlineButton.isSelected = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        upcomingTable.addPullToRefresh { (finished) in
            if finished{
                 self.makeTheHomeServiceCall()
            }
        }
        
        requestsTable.addPullToRefresh { (finished) in
            if finished{
                  self.makeTheHomeServiceCall()
            }
        }
        
        self.makeTheHomeServiceCall()  //Booking data
    }
    
//    deinit {
//        upcomingTable.dg_removePullToRefresh()
//        requestsTable.dg_removePullToRefresh()
//
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gotNewBooking"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "accountAccepted"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "makeOfflineFromAdmin"), object: nil);
        
        // invalidating all the timer running on request table
        if self.bookingData?.requestData != nil{
            if (self.bookingData?.requestData.count)! <= 1{
                return
            }else{
                for val in 1 ... (self.bookingData?.requestData.count)! - 1{
                    let indexPath = IndexPath(row: val, section: 0)
                    let cell: RequestsCell = requestsTable.cellForRow(at: indexPath) as! RequestsCell
                    cell.acceptTimer.invalidate()
                }
            }
        }
    }
    
    func showNotificationAlert(){
        
        
        let alert = UIAlertController(title: "", message: "Turn on notification to receive bookings when you are not in the app", preferredStyle: .alert)
        
        
        let remindAction = UIAlertAction(title: "Remind me later", style: .cancel, handler: nil)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alert.addAction(UIAlertAction(title: "Allow", style: .default, handler: { (action: UIAlertAction) in
            //DispatchQueue.main.async {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    } else {
                        UIApplication.shared.openURL(settingsUrl as URL)
                    }
                }
           // }
        }))
        alert.addAction(remindAction)
        alert.addAction(cancelAction)
        
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    /// - starts the distance calculation
    func handleDistanceTime() {
        self.distanceTime.invalidate()
        self.distanceTime = Timer.scheduledTimer(timeInterval:5,
                                                 target: self,
                                                 selector: #selector(updatetheDistanceTravelled),
                                                 userInfo: nil,
                                                 repeats: true)
        
    }
    
    
    
   
    
    // makes the assigned trip service call and retrives the data
    @objc func makeTheHomeServiceCall(){
        
        model.Delegate = self
        model.assignedBookingAPI { (responseData) in
            self.bookingData = responseData
            self.upcomingTable.reloadData()
            self.requestsTable.reloadData()
            
            if (self.bookingData?.acceptedData.count)! > 0{
                let count = (self.bookingData?.acceptedData.count)!
                self.upcomingBookingCount.text = String(describing:count)
            }else{
                self.upcomingBookingCount.text = "0"
            }
            
            if (self.bookingData?.requestData.count)!  > 0{
                let count = (self.bookingData?.requestData.count)!
                self.requestBookingCount.text = String(describing:count)
            }else{
                self.requestBookingCount.text = "0"
            }
            self.handleDistanceTime()
        }
    }
    
    //MARK:- update artist status
    @IBAction func offlineOnlineAction(_ sender: Any) {
        if self.offlineOnlineButton.currentTitle != "Deactivated"{
            if offlineOnlineButton.isSelected {
                model.updateMasterStatus(status: "3")
                Helper.showPI(message:loading.goOnline)
            }else{
                model.updateMasterStatus(status: "4")
                Helper.showPI(message:loading.goOffline)
            }
        }
    }
    
    
    ///************ segue identifier method****************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        UserDefaults.standard.set(sender, forKey: "selectedIndex")
        UserDefaults.standard.synchronize()
        
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(true)
        self.tabBarController?.tabBar.isHidden = true
        
        switch segue.identifier! {
        case "toAcceptRejectBooking":
            if let nextScene = segue.destination as? AcceptRejectViewController {
                nextScene.bookingData = bookingData?.requestData[sender as! Int]
                nextScene.delegate = self
            }
            break
        case "toBookingVC":
            if let nextScene = segue.destination as? OnBookingViewController {
                nextScene.bookingDict = bookingData?.acceptedData[sender as! Int]
                
            }
            break
        case "toTimerView":
            if let nextScene = segue.destination as? TimerViewController {
                nextScene.bookingDict = bookingData?.acceptedData[sender as! Int]
                
            }
            break
        case "homeToInvoice":
            if let nextScene = segue.destination as? InvoiceViewController {
                nextScene.bookingDict = bookingData?.acceptedData[sender as! Int]
            }
            break
        default:
            break
        }
    }
}

extension MyEventsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == mainScrollView {
            /*
             indicator view position is not getting changed when the we change the Requests to Upcoming tab
             Owner : Vani Chikaraddi
             Date 27/02/2020
             */
//            indicatorView.frame = CGRect(x: scrollView.contentOffset.x / 2, y: indicatorView.frame.origin.y, width: self.view.frame.size.width/2, height: 2)
            indicatorView.layer.frame = CGRect(x: scrollView.contentOffset.x / 2, y: indicatorView.frame.origin.y, width: self.view.frame.size.width/2, height: 2)
            buttonEnable()
        }
    }
}

extension MyEventsViewController : MoveToUpcomingDelegate{
    func moveToUpcoming() {
        var frame: CGRect = self.mainScrollView.bounds;
        frame.origin.x = self.view.frame.width;
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
}

//Mark: - Home model delegate method
extension MyEventsViewController : EventsModelDelegate{
    
    //Bug id - 46, desc: GO ONLINE ,GO OFFLINE is showing only GO ONLINE. fix: fixed
    
    //last updated r updated driver status
    func checkTheDriverStatus(data:Int , statusRev:Int){
        
        
        if statusRev == 0{
            profileReviewLabel.isHidden = false
        }else{
            profileReviewLabel.isHidden = true
        }
        
        if data == 3
        {
            self.offlineOnlineButton.isSelected = false
            self.offlineOnlineButton.setTitle("Go Offline", for: .normal)
            self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "home_online_btn_off"), for: .normal)
        }else if data == 0{
            self.offlineOnlineButton.isSelected = false
            self.offlineOnlineButton.setTitle("Deactivated", for: .normal)
            self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "Deactive"), for: .normal)
        }
        else{
            self.offlineOnlineButton.isSelected = true
            self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "home_offline_btn_off"), for: .normal)
            self.offlineOnlineButton.setTitle("Go Online", for: .normal)
        }
    }
                
    //** Updated driver status reponse
    func updateTheDriverStatus(status: String ,success:Bool) {
        if status == "3" && success {
            self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "home_online_btn_off"), for: .normal)
            self.offlineOnlineButton.setTitle("Go Offline", for: .normal)
            self.offlineOnlineButton.isSelected = false
        }else if status == "4" && success{
            self.offlineOnlineButton.setBackgroundImage(#imageLiteral(resourceName: "home_offline_btn_off"), for: .normal)
             self.offlineOnlineButton.setTitle("Go Online", for: .normal)
            self.offlineOnlineButton.isSelected = true
        }else{
            self.offlineOnlineButton.isSelected = true
        }
    }
}


