//
//  MyEventsTableViewExtns.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 08/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


extension MyEventsViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch tableView {
            
        case requestsTable:
           if (bookingData?.requestData.count != nil && bookingData?.requestData.count != 0){
                noRequestView.isHidden  = true
                return 1
            }else{
                noRequestView.isHidden  = false
                return 0
            }
            
        case upcomingTable:
            if (bookingData?.acceptedData.count != nil && bookingData?.acceptedData.count != 0){
                noAcceptedView.isHidden  = true
                return 1
            }else{
                noAcceptedView.isHidden  = false
                return 0
            }
        default:
            
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case requestsTable:
            
            switch section {
            case 0:
                if (bookingData?.requestData.count) != nil  && (bookingData?.requestData.count) != 0{
                    return (bookingData?.requestData.count)!
                }else{
                    return 0
                }
            default:
                return 0
            }
            
        case upcomingTable:
            
            switch section {
            case 0:
                if (bookingData?.acceptedData.count) != nil && (bookingData?.acceptedData.count) != 0{
                    return (bookingData?.acceptedData.count)!
                }else{
                    return 0
                }
            default:
                return 0
            }
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case requestsTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: "requestsCell") as! RequestsCell
            cell.updateTheRequestData(data:(self.bookingData?.requestData[indexPath.row])!)
            return cell
            
        case upcomingTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: "upcomingCell") as! AcceptedTableCell
            cell.updateTheAcceptData(data:(self.bookingData?.acceptedData[indexPath.row ])!)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
      
            return 128;
    }
}

extension MyEventsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case requestsTable:
            performSegue(withIdentifier: "toAcceptRejectBooking", sender: indexPath.row)
        case upcomingTable:
            if (bookingData?.acceptedData.count) != nil  && (bookingData?.acceptedData.count) != 0{
                switch self.bookingData?.acceptedData[indexPath.row].statusCode{
                case 3?:
                    performSegue(withIdentifier: "toBookingVC", sender: indexPath.row)
                    break
                case 6?:
                    performSegue(withIdentifier: "toBookingVC", sender: indexPath.row)
                    break
                case 7?:
                    performSegue(withIdentifier: "toTimerView", sender: indexPath.row)
                    break
                case 8?:
                    performSegue(withIdentifier: "toTimerView", sender: indexPath.row)
                    break
                case 9?:
                    performSegue(withIdentifier: "homeToInvoice", sender: indexPath.row)
                    break
                default:
                    break
                }
            }
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

