//
//  HistoryController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 18/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit

class PayrollWebViewVC: UIViewController,WKUIDelegate {
    
    
//    @IBOutlet weak var payrollWebView: UIWebView!
    
    var activityIndicator = UIActivityIndicatorView()
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    var webView:WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
        
    }
    override func viewWillAppear(_ animated: Bool) {
//        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
//        animatedTabBar.animationTabBarHidden(false)
//        self.tabBarController?.tabBar.isHidden = false
        /*
                There was some blank space below the navigationBar
                Owner : Vani
                Date : 06/04/2020
                */
               super.viewWillAppear(animated)
               if #available(iOS 13.0, *) {
                    navigationController?.navigationBar.setNeedsLayout()
               }
        activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)

        self.webView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        requestUrls()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
//        animatedTabBar.animationTabBarHidden(false)
//        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backToVc(_ sender: Any) {
        if(webView.canGoBack) {
            //Go back in webview history
            webView.goBack()
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func requestUrls(){
        self.webView.load(URLRequest(url: URL(string: API.payRollWebView )!))
    }
}

//MARK: - Delegate of webview
/*
extension PayrollWebViewVC : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
   
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activityIndicator.stopAnimating()
        
    }
} */
extension PayrollWebViewVC : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        activityIndicator.stopAnimating()
    }
}
