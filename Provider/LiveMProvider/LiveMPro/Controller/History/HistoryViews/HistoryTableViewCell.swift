//
//  HistoryTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet var bookingTime: UILabel!
    @IBOutlet var bookAddress: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var eventType: UILabel!
    
    @IBOutlet weak var custImage: UIImageView!
    @IBOutlet weak var customerName: UILabel!
    
    @IBOutlet var topView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
