//
//  HistModel.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import Foundation

class AdditionalService {
    var price = ""
    var serviceName = ""
}

class CompletedBookings:NSObject {
    var addLine1 = ""
    var addLine2 = ""
    var bookingType = 1
    var averageRating = 0.0
    var bookingExpireTime:Int64 = 0
    var bookingRequestedFor:Int64 = 0
    var bookingRequestedAt:Int64 = 0
    var bookingRequestedEndTime:Int64 = 0
    var bookingId:Int64 = 0
    var distance:Int64 = 0
    var city = ""
    var country = ""
    var firstName = ""
    var gigTime = ""
    var lastName = ""
    var latitude = 0.0
    var longitude = 0.0
    var paymentMethod = 0
    var pincode = ""
    var placeId = ""
    var price = 0.0
    var cancellationFees = 0.0
    var discount = 0.0
    var myEarnings = 0.0
    var appEarnings = 0.0
    var profilePic = ""
    var typeofEvent = ""
    var statusCode = 0
    var mobileNum = ""
    var statusMsg = ""
    var signature = ""
    var GEGTime:gigTime?
    var customerID = ""
    var currencyType = ""
    var currencySymbol = ""
    var cancellationReason = ""
    var distanceMatrix = 0
    var serviceType = 0
    var totalHourlyFee = 0.0
    var totalJobTime = 0.0
    var isPaidByWallet = false
    var walletAmtPaid = 0.0
    var cardOrCashPaid = 0.0
    var actuallGigTimeStart:Int64 = 0
    var paidByWallet = false
     var paymentMethod2 = ""
   var categoryName = ""
    var services = [BookingServices]()
    var additionalSerice = [AdditionalService]()
}


class HistModel: NSObject {
    
    var bookingsData = [CompletedBookings]()
    var weeksData =  [Double]()
    var startDate:Int64 = 0
    var endDate:Int64 = 0
    var startDate1 = ""
     var endDate1 = ""
    var weekBookingData = [HistModel]()
    
    func parsingTheServiceResponse(responseData:[[String: Any]]) -> HistModel{
        Helper.hidePI()
        let bookingData = HistModel()
        
        for acceptData in responseData  {
            let model = CompletedBookings()
            if  let address1   =  acceptData["addLine1"] as? String {
                model.addLine1 = address1
            }
            
            if  let customerID   =  acceptData["customerId"] as? String {
                model.customerID = customerID
            }
            
            if  let category      =  acceptData["category"] as? String {
                model.categoryName = category
            }
            
            if   let eventStartTime     = acceptData["eventStartTime"] as? NSNumber {
                model.actuallGigTimeStart = Int64(eventStartTime)
            }
            
            if  let paymentType = acceptData["paymentMethod"] as? String {
                model.paymentMethod2 = paymentType
            }
            
            if  let cancelReason  =  acceptData["cancellationReason"] as? String {
                model.cancellationReason = cancelReason
            }
            
            if  let currType  = acceptData["currency"] as? String {
                model.currencyType = currType
            }
            
            if  let currSymbol  = acceptData["currencySymbol"] as? String {
                model.currencySymbol = currSymbol
            }
            
            if  let address2    = acceptData["addLine2"] as? String {
                model.addLine2 = address2
            }
            
         
            if   let expiryTime     = acceptData["bookingExpireTime"] as? NSNumber {
                model.bookingExpireTime = Int64(expiryTime)
            }
            
            if let bookId     = acceptData["bookingId"] as? NSNumber {
                model.bookingId = Int64(bookId)
            }
            
            if let distance     = acceptData["distance"] as? NSNumber {
                model.distance = Int64(distance)
            }
            
            if let distanceMatrix     = acceptData["distanceMatrix"] as? NSNumber {
                model.distanceMatrix = Int(distanceMatrix)
            }
            
            if  let city = acceptData["city"] as? String {
                model.city = city
            }
            
            if   let country     = acceptData["country"] as? String {
                model.country = country
            }
            
            if let firstName     = acceptData["firstName"] as? String {
                model.firstName = firstName
            }
            
            if  let lastname = acceptData["lastName"] as? String {
                model.lastName = lastname
            }
            
            if  let serviceType = acceptData["serviceType"] as? NSNumber {
                model.serviceType = Int(serviceType)
            }
            
            
            if   let lat     = acceptData["latitude"] as? NSNumber {
                model.latitude = Double(lat)
            }
            
            if let longt     = acceptData["longitude"] as? NSNumber {
                model.longitude = Double(longt)
            }
            
            if  let paymentType = acceptData["paymentMethod"] as? NSNumber {
                model.paymentMethod = Int(paymentType)
            }
            
            if let pincode     = acceptData["pincode"] as? String {
                model.pincode = pincode
            }
            
            if  let profileImg = acceptData["profilePic"] as? String {
                model.profilePic = profileImg
            }
            
            if let dict = acceptData["event"] as? [String:Any]{
                if  let eventType = dict["name"] as? String {
                    model.typeofEvent = eventType
                }
            }
            
            if let status = acceptData["status"] as? NSNumber{
                model.statusCode = Int(status)
            }
            
            if  let bookingRequestedFor  = acceptData["bookingRequestedFor"] as? NSNumber {
                model.bookingRequestedFor = Int64(bookingRequestedFor)
            }
            
            if  let bookingRequestedAt  = acceptData["bookingRequestedAt"] as? NSNumber {
                model.bookingRequestedAt = Int64(bookingRequestedAt)
            }
            
            if  let bookingEndtime  = acceptData["bookingEndtime"] as? NSNumber {
                model.bookingRequestedEndTime = Int64(bookingEndtime)
            }
            
            if let mobile  = acceptData["phone"] as? String{
                model.mobileNum = mobile
            }
            
            if let statMsg = acceptData["statusMsg"] as? String{
                model.statusMsg = statMsg
            }
            
            if let signatureUrl = acceptData["signatureUrl"] as? String{
                model.signature = signatureUrl
            }
            
            if let reviewData = acceptData["reviewByProvider"] as? [String:Any]{
                if  let averageRat  = reviewData["rating"] as? NSNumber {
                    model.averageRating = Double(Float(averageRat))
                }
            }
            
            if let account = acceptData["accounting"] as? [String:Any]{
                
                if let cancelAmount = account["cancellationFee"] as? NSNumber{
                    model.cancellationFees = Double(cancelAmount)
                }
                
                if let myEarning = account["providerEarning"] as? NSNumber{
                    model.myEarnings = Double(myEarning)
                }
                
                if let appEarning = account["appEarningPgComm"] as? NSNumber{
                    model.appEarnings = Double(appEarning)
                }
                
                if let totalFee = account["total"] as? NSNumber{
                    model.price =  Double(totalFee)
                }
                
                if let paidByWal = account["paidByWallet"] as? Int {
                    if paidByWal == 1 {
                        model.paidByWallet = true
                    }
                }
                if let discount = account["discount"] as? Double{
                    
                    if discount != 0 {
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = discount
                        serviceMod.serviceName = "Discount"
                        model.services.append(serviceMod)
                    }
                }
                if let visitFees = account["visitFee"] as? Double{
                    if visitFees > 0{
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = visitFees
                        serviceMod.serviceName = "Visit Fee"
                        model.services.append(serviceMod)
                    }
                }
                
                if let travelFree = account["travelFee"] as? Double {
                    if travelFree > 0 {
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = travelFree
                        serviceMod.serviceName = "Travel Fee"
                        model.services.append(serviceMod)
                    }
                }
                if let dues = account["lastDues"] as? Double {
                    if dues != 0 {
                        let serviceMod = BookingServices()
                        serviceMod.servicePrice = dues
                        serviceMod.serviceName = "Last Due"
                        model.services.append(serviceMod)
                    }
                }
                if let totalHourlyFee = account["totalActualHourFee"] as? NSNumber{
                    model.totalHourlyFee =  Double(totalHourlyFee)
                }
                
                if let totalTime = account["totalActualJobTimeMinutes"] as? NSNumber{
                    model.totalJobTime =  Double(totalTime)
                }
                
                if let paidByWal = account["paidByWallet"] as? Int {
                    if paidByWal == 1 {
                        model.isPaidByWallet = true
                    }
                }
                if let walletAmt = account["captureAmount"] as? NSNumber{
                    model.walletAmtPaid =  Double(walletAmt)
                }
                if let cashCardAmt = account["remainingAmount"] as? NSNumber{
                    model.cardOrCashPaid =  Double(cashCardAmt)
                }
                
            }
            
            if let addionalServices = acceptData["additionalService"] as? [[String:Any]]{
                for addtionalService in addionalServices{
                    let addService = AdditionalService()
                    let serviceMod = BookingServices()
                    if let price = addtionalService["price"] as? String{
                        addService.price = price
                        //serviceMod.servicePrice = Double(price)!
                    }
                    
                    if let serviceName = addtionalService["serviceName"] as? String{
                        addService.serviceName = serviceName
//                        serviceMod.serviceName = serviceName
                    }
//                    model.services.append(serviceMod)
                    model.additionalSerice.append(addService)
                }
            }
            
            if let dict = acceptData["service"] as? [String:Any]{
                let gigDuration = gigTime()
                if let name = dict["name"] as? String{
                    gigDuration.name = name
                    if let unit = dict["unit"] as? String{
                        model.gigTime = name + unit
                    }
                }
                
                if let unit = dict["unit"] as? String{
                    gigDuration.unit = unit
                }
                
                if let price = dict["price"] as? NSNumber{
                    gigDuration.price = Int(price)
                }
                
                if let second = dict["second"] as? NSNumber{
                    gigDuration.second = Int(second)
                }
                
                model.GEGTime = gigDuration
            }
            
            for service in (acceptData["item"] as? [[String:Any]])!{
                
                let serviceMod = BookingServices()
                if let quant = service["quntity"] as? NSNumber{
                    serviceMod.quantity =  Int(quant)
                }
                
                if let serviceAmt = service["amount"] as? NSNumber{
                    serviceMod.servicePrice = Double(serviceAmt)
                }
                
                if let serviceName = service["serviceName"] as? String{
                    serviceMod.serviceName = serviceName
                }
                model.services.append(serviceMod)
            }

            bookingData.bookingsData.append(model)
        }
        return bookingData
    }
    
    func parseTheWeeksAndBookingsCound(responseData:[[String: Any]]) -> [HistModel] {
        weekBookingData = [HistModel]()
        for week in responseData {
            let weekData = HistModel()
            if let startDate = week["startDate"] as? NSNumber{
                weekData.startDate = Int64(startDate)
            }
            
            if let endDate = week["endDate"] as? NSNumber{
                weekData.endDate = Int64(endDate)
            }
            
            if let sDate = week["sDate"] as? String{
                weekData.startDate1 = sDate
            }
            
            if let eDate = week["eDate"] as? String{
                weekData.endDate1 = eDate
            }
            
            if let weeksCount = week["count"] as? [Any]{
                var weeksBookings =  [Double]()
                for week in weeksCount {
                    if let count = week as? NSNumber{
                        if count == 0{
                            weeksBookings.append(0.0001)
                        }else{
                            weeksBookings.append(Double(count))
                        }
                    }
                }
                weekData.weeksData = weeksBookings
            }
            weekBookingData.append(weekData)
        }
        return weekBookingData
    }
}
