//
//  HistoryTableAndCollectionExnt.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 07/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import Kingfisher

///****** tableview datasource*************//
extension HistoryViewController: UITableViewDataSource {

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.completedData.count != 0 {
            noBookingHistLabel.isHidden = true
            emptyHistoryImage.isHidden = true
        }else{
            emptyHistoryImage.isHidden = false
            noBookingHistLabel.isHidden = false;
        }
        return self.completedData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"history") as! HistoryTableViewCell?
        cell?.customerName.text = self.completedData[indexPath.row].firstName
        cell?.amount.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",self.completedData[indexPath.row].price))
        cell?.bookAddress.text = self.completedData[indexPath.row].addLine1
        cell?.eventType.text = self.completedData[indexPath.row].typeofEvent
        
        let imageURL = self.completedData[indexPath.row].profilePic
        cell?.custImage.kf.setImage(with: URL(string: imageURL),
                                    placeholder:UIImage.init(named: "signup_profile_default_image"),
                                    options: [.transition(ImageTransition.fade(1))],
                                    progressBlock: { receivedSize, totalSize in
        },
                                    completionHandler: { image, error, cacheType, imageURL in
        })
        
        cell?.bookingTime.text = Helper.getTheDateFromTimeStamp(timeStamp:self.completedData[indexPath.row].bookingRequestedFor)
        
        cell?.statusLabel.text =  self.completedData[indexPath.row].statusMsg
        switch self.completedData[indexPath.row].statusCode{
        case 5:
            cell?.statusLabel.textColor = COLOR.bookingExpiredColor
            break
        case 4:
            cell?.statusLabel.textColor = COLOR.cancelDeclinedRed
            break
        case 12:
            cell?.statusLabel.textColor = COLOR.cancelDeclinedRed
            break
        case 11:
            cell?.statusLabel.textColor = COLOR.cancelDeclinedRed
            break
        case 10:
            cell?.statusLabel.textColor = COLOR.completedBookingColor
            break
        default:
            cell?.statusLabel.textColor = COLOR.onGoingBlue
            break
        }
        return cell!
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 110;
//    }
}

///***********tableview delegate methods**************//
extension HistoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.deselectRow(at: indexPath, animated: false)
        performSegue(withIdentifier: "toHistoryDetails", sender: nil)///*** moves to booking history***//
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier:"headerTableCell") as! HistHeaderTableViewCell?
        
        cell?.totalAmt.text = Helper.getTheAmtTextWithSymbol(amt:String(format:"%.2f",self.totalAmout))
        //String(describing:self.completedData.count) + " Boookings"
        //
        
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56
    }
}


//*************** uicollectionview datasource ******************//
extension HistoryViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "weeks", for: indexPath as IndexPath) as! WeeksCollectionViewCell
        
        if selectedWeek == indexPath.row {
            cell.weeksButton.isSelected = true
        }else{
            cell.weeksButton.isSelected = false
        }
        let startDate = Helper.changeDateFormatWithMonth(self.bookingArray[indexPath.row].startDate1)
        let endDate = Helper.changeDateFormatWithMonth(self.bookingArray[indexPath.row].endDate1)
        
        cell.weeksButton.setTitle(startDate + " - " + endDate, for: .normal)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.bookingArray.count
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if (self.bookingArray.count != 0){
            return 1
        }else{
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        return CGSize(width: screenSize.width/4, height: 60)
    }
}


//*************** uicollectionview delegate ******************//
extension HistoryViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell: WeeksCollectionViewCell = collectionView.cellForItem(at: indexPath) as! WeeksCollectionViewCell
        cell.weeksButton.isSelected = true
        selectedWeek = indexPath.row
        weeksCollectionView.scrollToItem(at: IndexPath(row: selectedWeek, section: 0), at: UICollectionView.ScrollPosition.right, animated: false)
        makeTheTotalForWeeks()
        self.AssignedTrips()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let cell: WeeksCollectionViewCell = collectionView.cellForItem(at: indexPath) as? WeeksCollectionViewCell {
            cell.weeksButton.isSelected = false
        }
    }
}

