//
//  HistoryViewController.swift
//  LiveM Provider
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
    
    @IBOutlet var noBookingHistLabel: UILabel!
    @IBOutlet var historyTableView: UITableView!
    @IBOutlet weak var emptyHistoryImage: UIImageView!
    @IBOutlet var weeksCollectionView: UICollectionView!
    @IBOutlet var barGraph: UIView!
    @IBOutlet weak var bookingsXaxisLabel: UILabel!
    var totalAmout = 0.0
    var dateSelected:Int = 0
    var selectedIndex:Int = 0
    var stardDate = String()
    var bookingID = String()
    var endDateSelected = String()
    
    var simpleBarGraph = SimpleBarChart()
    var graphInitiated = false
    var months: [String]!
    var selectedWeek:Int = 0
    var weeksTotal = [Double]()
    var values = [Double]()
    var barColors = [Any]()
    var currentBarColor: Int = 0
    var textLabel = [Any]()
    
    var bookingArray = [HistModel]()
    var histModel = HistoryModel()
    var completedData = [CompletedBookings]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        historyTableView.estimatedRowHeight = 10
        historyTableView.rowHeight = UITableView.automaticDimension
        let animatedTabBar = self.tabBarController as! RAMAnimatedTabBarController
        animatedTabBar.animationTabBarHidden(false)
        self.tabBarController?.tabBar.isHidden = false
        self.getTheWeeksData()
        
        bookingsXaxisLabel.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Get the weeks array from server
    func getTheWeeksData() {
        bookingArray = [HistModel]()
        histModel.getTheWeeksBookingData { (succeeded, weeksData) in
            if succeeded{
                self.bookingArray = weeksData
                if self.bookingArray.count != 0 {
                    self.getWeekCount()
                }
            }
        }
    }

/// update the collectionview, graph and get the booking from selected date
    func getWeekCount(){
        weeksCollectionView.reloadData()
        weeksCollectionView.scrollToItem(at: IndexPath(row: bookingArray.count-1, section: 0), at: UICollectionView.ScrollPosition.right, animated: false)
        selectedWeek = bookingArray.count-1
        let indexPath = IndexPath(item: selectedWeek, section: 0)
        self.weeksCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .right)
        self.AssignedTrips()
        self.makeTheTotalForWeeks()
    }
    
    
    // get the weeks completed, expired and cancelled bookings data
    func AssignedTrips(){
        histModel.getBookingHistoryData(startDate:self.bookingArray[selectedWeek].startDate1) { (succeeded, historyData) in
            if succeeded{
                Helper.hidePI()
                self.completedData = historyData.bookingsData
                self.totalAmout = 0.0
                for val in self.completedData{
                    self.totalAmout = self.totalAmout + val.price
                }
                self.historyTableView.reloadData()
            }
        }
    }
    
    // update the graph with data
    func makeTheTotalForWeeks(){
        weeksTotal = [Double]()
        weeksTotal =  self.bookingArray[selectedWeek].weeksData
        months = ["SUN","MON","TUE","WED","THU","FRI","SAT"]
        if graphInitiated{
            values = weeksTotal
            if weeksTotal.max()! == 1{
                simpleBarGraph.incrementValue = CGFloat(0.5)
            }else if weeksTotal.max()! == 2 || weeksTotal.max()! == 3 || weeksTotal.max()! == 4{
                simpleBarGraph.incrementValue = CGFloat(1)
            }else{
                simpleBarGraph.incrementValue = CGFloat(weeksTotal.max()!/4)
            }
            simpleBarGraph.reloadData()
        }else{
            months = [HistoryAlerts.Sun,HistoryAlerts.Mon,HistoryAlerts.Tue,HistoryAlerts.Wed,
                      HistoryAlerts.Thu,HistoryAlerts.Fri,HistoryAlerts.Sat]
            setChart(dataPoints: months, value: weeksTotal, maxValue: weeksTotal.max()!) //@Locate at HistoryGraphExtension
        }
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toHistoryDetails"{
            let nav =  segue.destination as! UINavigationController
            let nextScene = nav.viewControllers.first as! PastBookingController?
            nextScene?.bookingArray = self.completedData[selectedIndex]
        }
    }
}
