//
//  HistoryModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 10/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import RxCocoa
import RxSwift


class HistoryModel:NSObject {
    

    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    var historyManager = HistoryDBManager()
    
    
    /// get the booking history of particular week
    ///
    /// - Parameters:
    ///   - startDate: start date of the particular week
    ///   - completionHandler: return booking history and success resonse
    
    func getBookingHistoryData(startDate:String,completionHandler:@escaping (Bool,HistModel) -> ()) {
        var hadHistoryData  =  false
        historyManager.gethistoryData(forUserID: "bookingshistory") { (historyData) in
            hadHistoryData  =  true
            completionHandler(true,HistModel().parsingTheServiceResponse(responseData: historyData))
        }
        if !hadHistoryData{
            Helper.showPI(message:loading.load)
        }
        
        let rxApiCall = HistoryAPI()
        rxApiCall.getBookingsHistoryAPi(method:API.METHOD.BOOKINGSHISTORY + "/" + startDate) //Helper.yearMonthDateTime(timeStamp: startDate)
        rxApiCall.History_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    if  let data =  responseModel.data["data"] as? [[String:Any]]{
                        let history  : [String : Any] = ["historyDict": data]
                        self.historyManager.saveThehistory(forUserID: "bookingshistory", dict: history)
                        completionHandler(true,HistModel().parsingTheServiceResponse(responseData: (data)))
                    }
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    
    /// get the weeks array of dictionary
    ///
    /// - Parameter completionHandler: returns history model contains weeks array
    func getTheWeeksBookingData(completionHandler:@escaping (Bool,[HistModel]) -> ()) {
        
        historyManager.getWeeksData(forUserID: "weeksdata") { (weeksData) in
            if weeksData.count == 0{
                Helper.showPI(message: loading.load)
            }
            completionHandler(true, HistModel().parseTheWeeksAndBookingsCound(responseData: weeksData))
        }
        
        let rxApiCall = HistoryAPI()
        rxApiCall.getBookingsHistoryAPi(method:API.METHOD.WEEKSHISTORY)
        rxApiCall.History_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    if  let data =  responseModel.data["data"] as? [[String:Any]]{
                        let history  : [String : Any] = ["weeksDict": data]
                        self.historyManager.saveTheWeeksData(forUserID: "weeksdata", dict: history)
                        completionHandler(true, HistModel().parseTheWeeksAndBookingsCound(responseData: (data)))
                    }
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}
