//
//  CancelBookingModel.swift
//  LiveM
//
//  Created by Vengababu Maparthi on 12/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import RxAlamofire

enum cancelApiResults {
    case success([cancelReasons])
    case failure(Bool)
}

class cancelReasons: NSObject {
    var reason = ""
    var reasonID = 0
}


class CanelModel:NSObject{
    var cancelReasonsArray = [cancelReasons]()
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    var cancelManager = CancelDBManager()
    
    
    
    /// cancel the booking with reason
    ///
    /// - Parameters:
    ///   - dict: reason, booking id
    ///   - completionHandler: return true if the booking cancelled succeeded
    func cancelBooking(dict:[String:Any] ,
                       completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        let rxApiCall = CancelBookingAPI()
        rxApiCall.cancelBooking(method:API.METHOD.CANCELBOOKING,params:dict)
        rxApiCall.cancel_Response
            .subscribe(onNext: {responseModel in
                Helper.hidePI()
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
  
                case .SuccessResponse:
                    Helper.hidePI()
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)

                    completionHandler(true)
                    break
                default:
                    break
                }
            }, onError: {error in
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    ///MARK:- get the cancellation reasons
    func getCancellationReasonsAPI(completionHandler:@escaping (cancelApiResults) -> ()) {
        self.cancelReasonsArray = [cancelReasons]()
        self.cancelManager.getcancelData(forUserID: "cancelReasons") { (cancelData) in
            completionHandler(.success(self.cancelReasonArray(reasonsDict: cancelData)))
            if cancelData.count == 0{
                Helper.showPI(message: loading.load)
            }
        }
        
        let rxApiCall = CancelBookingAPI()
        rxApiCall.getCancellationReasonsAPI(method:API.METHOD.CANCELREASONS)
        rxApiCall.cancel_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                    
                case .SuccessResponse:
                    self.cancelReasonsArray = [cancelReasons]()
                    if let cancelDict = responseModel.data["data"] as? [[String:Any]]{
                        let cancel  : [String : Any] = ["cancelDict": cancelDict]
                        self.cancelManager.saveTheCancelReasons(forUserID: "cancelReasons", dict: cancel)
                        completionHandler(.success(self.cancelReasonArray(reasonsDict: cancelDict)))
                    }
                    break
                default:
                    
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    //MARK: - parse the cancel dictionary
    func cancelReasonArray(reasonsDict:[[String:Any]]) -> [cancelReasons] {
        
        for reason in reasonsDict {
            let cancelReason = cancelReasons()
            if let reasonText = reason["reason"] as? String{
                cancelReason.reason = reasonText
            }
            
            if let reasonID = reason["res_id"] as? NSNumber{
                cancelReason.reasonID = Int(reasonID)
            }
            cancelReasonsArray.append(cancelReason)
        }
        return cancelReasonsArray
    }
}



