//
//  DefaultAddressAlert.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 05/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class DefaultAddressAlert: UIView {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    var isShown:Bool = false
    
    private static var share: DefaultAddressAlert? = nil
    
    static var instance: DefaultAddressAlert {
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("DefaultAddressView",
                                                   owner: nil,
                                                   options: nil)?.first as? DefaultAddressAlert
        }
        return share!
    }
    
    @IBAction func okAction(_ sender: Any) {
        self.hide()
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.hide()
    }
    
    /// Show Network
    func show() {
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
    }
    
    /// Hide
    func hide() {
        
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            DefaultAddressAlert.share = nil
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
}

