//
//  AlertViewEmailSent.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 11/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

protocol AlertViewCancelDelegate:class
{
    func backToSignin()
}

class AlertViewEmailSent: UIView {
    open weak var delegate: AlertViewCancelDelegate?
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    var isShown:Bool = false
    
    private static var share: AlertViewEmailSent? = nil
    
    static var instance: AlertViewEmailSent {
        
        if (share == nil) {
            
            share = Bundle(for: self).loadNibNamed("AlertEmailSent",
                                                   owner: nil,
                                                   options: nil)?.first as? AlertViewEmailSent
            
        }
        return share!
    }
    
    @IBAction func okAction(_ sender: Any) {
        delegate?.backToSignin()
         self.hide()
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.hide()
    }
    
    /// Show Network
    func show() {
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
    }
    
    /// Hide
    func hide() {
        
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            AlertViewEmailSent.share = nil
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
}
