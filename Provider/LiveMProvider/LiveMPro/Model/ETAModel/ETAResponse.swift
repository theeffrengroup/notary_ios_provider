//
//  ETAResponse.swift
//  DayRunner
//
//  Created by Vasant Hugar on 26/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ETAResponse: NSObject {
    
    var distance: Int = 0
    var distanceText: String = ""
    
    var duration: String = ""
    var durationText: String = ""

}
