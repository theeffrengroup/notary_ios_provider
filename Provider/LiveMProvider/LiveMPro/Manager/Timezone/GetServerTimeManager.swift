//
//  GetServerTime.swift
//  Notary
//
//  Created by 3Embed on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import RxCocoa
import RxSwift
import CocoaLumberjack
import SwiftKeychainWrapper
class GetServerTimeManager {
    
    static let sharedInstance = GetServerTimeManager()
    var apiCall = APILibrary()
    
    let rxServerTimeAPICall = ServerTimeAPI()
    let disposebag = DisposeBag()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)

    /// Method to call get list of saved address service API
    func getServerTime() {
        
        if !UserDefaults.standard.bool(forKey: "hasInternet") {
            return
        }
        
        if !rxServerTimeAPICall.serverTime_Response.hasObservers {
            
            rxServerTimeAPICall.serverTime_Response
                .subscribe(onNext: {json in
                    
                    let dict  = json.data
                    let statuscode:Int = json.httpStatusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                    //                    self.WebServiceResponse(response: response, requestType: RequestType.GetServerTime)
                }, onError: { (error) in
                    
                    
                }).disposed(by: disposebag)
            
        }
        
        rxServerTimeAPICall.getServerTimeServiceAPICall()
        
    }

    
    
//
//    //MARK - WebService Response -
    
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        
        
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        
        let serviceResponseDate = Date()
        
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            let defaults = UserDefaults.standard
            if let sessionToken = responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            
            if let currentGMTTimeStamp = responseDict["data"] as? Int64 {
                
                appDelegate?.isDateChanged = false
                
                let currentDateFromServer = Date.init(timeIntervalSince1970: TimeInterval(currentGMTTimeStamp))
                
                let differenceTimeInterval = currentDateFromServer.timeIntervalSince(serviceResponseDate)
                
                print("\(differenceTimeInterval)")
                
                var differenceTime = differenceTimeInterval
                
                if differenceTimeInterval < 0 {
                    
                    differenceTime = differenceTimeInterval * -1
                }
                
                if differenceTime > 60 {
                    
                    UserDefaults.standard.set(differenceTimeInterval, forKey: USER_INFO.TIMEZONETIMEINTERVAL)
                    
                } else {
                    
                    UserDefaults.standard.set(0.0, forKey: USER_INFO.TIMEZONETIMEINTERVAL)
                }
                
                UserDefaults.standard.synchronize()
            }
            
            break
        }
    }

  
}
