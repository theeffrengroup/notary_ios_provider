//
//  MQTTModule.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 11/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import Foundation
import CocoaLumberjack

/// This class is a model for interacting with the SwiftMQTT library.
class MQTT: NSObject {
    
    struct Constants {
        ///Ask server guy for host and port.
       
        fileprivate static let host = "18.188.184.181"//
        fileprivate static let port:UInt32 = 2052
    }
    
    var sessionConnected = false
    var sessionError = false
    var sessionReceived = false
    var sessionSubAcked = false

    /// Shared instance object for gettting the singleton object
    //vani 24/02/2020
    @objc static let sharedInstance = MQTT()
    
    ///This flag will tell you that you are connected or not.
    @objc var isConnected : Bool = false
    
    ///current session object will going to store in this.
      var manager : MQTTSessionManager!
    
    ///Used for running the task in the background.
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    
    /// MQTT delegate object, its going to store the objects of the delegate receiver class.
    let mqttMessageDelegate = MQTTDelegate()
    
    
    /// Used for creating the initial connection.
    func createConnection() {
        
        /// Observer for app coming in foreground.
        NotificationCenter.default.addObserver(self, selector: #selector(MQTT.reinstateBackgroundTask), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        registerBackgroundTask()
        
        
        ///creating connection with the proper client ID.
        
        self.connect(withClientId: Utility.userId) 
        ///Subscribing Acknowledgment Channel
        // self.subscribeChannel(withChannelName: Constants.yourTopicName)
    }
    
    
    /// Used for subscribing the channel
    ///
    /// - Parameters:
    ///   - topic: name of the current topic (It should contain the name of the topic with saperators)
    ///
    /// eg- Message/UserName
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    fileprivate func subscribe(topic : String, withDelivering Delivering : MQTTQosLevel) {
        
        if (manager != nil) {
            if (manager.subscriptions != nil) {
                var subscribeDict = manager.subscriptions!
                if (subscribeDict[topic] == nil) {
                    subscribeDict[topic] = Delivering.rawValue as NSNumber
                }
                self.manager.subscriptions = subscribeDict
            } else {
                let subcription = [topic : Delivering.rawValue as NSNumber]
                self.manager.subscriptions = subcription
            }
            self.manager.connect(toLast: { (error) in
                if let error = error{
                    print("error ",error.localizedDescription)
                }
            })
        }
    }
    
    /// Used for reinstate the background task
    @objc func reinstateBackgroundTask() {
        if (backgroundTask == UIBackgroundTaskIdentifier.invalid) {
            registerBackgroundTask()
        }
    }
    
    ///Here I am registering for the background task.
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskIdentifier.invalid)
    }
    
    
    ///Before background task ending this method is going to be called.
    func endBackgroundTask() {
        DDLogDebug("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskIdentifier.invalid
    }
    
    /// Used for subscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to subscribe.
    func subscribeChannel(withChannelName channelName : String ) {
        let topicToSubscribe = channelName
        self.subscribe(topic: "\(topicToSubscribe)", withDelivering: .exactlyOnce)
    }
    
    /// Used for subscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to subscribe.
    func subscribeTopic(withTopicName topicName : String,withDelivering delivering:MQTTQosLevel ) {
        let topicToSubscribe = topicName
        self.subscribe(topic: "\(topicToSubscribe)", withDelivering: delivering)
    }

    
    /// Used for Unsubscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to Unsubscribing.
    func unsubscribeTopic(topic : String) {
        var unsubscribeDict = manager.subscriptions
        if unsubscribeDict?[topic] != nil {
            unsubscribeDict?.removeValue(forKey:topic)
        }
        self.manager.subscriptions = unsubscribeDict
        self.manager.connect(toLast: { (error) in
            if let error = error{
                print("error ",error.localizedDescription)
            }
        })
    }


    
    
    /// Used for pubishing the data in between channels.
    ///
    ///   - Parameters:
    ///   - jsonData: Data in JSON format.
    ///   - channel: current channel name to publish the data to.
    ///   - messageID: current message ID (this ID should be unique)
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    ///   - retain: true if you wanted to retain the messages or False if you don't
    ///   - completion: This will going to return MQTTSessionCompletionBlock.
    
    @objc func publishData(wthData jsonData: Data, onTopic topic : String, retain : Bool, withDelivering delivering : MQTTQosLevel) {
        
        if self.isConnected{
            if (self.manager != nil) {
                manager.send(jsonData, topic: topic, qos: delivering, retain: retain)
            }
            else {
                if Utility.userId == "user_id" {
                    self.isConnected = false
                    return
                }
                if (Utility.userId.count>1) {
                    self.connect(withClientId: Utility.userId)
                }
            }
        }
    }
    ///checkDup logs commented  in pod
   
    /// Used for connecting with the server.
    ///
    /// - Parameter clientId: current Client ID.
    func connect(withClientId clientId :String) {
        
        if Utility.userId == "user_id" {
            self.isConnected = false
            return
        }
        if (self.manager == nil) {
            self.manager = MQTTSessionManager()
            let host = Constants.host
            let port: UInt32 = Constants.port
            
            manager.delegate = mqttMessageDelegate
            
            manager.connect(to: host, port: Int(port), tls: false, keepalive: 45, clean: false, auth: false, user: nil, pass: nil, will: false, willTopic: nil, willMsg: nil, willQos: .atMostOnce, willRetainFlag: false, withClientId: clientId, securityPolicy: nil, certificates: nil, protocolLevel: .version311, connectHandler: { (error) in
                if let error = error{
                    print("error ",error.localizedDescription)
                }
            })

        }else{
            self.manager.connect(toLast: { (error) in
                if let error = error{
                    print("error ",error.localizedDescription)
                }
            })
        }
    }
}
