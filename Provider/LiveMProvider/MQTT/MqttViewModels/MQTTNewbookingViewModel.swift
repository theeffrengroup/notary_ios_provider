//
//  MQTTResponseHanlder.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 26/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UserNotifications
import AVFoundation
import SwiftKeychainWrapper
class MQTTResponseHandler:NSObject {
    
    var apiCall = APILibrary()
    var window: UIWindow?
    
    /// handling the cancelled booking, newbooking and scheduled booking from mqtt
    ///
    /// - Parameters:
    ///   - responseData: it contains bookings data, cancelled booking data
    ///   - topicName: jobstatus/user_id & jobstatus/user_id
    func gotResponeFromMqtt(responseData:Data, topicChannel topicName:String){
        
        if Utility.userId == "user_id" {
            return
        }
        
        do {
            guard let json = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String : Any] else { return }
            if topicName == "jobStatus/"  + Utility.userId {
                let imageDataDict:[String: Any] = json["data"] as! [String : Any]
                if let bookingType = imageDataDict["bookingType"] as? NSNumber{
                    if bookingType == 2{  // schedule booking
                        NotificationCenter.default.post(name: Notification.Name("cancelledBooking"), object: nil, userInfo: imageDataDict)
                        
                        if let status = imageDataDict["status"] as? NSNumber{
                            if status == 3{
                                let reminderData = ReminderModel()
                                if let requestedFor = imageDataDict["bookingRequestedFor"] as? NSNumber{
                                    reminderData.startDate = Int64(requestedFor)
                                }
                                
                                if let endTime = imageDataDict["bookingEndtime"] as? NSNumber{
                                    reminderData.enddate = Int64(endTime)
                                }
                                
                                if let lat = imageDataDict["latitude"] as? NSNumber{
                                    reminderData.latit = Double(lat)
                                }
                                
                                if let long = imageDataDict["longitude"] as? NSNumber{
                                    reminderData.logit = Double(long)
                                }
                                
                                if let bookingID = imageDataDict["bookingId"] as? NSNumber{
                                    reminderData.bookingID = Int64(bookingID)
                                }
                                
                                if let address = imageDataDict["addLine1"] as? String{
                                    reminderData.address = address
                                }
                                
                                ReminderModel().eventReminder(data: reminderData, completion: { (reminderID) in
                                    
                                })
                                
                            }else if status == 4{
                                
                            }else{
                                if let startTime = imageDataDict["bookingRequestedFor"] as? NSNumber{
                                    if let endTime = imageDataDict["bookingEndtime"] as? NSNumber{
                                        ReminderModel().removeReminder(Int64(startTime), endDate: Int64(endTime))
                                    }
                                }
                            }
                        }
                    }else{
                        NotificationCenter.default.post(name: Notification.Name("cancelledBooking"), object: nil, userInfo: imageDataDict)
                        // normal booking
                    }
                }
            }else if topicName == "booking/"  + Utility.userId {
                
                
                
                print("data received on topic \(topicName) message \(json)")
                self.acknowledgingTheBookingToServer(bookingDict: json["data"] as! [String : Any], completionHandler: { (succeeded) in
                    if succeeded{
                        let ud = UserDefaults.standard
                        ud.removeObject(forKey: "lastBid")
                        let notificationName = Notification.Name("gotNewBooking")
                        NotificationCenter.default.post(name: notificationName, object: nil)
                    }else{
                    }
                })
            }
        } catch let jsonError {
            print("Response Data Error !!!",jsonError) // if there is any error in parsing then it is going to be print here.
        }
    }
    
    
    //Creating local notification
    func showLocationNotification(){
        if #available(iOS 10.0, *) {
            let content   = UNMutableNotificationContent()
            content.title = "New booking"
            content.body  = "Congratulations you got New Booking"
            content.categoryIdentifier = "NewBooking"
            content.sound =  UNNotificationSound.init(named: UNNotificationSoundName(rawValue: "taxina-1.mp3"))
            content.badge = 1
            let trigger   = UNTimeIntervalNotificationTrigger(timeInterval:1, repeats:false)
            let request   = UNNotificationRequest(identifier: "NEWBOOKING", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler:nil)
        }else{
            let date      = Date(timeIntervalSinceNow:1)
            let notification = UILocalNotification()
            notification.fireDate = date
            notification.soundName = "taxina-1.mp3"
            notification.alertTitle = "New booking"
            notification.alertBody  = "Congratulations you got New Booking"
            notification.repeatInterval = NSCalendar.Unit.hour
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
    
    
    /// acknowledging service
    ///
    /// - Parameters:
    ///   - bookingDict: got the booking details imn push r mqtt
    ///   - completionHandler: once acknowledge service success, completion handler call the bookings api
    func acknowledgingTheBookingToServer(bookingDict:[String:Any],
                                         completionHandler:@escaping (Bool) -> ()) {
        let ud = UserDefaults.standard
        self.window  = UIApplication.shared.keyWindow
        if (ud.value(forKey: "lastBid") != nil ){
            let lastBid:Int64 = ud.value(forKey: "lastBid") as! Int64
            if let bid = bookingDict["bookingId"] as? NSNumber{
                if Int64(bid) == lastBid{
                    ud.removeObject(forKey: "lastBid")
                    return
                }
            }
        }
        // vani 31/03/2020
        //Hide the local notification on arrival of booking
//        self.showLocationNotification()
        
        let state: UIApplication.State = UIApplication.shared.applicationState
        
        if state == .background {
            
            if self.window!.visibleViewController()! is MyEventsViewController{
                
            }else {
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let tabbar: RAMAnimatedTabBarController? = storyboard.instantiateViewController(withIdentifier: "HomeTabBarController") as? RAMAnimatedTabBarController
                let window = UIApplication.shared.keyWindow
                window?.rootViewController = tabbar
            }
        }
        
        
        if let bid = bookingDict["bookingId"] as? NSNumber{
            ud.set(Int64(bid), forKey: "lastBid")
        }
        
        let params : [String : Any] =  ["bookingId"  :bookingDict["bookingId"] as Any,
                                        "latitude"    : ud.object(forKey: "currentLat") as Any,
                                        "longitude"   : ud.object(forKey: "currentLog") as Any]
        
        Helper.showPI(message:loading.load)
        
        apiCall.acknowledgingTheBookingToServer(method: API.METHOD.ACKNOWLEDGEBK, params: params) { (responseModel) in
            let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
            Helper.hidePI()
            
            switch responseCodes{
            case .UserLoggedOut:
                Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                Session.expired()
                break
                
            case .TokenExpired:
                let defaults = UserDefaults.standard
                if let sessionToken =  responseModel.data["data"]   as? String  {
                    KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                    self.apiCall.getTheNewSessionToken(completionHandler: { (success) in
                        if success{
                            
                        }
                    })
                }
                break
                
            case .dataNotFound:
                     completionHandler(true)
                ud.removeObject(forKey: "lastBid")
                break
                
            case .SuccessResponse:
                completionHandler(true)
                break
                
            default:
                completionHandler(false)
                break
            }
        }
    }
}
