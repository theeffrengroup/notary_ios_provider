//
//  MQTTManager.swift
//  webRtc_module
//
//  Created by Imma Web Pvt Ltd on 04/09/17.
//  Copyright © 2017 3embed. All rights reserved.
//

import UIKit
import CocoaLumberjack

class MQTTCallManager: NSObject {
    
    
    //sendCallRequest to Other User
    //parameters:
    //callType          : String - call type audio = 0  / video = 1
    //callerName        : String - user 's Name
    //callerImage       : String - userImage Url
    //callerIdentifier  : String - user 's email or phoneNumber
    //topic             : String - channel name
    class  func sendCallRequest(callType: String ,callerName: String , callerImage: String,callerIdentifier: String,topic: String, callerID: String,callID: String)  {
        
        
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        var dict = ["callerId" : userID ,
                    "callId" :  callID,
                    "callerName" : callerName,
                    "callerImage" : callerImage,
                    "callerIdentifier" : callerIdentifier, ///your own number hola registerNumber
                    "type": 0 ,
                    "callType" : callType,
                    
            ] as [String:Any]
        
        
        NSLog("**********\n******\n sendCallRequest \(dict) *************\n\n")
        
        
        if let videoView = UIApplication.shared.keyWindow!.viewWithTag(17) as? IncomingVideocallView{
            dict["callId"] = videoView.callId
            videoView.setMessageData(messageData: dict)
        }
        
        
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            MQTT.sharedInstance.publishData(wthData: jsonData , onTopic: topic, retain: false, withDelivering: .atMostOnce)
            
        }catch  {
            DDLogDebug(error.localizedDescription)
            
        }
    }
    
    
    
    
    
    //sendCall Accept Request
    //params:-
    //messageData : dict [String: Any]
    class func sendAcceptCallStatus(messageData:[String: Any]){
        
        let dict  = ["callerId" : messageData["callerId"]  as! String,
                     "callId" : messageData["callId"] as! String,
                     "callerIdentifier": messageData["callerIdentifier"] as! String,
                     "type" : 1 as Int
            
            ] as [String:Any]
        
        let topic = AppConstants.MQTT.calls + (dict["callerId"] as! String)
        
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            MQTT.sharedInstance.publishData(wthData: jsonData , onTopic: topic, retain: false, withDelivering: .atMostOnce)
        }catch  {
            DDLogDebug(error.localizedDescription)
        }
    }
    
    
    
    
    //send call Avilibility status
    //paramas: -
    //status : Int 0 = busy 1 = avilable
    //topic: String - channel name
    class  func sendcallAvilibilityStatus(status:Int ,topic: String) {
        
        let dict = ["status": status ] as [String: Any]
        
        NSLog("***************\n\n *******send call Availibilite =\(dict) ***********\n\n")
        
        do{
            
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            MQTT.sharedInstance.publishData(wthData: jsonData, onTopic: topic, retain: true, withDelivering: .atMostOnce)
            
        }catch{
            DDLogDebug(error.localizedDescription)
            
        }
    }
    
    
    
    
    
    
    //Recieve Message from Mqtt
    //parameters:
    //message           : [String : Any] - json response
    //topic             : String - channel name
    class func didRecieve(withMessage message: [String : Any], inTopic topic: String) {
        
        
        DDLogDebug("******didrecive call message statussss topic:\(topic) and data here *********\(message)")
        
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }

        var callType = ""
        var otherUserId = ""
        var callID = ""
       
          let registerNum = "+918866815490"
        
        if let dict = UserDefaults.standard.object(forKey: "storeIndexPath") as? [String:Any]{
            
            callType = dict["callType"] as! String
            otherUserId = dict["callerId"] as! String
            callID = dict["callId"] as! String
            
        }
        
        let selectedTopic = AppConstants.MQTT.callsAvailability + otherUserId
        
        
        if topic == selectedTopic{
            
            //unsubscribeTopic callavlibility
            MQTT.sharedInstance.unsubscribeTopic(topic: topic)
            
            
            if  UserDefaults.standard.bool(forKey:"iscallBtnCliked") {
                
                UserDefaults.standard.set(false, forKey: "iscallBtnCliked")
                DDLogDebug("***********+++++++++unsubscirbe called++++++++++++++*****************")
                
                //check call Avalible or not//
                if message["status"] as! Int == 1{
                    
                     let userName =  "" //add userName here
                     let userImage = "" //Aff user Profile Pic
                    
                   guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
                    let callChannel =  AppConstants.MQTT.calls + otherUserId
                    
                    self.sendCallRequest(callType:callType , callerName: userName , callerImage:userImage, callerIdentifier: registerNum ,topic: callChannel, callerID: otherUserId,callID: callID)
                    
                    self.sendcallAvilibilityStatus(status: 0, topic:AppConstants.MQTT.callsAvailability + userID)
                    
                }else{
                    
                    if let audioView = appDelegetConstant.window.viewWithTag(15) as? AudioCallView{
                        audioView.callTimerLbl.text = "User is on other call"}
                    
                    if let videoView = appDelegetConstant.window.viewWithTag(17) as? IncomingVideocallView{
                        videoView.calling_status.text = "User is on other call"
                    }
                }
            }
            
        }else if (topic == (AppConstants.MQTT.calls + userID) ){
            
            
            if (message["type"] as! Int == 0){
                
                //send busy status
                guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
                self.sendcallAvilibilityStatus(status: 0, topic: AppConstants.MQTT.callsAvailability + userID)
                
                
                //show incoming calling screen here//
                let appdelete = UIApplication.shared.delegate as! AppDelegate
                appdelete.showIncomingCallingScreen(callData: message)
                
            }
            else if (message["type"] as! Int == 2){
                
                //End call
                
                let callID = message["callId"] as! String
              guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }

                let window = UIApplication.shared.keyWindow!
                if  let  audioView = window.viewWithTag(15) as? AudioCallView{
                    if callID == audioView.callId{
                        audioView.timer?.invalidate()
                        audioView.webRtc?.disconnect()
                        audioView.playSound("end_of_call", loop: 1)
                        let when = DispatchTime.now() + 0.30
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            audioView.removeFromSuperview()
                        }
                        self.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
                    }
                }
                
                if let incomingView = window.viewWithTag(16) as? IncomingAudiocallView{
                    if callID == incomingView.callID{
                        incomingView.player?.stop()
                        incomingView.playSound("end_of_call", loop: 0)
                        let when = DispatchTime.now() + 0.30
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            incomingView.removeFromSuperview()
                        }
                        self.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
                    }
                }
                
                if let videoView = appDelegetConstant.window.viewWithTag(17) as? IncomingVideocallView{
                    if callID == videoView.callId{
                        videoView.webRtc?.disconnect()
                        videoView.timer?.invalidate()
                        videoView.playSound("end_of_call", loop: 1)
                        let when = DispatchTime.now() + 0.30
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            videoView.removeFromSuperview()
                        }
                        self.sendcallAvilibilityStatus(status: 1, topic:AppConstants.MQTT.callsAvailability + userID)
                    }
                }
                
                //check cxprovider object
                let appdele = UIApplication.shared.delegate as? AppDelegate
                if ((appdele?.callProviderDelegate) != nil){
                    
                    appdele?.callProviderDelegate?.provider.reportCall(with: (appdele?.callProviderDelegate?.uuId)!, endedAt: nil, reason: .remoteEnded)
                    self.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
                }
                
                
            }else if (message["type"] as! Int == 1){
                
                
                //init webRTC here with callId ..accept call
                //For Audio call
                if let audioView = appDelegetConstant.window.viewWithTag(15) as? AudioCallView{
                    audioView.timer?.invalidate()
                    audioView.initWebrtc(messageData: message )
                }
                
                //For Video call
                if let videoView = appDelegetConstant.window.viewWithTag(17) as? IncomingVideocallView{
                    videoView.initWebrtc(messageData: message)
                    videoView.removeCameraLayer()
                    videoView.timer?.invalidate()
                    videoView.switchViews()
                }
                
                
            }else if (message["type"] as! Int == 3){
                
                
                //No answer by reciver after 1 min
                let callID = message["callId"] as! String
                if  let  audioView = UIApplication.shared.keyWindow!.viewWithTag(15) as? AudioCallView{
                    if callID == audioView.callId{
                        audioView.webRtc?.disconnect()
                        audioView.playSound("end_of_call", loop: 1)
                        let when = DispatchTime.now() + 0.30
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            audioView.removeFromSuperview()
                        }
                    }
                }
                
                if let incomingView = UIApplication.shared.keyWindow!.viewWithTag(16) as? IncomingAudiocallView{
                    if callID == incomingView.callID{
                        
                        incomingView.playSound("end_of_call", loop: 1)
                        let when = DispatchTime.now() + 0.30
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            incomingView.removeFromSuperview()
                        }
                        
                    }
                }
                
                
                if let videoView = appDelegetConstant.window.viewWithTag(17) as? IncomingVideocallView{
                    if callID == videoView.callId {
                        videoView.webRtc?.disconnect()
                        videoView.playSound("end_of_call", loop: 1)
                        let when = DispatchTime.now() + 0.30
                        DispatchQueue.main.asyncAfter(deadline: when) {
                          
                            videoView.removeFromSuperview()
                        }
                    }
                }
                
                
                guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }

                self.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
                
                
                
            }else if (message["type"] as! Int == 5){
                
                //Start video call on ui button
                
            }else if (message["type"] as! Int == 6){
                
                //stop video call on UI button
                
                
                
            }else if (message["type"] as! Int == 7){
                
                 guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
                // timeout on caller side(no response within 60 sec)
                let callID = message["callId"] as! String
                let window = UIApplication.shared.keyWindow!
                if  let  audioView = window.viewWithTag(15) as? AudioCallView{
                    if callID == audioView.callId {
                        audioView.playSound("end_of_call", loop: 1)
                        let when = DispatchTime.now() + 0.30
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            audioView.removeFromSuperview()
                        }
                    }
                    
                    self.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
                }
                
                if let incomingView = window.viewWithTag(16) as? IncomingAudiocallView{
                    if callID == incomingView.callID{
                        incomingView.playSound("end_of_call", loop: 1)
                        let when = DispatchTime.now() + 0.30
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            incomingView.removeFromSuperview()
                        }
                        self.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
                    }
                }
                
                
                if let videoView = appDelegetConstant.window.viewWithTag(17) as? IncomingVideocallView{
                    
                    if callID == videoView.callId{
                        videoView.webRtc?.disconnect()
                        videoView.playSound("end_of_call", loop: 1)
                        let when = DispatchTime.now() + 0.30
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            videoView.removeFromSuperview()
                        }
                        self.sendcallAvilibilityStatus(status: 1, topic:AppConstants.MQTT.callsAvailability + userID)
                    }
                }
               
                
                //check cxprovider object
                let appdele = UIApplication.shared.delegate as? AppDelegate
                if ((appdele?.callProviderDelegate) != nil){
                    
                    appdele?.callProviderDelegate?.provider.reportCall(with: (appdele?.callProviderDelegate?.uuId)!, endedAt: nil, reason: .remoteEnded)
                    self.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
                }
                
            }
        }
    }
    
    
    
    //sendEndCall Request
    //paramas : -
    //callID : String - callId
    //callerID : String - callerID
    class func sendEndcallRequest(callID:String, callerID: String, targetID: String,callType: String){
        
       guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        
        let dict = ["type": 2,
                    "callId": callID,
                    "targetId" : targetID,
                    "callerId": callerID,
                    "callType":callType,
                    "userId":userID
        ] as [String:Any]
        
        let topic = AppConstants.MQTT.calls + callerID
        NSLog("***********\n************\n send end call req \(dict)  .. topic ..\(topic) \n\n*****************")
        
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            MQTT.sharedInstance.publishData(wthData: jsonData , onTopic: topic, retain: false, withDelivering: .atMostOnce)
            
        }catch  {
            DDLogDebug(error.localizedDescription)
        }
    }
    
    

    
    
    //send timeout call Request
    //paramas:
    //callID : String - callId
    //callerID : String - callerID
    class func sendTimeoutcallRequest(callID: String , callerID: String ,type: Int ){
        
        let dict  = ["type" : type,
                     "callId": callID
            ]as [String :Any]
        
        let topic = AppConstants.MQTT.calls + callerID
        
        NSLog("***********\n************\n send timeout call req \(dict)  .. topic ..\(topic) \n\n*****************")
        
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            MQTT.sharedInstance.publishData(wthData: jsonData , onTopic: topic, retain: false, withDelivering: .atMostOnce)
            
        }catch  {
            DDLogDebug(error.localizedDescription)
            
        }
    }
    
}





//generate random number
//params :-
//length : Int -
func randomString(length: Int) -> String {
    
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
}







