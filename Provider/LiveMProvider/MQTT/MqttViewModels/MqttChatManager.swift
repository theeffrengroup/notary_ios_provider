//
//  MqttChatManager.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 12/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


class MQTTChatResponseHandler:NSObject {
    var window: UIWindow?
    var alert:UIAlertController!
    
    
    /// it gets the chat data from mqtt delegate
    ///
    /// - Parameters:
    ///   - responseData: contains chat data
    ///   - topicName: message/user_id
    func gotChatResponeFromMqtt(responseData:Data, topicChannel topicName:String){
        self.window  = UIApplication.shared.keyWindow
        do {
            guard let json = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String : Any] else { return }
            if let dict = json["data"] as? [String:Any]{
                if  self.window!.visibleViewController()! is ChatVC{
                    let imageDataDict:[String: Any] = dict
                    NotificationCenter.default.post(name: Notification.Name("gotNewMessage"), object: nil, userInfo: imageDataDict)
                }else{
                    
                    let state: UIApplication.State = UIApplication.shared.applicationState
                    if state == .background {
                   
                    }else{
                        alertForChat(booking:dict)
                    }
                }
            }
        } catch let jsonError {
            print("Response Data Error !!!",jsonError) // if there is any error in parsing then it is going to be print here.
        }
    }
    
    // open chat controller
    /// it shows the chat controller when the app in background it will execute
    ///
    /// - Parameter booking: booking id, custprofile, cust name, message
    func showChatController(booking:[String:Any]) {

        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Chat") as? ChatVC
        
            self.window  = UIApplication.shared.keyWindow
        controller?.bookingID = String(describing:booking["bid"]!)
            
            if let name =  booking["name"] as? String{
                controller?.custName = name
            }
            
            if let custImage  = booking["profilePic"] as? String{
                controller?.custImage = custImage
            }
            
            if let custID  = booking["fromID"] as? String{
                controller?.customerID = custID
            }
      
        
            TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                      subType: CATransitionSubtype.fromTop.rawValue,
                                                                      for: (self.window!.visibleViewController()!.navigationController?.view)!,
                                                                      timeDuration: 0.3)
            let animatedTabBar = self.window!.visibleViewController()!.tabBarController as! RAMAnimatedTabBarController
            animatedTabBar.animationTabBarHidden(true)
        self.window!.visibleViewController()!.navigationController?.pushViewController(controller!, animated: false)
        
       
    }
    
    
    /// initate the chat alert
    ///
    /// - Parameter booking: booking id, custprofile, cust name, message
    func alertForChat(booking:[String:Any]){
        
        if (alert != nil){
            self.alert.dismiss(animated: true, completion: {
                self.updateTheAlert(booking: booking)
            })
        }else{
            self.updateTheAlert(booking: booking)
        }
    }
    
    
    /// show the alerts
    /// show the chat alert when the app is in foreground and not in chat controller
    ///
    /// - Parameter booking: booking id, custprofile, cust name, message
    func updateTheAlert(booking:[String:Any]){
        
        
        alert = UIAlertController(title: "Message", message: "You got a new message from booking ID: " + String(describing:booking["bid"]!), preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as? ChatVC {
                controller.bookingID = String(describing:booking["bid"]!)
                
                if let name =  booking["name"] as? String{
                    controller.custName = name
                }
                
                if let custImage  = booking["profilePic"] as? String{
                    controller.custImage = custImage
                }
                
                if let custID  = booking["fromID"] as? String{
                    controller.customerID = custID
                }
                self.alert = nil
                TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                          subType: CATransitionSubtype.fromTop.rawValue,
                                                                          for: (self.window!.visibleViewController()!.navigationController?.view)!,
                                                                          timeDuration: 0.3)
                let animatedTabBar = self.window!.visibleViewController()!.tabBarController as! RAMAnimatedTabBarController
                animatedTabBar.animationTabBarHidden(true)
                self.window!.visibleViewController()!.navigationController?.pushViewController(controller, animated: false)
            }
        })
        
        let noButton = UIAlertAction(title: "No", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            print("you pressed No button")
            self.alert = nil
            // call method whatever u need
        })
        alert.addAction(yesButton)
        alert.addAction(noButton)
      //  self.window!.visibleViewController()!.present(self.alert, animated: true) { _ in }
    }
}
