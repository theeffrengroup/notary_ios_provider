//
//  MessageNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 31/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import CocoaLumberjack


class MQTTDelegate: NSObject, MQTTSessionManagerDelegate {
    
    let mqttHandler = MQTTResponseHandler()
    let mqttChatHandler = MQTTChatResponseHandler()
    
    func handleMessage(_ data: Data!, onTopic topic: String!, retained: Bool) {
        do {
            guard let dataObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            DDLogDebug("Received \(dataObj) on:\(topic) r\(retained)")
            // After getting new message this method will respond.
            
            if topic == "message/" + Utility.userId {
                self.mqttChatHandler.gotChatResponeFromMqtt(responseData: data, topicChannel: topic)
            }else if (topic.range(of: AppConstants.MQTT.callsAvailability) != nil ) || (topic.range(of: AppConstants.MQTT.calls) != nil){
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
                    
                    MQTTCallManager.didRecieve(withMessage: json, inTopic: topic)
                } catch let jsonError {
                    print("Error !!!\(jsonError)")
                }
            }
            else{
                self.mqttHandler.gotResponeFromMqtt(responseData: data, topicChannel: topic)
            }
            
        } catch let jsonError {
            DDLogDebug("Error !!!\(jsonError)")
        }
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didDeliverMessage msgID: UInt16) {
        print("Message delivered")
    }
    
    func messageDelivered(_ session: MQTTSession!, msgID: UInt16, topic: String!, data: Data!, qos: MQTTQosLevel, retainFlag: Bool) {
        DDLogDebug( "\(msgID)Message delivered")
        guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
        session.persistence.deleteAllFlows(forClientId: userID)
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didChange newState: MQTTSessionManagerState) {
          let mqtt = MQTT.sharedInstance
        switch newState {
        case .connected:
            print("connected")
          
            if Utility.userId == "user_id" {
                mqtt.isConnected = false
                return
            }
            
            mqtt.isConnected = true
            mqtt.subscribeChannel(withChannelName: "booking/" + Utility.userId)
            mqtt.subscribeChannel(withChannelName: "jobStatus/" + Utility.userId)
            mqtt.subscribeChannel(withChannelName: "message/" + Utility.userId)
            
            guard let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String else { return }
            MQTTCallManager.sendcallAvilibilityStatus(status: 1, topic: AppConstants.MQTT.callsAvailability + userID)
            MQTT.sharedInstance.subscribeTopic(withTopicName: AppConstants.MQTT.calls+userID, withDelivering: .atLeastOnce)
            
        case .closed:
            mqtt.isConnected = false
            DDLogDebug("disconnected")
            
            //            if let userID = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userID) as? String {
            //                if MQTT.sharedInstance.isConnected {
            //                    if userID.count>1 {
            //                        MQTT.sharedInstance.manager.reconnect()
            //                    }
            //                }
            //            }
            
        case .error: print("error \(sessionManager.lastErrorCode)")
            
        default:
            DDLogDebug("disconnected")
        }
    }
}
