//
//  WalletAPI.swift
//  LSP
//
//  Created by Vengababu Maparthi on 24/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//


import Foundation



import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack
import Alamofire
import SwiftKeychainWrapper
class CardAPI:NSObject {
    
    let disposebag = DisposeBag()
    let Card_Response = PublishSubject<APIResponseModel>()
    

    func addNewCard(paramDict: [String:Any]){
        Helper.showPI(message: "sending..")
        let strURL = API.BASE_URL + API.METHOD.CARDDETAILS
        
        RxAlamofire
            .requestJSON(.post, strURL,parameters:paramDict, encoding:JSONEncoding.default, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.Card_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    func getTheCards(){
        let strURL = API.BASE_URL + API.METHOD.CARDDETAILS
        
        RxAlamofire
            .requestJSON(.get, strURL, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.Card_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    func makeCardDefault(paramDict: [String:Any]){
        Helper.showPI(message: "Creating ticket..")
        let strURL = API.BASE_URL + API.METHOD.CARDDETAILS
        
        RxAlamofire
            .requestJSON(.patch, strURL,parameters:paramDict,encoding:JSONEncoding.default, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.Card_Response.onError(error)
            }).disposed(by: disposebag)
    }
    

    func deleteCard(paramDict: [String:Any]){
        
        let strURL = API.BASE_URL + API.METHOD.CARDDETAILS
        RxAlamofire
            .requestJSON(.delete, strURL, parameters:paramDict, encoding:JSONEncoding.default, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.Card_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        Helper.hidePI()
        
        DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.Card_Response.onNext(responseModel)
            break
        }
    }
}




