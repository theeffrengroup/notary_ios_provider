//
//  UploadImageAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 03/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import Alamofire
class ImageUploadAPI:NSObject{
    
    
    
    /// upload image to server
    ///
    /// - Parameters:
    ///   - imageData: the image data to upload
    ///   - parameters: attaching bid to the image
    ///   - onCompletion: return image url and success response
    ///   - onError: return the error if api fails
    /*
    func requestWith(imageData: Data?, onCompletion: ((Bool,String) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = "https://api.go-tasker.com/uploadImage"//http://45.77.190.140:8009/upload"
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
//            for (key, value) in parameters {
//                if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
//                    multipartFormData.append(data, withName: key)
//                }
//            }
            multipartFormData.append(imageData!, withName: "photo", fileName: "image.jpg", mimeType: "image/jpeg")
        },
                         to: url,method:HTTPMethod.post,
                         headers:RXNetworkHelper.getAOTHHeader(), encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload
                                    .validate()
                                    .responseJSON { response in
                                        switch response.result {
                                        case .success(let value):
                                            do {
                                                if let dictonary = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String:Any]{
                                                    onCompletion!(true,dictonary["data"] as! String)
                                                }
                                            } catch let error as NSError {
                                                print(error)
                                            }
                                            
                                            print("responseObject: \(value)")
                                        case .failure(let responseError):
                                            print("responseError: \(responseError)")
                                            onCompletion!(false,"")
                                        }
                                }
                            case .failure(let encodingError):
                                onCompletion!(false,"")
                                Helper.hidePI()
                                print("encodingError: \(encodingError)")
                            }
        })
    } */
    
    //Alamofire updates
        func requestWith(imageData: Data?, onCompletion: ((Bool,String) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
    //        let url = "https://api.go-tasker.com/uploadImage" //API.BASE_URL + "uploadImage"
            let url = API.BASE_URL + API.METHOD.IMAGEUPLOAD
            let parameters:[String:String] = ["uploadTo":"1","folder":"provider"]
            Helper.showPI(message: "Saving...")
            AF.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(imageData!, withName: "file", fileName: "image.jpg", mimeType: "image/jpeg")
                for (key, value) in parameters {
                    if let data = value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
                    multipartFormData.append(data, withName: key)
                    }
                }

            }, to: url,method:HTTPMethod.post,
                headers:RXNetworkHelper.getAOTHHeader()).responseJSON { response in
                        switch response.result {
                        case .success(let value):
                            do {
                                Helper.hidePI()
                                if let dictonary = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String:Any]{
    //                                onCompletion!(true,dictonary["data"] as! String)
                                    if let data = dictonary["data"] as? [String:Any] {
                                        onCompletion!(true,data["imageUrl"] as! String)
                                    }
                                }
                            } catch let error as NSError {
                                Helper.hidePI()
                                print(error)
                            }
                            
                            print("responseObject: \(value)")
                        case .failure(let responseError):
                            Helper.hidePI()
                            print("responseError: \(responseError)")
                            onCompletion!(false,"")
                        }
                }
        }
}
