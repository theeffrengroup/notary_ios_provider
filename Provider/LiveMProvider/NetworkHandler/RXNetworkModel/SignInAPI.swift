//
//  SignInAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import RxSwift
import CocoaLumberjack
import SwiftKeychainWrapper
class SignInAPI:NSObject {
    
    let disposebag = DisposeBag()
    let signIN_Response = PublishSubject<APIResponseModel>()
    var apiCall = APILibrary()
        
    /// Signup API
    ///
    /// - Parameters:
    ///   - method: provider/signUp
    ///   - parameters: provider details
    func makeApiCallForSignIn(method:String , parameters: SigninModel?){
        
        let paramDict : [String : Any] =  [SIGNIN_REQUEST.EMAIL_PHONE   : parameters!.emailRPhoneNum as Any,
                                           SIGNIN_REQUEST.PASSWORD         : parameters!.passwordField as Any,
                                           SIGNIN_REQUEST.DEVICE_ID           : Utility.deviceId,
                                           SIGNIN_REQUEST.PUSH_TOKEN         : Utility.pushToken ,
                                           SIGNIN_REQUEST.APP_VERSION        :Utility.appVersion,
                                           SIGNIN_REQUEST.DEVICE_MAKE       : "Apple",
                                           SIGNIN_REQUEST.DEVICE_MODEL      : Utility.modelName,
                                           SIGNIN_REQUEST.DEVICE_TYPE          :"1",
                                           SIGNIN_REQUEST.DEVICETIME        : Helper.currentGMTDateTime,
                                           SIGNIN_REQUEST.BATTERY_PER        :String(describing:Utility.batteryLevel),
                                           SIGNIN_REQUEST.DEVICE_VERSION : Utility.deviceVersion]
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.post, strURL ,parameters:paramDict, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                if  let dict  = json as? [String:Any]{
                           DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.signIN_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    
    /// Response parsing
    ///
    /// - Parameters:
    ///   - statusCode: HTTPSResponseCodes
    ///   - responseDict: signin response
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
             DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                  defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .profileBanned:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.signIN_Response.onNext(responseModel)
            break
        }
    }
}




