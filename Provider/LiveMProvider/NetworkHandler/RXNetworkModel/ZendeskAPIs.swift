//
//  PostTicketZendesk.swift
//  Zendesk
//
//  Created by Vengababu Maparthi on 26/12/17.
//  Copyright © 2017 Vengababu Maparthi. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack
import SwiftKeychainWrapper
class ZendeskAPI:NSObject {
    
    let disposebag = DisposeBag()
    let Zendesk_Response = PublishSubject<APIResponseModel>()
    

    /// creating new ticket
    ///
    /// - Parameter paramDict:subject, comment, priority
    func postTheNewTicket(paramDict: [String:Any]){
         Helper.showPI(message: "Creating ticket..")
        let strURL = API.BASE_URL + API.METHOD.ZENDESKTickets
        
        RxAlamofire
            .requestJSON(.post, strURL,parameters:paramDict, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                         Helper.alertVC(errMSG: error.localizedDescription)
                self.Zendesk_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    ///get the tickets data using request id or email id
    func getTheTicketData(id: String){
        let strURL = API.BASE_URL + API.METHOD.GETZENDESKTickets + id
        
        RxAlamofire
            .requestJSON(.get, strURL, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.Zendesk_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    /// get the ticketid data using ticket id
    ///
    /// - Parameter id: ticket id
    func getTheTicketHistory(id: String){
        Helper.showPI(message: "loading..")
        let strURL = API.BASE_URL + API.METHOD.GETZENDESKTicketHistory + id
        
        RxAlamofire
            .requestJSON(.get, strURL, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                         Helper.alertVC(errMSG: error.localizedDescription)
                self.Zendesk_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    
    /// update to new comment to a ticket with id and requestid
    ///
    /// - Parameter paramDict: new comments for ticketid
    func postTheNewTicketComment(paramDict: [String:Any]){
        Helper.showPI(message: "sending..")
        let strURL = API.BASE_URL + API.METHOD.ZENDESKTicketComment
        
        RxAlamofire
            .requestJSON(.put, strURL,parameters:paramDict, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                         Helper.alertVC(errMSG: error.localizedDescription)
                self.Zendesk_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    /// response of all above Zendesks
    ///
    /// - Parameters:
    ///   - statusCode: 200 is success
    ///   - responseDict: response data after success
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        Helper.hidePI()

        DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.Zendesk_Response.onNext(responseModel)
            break
        }
    
    }
}



