//
//  ConfigModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 13/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack
import FirebaseMessaging
import SwiftKeychainWrapper
class ConfigModelClass {
    var apiCall = APILibrary()
        let disposebag = DisposeBag()
    
    func makeServiceCallForConfigData(){  //API.METHOD.CONFIG
        
        let strURL = API.BASE_URL + API.METHOD.CONFIG
        RxAlamofire
            .requestJSON(.get, strURL, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(API.METHOD.CONFIG)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    /// parse the Config api response
    ///
    /// - Parameters:
    ///   - statusCode: httpstatuscodes
    ///   - responseDict: response data
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            if let configData = responseModel.data["data"] as? [String : Any] {
                self.handlingTheCongigData(response: configData)
            }
            break
        }
    }
    
    func handlingTheCongigData(response:[String:Any]) {
        let ud = UserDefaults.standard
        if let currencySymbol = response["currencySymbol"] as? String{
            ud.set(currencySymbol, forKey: USER_INFO.CURRENCYSYMBOL)
        }
        
        if let currencyAbbr = response["currencyAbbr"] as? String{
            ud.set(Int(currencyAbbr), forKey: USER_INFO.CURRENCYPLACE)
        }
        
        if let mileageMetric = response["mileage_metric"] as? String{
            //ud.set(mileageMetric, forKey: USER_INFO.DISTANCE)
        }
        
        if let distanceUnit = response["distanceMatrix"] as? Int {
            ud.set(distanceUnit, forKey: USER_INFO.DISTANCE)
        }
        
        
        
        if let providerIntervalDict =  response["providerFrequency"] as? [String:Any]{
            if let liveTrackInt = providerIntervalDict["liveTrackInterval"] as? NSNumber{
                ud.set(liveTrackInt as! Int, forKey: USER_INFO.BOOKAPIINTERVAL)
            }
            if let apiInterval = providerIntervalDict["locationPublishInterval"] as? NSNumber{
                ud.set(apiInterval as! Int, forKey: USER_INFO.APIINTERVAL)
            }
        }
        
        if let mqttTimeOut = response["latLongDisplacement"] as? NSNumber{
            ud.set(mqttTimeOut as! Int, forKey: USER_INFO.DISTANCESTORINGDATA)
        }
        
        if let manatory = response["mandatory"] as? NSNumber {
            ud.set(manatory, forKey: USER_INFO.VERSIONMANDATORY)
        }
        
        if let stripeKeyData = response["stripeKeys"] as? String {
            ud.set(stripeKeyData , forKey: USER_INFO.STRIPE_KEY)
        }
                
        var topicsArray = [String]()
        if let topicsData = response["pushTopics"] as? [String:Any] {
            if let cities = topicsData["city"] as? String{
                let url:String = "/topics/" + cities
                topicsArray.append(url)
                Messaging.messaging().subscribe(toTopic:url)
            }
            if let allPros = topicsData["allProvider"]  as? String{
                let url:String = "/topics/" + allPros
                topicsArray.append(url)
                Messaging.messaging().subscribe(toTopic:url)
            }
            if let allCities = topicsData["allCities"]  as? String{
                let url:String = "/topics/" + allCities
                topicsArray.append(url)
                Messaging.messaging().subscribe(toTopic:url)
            }
        }
        
        ud.set(topicsArray, forKey: "topicsArray")
        ud.synchronize()
        
        
    }
    
}
