//
//  RXNetworkHelper.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 03/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper
enum HTTPSResponseCodes: Int {
    
    case TokenExpired = 440
    case UserLoggedOut = 498
    case SuccessResponse = 200
    case preconditionFailed = 412
    case preconditionFailed1 = 413
    case invalidEnter = 406
    case dataNotFound = 404
    case uauthoriedUser = 401
    case profileReject  = 403
    case adminNotAccepted = 409
    case otpExpired      = 410
    case tooManyAttempts = 429
    case accountDeactivate = 421
    case BadRequest = 400
    case InternalServerError = 500
    case notAtAcceptedFromAdmin = 430
    case requestTimeOut = 408
    case profileBanned = 411
}


class RXNetworkHelper: NSObject {
    /// Athenticate Header
    ///
    /// - Returns: Dict of Athentication
        class func getAOTHHeader() -> HTTPHeaders {
        
        var sessionToken = String()
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            sessionToken = Utility.sessionCheck
        }else{
            sessionToken = String(format: "%@", Utility.sessionToken)
        }
        let dict: HTTPHeaders = ["authorization": (sessionToken),"lan":"en"]
        return dict
    }
}
