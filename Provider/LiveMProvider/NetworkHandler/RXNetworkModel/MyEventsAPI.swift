//
//  MyEventsAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import RxCocoa
import RxSwift
import CocoaLumberjack
import SwiftKeychainWrapper
class MyEventApi: NSObject {
    
    let disposebag = DisposeBag()
    let booking_Response = PublishSubject<APIResponseModel>()
    var apiCall = APILibrary()
    
    /// bookings data
    ///
    /// - Parameter method: provider/bookings
    func getBookingsDataAPi(method: String){
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.get, strURL , headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                self.booking_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    /// bookings data
    ///
    /// - Parameter method: provider/updateDriverStatus
    func updateDriverStatus(params:[String:Any],method: String){
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.patch, strURL , parameters:params, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                           DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                        Helper.hidePI()
                self.booking_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    
    
    
    /// parse the bookings and provider status api response
    ///
    /// - Parameters:
    ///   - statusCode: httpstatuscodes
    ///   - responseDict: response data
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        DDLogVerbose(" Response: \(responseDict)");
        Helper.hidePI()
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{

                    }
                })
            }
          //      else{
//                //Helper.alertVC(errMSG: responseDict["message"] as! String)
//                Session.expired()
//            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.booking_Response.onNext(responseModel)
            break
        }
    }
}



