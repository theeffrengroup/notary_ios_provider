//
//  SignUpAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa
import CocoaLumberjack
import SwiftKeychainWrapper
class ChatAPI:NSObject {
    
    let disposebag = DisposeBag()
    let chat_Response = PublishSubject<APIResponseModel>()
    
    
    func getTheChatHist(bookingID: String){
        
        let strURL = API.BASE_URL + "chatHistory/"  + bookingID
        RxAlamofire
            .requestJSON(.get, strURL,headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(strURL)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                    Helper.hidePI()
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.chat_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    func getTheBookingsChat(){
        
        let strURL = API.BASE_URL + API.METHOD.BOOKINGCHAT
        RxAlamofire
            .requestJSON(.get, strURL,headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(strURL)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                    Helper.hidePI()
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.chat_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    func postTheMessage(parameters: [String:Any]?){
        let strURL = API.BASE_URL + "message"  //http://chat.go-tasker.com/message"
        RxAlamofire
            .requestJSON(.post, strURL ,parameters:parameters ,headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(strURL)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                    
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.chat_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    /// parse the chat api response
    ///
    /// - Parameters:
    ///   - statusCode: httpstatuscodes
    ///   - responseDict: response data
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        
        DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.chat_Response.onNext(responseModel)
            break
        }
    }
}

