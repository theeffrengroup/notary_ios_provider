//
//  VerifyOTPAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import RxCocoa
import RxSwift
import CocoaLumberjack
import SwiftKeychainWrapper
class VerifyOTPAPI: NSObject {
    
    let disposebag = DisposeBag()
    let verify_response = PublishSubject<APIResponseModel>()
    var apiCall = APILibrary()
    
    /// verification
    ///
    /// - Parameter method: provider/verifyPhonenumber while signup
    func verifyPhoneNumberForSignup(params:[String:Any],method: String){
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.post, strURL , parameters:params, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                           DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.verify_response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    
    
    /// resendOtpFor signup verification and forgotpassword
    ///
    /// - Parameters:
    ///   - params: provider id
    ///   - method: resendOtp
    func resendOtpForSignup(params:[String:Any],method: String){
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.post, strURL , parameters:params, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                           DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.verify_response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    /// verifiying otp for forgot password, phonenumber
    ///
    /// - Parameters:
    ///   - params: provider id
    ///   - method: verifyfor number change and forgotpassword
    func verifyOTPForNumberAndPassword(params:[String:Any],method: String){
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.post, strURL , parameters:params, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                           DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.verify_response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    
    
    /// responseHandling with status codes
    ///
    /// - Parameters:
    ///   - statusCode: HttpStatusCodes
    ///   - responseDict: response data
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
             DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                  defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.verify_response.onNext(responseModel)
            break
        }
    }
}



