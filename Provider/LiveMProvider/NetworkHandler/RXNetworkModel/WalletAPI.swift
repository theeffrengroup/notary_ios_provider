//
//  WalletAPI.swift
//  LSP
//
//  Created by Vengababu Maparthi on 24/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//


import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack
import SwiftKeychainWrapper
class WalletAPI:NSObject {
    
    let disposebag = DisposeBag()
    let Wallet_Response = PublishSubject<APIResponseModel>()
    ///get the tickets data using request id or email id
    func getTheWalletData(){
        let strURL = API.BASE_URL + API.METHOD.PAYMENTSETT
        
        RxAlamofire
            .requestJSON(.get, strURL, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.Wallet_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    func getTheRecentTransaction(index: Int){
        let strURL = API.BASE_URL +  API.METHOD.TRANSACTION  + "/" + index.description
        
        RxAlamofire
            .requestJSON(.get, strURL, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.Wallet_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    /// creating new ticket
    ///
    /// - Parameter paramDict:subject, comment, priority
    func rechargeTheWalletAmt(paramDict: [String:Any]){
        Helper.showPI(message: "Creating ticket..")
        let strURL = API.BASE_URL + API.METHOD.RECHARGE_WALLET
        
        RxAlamofire
            .requestJSON(.post, strURL,parameters:paramDict, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                self.Wallet_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    /// response of all above Zendesks
    ///
    /// - Parameters:
    ///   - statusCode: 200 is success
    ///   - responseDict: response data after success
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        Helper.hidePI()
        
        DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.Wallet_Response.onNext(responseModel)
            break
        }
        
    }
}



