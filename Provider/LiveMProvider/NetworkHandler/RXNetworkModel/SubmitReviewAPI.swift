//
//  AcceptRejectBookingAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack
import SwiftKeychainWrapper
class SubmitReviewAPI:NSObject {
    
    let disposebag = DisposeBag()
    let SubmitReview_Response = PublishSubject<APIResponseModel>()
    var apiCall = APILibrary()
    
    /// SubmitReview
    ///
    /// - Parameter method: provider/submitReview
    func submitReview(method: String, parameters: [String:Any]?){
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.post, strURL ,parameters:parameters,  headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                           DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.SubmitReview_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    /// parse the submitReview api response
    ///
    /// - Parameters:
    ///   - statusCode: httpstatuscodes
    ///   - responseDict: response data
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
             DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                  defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.SubmitReview_Response.onNext(responseModel)
            break
        }
    }
}




