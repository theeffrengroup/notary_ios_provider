//
//  SignUpAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack
import Alamofire
import SwiftKeychainWrapper

class SignUpAPI:NSObject {
    
    let disposebag = DisposeBag()
    let signup_Response = PublishSubject<APIResponseModel>()
    var apiCall = APILibrary()
    
    
    /// Signup API
    ///
    /// - Parameters:
    ///   - method: provider/signUp
    ///   - parameters: provider details
    func makeApiCallForSignup(method:String , params: SignUPParams?){
        
        
        
        let saveTheParameters : [String : Any] = [SIGNUP_REQUEST.NAME          : params!.firstName,
                                                  SIGNUP_REQUEST.LAST_NAME       : params!.lastName,
                                                  SIGNUP_REQUEST.EMAIL           : params!.emailAddress,
                                                  SIGNUP_REQUEST.PASSWORD        : params!.password,
                                                  SIGNUP_REQUEST.PHONE_NUMBER    : params!.phoneNum,
                                                  SIGNUP_REQUEST.COUNTRY_CODE    : params!.countryCode,
                                                  SIGNUP_REQUEST.COUNTRYCODE_SYMBOL    : params!.countryCodeSymbol,
                                                  SIGNUP_REQUEST.LATITUDE        : params!.lat,
                                                  SIGNUP_REQUEST.LONGITUDE       : params!.log,
                                                  SIGNUP_REQUEST.USER_IMAGE      : params!.proImg,
                                                  SIGNUP_REQUEST.DEVICE_ID       : Utility.deviceId,
                                                  SIGNUP_REQUEST.DATE_OF_BIRTH   : params!.DOB,
                                                  SIGNUP_REQUEST.ARTIST_CATEGORY : params!.catID,
                                                  SIGNUP_REQUEST.DEVICE_TYPE     : "1",
                                                  SIGNUP_REQUEST.PUSH_TOKEN      : Utility.pushToken ,
                                                  SIGNUP_REQUEST.APP_VERSION     : Utility.appVersion,
                                                  SIGNUP_REQUEST.DEVICE_MAKE     : "Apple",
                                                  SIGNUP_REQUEST.DEVICE_MODEL    : Utility.modelName,
                                                  SIGNUP_REQUEST.CITY_ID         : params!.cityID,
                                                  SIGNUP_REQUEST.DEVICE_VERSION  :Utility.deviceVersion,
                                                  SIGNUP_REQUEST.ARTIST_ADDRESS  :params!.addressArtist,
                                                  SIGNUP_REQUEST.DOCUMENTS       :params!.documentDict,
                                                  SIGNUP_REQUEST.TAGGED_ID       :params!.taggedAs,
                                                  "city"                        :params!.city,
                                                  "state"                       :params!.state,
                                                  "country"                     :params!.country,
                                                  "pincode"                    :params!.pinCode,
                                                  "subCatlist"                 :params!.subCatList,
                                                  "gender"                      :params!.gender,
                                                  "referralCode": params!.referral]
        
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.post, strURL ,parameters:saveTheParameters, encoding:JSONEncoding.default, headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.signup_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    
    /// Show Categories by City
    ///
    /// - Parameter method: provider/serviceCateogries
    func getCategoriesAccordingToCity(method: String){
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.get, strURL , headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.signup_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    
    /// validate email address
    ///
    /// - Parameter method: provider/emailValidation
    func validateEmail(method: String, parameters: ValidatePhoneEmail){
         let emailParams : [String : Any] = [VALIDATEEMAIL_PHONE_REQUEST.EMAIL:parameters.emailAddress]
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.post, strURL ,parameters:emailParams,  headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.signup_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    /// validate phone address
    ///
    /// - Parameter method: provider/validateReferral
    func validatePhone(method: String, parameters: ValidatePhoneEmail){
        let phoneParams : [String : Any] = [VALIDATEEMAIL_PHONE_REQUEST.MOBILE:  parameters.phoneNum,
                                            VALIDATEEMAIL_PHONE_REQUEST.COUNTRY_CODE : parameters.countryCode]
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.post, strURL ,parameters:phoneParams,  headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.signup_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    
    
    /// validate referral code
    ///
    /// - Parameter method: provider/emailValidation
    func validateReferralCode(method: String, parameters: [String:Any]?){
        
        let strURL = API.BASE_URL + method
        RxAlamofire
            .requestJSON(.post, strURL ,parameters:parameters,  headers: RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                if  let dict  = json as? [String:Any]{
                           DDLogVerbose(" API: \(method)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                self.signup_Response.onError(error)
                Helper.alertVC(errMSG: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    
    /// parse the response
    ///
    /// - Parameters:
    ///   - statusCode: httpstatuscodes
    ///   - responseDict: authenticate response data
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: statusCode)!
        let apiCall = APILibrary()
        Helper.hidePI()
        switch responseCodes {
        case .BadRequest:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .InternalServerError:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        case .TokenExpired:
            
            let defaults = UserDefaults.standard
            if let sessionToken =  responseDict["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                defaults.synchronize()
                apiCall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
            
        case .UserLoggedOut:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
            
        case .profileReject:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            Session.expired()
            break
        case .adminNotAccepted:
            Helper.alertVC(errMSG: responseDict["message"] as! String)
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.signup_Response.onNext(responseModel)
            break
        }
    }
}



