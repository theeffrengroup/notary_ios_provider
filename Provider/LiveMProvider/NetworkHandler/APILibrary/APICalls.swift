//
//  APICalls.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 05/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper
class APILibrary {
    
    //MARK: - App configuration api
    func appConfigDataService( completionHandler:@escaping (APIResponseModel) -> ()) {
        
        //Helper.showPI(message:loading.load)
        
        NetworkHelper.requestGETURL(method: API.METHOD.CONFIG,
                                    success: { (response) in
                                        Helper.hidePI()
                                        
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - get the artist address
    func getLocationData( completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestGETURL(method: API.METHOD.ADDRESS,
                                    success: { (response) in
                                        Helper.hidePI()
                                        
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - add the artist address
    func postLocationData(params: [String : Any], completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestPOST(serviceName: API.METHOD.ADDRESS,
                                  params: params,
                                  success:{ (response) in
                                    Helper.hidePI()
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //Delete the artist address
    func deleteArtistAddress(params: [String : Any],completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestDELETEURL(method: API.METHOD.ADDRESS,
                                       params: params,
                                       success: { (response) in
                                        Helper.hidePI()
                                        completionHandler(response)
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - SIGNUP API
    class func makeApiCallForSignup(method: String, params:[String:Any] ,
                                    completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPOST(serviceName: method,
                                  params: params,
                                  success:{ (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    // MARK: - SIGNIN API
    func makeApiCallForSignIn(params:[String:Any] ,
                              completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPOST(serviceName: API.METHOD.LOGIN,
                                  params: params,
                                  success:{ (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Validate Phone number
    func validatePhone( params:[String:Any] ,
                        completionHandler:@escaping (APIResponseModel) -> ()) {
        
        
        Helper.showPI(message:signup.verifyMobile)
        
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.PHONEVALIDATE,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - Validate Email Address
    func validateEmail(params:[String:Any] ,
                       completionHandler:@escaping (APIResponseModel) -> ()) {
        
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.EMAILVALIDATE,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    
    //MARK: - VERIFY Phone number for signup
    func verifyPhoneNumberForSignup( params:[String:Any] ,
                                     completionHandler:@escaping (APIResponseModel) -> ()) {
        
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.VERIFYPHONESIGNUP,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - resend otp for signup
    func resendOtpForSignup( params:[String:Any] ,
                             completionHandler:@escaping (APIResponseModel) -> ()) {
        
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.RESENDOTP,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - resend otp for ForgotPassword
    func resendOtpForForgotPassword( params:[String:Any] ,
                                     completionHandler:@escaping (APIResponseModel) -> ()) {
        
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.RESENDOTP,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Get Cities API
    func getCityAPI( completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestGETURL(method: API.METHOD.getCities,
                                    success: { (response) in
                                        Helper.hidePI()
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - Get Categories According to API
    func getCategoriesAccordingToCity(method: String, completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestGETURL(method: method,
                                    success: { (response) in
                                        
                                        Helper.hidePI()
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - BOOKINGS
    //MARK: - Bookings API
    
    func getBookingsDataAPi(method: String, completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestGETURL(method: API.METHOD.ASSIGNEDTRIPS,
                                    success: { (response) in
                                        
                                        Helper.hidePI()
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Bookigns ACknowledge service
    func acknowledgingTheBookingToServer(method: String, params:[String:Any] ,
                                         completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.ACKNOWLEDGEBK,
                                   params: params,
                                   success: { (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Bookings History API
    
    func getBookingsHistoryAPi(method: String, completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestGETURL(method: API.METHOD.BOOKINGSHISTORY,
                                    success: { (response) in
                                        Helper.hidePI()
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Bookings History API
    
    func getSupportData( completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        NetworkHelper.requestGETURL(method: API.METHOD.SUPPORT,
                                    success: { (response) in
                                        Helper.hidePI()
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Bookings Accept Reject API
    func acceptRejectModelBooking(params:[String:Any],
                                  completionHandler:@escaping (APIResponseModel) -> ()) {
        
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.ACCEPTREJECTRESPONSE,
                                   params: params,
                                   success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - Bookings status updation
    func updateBookingStatus(dict:[String:Any] ,
                             completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.UPDATEBOOKINGSTATUS,
                                   params: dict,
                                   success: { (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Bookings status updation
    func updateDriverStatus(dict:[String:Any] ,
                             completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.UPDATEDRIVERSTATUS,
                                   params: dict,
                                   success: { (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - Update Booking Timer status pause and start
    func updateBookingTimer(dict:[String:Any] ,
                            completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.UPDATETIMERSTATUS,
                                   params: dict,
                                   success: { (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - Cancel Booking API
    func cancelBooking(params:[String:Any] ,
                       completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.CANCELBOOKING,
                                   params: params,
                                   success: { (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Get the cancellation reasons API
    func getCancellationReasonsAPI(completionHandler:@escaping (APIResponseModel) -> ()) {
        Helper.showPI(message: loading.load)
        
        
        NetworkHelper.requestGETURL(method: API.METHOD.CANCELREASONS,
                                    success: { (response) in
                                        Helper.hidePI()
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Submit Review API
    func submitReview(params: [String : Any],completionHandler:@escaping (APIResponseModel) -> ()) {
        
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.SUBMITREVIEW,
                                  params: params,
                                  success: { (response) in
                                    Helper.hidePI()
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
        
    }
    
    //MARK: - Logout API
    func logoutActionService(completionHandler:@escaping (APIResponseModel) -> ()) {
        let params: [String : Any] =  [
            "userType" :2]
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPOST(serviceName:API.METHOD.LOGOUT,
                                  params: params,
                                  success: { (response) in
                                    Helper.hidePI()
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Get Profile Data
    func profileDetailsService(completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestGETURL(method: API.METHOD.PROFILEDATA,
                                    success: { (response) in
                                        
                                        Helper.hidePI()
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Profile data Update API
    func profileDataUpdateService(params:[String:Any] ,
                                  completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.UPDATEPROFILE,
                                   params: params,
                                   success: { (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - forgotpassword API through email and phone number
    
    func forgotPasswordWithEmailAndPhoneAPi(params: [String : Any],completionHandler:@escaping (APIResponseModel) -> ()) {
        
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.FORGOTPASSWORD,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Verify verification number, when forgot password time
    func verifyOTPForNumberAndPassword(params: [String : Any],completionHandler:@escaping (APIResponseModel) -> ()) {
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.verifyOTP,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    
    //MARK: - Phone number update API
    func updateNewPhoneNumber( params:[String:Any] ,
                              completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.UPDATEPHONE,
                                   params: params,
                                   success: { (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    
    //MARK: - update New password API after forgot password
    func UpdateNewPassword( params:[String:Any] ,
                           completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.UPDATEPASSWORD,
                                   params: params,
                                   success: { (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Change password from profile
    
    
    func changePassword( params:[String:Any] ,
                            completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPatch(serviceName:API.METHOD.CHANGEPASSWORD,
                                   params: params,
                                   success: { (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - Get new session token 
    func getTheNewSessionToken(completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestGETURL(method: API.METHOD.NEWACCESSTOKEN,
                                    success: { (response) in
                                        
                                        Helper.hidePI()
                                        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: response.httpStatusCode)!
                                        Helper.hidePI()
                                        switch responseCodes{
//                                        case .UserLoggedOut:
//                                            Helper.alertVC(errMSG: response.data["message"] as! String)
//                                            Session.expired()
//                                            break
                                       
                                        case .SuccessResponse:
                                            let defaults = UserDefaults.standard
                                            if let sessionToken =  response.data["data"]   as? String  {
                                                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                                                 defaults.synchronize()
                                            }
                                            completionHandler(true)
                                            break
                                        default:
                                            Helper.alertVC(errMSG: response.data["message"] as! String)
                                            completionHandler(false)
                                            break
                                        }
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Add Stripe account
    func addStripeAccount(params: [String : Any],completionHandler:@escaping (APIResponseModel) -> ()) {
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.ADDBANKSTRIPE,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - add bank account after stripe activated
    
    func addBankAccount(params: [String : Any],completionHandler:@escaping (APIResponseModel) -> ()) {
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.ADDBANKAFTERSTRIPE,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - get bank data
    func getAddedBankData(completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestGETURL(method: API.METHOD.ADDBANKSTRIPE,
                                    success: { (response) in
                                        
                                        Helper.hidePI()
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - make Account default
    
    func makeAccountDefault(params: [String : Any],completionHandler:@escaping (APIResponseModel) -> ()) {
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.DEFAULTBANK,
                                  params: params,
                                  success: { (response) in
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - delete bank account
    
    func deleteBankAccount(params: [String : Any],completionHandler:@escaping (APIResponseModel) -> ()) {
         Helper.showPI(message:loading.load)
        NetworkHelper.requestDELETEURL(method: API.METHOD.DELETEBANK,
                                       params: params,
                                       success: { (response) in
                                        completionHandler(response)
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - Schedule API
     func makeApiCallForSchedule(method: String, params:[String:Any] ,
                                      completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPOST(serviceName: method,
                                  params: params,
                                  success:{ (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - delete Schedule API
     func deleteScheduleApiCall(method: String, params:[String:Any] ,
                                     completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestDELETEURL(method: method,
                                       params: params,
                                       success: { (response) in
                                        completionHandler(response)
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    
    //MARK: - get schedule data
    func getMethodServiceCall(method: String,completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestGETURL(method: method,
                                    success: { (response) in
                                        
                                        Helper.hidePI()
                                        completionHandler(response)
                                        
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
    
    //MARK: - Schedule API
    func validateReferralCode(method: String, params:[String:Any] ,
                                completionHandler:@escaping (APIResponseModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        NetworkHelper.requestPOST(serviceName: method,
                                  params: params,
                                  success:{ (response) in
                                    
                                    completionHandler(response)
                                    
        },failure: { (error) in
            
            Helper.hidePI()
            Helper.alertVC(errMSG: error.localizedDescription)
        })
    }
}
