//
//  NetworkHelper.swift
//  Trustpals
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftKeychainWrapper
enum ErrorFlagType: Int {
    case Success = 0
    case Failure = 1
}



enum HTTPSCommonErrorCodes: Int {
    
    case BadRequest = 400
    case InternalServerError = 500
}
/*
class NetworkHelper: NSObject {
    
    
    class func ParseAPIResponse(response:DataResponse<Any>,
                          methodName:String,
                          completion: @escaping(APIResponseModel) -> Void,
                          failure:@escaping (Error) -> Void) {
        
     
        
        if response.result.isSuccess {
            print("\n\nHTTPS Status Code = \((response.response?.statusCode)!)")
            print("\n\n\(methodName) Response : \(response.description)\n\n")
            
            switch (response.response?.statusCode)! {
                
            case HTTPSCommonErrorCodes.BadRequest.rawValue:
                
                    failure(getCommonErrorDetails(response: response) as NSError)
                
            case HTTPSCommonErrorCodes.InternalServerError.rawValue:
                
                
                    failure(getCommonErrorDetails(response: response))
                
            default:
                
                let responseModel:APIResponseModel!
                
                if let dict: [String: Any] = JSON(response.result.value!).dictionaryObject {
                    
                    responseModel = APIResponseModel.init(statusCode: (response.response?.statusCode)!, dataResponse: dict)
                    
                    completion(responseModel)
                }
                else {
                    
                    responseModel = APIResponseModel.init(statusCode: (response.response?.statusCode)!, dataResponse: [:])
                    
                    completion(responseModel)
                }
            }
        }
        else {
            
            if let error : NSError = response.result.error as NSError? {
                
                failure(error)
            }
        }
    }
    
   class func getCommonErrorDetails(response:DataResponse<Any>) -> Error {
        
        var error:NSError = NSError(domain: "", code: -60, userInfo: [:])
        
        if let dict: [String: Any] = JSON(response.result.value!).dictionaryObject {
            
            if let message = dict["message"] as? String {
                
                let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString(message, comment: message),
                                                    NSLocalizedFailureReasonErrorKey: NSLocalizedString(message, comment: message)]
                error = NSError(domain: "", code: -60, userInfo: userInfo as! [String : Any])
                
            }
        }
        
        return error
    }
    
    /// Request PUT
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
    class func requestPUT(serviceName: String,
                          params: [String : Any]?,
                          success: @escaping(APIResponseModel) -> Void,
                          failure: @escaping(Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            failure(networkError)
            return
        }
        
        print("\n\n\(serviceName.capitalized) : \(String(describing: params))\n\n")
        
        // URL
        let strURL = API.BASE_URL + serviceName
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        // Request
        manager.request(strURL,
                        method:.put,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                            
                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")
                            
                            self.ParseAPIResponse(response: response,
                                                  methodName: serviceName,
                                                  completion: { (responseModel) in
                                                    
                                                    success(responseModel)
                                                    
                            },failure: { (error) in
                                
                                failure(error)
                            })
                        })
    }
    
    
    /// Request Patch
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
    class func requestPatch(serviceName: String,
                          params: [String : Any]?,
                          success: @escaping(APIResponseModel) -> Void,
                          failure: @escaping(Error) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            failure(networkError)
            return
        }
        print("\n\n\(serviceName.capitalized) : \(String(describing: params))\n\n")
        
        // URL
        let strURL = API.BASE_URL + serviceName
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        // Request
        manager.request(strURL,
                        method:.patch,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                            
                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")
                            
                            self.ParseAPIResponse(response: response,
                                                  methodName: serviceName,
                                                  completion: { (responseModel) in
                                                    
                                                    success(responseModel)
                                                    
                            },failure: { (error) in
                                
                                failure(error)
                            })
                        })
    }
    
    
    
    ///Get method
    class func requestGETURL(method: String,
                             success:@escaping(APIResponseModel) -> Void,
                             failure:@escaping (Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            Helper.hidePI()
            failure(networkError)
            return
        }
        
        var strURL = ""
        if method == API.LiveChat.Licence_url {
            strURL = method
        }else{
            strURL = API.BASE_URL + method
        }
        
        
        Alamofire.request(strURL, method:.get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: getAOTHHeader()).responseJSON(completionHandler: {
                            response in
                            
                            self.ParseAPIResponse(response: response,
                                                  methodName: method,
                                                  completion: { (responseModel) in
                                                    
                                                    success(responseModel)
                                                    
                            },failure: { (error) in
                                failure(error)
                            })
                            
                          })
    }
    
    /// Request Post
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
    class func requestPOST(serviceName: String,
                           params: [String : Any]?,
                           success: @escaping(APIResponseModel) -> Void,
                           failure: @escaping(Error) -> Void) {
        if NetworkReachabilityManager()?.isReachable == false {
            
            failure(networkError)
            return
        }

        // URL
        let strURL = API.BASE_URL + serviceName
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        // Request
        manager.request(strURL,
                        method:.post,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                            
                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")
                            
                            self.ParseAPIResponse(response: response,
                                                  methodName: serviceName,
                                                  completion: { (responseModel) in
                                                    
                                                    success(responseModel)
                                                    
                            },failure: { (error) in
                                
                                failure(error)
                            })
                        })
    }
    
   
    
    /// Request Post with Url
    ///
    /// - Parameters:
    ///   - urlString: url
    ///   - success: success Handler
    ///   - failure: Failure Handler
    class func requestPOST(urlString : String!,
                           success:@escaping (JSON) -> Void,
                           failure:@escaping (NSError) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            Helper.hidePI()
            failure(networkError)
            return
        }
        
        Alamofire.request(urlString!).responseJSON(completionHandler: { response in
            
            print("\n\nSearch URL : %@\n",urlString)
            print(response)
            print("\n\n")
            
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                success(resJson)
            }
            else {
                let error : NSError = response.result.error! as NSError
                failure(error)
            }
        })
    }
    
    
    
    
    /// Request Delete with Url
    ///
    /// - Parameters:
    ///   - urlString: url
    ///   - success: success Handler
    ///   - failure: Failure Handler
    class func requestDELETEURL(method: String,
                                params: [String : Any]?,
                                success:@escaping(APIResponseModel) -> Void,
                                failure:@escaping (Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            Helper.hidePI()
            failure(networkError)
            return
        }
        let strURL = API.BASE_URL + method
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        
        
        manager.request(strURL,
                        method:.delete,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                            
                            print("\n\n\(method.capitalized) Response : \(response)\n\n")
                            
                            
                            self.ParseAPIResponse(response: response,
                                                  methodName: method,
                                                  completion: { (responseModel) in
                                                    
                                                    success(responseModel)
                                                    
                            },failure: { (error) in
                                
                                failure(error)
                            })
                        })
        
    }
    
    /// Request
    ///
    /// - Parameters:
    ///   - urlString: URL String to be Requested with
    ///   - success: Successfull block with Response in the format
    ///   - failure: Failure block
    class func request(urlString : String!,
                       success: @escaping([String: Any]) -> Void,
                       failure: @escaping(Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hidePI()
            failure(networkError)
            return
        }
        
        Alamofire.request(urlString!).responseJSON(completionHandler: { response in
            
            print("\n\nRequest URL : %@\n",urlString)
            print(response)
            print("\n\n")
            
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                if let dict: [String: Any] = resJson.dictionaryObject {
                    success(dict)
                }
                else {
                    success([:])
                }
            }
            else {
                Helper.hidePI()
                if let error : NSError = response.result.error as NSError? {
                    failure(error)
                }
            }
        })
    }
    
    /// Request Patch
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
    class func requestPatchForLocation(serviceName: String,
                            params: [String : Any]?,
                            success: @escaping([String: Any]) -> Void,
                            failure: @escaping(Error) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            failure(networkError)
            return
        }
        print("\n\n\(serviceName.capitalized) : \(String(describing: params))\n\n")
        
        // URL
        let strURL = API.BASE_URL + serviceName
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        // Request
        manager.request(strURL,
                        method:.patch,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")
                            if response.result.isSuccess {
                                let resJson = JSON(response.result.value!)
                                if let dict: [String: Any] = resJson.dictionaryObject {
                                    success(dict)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                Helper.hidePI()
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                        })
    }
    
    
    /// Network Error
    static var networkError: NSError {
        
        let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString("No network available.", comment: "No network available."),
                                            NSLocalizedFailureReasonErrorKey: NSLocalizedString("Failed to connect to server.", comment: "Failed to connect to server.")]
        return NSError(domain: "", code: -57, userInfo: userInfo as! [String : Any])
    }
    
    
    /// Athenticate Header
    ///
    /// - Returns: Dict of Athentication
    class func getAOTHHeader() -> [String: String] {
        
        var sessionToken = String()
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            sessionToken = Utility.sessionCheck
        }else{
           sessionToken = String(format: "%@", Utility.sessionToken)
        }
 
        return ["authorization": (sessionToken),"lan":"en"]
    }
}
*/
class NetworkHelper: NSObject {
    
    class func ParseAPIResponse(response:DataResponse<Any, AFError>,
                          methodName:String,
                          completion: @escaping(APIResponseModel) -> Void,
                          failure:@escaping (Error) -> Void) {
        
        switch response.result {
        case .success(let value):
            print("\n\nHTTPS Status Code = \((response.response?.statusCode)!)")
            print("\n\n\(methodName) Response : \(response.description)\n\n")
            
            switch (response.response?.statusCode)! {
                
            case HTTPSCommonErrorCodes.BadRequest.rawValue:
                
                failure(getCommonErrorDetails(response: response))
                
            case HTTPSCommonErrorCodes.InternalServerError.rawValue:
                
                
                failure(getCommonErrorDetails(response: response))
                
            default:
                
                let responseModel:APIResponseModel!
                
//                if let dict: [String: Any] = JSON(response.result).dictionaryObject {
//
//                    responseModel = APIResponseModel.init(statusCode: (response.response?.statusCode)!, dataResponse: dict)
//
//                    completion(responseModel)
//                }
//                else {
//
//                    responseModel = APIResponseModel.init(statusCode: (response.response?.statusCode)!, dataResponse: [:])
//
//                    completion(responseModel)
//                }
                //vani 08/10/2020
                //session token was not getting refreshed
                do {
                    let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any]
                    if let sessionToken = json["data"] as? String {
                        print("token:",sessionToken)
                        KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                        KeychainWrapper(serviceName: API.groupIdentifier).set( sessionToken, forKey: "sessionToken")
                    }
                        responseModel = APIResponseModel.init(statusCode: (response.response?.statusCode)!, dataResponse: json)
    
                        completion(responseModel)
                    
                } catch let error as NSError {
                    print(error)
                    responseModel = APIResponseModel.init(statusCode: (response.response?.statusCode)!, dataResponse: [:])
                    
                    completion(responseModel)
                }
            }
            
            print("responseObject: \(value)")
        case .failure(let responseError):
            print("responseError: \(responseError)")
            if let error : NSError = response.error as NSError? {
                
                failure(error)
            }
        }
    }

    class func getCommonErrorDetails(response:DataResponse<Any,AFError>) -> Error {
        
        var error:NSError = NSError(domain: "", code: -60, userInfo: [:])
        
    if let dict: [String: Any] = JSON(response.result).dictionaryObject {
            
            if let message = dict["message"] as? String {
                
                let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString(message, comment: message),
                                                    NSLocalizedFailureReasonErrorKey: NSLocalizedString(message, comment: message)]
                error = NSError(domain: "", code: -60, userInfo: userInfo as! [String : Any])
                
            }
        }
        
        return error
    }
    /// Request PUT
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
    class func requestPUT(serviceName: String,
                          params: [String : Any]?,
                          success: @escaping(APIResponseModel) -> Void,
                          failure: @escaping(Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            failure(networkError)
            return
        }
        
        print("\n\n\(serviceName.capitalized) : \(String(describing: params))\n\n")
        
        // URL
        let strURL = API.BASE_URL + serviceName
        
        let manager = AF.session //Alamofire.SessionManager.default
        manager.configuration.timeoutIntervalForRequest = 30
        // Request
        AF.request(strURL,
                        method:.put,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                            
                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")
                            
                            self.ParseAPIResponse(response: response,
                                                  methodName: serviceName,
                                                  completion: { (responseModel) in
                                                    
                                                    success(responseModel)
                                                    
                            },failure: { (error) in
                                
                                failure(error)
                            })
                        })
    }
    /// Request Patch
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
    class func requestPatch(serviceName: String,
                          params: [String : Any]?,
                          success: @escaping(APIResponseModel) -> Void,
                          failure: @escaping(Error) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            failure(networkError)
            return
        }
        print("\n\n\(serviceName.capitalized) : \(String(describing: params))\n\n")
        
        // URL
        let strURL = API.BASE_URL + serviceName
        
        let manager = AF.session //Alamofire.SessionManager.default
        manager.configuration.timeoutIntervalForRequest = 30
        // Request
        AF.request(strURL,
                        method:.patch,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                            
                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")
                            
                            self.ParseAPIResponse(response: response,
                                                  methodName: serviceName,
                                                  completion: { (responseModel) in
                                                    
                                                    success(responseModel)
                                                    
                            },failure: { (error) in
                                
                                failure(error)
                            })
                        })
    }
    
    ///Get method
    class func requestGETURL(method: String,
                             success:@escaping(APIResponseModel) -> Void,
                             failure:@escaping (Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            Helper.hidePI()
            failure(networkError)
            return
        }
        
        var strURL = ""
        if method == API.LiveChat.Licence_url {
            strURL = method
        }else{
            strURL = API.BASE_URL + method
        }
        
        
        AF.request(strURL, method:.get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: getAOTHHeader()).responseJSON(completionHandler: {
                            response in
                            
                            self.ParseAPIResponse(response: response,
                                                  methodName: method,
                                                  completion: { (responseModel) in
                                                    
                                                    success(responseModel)
                                                    
                            },failure: { (error) in
                                failure(error)
                            })
                            
                          })
    }
    /// Request Post
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
   class func requestPOST(serviceName: String,
                          params: [String : Any]?,
                          success: @escaping(APIResponseModel) -> Void,
                          failure: @escaping(Error) -> Void) {
       if NetworkReachabilityManager()?.isReachable == false {
           
           failure(networkError)
           return
       }

       // URL
       let strURL = API.BASE_URL + serviceName
       
    let manager = AF.session //Alamofire.SessionManager.default
       manager.configuration.timeoutIntervalForRequest = 30
       // Request
       AF.request(strURL,
                       method:.post,
                       parameters: params,
                       encoding: JSONEncoding.default,
                       headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                           
                           print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")
                           
                           self.ParseAPIResponse(response: response,
                                                 methodName: serviceName,
                                                 completion: { (responseModel) in
                                                   
                                                   success(responseModel)
                                                   
                           },failure: { (error) in
                               
                               failure(error)
                           })
                       })
   }
    
    /// Request Post with Url
    ///
    /// - Parameters:
    ///   - urlString: url
    ///   - success: success Handler
    ///   - failure: Failure Handler
    class func requestPOST(urlString : String!,
                           success:@escaping (JSON) -> Void,
                           failure:@escaping (NSError) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            Helper.hidePI()
            failure(networkError)
            return
        }
        
        AF.request(urlString!).responseJSON(completionHandler: { response in
            
            print("\n\nSearch URL : %@\n",urlString)
            print(response)
            print("\n\n")
            print(response.description)
            if (response.data != nil) {
                let resJson = JSON(response.data)
                success(resJson)
            }
            else {
                let error : NSError = response.error! as NSError //response.result.error! as NSError
                failure(error)
            }
        })
    }
    
    
    /// Request Delete with Url
    ///
    /// - Parameters:
    ///   - urlString: url
    ///   - success: success Handler
    ///   - failure: Failure Handler
    class func requestDELETEURL(method: String,
                                params: [String : Any]?,
                                success:@escaping(APIResponseModel) -> Void,
                                failure:@escaping (Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            Helper.hidePI()
            failure(networkError)
            return
        }
        let strURL = API.BASE_URL + method
        
        let manager = AF.session //Alamofire.SessionManager.default
        manager.configuration.timeoutIntervalForRequest = 30
        
        
        AF.request(strURL,
                        method:.delete,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                            
                            print("\n\n\(method.capitalized) Response : \(response)\n\n")
                            
                            
                            self.ParseAPIResponse(response: response,
                                                  methodName: method,
                                                  completion: { (responseModel) in
                                                    
                                                    success(responseModel)
                                                    
                            },failure: { (error) in
                                
                                failure(error)
                            })
                        })
        
    }
    /// Request
    ///
    /// - Parameters:
    ///   - urlString: URL String to be Requested with
    ///   - success: Successfull block with Response in the format
    ///   - failure: Failure block
    class func request(urlString : String!,
                       success: @escaping([String: Any]) -> Void,
                       failure: @escaping(Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            Helper.hidePI()
            failure(networkError)
            return
        }
        
        AF.request(urlString!).responseJSON(completionHandler: { response in
            
            print("\n\nRequest URL : %@\n",urlString)
            print(response)
            print("\n\n")
            
            if (response.data != nil) {
                let resJson = JSON(response.data!)
                if let dict: [String: Any] = resJson.dictionaryObject {
                    success(dict)
                }
                else {
                    success([:])
                }
            }
            else {
                Helper.hidePI()
                if let error : NSError = response.error as NSError? {//response.result.error as NSError? {
                    failure(error)
                }
            }
        })
    }
    /// Request Patch
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
    class func requestPatchForLocation(serviceName: String,
                            params: [String : Any]?,
                            success: @escaping([String: Any]) -> Void,
                            failure: @escaping(Error) -> Void) {
        
        
        if NetworkReachabilityManager()?.isReachable == false {
            
            failure(networkError)
            return
        }
        print("\n\n\(serviceName.capitalized) : \(String(describing: params))\n\n")
        
        // URL
        let strURL = API.BASE_URL + serviceName
        
        let manager = AF.session //Alamofire.SessionManager.default
        manager.configuration.timeoutIntervalForRequest = 30
        // Request
        AF.request(strURL,
                        method:.patch,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")
                            
                            switch response.result {
                            case .success(let value):
                                let resJson = JSON(response.data as Any)
                                if let dict: [String: Any] = resJson.dictionaryObject {
                                    success(dict)
                                }
                                else {
                                    success([:])
                                }
                                
                                print("responseObject: \(value)")
                            case .failure(let responseError):
                                print("responseError: \(responseError)")
                                Helper.hidePI()
                                if let error : NSError = response.error as NSError? {
                                    failure(error)
                                }
                            }
                        })
    }
    /// Network Error
    static var networkError: NSError {
        
        let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString("No network available.", comment: "No network available."),
                                            NSLocalizedFailureReasonErrorKey: NSLocalizedString("Failed to connect to server.", comment: "Failed to connect to server.")]
        return NSError(domain: "", code: -57, userInfo: userInfo as! [String : Any])
    }
    
    
    /// Athenticate Header
    ///
    /// - Returns: Dict of Athentication
    class func getAOTHHeader() -> HTTPHeaders {
        
        var sessionToken = String()
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            sessionToken = Utility.sessionCheck
        }else{
            sessionToken = String(format: "%@", Utility.sessionToken)
        }
        let dict: HTTPHeaders = ["authorization": (sessionToken),"lan":"en"]
        return dict
    }
}
