 //
//  AppDelegate.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 12/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import GoogleMaps
import UserNotifications
import AWSS3
import AWSCore
import AVFoundation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import CocoaLumberjack

import Fabric
import Crashlytics
import FBSDKCoreKit
import SwiftKeychainWrapper
enum PushType: String {
    
    case bookingPush = "1"
    case chatPush = "2"
    case zendeskPush = "5"
    case walletPush = "6"
    case ifAnyPush = "10"
}

enum PushAction: String {
    
    case acceptDeclinefromEmail = "18"
    case offlineAdmin = "21"
    case bannedAdminOrLogout = "22"
    case newBooking = "1"
    case cancelledBooking = "12"
    case expiredBooking = "5"
    case accountAccepted = "25"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var callProviderDelegate: CallProviderDelegate?
    let notificationDelegate = MQTTNotificationDelegate()
    
    var bookingDict = [String : Any]()
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    var player: AVAudioPlayer?
    var countDown:Int = 0
    var isNewBooking = false
    
    var isDateChanged = false
    
    var initalNotification = false
    
    var viewController = UIApplication.shared.keyWindow?.rootViewController
    var window: UIWindow?
    
    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // remove all keychain data if the app is installed for the first time or uninstalled and installed
        if !UserDefaults.standard.bool(forKey: "isFirstRun") {
            KeychainWrapper.standard.removeAllKeys()
            UserDefaults.standard.set(true, forKey: "isFirstRun")
        }
        Helper.getIPAddress()
        isDateChanged = true
        initalNotification = true
        GMSServices.provideAPIKey(SERVER_CONSTANTS.googleMapsApiKey)
        
        let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
        
        amazonWrapper.setConfigurationWithRegion(AWSRegionType.USEast1, accessKey: AmazonAccessKey, secretKey: AmazonSecretKey)
        
        
//        let locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
//        locationtracker.startLocationTracking()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(
                options: [.alert,.sound],
                completionHandler: { (granted,error) in
                    if !granted{
                    UserDefaults.standard.set(true, forKey: "NotificationDisabled")
                    UserDefaults.standard.synchronize()
//                        Helper.alertVC(errMSG: "In order to use this application, turn on notification permissions.")
                    }
            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert,.sound], categories: nil)
            application.registerUserNotificationSettings(settings)
          
        
        }
        
        DDLog.add(DDTTYLogger.sharedInstance!)
        DDLog.add(DDASLLogger.sharedInstance)
        
        // [END register_for_notifications]
        UIApplication.shared.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        

        //        OneSkyOTAPlugin.provideAPIKey(OneSkyKeys.APIKey, apiSecret: nil, projectID: OneSkyKeys.ProjectID)
        //        OneSkyOTAPlugin.checkForUpdate()

        
        Fabric.with([Crashlytics.self])
        
        if let remoteNotif = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: Any] {
            if let notification = remoteNotif["aps"] as? [AnyHashable : Any] {
                if (notification["data"] != nil) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                        self.handingThePushData(pushDict: notification as! [String : Any] ,backGround:true)
                    })
                }
            }
        }
        return true
    }

    
    /// registering the remote notification
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // With swizzling disabled you must set the APNs token here
        
        let token = String(format: "%@", deviceToken as CVarArg)
        print("registered Toke: ",token)
        // With swizzling disabled you must set the APNs token here.
        
        Messaging.messaging().apnsToken = deviceToken
    }

    /// usernotification delegate method, calls before the notification appears
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }

    /// this will be called when the category has been clicked
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // Called to let your app know which action was selected by the user for a given notification.
        let userInfo = response.notification.request.content.userInfo as!  [NSString:Any]
        print("\(String(describing: userInfo))")
        
        if (userInfo["data"] != nil) {
            if initalNotification{
                initalNotification = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                    self.handingThePushData(pushDict: userInfo as [String : Any],backGround:true)
                })
            }else{
                self.handingThePushData(pushDict: userInfo as [String : Any],backGround:true)
            }
        }
        
        completionHandler()
    }
    
    
    func addNotification(content:UNNotificationContent,trigger:UNNotificationTrigger?, indentifier:String){
        let request = UNNotificationRequest(identifier: indentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: {
            (errorObject) in
            if let error = errorObject{
                print("Error \(error.localizedDescription) in notification \(indentifier)")
            }
        })
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Push notification received: \(userInfo)")
            initalNotification = false
        if let imageDataDict = userInfo as? [String : Any]{
            if (imageDataDict["data"] != nil) {
                
                let state: UIApplication.State = UIApplication.shared.applicationState
                if state != .background {
                    self.handingThePushData(pushDict: imageDataDict ,backGround:false)
                }
                
            }else{
                let locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
                locationtracker.startLocationTracking()
                locationtracker.updateLocationToServer()
            }
        }
    } 
    
    
    func handingThePushData(pushDict:[String:Any] , backGround:Bool)  {
        if  let pushType = pushDict["pushType"] as? String{
           if let typeOfPush : PushType = PushType(rawValue: pushType) {
            switch typeOfPush {
            case .bookingPush:
                if  let actionType = pushDict["action"] as? String{
                    let pushAction : PushAction = PushAction(rawValue: actionType)!

                    switch pushAction {
                    case .cancelledBooking: // cancelled booking
                        let imageDataDict =  self.convertToDictionary(text: pushDict["data"] as! String) // updates the booking status
                        let bid = "Bid:" + String(describing:imageDataDict!["bookingId"]!)
                        if  let msg = imageDataDict?["statusMsg"] as? String{
                            if !backGround{
                               // Helper.alertVC(errMSG: bid + " , " + msg)
                            }
                        }
                        
                        NotificationCenter.default.post(name: Notification.Name("cancelledBooking"), object: nil, userInfo: imageDataDict)
                        if let bookingType = imageDataDict!["bookingType"] as? NSNumber{
                            if bookingType == 2{ // schedule booking
                                if let startTime = imageDataDict!["bookingRequestedFor"] as? NSNumber{
                                    if let endTime = imageDataDict!["bookingEndtime"] as? NSNumber{
                                        ReminderModel().removeReminder(Int64(startTime), endDate: Int64(endTime))
                                    }
                                }
                            }
                        }
                        break
                    case .newBooking: // got new booking
                        let imageDataDict =  self.convertToDictionary(text: pushDict["data"] as! String)
                        MQTTResponseHandler().acknowledgingTheBookingToServer(bookingDict: imageDataDict!, completionHandler: { (success) in
                            if success{
                                let notificationName = Notification.Name("gotNewBooking")
                                NotificationCenter.default.post(name: notificationName, object: nil)
                            }
                        })
                        break
                     
                    case .expiredBooking:
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BookingExpired"), object: nil)
                        break
                    case .bannedAdminOrLogout: // logout & banned from admin
                        if let bannedMsg = pushDict["msg"] as? String{
                            if !backGround{
                                //Helper.alertVC(errMSG: bannedMsg)
                            }
                        }
                        Session.expired()
                        break
                    case .offlineAdmin:   // offline from admin
                        Events().stopLocationUpdates()
                        NotificationCenter.default.post(name: Notification.Name("makeOfflineFromAdmin"), object: nil, userInfo: nil)
                        break
                    case .accountAccepted:
                        NotificationCenter.default.post(name: Notification.Name("accountAccepted"), object: nil, userInfo: nil)
                        break
                    case .acceptDeclinefromEmail:  // accepting the booking through email
                        let imageDataDict =  self.convertToDictionary(text: pushDict["data"] as! String) // updates the booking status
                        let bid = "Bid:" + String(describing:imageDataDict!["bookingId"]!)
                        if  let msg = imageDataDict?["statusMsg"] as? String{
                            if !backGround{
                                Helper.alertVC(errMSG: bid + " , " + msg)
                            }
                        }
                        
                        NotificationCenter.default.post(name: Notification.Name("cancelledBooking"), object: nil, userInfo: imageDataDict)

                        if let bookingType = imageDataDict!["bookingType"] as? NSNumber{
                            if bookingType == 2{
                                if let status = imageDataDict!["status"] as? NSNumber{
                                    if status == 3{
                                        let reminderData = ReminderModel()
                                        if let requestedFor = imageDataDict!["bookingRequestedFor"] as? NSNumber{
                                            reminderData.startDate = Int64(requestedFor)
                                        }
                                        
                                        if let endTime = imageDataDict!["bookingEndtime"] as? NSNumber{
                                            reminderData.enddate = Int64(endTime)
                                        }
                                        
                                        if let lat = imageDataDict!["latitude"] as? NSNumber{
                                            reminderData.latit = Double(lat)
                                        }
                                        
                                        if let long = imageDataDict!["longitude"] as? NSNumber{
                                            reminderData.logit = Double(long)
                                        }
                                        
                                        if let bookingID = imageDataDict!["bookingId"] as? NSNumber{
                                            reminderData.bookingID = Int64(bookingID)
                                        }
                                        
                                        if let address = imageDataDict!["addLine1"] as? String{
                                            reminderData.address = address
                                        }
                                        ReminderModel().eventReminder(data: reminderData, completion: { (reminderID) in
                                            
                                        })
                                    }
                                }
                            }
                        }
                        break
                    }
                }
                break
            case .chatPush: // chat push
                if let chatDict =  self.convertToDictionary(text: pushDict["data"] as! String){
                    self.window  = UIApplication.shared.keyWindow
                    if  self.window!.visibleViewController()! is ChatVC{
                        NotificationCenter.default.post(name: Notification.Name("gotNewMessage"), object: nil, userInfo: chatDict)
                    }else{
                        if backGround{
                            MQTTChatResponseHandler().showChatController(booking: chatDict)
                        }
                    }
                }
                break
            case .zendeskPush:  // Zendesk ticket Push
                if let ticketUpdate = pushDict["msg"] as? String{
                    Helper.alertVC(errMSG: ticketUpdate)
                }
                break
            case .walletPush:  // wallet soft and hard limit Push
                if let pushMsg = pushDict["msg"] as? String{
                    Helper.alertVC(errMSG: pushMsg)
                }
                break
            default:
                if let pushMsg = pushDict["msg"] as? String{
                    Helper.alertVC(errMSG: pushMsg)
                }
                break
            }
           }else{
//            if let pushMsg = pushDict["msg"] as? String{
//                Helper.alertVC(errMSG: pushMsg)
//            }
            }
        }
    }
    
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 1
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIDevice.current.isBatteryMonitoringEnabled = true
        UIApplication.shared.statusBarStyle = .default
        initalNotification = false
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
        self.checkDateChanged()
        
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            
        }else{
            getConfigData()
        }
        AppEvents.activateApp()
        
       updateTheFields()
    }
    
    func applicationSignificantTimeChange(_ application: UIApplication) {
        
        DDLogDebug("Device time has been changed...")
        isDateChanged = true
    }
    
    func checkDateChanged() {
        
        if isDateChanged {
            
            GetServerTimeManager.sharedInstance.getServerTime()
           
        }
    }
    
    func updateTheFields() {
        //**** *************************************** For  User A   ************************************************////////////
//        UserDefaults.standard.set("j89999887778999jjk999", forKey: AppConstants.UserDefaults.userID)
//        UserDefaults.standard.set("User A", forKey: AppConstants.UserDefaults.User)
        //  ***************************************************************************************************//////////
        //**** *************************************** For  User B   ************************************************////////////
                         UserDefaults.standard.set("9384524085ngef09t79204593", forKey: AppConstants.UserDefaults.userID)
                         UserDefaults.standard.set("User B", forKey: AppConstants.UserDefaults.User)
        //  ***************************************************************************************************//////////
    }
    
    /// ShowIncomingCallling screen
    ///
    /// - Parameter callData: CallData MQTT data [String:Any]
    func showIncomingCallingScreen(callData:[String:Any]) {
        let window = UIApplication.shared.keyWindow!
        window.endEditing(true)
        
        if UIApplication.shared.applicationState == .active{
            
            if AppConstants.CallTypes.audioCall == callData["callType"] as! String {
                //show incoming audioview
                let incomingAudioView = IncomingAudiocallView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
                incomingAudioView.tag = 16
                incomingAudioView.playSound("ringtone", loop: 10)  //add Ringtone here
                incomingAudioView.userName.text = self.getNameFormDatabase(num: (callData["callerIdentifier"] as? String)!) //show other user Name here
                incomingAudioView.setCallId(messageData: callData)
                incomingAudioView.userImageView.image = #imageLiteral(resourceName: "profile_defaultimage")  //show otheruser profile Pic here
                window.addSubview(incomingAudioView);
            }
            else{
                
                //show incoming videoView
                let incomingVideoview = IncomingVideocallView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
                incomingVideoview.tag = 17
                incomingVideoview.playSound("ringtone", loop: 10) //add Ringtone here
                incomingVideoview.userName.text = self.getNameFormDatabase(num: (callData["callerIdentifier"] as? String)!)  //show other user Name here
                incomingVideoview.addCameraView()
                incomingVideoview.setMessageData(messageData: callData)
                window.addSubview(incomingVideoview)
            }
        }
    }
    
    
    /// Get name from your DataBase
    ///
    /// - Parameter num: num -> Other user Number
    /// - Returns: Name
    func getNameFormDatabase(num:String) -> String{
        
        return "Jayesh"
    }
    
    
    @objc func networkStatusChanged(_ notification: Notification) {
        //  let userInfo = (notification as NSNotification).userInfo
        
        let ud = UserDefaults.standard
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            ud.set(false, forKey: "hasInternet")
            ud.synchronize()
            let mqttModel = MQTT.sharedInstance
            mqttModel.isConnected = false
            ReachabilityView.instance.show()
        case .online(.wwan):
            ud.set(true, forKey: "hasInternet")
            ud.synchronize()
            ReachabilityView.instance.hide()
            if Utility.userId != "user_id"{
                self.createConnection()
            }
            
        case .online(.wiFi):
            ud.set(true, forKey: "hasInternet")
            ReachabilityView.instance.hide()
            ud.synchronize()
            if Utility.userId != "user_id"{
                self.createConnection()
            }
        }
    }
    
    func createConnection(){
        let mqttModel = MQTT.sharedInstance
        if !mqttModel.isConnected {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                mqttModel.createConnection()
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {

        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    // MARK: - Core Data stack
    
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "DayRunnerDriver")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
        else {
            // Fallback on earlier versions
            saveContextForBelowiOS10()
        }
    }
    
    
    
    // MARK: - Core Data stack For XCode7 and iOS Version lesser than 10.0
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.demo.imageList.DBDemo_Swift_Xcode7" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "DayRunnerDriver", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Trustpals.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support for iOS Version Lesser than 10
    
    func saveContextForBelowiOS10 () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    func getConfigData(){
        let configModel = ConfigModelClass()
        configModel.makeServiceCallForConfigData()
    }
}

extension AppDelegate : MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        if let refreshedToken = Messaging.messaging().fcmToken {
            print("FCM token: \(refreshedToken)")
            let defaults = UserDefaults.standard
            defaults.set(refreshedToken , forKey: USER_INFO.PUSH_TOKEN)
            
            Messaging.messaging().shouldEstablishDirectChannel = true
            
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
    }
}

