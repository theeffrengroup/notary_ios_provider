//
//  Date.swift
//  Notary
//
//  Created by 3Embed on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension Date {
    
    static func dateWithDifferentTimeInterval() -> Date  {
        
        return Date().addingTimeInterval(Utility.diffTimeInterval)
    }
}
