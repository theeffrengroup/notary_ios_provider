//
//  UIViewController+Session.swift
//  DayRunner
//
//  Created by 3Embed on 29/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import FirebaseMessaging
import SwiftKeychainWrapper
extension UIViewController {
    
    /// Handle Session Expired
    func sessionExpired() {
        Session.expired()
    }
}

class Session {
    
    /// Handle Session Expired
    class func expired() {

        print("\nSession Token: \(Utility.sessionToken)\n")
        
//        guard Utility.sessionToken != "session_token" else {
//            return
//        }
        
        
        removeUserDefaultData()
        

        
        //  if Utility.sessionToken == userin
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let splash: SplashViewController? = storyboard.instantiateViewController(withIdentifier: storyBoardIDs.splashVC!) as? SplashViewController
        
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = splash
        let mqttModel = MQTT.sharedInstance
        mqttModel.isConnected = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        if (UserDefaults.standard.array(forKey:"topicsArray") != nil){
            for topic in UserDefaults.standard.array(forKey:"topicsArray") as! [String] {
                Messaging.messaging().unsubscribe(fromTopic:topic)
            }
        }
        
        let url:String = "/topics/" + Utility.fcmTopic
        Messaging.messaging().unsubscribe(fromTopic:url)
    }
    
  class  func removeUserDefaultData() {
        let ud = UserDefaults.standard
    KeychainWrapper.standard.removeObject(forKey: USER_INFO.SESSION_TOKEN)
        ud.removeObject(forKey: "NotificationDisabled")
        ud.removeObject(forKey: USER_INFO.LAST4)
         ud.removeObject(forKey: USER_INFO.CARDBRAND)
        ud.removeObject(forKey: USER_INFO.CARDID)
        
        ud.removeObject(forKey: USER_INFO.hardLimit)
         ud.removeObject(forKey: USER_INFO.softLimit)
         ud.removeObject(forKey: USER_INFO.walletAmount)
         ud.removeObject(forKey: USER_INFO.USERIMAGE)
        ud.removeObject(forKey: USER_INFO.USER_NAME)
         ud.synchronize()
    }
}
