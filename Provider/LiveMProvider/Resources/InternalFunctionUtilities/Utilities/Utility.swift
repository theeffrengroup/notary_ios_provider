//
//  Utility.swift
//  Dayrunner
//
//  Created by Rahul Sharma on 28/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
class Utility: NSObject {
    
    /*****************************************************/
    //MARK: -            (Getter Methods)
    /*****************************************************/
    
    @objc static var deviceId: String {
        if let ID: String = UIDevice.current.identifierForVendor?.uuidString {
            return ID
        }
        else {
            return "iPhone_Simulator_ID"
        }
    }
    
    @objc static var batteryLevel: Float {
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        UserDefaults(suiteName: API.groupIdentifier)!.set( UIDevice.current.batteryLevel  * 100, forKey: "batteryPer")
        return UIDevice.current.batteryLevel  * 100
    }
    
    
    static var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    
    static var appName: String {
        return (Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String)!
    }
    
    @objc static var appVersion: String {
        
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        UserDefaults(suiteName: API.groupIdentifier)!.set( version, forKey: "appVersion")
        return version//(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!
        
    }
    
    static var deviceName: String {
        return UIDevice.current.systemName as String
    }
    
    static var deviceVersion: String {
        return UIDevice.current.systemVersion as String
    }
    
    static var CardBrand : String {
        if let brand = UserDefaults.standard.string(forKey:USER_INFO.CARDBRAND){
            return brand
        }
        return ""
    }
    
    static var CardID : String {
        if let cardid = UserDefaults.standard.string(forKey:USER_INFO.CARDID){
            return cardid
        }
        return ""
    }
    
    static var cardLast4 : String {
        if let last4 = UserDefaults.standard.string(forKey: USER_INFO.LAST4 ){
            return last4
        }
        return "Select Card"
    }
    
    static var stripeKey : String {
        if let currency = KeychainWrapper.standard.string(forKey: USER_INFO.STRIPE_KEY){
            return currency
        }
        return  USER_INFO.STRIPE_KEY
    }
    
    // Getwallet enabled
    static var walletEnabled: Int {
        if let enabled = UserDefaults.standard.value(forKey: USER_INFO.walletEnabled) as? Int {
            return enabled
        }
        return 0
    }
    
    // Get soft limit reached
    static var softLimitReached: Int64 {
        if let softReached = UserDefaults.standard.value(forKey: USER_INFO.softLimitReached) as? Int64 {
            return softReached
        }
        return 0
    }
    
    // Gethard limit reached
    static var hardLimitReached: Int64 {
        if let hardReached = UserDefaults.standard.value(forKey: USER_INFO.hardLimitReached) as? Int64 {
            return hardReached
        }
        return 0
    }
    
    // Get hard limit
    static var hardLimit: Int64 {
        if let hardReached = UserDefaults.standard.value(forKey: USER_INFO.hardLimit) as? Int64 {
            return hardReached
        }
        return 0
    }
    
    // Get soft limit
    static var softLimit: Int64 {
        if let hardReached = UserDefaults.standard.value(forKey: USER_INFO.softLimit) as? Int64 {
            return hardReached
        }
        return 0
    }
    
    // Get wallet ammount
    static var walletAmt: Float {
        if let walletAmount = UserDefaults.standard.value(forKey: USER_INFO.walletAmount) as? Float {
            return walletAmount
        }
        return 0.00
    }
    
    // Get Session Token
    static var sessionToken: String {
        if let token: String = KeychainWrapper.standard.string(forKey: USER_INFO.SESSION_TOKEN) as! String? {
            return token
        }
        return USER_INFO.SESSION_TOKEN
    }
    
    // Get Driver Channel
    static var driverChannel: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.DRIVERCHANNEL) as! String? {
            return token
        }
        return USER_INFO.DRIVERCHANNEL
    }

    
    // Get presence channel
    static var presenceChannel: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.PRESENCECHANNEL) as! String? {
            return token
        }
        return USER_INFO.PRESENCECHANNEL
    }


    // Get publish key
    static var publishKey: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.PUBLISHKEY) as! String? {
            return token
        }
        return USER_INFO.PUBLISHKEY
    }

    // Get Subsribe key
    static var subscribeKey: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.SUBSCRIBEKEY) as! String? {
            return token
        }
        return USER_INFO.SUBSCRIBEKEY
    }
    
    // Get Server channel
    static var serverChannel: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.SERVERCHANNEL) as! String? {
            return token
        }
        return USER_INFO.SERVERCHANNEL
    }
    
    // Get VehicleType ID
    static var vehicleTypeID: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.VEHTYPEID) as! String? {
            return token
        }
        return USER_INFO.VEHTYPEID
    }

    
    static var serverdriverVer:String{
        if let version:String = UserDefaults.standard.object(forKey: USER_INFO.ServerDriverVersion)as! String? {
            return version
        }
        return USER_INFO.ServerDriverVersion
    }
    
    // Get UserID
    @objc static var userId: String {
        if let ID: String = UserDefaults.standard.value(forKey: USER_INFO.USER_ID) as! String? {
            return ID
        }
        return USER_INFO.USER_ID
    }
    
    // Get first name
    static var firstName: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.FIRSTNAME) as! String? {
            return name
        }
        return USER_INFO.FIRSTNAME
    }
    
    // Get last name
    static var lastName: String {
        if let last: String = UserDefaults.standard.value(forKey: USER_INFO.LASTNAME) as! String? {
            return last
        }
        return USER_INFO.LASTNAME
    }
    
    
    // Get referralCode
    static var referralCode: String {
        if let ID: String = UserDefaults.standard.value(forKey: USER_INFO.REFERRAL_CODE) as! String? {
            return ID
        }
        return USER_INFO.REFERRAL_CODE
    }
    
    
    // Get checkOldPassword
    static var checkOldPassword: String {
        if let ID: String = KeychainWrapper.standard.string(forKey: USER_INFO.OLDPASSWORD) as! String? {
            return ID
        }
        return USER_INFO.OLDPASSWORD
    }
    
    // Get User Name
    static var userName: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.USER_NAME) as! String? {
            return name
        }
        return USER_INFO.USER_NAME
    }
    
    // Get User Name
    static var userImage: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.USERIMAGE) as! String? {
            return name
        }
        return USER_INFO.USERIMAGE
    }
    
    // Get Push Token
    static var pushToken: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.PUSH_TOKEN) as! String? {
            return token
        }
        return  USER_INFO.PUSH_TOKEN
    }
    
    // Get currencySymbol
    static var currencySymbol: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.CURRENCYSYMBOL) as! String? {
            return name
        }
        return USER_INFO.CURRENCYSYMBOL
    }
    
    static var CurrencyPlace:Int {
        if let place:Int = UserDefaults.standard.value(forKey: USER_INFO.CURRENCYPLACE) as? Int{
            return place
        }
        return 1
    }
    
    // Get currencySymbol
    static var WeightUnits: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.WEIGHT) as! String? {
            return name
        }
        return USER_INFO.WEIGHT
    }
    
    // Get currencySymbol
    static var fcmTopic: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.FCMTOPIC) as! String? {
            return name
        }
        return USER_INFO.FCMTOPIC
    }
    
    // Get DistanceUdits
    static var DistanceUnits: String {
        if let name: Int = UserDefaults.standard.value(forKey: USER_INFO.DISTANCE) as! Int? {
            if name == 1{
                return " Miles"
            }else{
                return " Kms"
            }
        }
        return USER_INFO.DISTANCE
    }
    
    // Get pubnubChn
    static var driverPublishChn: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.DRIVERPUBCHANNEL) as! String? {
            return name
        }
        return USER_INFO.DRIVERPUBCHANNEL
    }

    // Get apiInterval
    static var apiInterval: Int {
        if let apiInt: Int = UserDefaults.standard.value(forKey: USER_INFO.APIINTERVAL) as? Int {
            return apiInt
        }
        return Int(USER_INFO.APIINTERVAL)!
    }

    // Get bookInterval
    static var bookingInterval: Int {
        if let bookInt: Int = UserDefaults.standard.value(forKey: USER_INFO.BOOKAPIINTERVAL) as? Int {
            return bookInt
        }
        return Int(USER_INFO.BOOKAPIINTERVAL)!
    }
    
    // Get bookInterval
    @objc static var distanceAccordingStoring: Int {
        if let distance: Int = UserDefaults.standard.value(forKey: USER_INFO.DISTANCESTORINGDATA) as! Int? {
            return distance
        }
        return Int(USER_INFO.DISTANCESTORINGDATA)!
    }
    
    // Get sessionCheck
    static var sessionCheck: String {
        if let session: String = KeychainWrapper.standard.string(forKey: USER_INFO.SESSIONCHECK) as! String? {
            return session
        }
        return USER_INFO.SESSIONCHECK
    }

    // Get User Phone
    static var savedID: String {
        if let id: String = UserDefaults.standard.value(forKey: USER_INFO.SAVEDID) as! String? {
            return id
        }
        return ""
    }
    
    // Get User Phone
    static var savedPassword: String {
        if let password: String = KeychainWrapper.standard.string(forKey: USER_INFO.SAVEDPASSWORD) {
            return password
        }
        return ""
    }
    // Get mandatory update
    static var mandtoryUpdate: Int {
        if let status = UserDefaults.standard.value(forKey: USER_INFO.VERSIONMANDATORY) as? Int {
            return status
        }
        return 0
    }
    
    
       // Get User Email
    static var userEmail: String {
        if let email: String = UserDefaults.standard.value(forKey: USER_INFO.USER_EMAIL) as! String? {
            return email
        }
        return USER_INFO.USER_EMAIL
    }
    
    // Get User Dial Code
    static var userDialCode: String {
        if let dialcode: String = UserDefaults.standard.value(forKey: USER_INFO.USER_DIALCODE) as! String? {
            return dialcode
        }
        return USER_INFO.USER_DIALCODE
    }
    
    // Get User Phone
    static var userPhone: String {
        if let phone: String = UserDefaults.standard.value(forKey: USER_INFO.USER_PHONE) as! String? {
            return phone
        }
        return USER_INFO.USER_PHONE
    }
   
    // Get User Phone
    @objc static var onGoingBid: String {
        if let bid: String = UserDefaults.standard.value(forKey: USER_INFO.SELBID) as! String? {
            return bid
        }
        return ""
    }
    
    // Get User Phone
    static var presence: String {
        if let pre: String = UserDefaults.standard.value(forKey: USER_INFO.PRESENCE) as! String? {
            return pre
        }
        return USER_INFO.PRESENCE
    }


    // Get User Phone
    static var onGoingCustChn: String {
        if let chn: String = UserDefaults.standard.value(forKey: USER_INFO.SELCHN) as! String? {
            return chn
        }
        return USER_INFO.SELCHN
    }
    
    // Get User Phone
    @objc static var bookStat: String {
        if let status: String = UserDefaults.standard.value(forKey: USER_INFO.BOOKSTATUS) as! String? {
            return status
        }
        return USER_INFO.BOOKSTATUS
    }
    
    // Get User Phone
    static var providerType: String {
        if let type: String = UserDefaults.standard.value(forKey: USER_INFO.WHICHPROVIDERTYPE) as! String? {
            return type
        }
        return "0"
    }
    
    // Get request ID
    static var RequestID: Int64 {
        if let id = UserDefaults.standard.value(forKey: USER_INFO.REQUEST_ID) as? Int64 {
            return Int64(id)
        }
        return 0
    }
    
    //Get timezone time differnce
    
    static var diffTimeInterval: TimeInterval{
        if let interval = UserDefaults.standard.value(forKey:USER_INFO.TIMEZONETIMEINTERVAL) as? TimeInterval {
            return interval
        }
        return 0.0
    }
    
    // Get Amazon Phone
    static var amazonUrl: String {
      
        return "https://s3.amazonaws.com/" + Bucket + "/"
    }
    
    // Get User Type
    static var userType: Int = 1
    
    class func deleteSavedData() -> Bool {
        let deleteProfileDoc = Couchbase.sharedInstance
        deleteProfileDoc.deleteDocument(withDocID:Utility.userId)
        deleteProfileDoc.deleteDocument(withDocID:"scheduleData")
        deleteProfileDoc.deleteDocument(withDocID:"schedulemonthlywise")
        deleteProfileDoc.deleteDocument(withDocID:"cancelReasons")
        deleteProfileDoc.deleteDocument(withDocID:"weeksdata")
        deleteProfileDoc.deleteDocument(withDocID:"bookingshistory")
        deleteProfileDoc.deleteDocument(withDocID:"RateCardData")
        deleteProfileDoc.deleteDocument(withDocID:Utility.userEmail)
        
        
        let ud = UserDefaults.standard
        ud.removeObject(forKey: USER_INFO.DEVICE_ID)
        ud.removeObject(forKey: USER_INFO.DID_AGREE_TC)
        ud.removeObject(forKey: USER_INFO.COUPON)
        KeychainWrapper.standard.removeObject(forKey: USER_INFO.STRIPE_KEY)
        ud.removeObject(forKey: USER_INFO.USER_ID)
        ud.removeObject(forKey: USER_INFO.USER_NAME)
        ud.removeObject(forKey: USER_INFO.USER_EMAIL)
        ud.removeObject(forKey: USER_INFO.USER_PHONE)
        ud.removeObject(forKey: USER_INFO.USERIMAGE)
        ud.removeObject(forKey: USER_INFO.USER_DIALCODE)
        ud.removeObject(forKey: USER_INFO.VEHTYPEID)
        ud.synchronize()

        return true
    }
    
    /***************************** Generic Utility ********************************/
    
    /// Int for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: Int Value
    class func intForObj(object: Any?) -> Int {
        
        switch object {
            
        case is Int, is Int8, is Int16, is Int32, is Int64:
            
            return object as! Int
            
        case is String:
            
            if (object as! String) == "" {
                return 0
            }
            return Int(object as! String)!
            
        default:
            
            return 0
        }
    }
    
    /// Float for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: Float Value
    class func floatForObj(object: Any?) -> Float {
        
        switch object {
            
        case is Float, is Int:
            
            return object as! Float
            
        case is String:
            
            if (object as! String) == "" {
                return 0.0
            }
            return Float(object as! String)!
            
        default:
            return 0.0
        }
    }
    
    /// String for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: String
    class func strForObj(object: Any?) -> String {
        
        switch object {
            
        case is String: // String
            
            switch object as! String {
                
            case "(null)", "(Null)", "<null>", "<Null>":
                
                return ""
                
            default:
                
                return object as! String
            }
            
        case is Int, is Float:
            
            return String(describing: object)
            
        default:
            
            return ""
        }
    }
    /// Dictionary for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: [String: AnyObject]
    class func dictionaryForObj(object: Any?) -> [String: AnyObject] {
        
        switch object {
            
        case is [String: AnyObject]: // Dictionary of Type [String: AnyObject]
            
            return object as! [String : AnyObject]
            
        case is [String: Any]: // Dictionary of Type [String: Any]
            
            return object as! [String : AnyObject]
            
        default:
            
            return [:]
        }
    }
    
    /// Array for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: [AnyObject]
    class func arrayForObj(object: Any?) -> [AnyObject] {
        
        switch object {
            
        case is [AnyObject]: // Array of Type [AnyObject]
            
            return object as! [AnyObject]
            
        case is [Any]: // Array of Type [Any]
            
            return object as! [AnyObject]
            
        default:
            
            return []
        }
    }
}
