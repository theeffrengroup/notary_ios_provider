//
//  Helper.swift
//  Dayrunner
//
//  Created by Rahul Sharma on 29/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//


//let activityData = ActivityData(size: CGSize(width: 30,height: 30),
//                                message: "Loading...",
//                                type: NVActivityIndicatorType.ballRotateChase,
//                                color: UIColor.white,
//                                padding: nil,
//                                displayTimeThreshold: nil,
//                                minimumDisplayTime: nil)
//NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)

import UIKit
import Stripe


class Helper: NSObject {

    
    /********************************************************/
    //MARK: - METHODS
    /********************************************************/
    
    /// Method to get Country Code
    ///
    /// - Returns: Country code
    class func getCountyCode() -> String {
        let currentLocale:Locale = (Locale.current as NSLocale) as Locale
        
        return ("" as? String?)!!
      //  return (currentLocale.object(forKey: .countryCode) as? String)!
    }
    
    
   class func appVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "\(version) build \(build)"
    }
    
    /// Method to get Country Name
    ///
    /// - Returns: Returns Country Name
    class func getCountryName() -> String {
        
        if let name = (Locale.current as NSLocale).displayName(forKey: .
            countryCode, value: getCountyCode()) {
            // Country name was found
            return name
        }
        else {
            // Country name cannot be found
            return getCountyCode()
        }
    }
    
    /// Get Fonts Family
    class func fonts() {
        
        for family: String in UIFont.familyNames {
            print("\n==== : Font family : \(family)")
            
            for name: String in UIFont.fontNames(forFamilyName: family) {
                print("************ : \(name)")
            }
        }
    }
    
    /// Get Card Image
    ///
    /// - Parameter type: Type of Car
    /// - Returns: Return Value is Card Image
    class func cardImage(with type: String) -> UIImage {
        
        switch type {
            
        case "Visa":
            return STPImageLibrary.brandImage(for: STPCardBrand.visa)
            
        case "MasterCard":
            return STPImageLibrary.brandImage(for: STPCardBrand.masterCard)
            
        case "Discover":
            return STPImageLibrary.brandImage(for: STPCardBrand.discover)
            
        case "American Express":
            return STPImageLibrary.brandImage(for: STPCardBrand.amex)
            
        case "JCB":
            return STPImageLibrary.brandImage(for: STPCardBrand.JCB)
            
        case "Diners Club":
            return STPImageLibrary.brandImage(for: STPCardBrand.dinersClub)
            
        default:
            return STPImageLibrary.brandImage(for: STPCardBrand.unknown)
        }
    }
    
    
    /// Array for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: [AnyObject]
    class func arrayForObj(object: Any?) -> [AnyObject] {
        
        switch object {
            
        case is [AnyObject]: // Array of Type [AnyObject]
            
            return object as! [AnyObject]
            
        case is [Any]: // Array of Type [Any]
            
            return object as! [AnyObject]
            
        default:
            
            return []
        }
    }
    
    // alertVC
    class func alertVC(title:String, message:String)->UIAlertController {
        let alertController = UIAlertController(title: title as String, message: message as String, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        alertController.addAction(okAction)
        return alertController;
    }
    
    
    /********************************************************/
    //MARK: - Variables
    /********************************************************/
    
    /// Current Date and Time
    static var currentDateTime: String {
        
        let now = Date.dateWithDifferentTimeInterval()
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        let indianLocale = Locale(identifier: "en_US")
        dateFormatter.locale = indianLocale as Locale?
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: now)
    }
    
    /// Current Date and Time
    static var saveProfileImg: String {
        
        let now = Date.dateWithDifferentTimeInterval()
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        let indianLocale = Locale(identifier: "en_US")
        dateFormatter.locale = indianLocale as Locale?
        dateFormatter.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        return dateFormatter.string(from: now)
    }

    
    /// Current time stamp
    static var currentTimeStamp: String {
        return String(format: "%0.0f", Date().timeIntervalSince1970 * 1000)
    }
    
    /// Current time Stamp in Int64 format
    static var currentTimeStampInt64: Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    /// Get getCurrentGMTDateTime
    static var currentGMTDateTime: String {
        let now = Date.dateWithDifferentTimeInterval()
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        //dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTimeInIsoFormatForZuluTimeZone: String = dateFormatter.string(from: now)
        return dateTimeInIsoFormatForZuluTimeZone
    }
    
    /// Change the Date format
    ///
    /// - Parameter dateString: Old formate
    /// - Returns: "yyyy-MM-dd HH:mm:ss"
    class func change(_ date: String) -> String {
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateFormat = "dd-MMM-YYYY hh:mm a"
        let oldDate = dateFormatter.date(from: date)
        
        let indianLocale = Locale(identifier: "en_US")
        dateFormatter.locale = indianLocale as Locale?
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: oldDate!)
    }
    
    class func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func colorFromHexString(hexCode:String!)->UIColor{
        let scanner  = Scanner(string:hexCode)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "$+#")
        var hex: CUnsignedInt = 0
        if(!scanner.scanHexInt32(&hex))
        {
            return UIColor()
        }
        let  r  = (hex >> 16) & 0xFF;
        let  g  = (hex >> 8) & 0xFF;
        let  b  = (hex) & 0xFF;
        
        return UIColor.init(red: CGFloat(r) / 255.0, green:  CGFloat(g) / 255.0, blue:  CGFloat(b) / 255.0, alpha: 1)
    }
    /********************************************************/
    //MARK: - Time Format
    /********************************************************/
    
    /// Change the Format to 07-Dec-2016
    ///
    /// - Parameter date: Date type
    /// - Returns: Returns 07-Dec-2016 as String
    class func changeDate(_ date: Date) -> String {
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateFormat = "dd-MMM-yyyy" //22-Nov-2012
        let formattedDateString = dateFormatter.string(from: date)
        return formattedDateString
    }
    
    /// Get Date From String
    ///
    /// - Parameter dateString: String of Date
    /// - Returns: Date
    class func dateFromString(_ dateString: String) -> Date {
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        let date: Date? = dateFormatter.date(from: dateString)
        return date!
    }
    
    /// Change Date format from "yyyy-MM-dd HH:mm:ss" to "dd-MMM-yyyy"
    ///
    /// - Parameter dateString: String of Date
    /// - Returns: "10-DEC-1991"
    class func changeDateFormat(_ dateString: String) -> String {
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date: Date? = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
    class func changeDateFormatWithMonth(_ dateString: String) -> String {
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date: Date? = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "MMM dd"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
   
    
    class func changeDateFormatForHome(_ dateString: String) -> String {
        
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "MMM dd, hh:mm a"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
    class func roundTheTime() -> Date {
        
        var time: DateComponents? = Calendar.current.dateComponents([.hour, .minute], from: Date())
        var minutes: Int? = time?.minute
        let minuteUnit = ceil(Float(minutes!) / 30.0)
        minutes = Int((minuteUnit * 30.0))
        time?.minute = minutes ?? 0
        return Calendar.current.date(from: time!)!
    }
    
    /********************************************************/
    //MARK: - Current Symbol
    /********************************************************/
    class func getCurrency(_ amount: Float) -> String {
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.locale = Locale.current
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.minimumFractionDigits = 2
        currencyFormatter.alwaysShowsDecimalSeparator = true
        currencyFormatter.numberStyle = .currency
        let someAmount: NSNumber = NSNumber(value: amount)
        let currencystring: String? = currencyFormatter.string(from: someAmount)
        
        return currencystring!
    }
    

    //MARK: - Progress Ideicator
    // Show with Default Message
    class func showPI(message: String) {
        
        
        let activityData = ActivityData(size: CGSize(width: 30,height: 30),
                                        message: message ,
                                        type: NVActivityIndicatorType.ballRotateChase,
                                        color: UIColor.white,
                                        padding: nil,
                                        displayTimeThreshold: nil,
                                        minimumDisplayTime: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
       // Hide
    class func hidePI() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
      //************ add border shadow*****************//
    class func addBorderShadow(view : UIView){
        
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
        view.layer.shouldRasterize = true
    }
    
     //************ background color gradient added *****************//
    class func setViewBackgroundGradient(sender: UIView, _ topColor:UIColor, _ bottomColor:UIColor)->UIView {
        
        let gradient = CAGradientLayer()
        
        gradient.frame = sender.bounds
        gradient.colors = [topColor, bottomColor]
        
        sender.layer.insertSublayer(gradient, at: 0)
        return sender
    }
    
      //************ Add shadow to the view*****************//
    class func shadowView(sender: UIView, width:CGFloat, height:CGFloat ) {
        
        //sender.layer.cornerRadius = 4
        // Drop Shadow
        let squarePath = UIBezierPath(rect:CGRect.init(x: 0, y:0 , width: width, height: height)).cgPath
            
        sender.layer.shadowPath = squarePath;
        //sender.layer.shadowRadius = 4
        sender.layer.shadowOffset = CGSize(width: CGFloat(0.5), height: CGFloat(0.5))
        sender.layer.shadowColor = UIColor.lightGray.cgColor
        sender.layer.shadowOpacity = 0.5
        sender.layer.masksToBounds = false
    }
    
      //************ time differene to drop*****************//
    class func getTheTimeDifferenceDrop(_ stringDate: String) -> String {
        let df = DateFormatter.initTimeZoneDateFormat()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date1: Date? = df.date(from: stringDate)
        let date2 = Date.dateWithDifferentTimeInterval()
        let interval: TimeInterval = date2.timeIntervalSince(date1!)
        var hours = Int(interval) / 3600
        // integer division to get the hours part
        
        
        var minutes: Int = (Int(interval) - (hours * 3600)) / 60
        // interval minus hours part (in seconds) divided by 60 yields minutes
        var timeDiff: String
        if minutes < 0 || hours <= 0 {
            if hours < 0 {
                hours = -(hours)
            }
            minutes = -(minutes)
            timeDiff = "-" + String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
            //  timeDiff = "-\(hours):%02d"
        }
        else {
            timeDiff =  String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
            //  timeDiff = "\(hours):%02d"
        }
        return timeDiff
    }
    
    class func isValidPassword(password:String) -> Bool {
        let PASSWORD_REG_EXP = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\\d)[A-Za-z\\d$@$!%*?&]{7,}"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", PASSWORD_REG_EXP)
        return passwordTest.evaluate(with: password)
    }
    
    class  func validateMobile(value: String) -> Bool {
        
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }

       //************ time differene to pickup*****************//
    class func getTheTimeDifference(_ stringDate: String) -> String {
        let df = DateFormatter.initTimeZoneDateFormat()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date1: Date? = df.date(from: stringDate)
        let date2 = Date.dateWithDifferentTimeInterval()
        let interval: TimeInterval? = date1?.timeIntervalSince(date2)
        var hours = Int(interval!) / 3600
        // integer division to get the hours part
        var minutes: Int = (Int(interval!) - (hours * 3600)) / 60
        // interval minus hours part (in seconds) divided by 60 yields minutes
        var timeDiff: String
        if minutes < 0 || hours <= 0 {
            if hours < 0 {
                hours = -(hours)
            }
            minutes = -(minutes)
            timeDiff = "-" + String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
        }
        else {
             timeDiff =  String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
        }
        return timeDiff
    }

     //************ to show toast view *****************//
    class  func toastView(messsage : String, view: UIView ){
        let toastLabel = UILabel(frame: CGRect(x: 0, y: 100, width: view.frame.size.width,  height : 35))
        toastLabel.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = NSTextAlignment.center;
        view.addSubview(toastLabel)
        toastLabel.text = messsage
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 2.0, delay: 0.3, options: UIView.AnimationOptions.curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        })
    }
    
    //************ show alert view on root viewController window *****************//
    class func alertVC(errMSG:String){
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1
        alertWindow.makeKeyAndVisible()
        
        alertWindow.rootViewController?.present(Helper.alertVC(title: alertMsgCommom.Message , message:errMSG), animated: true, completion: nil)
    }
    
    
    //************ Resize the width and height of image *****************//
    class func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

     //************ To get difference between two times *****************//
    class func getDifferenceForTwoTimes(currentDate:String,previousDate:String)->AnyObject{
        let dateFormatter = DateFormatter.initTimeZoneDateFormat()
        let userCalendar = Calendar.current
        
        let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startTime = dateFormatter.date(from: previousDate)
        let endTime = dateFormatter.date(from: currentDate)
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime!, to: endTime!)
        return timeDifference as AnyObject
    }

    class func yearMonthDateTime(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter.initTimeZoneDateFormat()
        df.dateFormat = "yyyy-MM-dd"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    class func countLabelLines(label: UILabel) -> Int {
        // Call self.layoutIfNeeded() if your view uses auto layout
        let myText = label.text! as NSString
        
        let rect = CGSize(width: label.bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: label.font], context: nil)
        
        return Int(ceil(CGFloat(labelSize.height) / label.font.lineHeight))
    }
    
    class func getTheDateFromTimeStamp(timeStamp:Int64) -> String{
        let date = Date(timeIntervalSince1970:TimeInterval(timeStamp))
        
        let df = DateFormatter.initTimeZoneDateFormat()
        
        df.dateFormat = "dd MMMM yyyy | hh:mm a"
        let dateString = df.string(from: date)
        return dateString
    }
    
    class func getTheOnlyDateFromTimeStamp(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter.initTimeZoneDateFormat()
        df.dateFormat = "dd MMMM yyyy"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    
    class func getTheScheduleViewDataFormat(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter.initTimeZoneDateFormat()
        df.dateFormat = "dd MMM yyyy"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    class func getTheScheduleViewTimeFormat(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter.initTimeZoneDateFormat()
        df.dateFormat = "hh:mm a"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    class func getTheScheduleTimeFormat(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter.initTimeZoneDateFormat()
        df.dateFormat = "hh:mm"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    
    class func getTheScheduleTimeAmorPM(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter.initTimeZoneDateFormat()
        df.dateFormat = "a"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    
    class func getTheMonthAndDate(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter.initTimeZoneDateFormat()
        df.dateFormat = "MMM dd"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    //vani 13/11/2019
    //************ To get the IP address *****************//
    /*
    class func getIPAddresses() -> [String] {
        var addresses = [String]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return [] }
        guard let firstAddr = ifaddr else { return [] }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        return addresses
    } */
    class func getIPAddress(){
        let url = URL(string: "https://icanhazip.com/")
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if data != nil {
                
                if var iPAddress = String(data: data!, encoding: String.Encoding.utf8) {
                    
                    iPAddress = iPAddress.trimmingCharacters(in: CharacterSet.newlines)
                    UserDefaults.standard.set(iPAddress, forKey: "ipAddress")
                    UserDefaults.standard.synchronize()
                }
                
            }
            
        }
        
        task.resume()
    }
    
    class func getTheAmtTextWithSymbol(amt:String) -> String {
        if Utility.CurrencyPlace == 1{
            return Utility.currencySymbol + " " + amt
        }else{
            return  amt + " " + Utility.currencySymbol
        }
    }
    
    //Time Zone methods
    
    class func getTimeZoneFromCurrentLoaction() -> TimeZone? {
        
        //let locationManager = LocationManager.sharedInstance
        
      //  let location = CLLocationCoordinate2D(latitude: 40.7128, longitude: 74.0060)
    //  return TimezoneMapper.latLngToTimezone(location)
        
//        let locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
//        locationtracker.startLocationTracking()
//
//        return TimezoneMapper.latLngToTimezone(locationtracker.currentLocation.coordinate)
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                
            case .notDetermined, .restricted, .denied:
                print("No access")
                return TimeZone.current
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                let locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
                locationtracker.startLocationTracking()
                return TimezoneMapper.latLngToTimezone(locationtracker.currentLocation.coordinate)
            @unknown default:
                return TimeZone.current
                
            }
        }
        print("Location services are not enabled")
        return TimeZone.current
    }
    
    class func getTimeZoneFromLoaction(latitude:Double,
                                       longitude:Double) -> TimeZone? {
        
        
        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        
        return TimezoneMapper.latLngToTimezone(location)
    }
    
   class func timeInHourMin(_ totalTime: Double) -> String {
        
        let Hours  = Int(totalTime / 60)
    
        let min  = Int(totalTime.truncatingRemainder(dividingBy: 60))
        
        
        if totalTime <= 0 {
            return "00 Hr:00 Mins"
        }else{
            var hoursString = ""
            var minsString = ""
           
            if Hours < 10{
                hoursString = "0" + String(describing:Hours)
            }else{
                hoursString = String(describing:Hours)
            }
            
            if min < 10{
                minsString = "0" + String(describing:min)
            }else{
                minsString = String(describing:min)
            }
            
            if Hours == 1 {
                 return hoursString + " Hr : "  + minsString + " Min"
            }
           
            else{
                return hoursString + " Hrs : "  + minsString + " Mins"
            }
            
            
        }
    }
}

