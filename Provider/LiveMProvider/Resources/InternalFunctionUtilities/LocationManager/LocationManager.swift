//
//  LocationManager.swift
//
//
//  Created by Jimmy Jose on 14/08/14.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


import UIKit
import CoreLocation
import MapKit




// Todo: Keep completion handler differerent for all services, otherwise only one will work
enum GeoCodingType{
    
    case geocoding
    case reverseGeocoding
}

class LocationManager: NSObject,CLLocationManagerDelegate {
    
    
    // couchDB
    let manager = CBLManager.sharedInstance()
    var lastLat:Double = 0.00
    var lastLog:Double = 0.00
    
    var startLocationTime   = Timer()
    
    /* Private variables */
    var bookingInfo = [Any]()
    var timerLocationDelay       = Timer()
    var headingVal:Double = 0.00
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    fileprivate var locationStatus : NSString = "Calibrating"// to pass in handler
    fileprivate var locationManager: CLLocationManager!
    fileprivate var verboseMessage = "Calibrating"
    
    fileprivate let verboseMessageDictionary = [CLAuthorizationStatus.notDetermined:"You have not yet made a choice with regards to this application.",
                                                CLAuthorizationStatus.restricted:"This application is not authorized to use location services. Due to active restrictions on location services, the user cannot change this status, and may not have personally denied authorization.",
                                                CLAuthorizationStatus.denied:"You have explicitly denied authorization for this application, or location services are disabled in Settings.",
                                                CLAuthorizationStatus.authorizedAlways:"App is Authorized to use location services.",
                                                CLAuthorizationStatus.authorizedWhenInUse:"You have granted authorization to use your location only when the app is visible to you."]
    
    
    var delegate:LocationManagerDelegate? = nil
    
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    
    var latitudeAsString:String = ""
    var longitudeAsString:String = ""
    
    
    var lastKnownLatitude:Double = 0.0
    var lastKnownLongitude:Double = 0.0
    
    var lastKnownLatitudeAsString:String = ""
    var lastKnownLongitudeAsString:String = ""
    
    
    var keepLastKnownLocation:Bool = true
    var hasLastKnownLocation:Bool = true
    
    var autoUpdate:Bool = false
    
    var showVerboseMessage = false
    
    var locationdataArray = [Any]()
 //   var bgTask = BackgroundTaskManager.sharedBackgroundTaskManager()
    
    
    
    class var sharedInstance : LocationManager {
        struct Static {
            static let instance : LocationManager = LocationManager()
        }
        return Static.instance
    }
    
    
    fileprivate override init(){
        
        super.init()
        if(!autoUpdate){
            autoUpdate = !CLLocationManager.significantLocationChangeMonitoringAvailable()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
    }
    
    @objc func applicationEnterBackground(){
       // bgTask?.beginNewBackgroundTask()
        self.startUpdatingLocation()
    }
    
    fileprivate func resetLatLon(){
        
        latitude = 0.0
        longitude = 0.0
        
        latitudeAsString = ""
        longitudeAsString = ""
        
    }
    
    fileprivate func resetLastKnownLatLon(){
        
        hasLastKnownLocation = false
        
        lastKnownLatitude = 0.0
        lastKnownLongitude = 0.0
        
        lastKnownLatitudeAsString = ""
        lastKnownLongitudeAsString = ""
        
    }
    
    
    func startUpdatingLocation(){
        
        initLocationManager()
    }
    
    func stopUpdatingLocation(){
        
        //    if(autoUpdate){
        locationManager.stopUpdatingLocation()
        //        }else{
        //
        //            locationManager.stopMonitoringSignificantLocationChanges()
        //        }
        
        
        resetLatLon()
        if(!keepLastKnownLocation){
            resetLastKnownLatLon()
        }
    }
    
    fileprivate func initLocationManager() {
        
        // App might be unreliable if someone changes autoupdate status in between and stops it
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        // locationManager.locationServicesEnabled
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingHeading()
        
        let Device = UIDevice.current
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS8 = iosVersion >= 8
        
        if iOS8{
            locationManager.requestAlwaysAuthorization() // add in plist NSLocationAlwaysUsageDescription
            // locationManager.requestWhenInUseAuthorization() // add in plist NSLocationWhenInUseUsageDescription
            locationManager.allowsBackgroundLocationUpdates = true
        }
        startLocationManger()
    }
    
    fileprivate func startLocationManger(){
        
        //     if(autoUpdate){
        
        locationManager.startUpdatingLocation()
        
        //        }else{
        //
        //            locationManager.startMonitoringSignificantLocationChanges()
        //        }
    }
    
    fileprivate func stopLocationManger(){
        
        //    if(autoUpdate){
        
        locationManager.stopUpdatingLocation()
        //        }else{
        //
        //            locationManager.stopMonitoringSignificantLocationChanges()
        //        }
    }
    
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        stopLocationManger()
        
        resetLatLon()
        
        if(!keepLastKnownLocation){
            
            resetLastKnownLatLon()
        }
        
        if ((delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerReceivedError(_:))))!){
            delegate?.locationManagerReceivedError!(error.localizedDescription as NSString)
        }
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        headingVal = newHeading.trueHeading
        // newHeading.trueHeading
        //   print("new header ",  newHeading.magneticHeading)
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let arrayOfLocation = locations as NSArray
        let location = arrayOfLocation.lastObject as! CLLocation
        let coordLatLon = location.coordinate
        
        latitude  = coordLatLon.latitude
        longitude = coordLatLon.longitude
        
        latitudeAsString  = coordLatLon.latitude.description
        longitudeAsString = coordLatLon.longitude.description
        
        
        lastKnownLatitude = coordLatLon.latitude
        lastKnownLongitude = coordLatLon.longitude
        
        lastKnownLatitudeAsString = coordLatLon.latitude.description
        lastKnownLongitudeAsString = coordLatLon.longitude.description
        
        hasLastKnownLocation = true
        
        let latLongs : [String : Any] =
            ["latitude"   : latitude,
             "longitude"  :longitude]
        
        
        locationdataArray.append(latLongs)
        
        if self.startLocationTime.isValid{
            return
        }
        
//        bgTask = BackgroundTaskManager.sharedBackgroundTaskManager()
//        bgTask?.beginNewBackgroundTask()
//        
//        self.startLocationTime = Timer.scheduledTimer(timeInterval:20,
//                                                    target: self,
//                                                    selector: #selector(restartLocation),
//                                                    userInfo: nil,
//                                                    repeats: true)
//        
//        if self.timerLocationDelay.isValid{
//            self.timerLocationDelay.invalidate()
//        }
//        
//        self.timerLocationDelay = Timer.scheduledTimer(timeInterval:8,
//                                                      target: self,
//                                                      selector: #selector(delayTheLocationUpdatesFor10Seconds),
//                                                      userInfo: nil,
//                                                      repeats: true)
        
        
        
        if (delegate != nil){
            if((delegate?.responds(to: #selector(LocationManagerDelegate.locationFoundGetAsString(_:longitude:))))!){
                delegate?.locationFoundGetAsString!(latitudeAsString as NSString,longitude:longitudeAsString as NSString)
            }
            if((delegate?.responds(to: #selector(LocationManagerDelegate.locationFound(_:longitude:))))!){
                delegate?.locationFound(latitude,longitude:longitude)
            }
        }
    }
    
    func restartLocation(){
        if self.startLocationTime.isValid {
            self.startLocationTime.invalidate()
        }
        // App might be unreliable if someone changes autoupdate status in between and stops it
        locationManager.stopUpdatingLocation()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingHeading()
        
        
        locationManager.startUpdatingLocation()
        let Device = UIDevice.current
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS8 = iosVersion >= 8
        
        if iOS8{
            locationManager.requestAlwaysAuthorization()
            locationManager.allowsBackgroundLocationUpdates = true
        }
        
    }
    
    func delayTheLocationUpdatesFor10Seconds()
    {
        self.stopLocationManger()
    }
    
    
    func locationUpdatesToCustomerChannel(val:String) {
        if UserDefaults.standard.bool(forKey: "LocationEnabled") {
            if (locationdataArray.count) != 0{
                let dict = self.locationdataArray[locationdataArray.count-1] as? [String:Any]
                let lat = dict?["latitude"] as! Double
                let log = dict?["longitude"] as! Double
                self.updateLocationdetailsToPubnub(lat: lat, long: log)
                lastLat = lat
                lastLog = log
                locationdataArray = [Any]()
            }
        }else{
            self.updateLocationdetailsToPubnub(lat: lastLat, long: lastLog)
        }
    }
    
    
    func updateLocationdetailsToPubnub(lat:Double, long: Double)  {
        let ud = UserDefaults.standard
        let  database  = try! manager.databaseNamed("dayrunnerlatlogs")
        
        var locationFlag:Int = 0
        if UserDefaults.standard.bool(forKey: "LocationEnabled") {
            locationFlag = 1
        }else{
            locationFlag = 0
        }
        
        
        var message : [String : Any] =  ["lt"            : lat,
                                         "lg"            : long,
                                         "vt"            : Utility.vehicleTypeID,
                                         "battery_Per"   : Utility.batteryLevel,
                                         "app_version"   : Utility.appVersion,
                                         "device_type"   : 1,
                                         "location_check": locationFlag,
                                         "pubnubStr"     : Utility.onGoingBid,
                                         "location_Heading": headingVal,
                                         "presenceTime"  :Utility.presence,
                                         "transit":0]
        
        
        if (UserDefaults.standard.object(forKey: "distLat") != nil) {
            
            let currentLatlog = CLLocation(latitude: lat, longitude: long)
            let custDist = CLLocation(latitude: UserDefaults.standard.object(forKey: "distLat") as! CLLocationDegrees, longitude: UserDefaults.standard.object(forKey: "distLog") as! CLLocationDegrees)
            let itemDist: Float = Float(currentLatlog.distance(from: custDist))
            
            if itemDist >= Float(Utility.distanceAccordingStoring) {
                
                if ud.bool(forKey: "hasBookings"){
                    if !ud.bool(forKey: "hasInternet") {
                        //  let latlongs: [String: Any] = ["latitude": lat,
                        //                                 "longitude": long]
                        
                        let latLongs = String(describing:lat) + "," + String(describing:long)
                        
                        bookingInfo.append(latLongs)
                        
                        let properties  = ["latLongs": bookingInfo] as [String : Any]
                        
                        if (UserDefaults.standard.object(forKey: "documentID") != nil){
                            if let documents = database.existingDocument(withID:ud.object(forKey: "documentID") as! String){
                                var properties = documents.properties
                                properties?["latLongs"] = bookingInfo
                                try! documents.putProperties(properties!)
                            }
                        }
                        else{
                            let document = database.createDocument()
                            try! document.putProperties(properties)
                            ud.set(document.documentID, forKey: "documentID")
                            ud.synchronize()
                        }
                        UserDefaults.standard.set(false, forKey: "updatedOfflineLatLongs")
                    }
                }
                UserDefaults.standard.set(lat, forKey: "distLat")
                UserDefaults.standard.set(long, forKey: "distLog")
                UserDefaults.standard.synchronize()
                if ud.bool(forKey: "updatedOfflineLatLongs"){
                    message.updateValue(1, forKey: "transit")
                    self.updateLocationToServer(dict:message)
                }
            }else{
                if ud.bool(forKey: "updatedOfflineLatLongs"){
                    self.updateLocationToServer(dict:message)
                }
            }
        }else{
            UserDefaults.standard.set(lat, forKey: "distLat")
            UserDefaults.standard.set(long, forKey: "distLog")
            UserDefaults.standard.synchronize()
        }
        
        if ud.bool(forKey: "hasInternet") && !ud.bool(forKey: "updatedOfflineLatLongs") {
            self.updateOfflineLatLongs()
        }
    }
    
    
    func updateOfflineLatLongs(){
        var arrayLatLongs:Any = ""
        let  database  = try! manager.databaseNamed("dayrunnerlatlogs")
        if (UserDefaults.standard.object(forKey: "documentID") != nil){
            if let documents = database.existingDocument(withID: UserDefaults.standard.object(forKey: "documentID") as! String){
                let properties = documents.properties
                arrayLatLongs = properties!["latLongs"]! as Any
            }else{
                arrayLatLongs = []
            }
            let dict:[String:Any]  = ["lt_lg": arrayLatLongs]
            
            
            NetworkHelper.requestPUT(serviceName: API.METHOD.OFFLINELATLONGS,
                                      params: dict,
                                      success: { (response) in
                                   
                                        
            }) { (Error) in
                Helper.hidePI()
            }
        }else {
             UserDefaults.standard.set(true, forKey: "updatedOfflineLatLongs")
        }
    }
    
    
    func deletingTheDataFromFromParticularID(){
        if (UserDefaults.standard.object(forKey: "documentID") != nil) {
            
            let  database  = try! manager.databaseNamed("dayrunnerlatlogs")
            let document = database.document(withID: UserDefaults.standard.object(forKey: "documentID") as! String)
            
            do
            {
                try document?.delete()
                bookingInfo = [Any]()
                UserDefaults.standard.removeObject(forKey: "documentID")
                UserDefaults.standard.synchronize()
            }
            catch
            {
                print("Error when deleting the guest database during migrating guest data : \(error.localizedDescription)")
            }
        }
    }
    
    
    func updateLocationToServer(dict:[String : Any]) {
        
 
    }
    

    
    
    internal func locationManager(_ manager: CLLocationManager,
                                  didChangeAuthorization status: CLAuthorizationStatus) {
        var hasAuthorised = false
        let verboseKey = status
        switch status {
        case CLAuthorizationStatus.restricted:
            let view = LocationEnableView.shared
            view.show()
            locationStatus = "Restricted Access"
        case CLAuthorizationStatus.denied:
            let view = LocationEnableView.shared
            view.show()
            
            locationStatus = "Denied access"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "Not determined"
        default:
            let view = LocationEnableView.shared
            view.hide()
            locationStatus = "Allowed access"
            hasAuthorised = true
        }
        
        verboseMessage = verboseMessageDictionary[verboseKey]!
        
        if (hasAuthorised == true) {
            UserDefaults.standard.set(true, forKey: "LocationEnabled")
            startLocationManger()
        }else{
              UserDefaults.standard.set(false, forKey: "LocationEnabled")
            resetLatLon()
            if (!locationStatus.isEqual(to: "Denied access")){
                
                var verbose = ""
                if showVerboseMessage {
                    
                    verbose = verboseMessage
                    
                    if ((delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerVerboseMessage(_:))))!){
                        
                        delegate?.locationManagerVerboseMessage!(verbose as NSString)
                        
                    }
                }
            }
            if ((delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerStatus(_:))))!){
                delegate?.locationManagerStatus!(locationStatus)
            }
        }
        
    }
    
}


@objc protocol LocationManagerDelegate : NSObjectProtocol
{
    func locationFound(_ latitude:Double, longitude:Double)
    @objc optional func locationFoundGetAsString(_ latitude:NSString, longitude:NSString)
    @objc optional func locationManagerStatus(_ status:NSString)
    @objc optional func locationManagerReceivedError(_ error:NSString)
    @objc optional func locationManagerVerboseMessage(_ message:NSString)
}


