//
//  LocationTracker.h
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


#import "LocationTracker.h"
#import "Notary_Provider-Swift.h"
//Notary_Provider
#define LATITUDE @"latitude"
#define LONGITUDE @"longitude"
#define ACCURACY @"theAccuracy"


@interface LocationTracker()
{
    double heading;
    MQTT *mqtt;
    float oldBearing;
    float currentBearing;
}

@property(nonatomic,strong)NSString *pubNubChannel;
@property LocationEnableView *locationEnabled;

@end
static LocationTracker *locationtracker;
@implementation LocationTracker

+ (id)sharedInstance {
	if (!locationtracker) {
		locationtracker  = [[self alloc] init];
	}
	return locationtracker;
}


+ (CLLocationManager *)sharedLocationManager {
	static CLLocationManager *_locationManager;
	
	@synchronized(self) {
		if (_locationManager == nil) {
			_locationManager = [[CLLocationManager alloc] init];
           
            _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [_locationManager requestAlwaysAuthorization];
            }
            
            if([_locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]){
                [_locationManager setAllowsBackgroundLocationUpdates:YES];
                [_locationManager setPausesLocationUpdatesAutomatically:NO];
            }
		}
	}
	return _locationManager;
}


///******instantiate the location tracker**********/////
- (id)init {
	if (self==[super init]) {
        //Get the share model and also initialize myLocationArray
        self.shareModel = [LocationShareModel sharedModel];
        mqtt = [MQTT sharedInstance];
        oldBearing = 0;
        _locationEnabled = [LocationEnableView shared];
        self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
        
        ///********** did calls when the app enter background*************//
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
	}
	return self;
}


////********** when the app enters background ************//
-(void)applicationEnterBackground{
    
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
        float iosVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if(iosVersion >= 9.0f)
        {
            [locationManager setAllowsBackgroundLocationUpdates:YES];
        }
    
    //Use the BackgroundTaskManager to manage all the background Task
    self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
}

- (void) restartLocationUpdates
{
    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
        float iosVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if(iosVersion >= 9.0f)
        {
        [locationManager setAllowsBackgroundLocationUpdates:YES];
    }
}


- (void)startLocationTracking {
    
    if ([CLLocationManager locationServicesEnabled] == NO) {
        [_locationEnabled show];
        
    } else {
//          [_locationEnabled hide];
        CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
        
        if(authorizationStatus == kCLAuthorizationStatusDenied || authorizationStatus == kCLAuthorizationStatusRestricted){
            [_locationEnabled show];
        }
        else
        {
            CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            [locationManager startUpdatingLocation];
              [locationManager setAllowsBackgroundLocationUpdates:YES];
            float iosVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
            if(iosVersion >= 9.0f)
            {
                [locationManager setAllowsBackgroundLocationUpdates:YES];
            }
        }
    }
}

- (void)stopLocationTracking
{
    if (self.shareModel.timer)
    {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    [locationManager stopUpdatingLocation];
}



#pragma mark - CLLocationManagerDelegate Methods

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"status: %d",status);
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
            [_locationEnabled show];
            break;
            
        case kCLAuthorizationStatusAuthorizedAlways:
            [_locationEnabled hide];
            break;
            
        case  kCLAuthorizationStatusAuthorizedWhenInUse:
            [_locationEnabled hide];
            NSLog(@"");
            break;
            
        default:
            break;
    }
}


-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading{
    heading = newHeading.trueHeading;
}

- (float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc
{
    float fLat = degreesToRadians(fromLoc.latitude);
    float fLng = degreesToRadians(fromLoc.longitude);
    float tLat = degreesToRadians(toLoc.latitude);
    float tLng = degreesToRadians(toLoc.longitude);
    
    float degree = radiansToDegrees(atan2(sin(tLng-fLng)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLng-fLng)));
    
    if (degree >= 0) {
        return degree;
    }
    else {
        return 360+degree;
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    BOOL needToPublishLocation = NO;
    if(_currentLocation == nil) {
        
        _currentLocation = [locations lastObject];
    }
    
    oldBearing = currentBearing;
    
    currentBearing = [self getHeadingForDirectionFromCoordinate:_currentLocation.coordinate toCoordinate:[[locations lastObject] coordinate]];
    
    if(currentBearing - oldBearing > 10 || oldBearing - currentBearing > 10)
    {
        needToPublishLocation = YES;
    }
    
    _currentLocation = [locations lastObject];
    
    for(int i=0;i<locations.count;i++)
    {
        CLLocation * newLocation = [locations objectAtIndex:i];
        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        CLLocationAccuracy theAccuracy = newLocation.horizontalAccuracy;
        
        float distanceChange = [newLocation distanceFromLocation:_lastLocaiton];
        if(distanceChange >= 20.00)
        {
        self.distance += distanceChange;
        }
        // [Helper showAlertWithTitle:@"Distance" Message:[NSString stringWithFormat:@"%f",self.distance]];
        if (self.distanceCallback)
        {
            if (self.distance >= 20.00)
            {
                self.distanceCallback(self.distance, newLocation);
            }
        }
        
        if (self.distanceCallbackOnOff)
        {
            self.distanceCallbackOnOff(self.distanceOnOff,newLocation);
        }
         _lastLocaiton = newLocation;
        
        
        NSTimeInterval locationAge = - [newLocation.timestamp timeIntervalSinceNow];
        
        if (locationAge > 30.0)
        {
            continue;
        }
        //NSLog(@"the accuaracy %f",theAccuracy);
        
        //Select only valid location and also location with good accuracy
        if(newLocation!=nil && theAccuracy>0
           &&theAccuracy<2000
           &&(!(theLocation.latitude==0.0&&theLocation.longitude==0.0))){
            
            self.myLastLocation = theLocation;
            self.myLastLocationAccuracy= theAccuracy;
            
            NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[NSNumber numberWithFloat:theLocation.latitude] forKey:@"latitude"];
            [dict setObject:[NSNumber numberWithFloat:theLocation.longitude] forKey:@"longitude"];
            [dict setObject:[NSNumber numberWithFloat:theAccuracy] forKey:@"theAccuracy"];
            
            //Add the vallid location with good accuracy into an array
            //Every 1 minute, I will select the best location based on accuracy and send to server
            [self.shareModel.myLocationArray addObject:dict];
        }
    }
    
    //If the timer still valid, return it (Will not run the code below)
    if (self.shareModel.timer) {
        return;
    }
    
    /// background task manager begins
    self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
    
    //Will only restart the locationManager after 20 seconds, so that we can get some accurate locations
    //The location manager will restart because its been stopped for 10 seconds
    self.shareModel.timer = [NSTimer scheduledTimerWithTimeInterval:20 target:self
                                                           selector:@selector(restartLocationUpdates)
                                                           userInfo:nil
                                                            repeats:NO];
    
    //Will only stop the locationManager after 10 seconds, so that we can get some accurate locations
    //The location manager will only operate for 10 seconds to save battery
    
    if (self.shareModel.delay10Seconds)
    {
        [self.shareModel.delay10Seconds invalidate];
        self.shareModel.delay10Seconds = nil;
    }

    
    self.shareModel.delay10Seconds = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                                    selector:@selector(stopLocationDelayBy10Seconds)
                                                                    userInfo:nil
                                                                     repeats:NO];
    
//    if(needToPublishLocation) {
//
//        [self updateLocationToServer];
//    }
    
}


//Stop the locationManager
-(void)stopLocationDelayBy10Seconds{
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    [locationManager stopUpdatingLocation];
    
}


//Send the location to Server
- (void)updateLocationToServer
{
    // Find the best location from the array based on accuracy
    NSMutableDictionary * myBestLocation = [[NSMutableDictionary alloc]init];
    
    for(int i=0;i<self.shareModel.myLocationArray.count;i++){
        
        NSMutableDictionary * currentLocation = [self.shareModel.myLocationArray objectAtIndex:i];
        
        if(i==0)
            myBestLocation = currentLocation;
        else
        {
            if([[currentLocation objectForKey:ACCURACY]floatValue]<=[[myBestLocation objectForKey:ACCURACY]floatValue]){
                myBestLocation = currentLocation;
            }
        }
    }
    //If the array is 0, get the last location
    //Sometimes due to network issue or unknown reason, you could not get the location during that  period, the best you can do is sending the last known location to the server
    if(self.shareModel.myLocationArray.count==0)
    {
        self.myLocation=self.myLastLocation;
        self.myLocationAccuracy=self.myLastLocationAccuracy;
        [self startPubNubSream:self.myLocation.latitude Longitude:self.myLocation.longitude]; //*** if location disabled, then last longs will update ********//
    }
    else
    {
        CLLocationCoordinate2D theBestLocation;
        theBestLocation.latitude =[[myBestLocation objectForKey:LATITUDE]floatValue];
        theBestLocation.longitude =[[myBestLocation objectForKey:LONGITUDE]floatValue];
        self.myLocation=theBestLocation;
        self.myLocationAccuracy =[[myBestLocation objectForKey:ACCURACY]floatValue];
        
        [self startPubNubSream:self.myLocation.latitude Longitude:self.myLocation.longitude];

        //After sending the location to the server successful, remember to clear the current array with the following code. It is to make sure that you clear up old location in the array and add the new locations from locationManager
        [self.shareModel.myLocationArray removeAllObjects];
        self.shareModel.myLocationArray = nil;
        self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
    }
}

#pragma mark server API

-(void)startPubNubSream:(double)latitude Longitude:(double)longitude
{
    
    NSError *error;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSUserDefaults *myDefaults = [[NSUserDefaults alloc]
                                  initWithSuiteName:@"group.com.3embed.GoTaskerPro"];
    
    NSString *locationCheck;
    
    if ([CLLocationManager locationServicesEnabled] == NO) { // location check
        locationCheck = @"0"; // location disabled
    }else{
        locationCheck = @"1";// location Enabled
    }
    
    /// saving the last updated lat longs to update accepted time
    [ud setValue:[NSString stringWithFormat:@"%lf",latitude] forKey:@"currentLat"];
    [ud setValue:[NSString stringWithFormat:@"%lf",longitude]  forKey:@"currentLog"];
    
    [myDefaults setObject:[NSString stringWithFormat:@"%lf",latitude] forKey:@"currentLat"];
    [myDefaults setObject:[NSString stringWithFormat:@"%lf",longitude] forKey:@"currentLog"];

    
    [ud synchronize];
    
    
    NSDictionary *message = @{
                              @"latitude": [NSNumber numberWithDouble:latitude],
                              @"longitude": [NSNumber numberWithDouble:longitude],
                              @"batteryPercentage"   :[NSString stringWithFormat:@"%f", Utility.batteryLevel],
                              @"appVersion"   : Utility.appVersion,
                              @"status": Utility.bookStat,
                              @"bookingStr"     : Utility.onGoingBid,
                              @"locationHeading": [NSString stringWithFormat:@"%f",heading],
                              @"transit": [NSNumber numberWithInt:0],
                              };
    
    NSDictionary *topicsData = @{
                                 @"latitude": [NSNumber numberWithDouble:latitude],
                                 @"longitude": [NSNumber numberWithDouble:longitude],
                                 @"pid":Utility.userId,
                                 @"batteryPercentage"   :[NSString stringWithFormat:@"%f", Utility.batteryLevel],
                                 @"appVersion"   : Utility.appVersion,
                                 @"status": Utility.bookStat,
                                 @"locationHeading": [NSString stringWithFormat:@"%f",heading],
                                 };
    
    NSData *jsondata = [NSJSONSerialization dataWithJSONObject:topicsData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if ([mqtt isConnected]) {
        [mqtt publishDataWithWthData:jsondata onTopic:[NSString stringWithFormat:@"liveTrack/%@", Utility.userId ]  retain:true withDelivering:1];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"distLat"]) {
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:[[ud objectForKey:@"distLat"] doubleValue] longitude:[[ud objectForKey:@"distLog"] doubleValue]];
        
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        //******check the distance has to satisfies the configuration distance********//
        if (distance >= Utility.distanceAccordingStoring) {
            NSMutableArray *latlongsArray = [[NSMutableArray alloc]init];
            
            if ([ud boolForKey:@"hasBookings"] && ![ud boolForKey:@"hasInternet"]) {
                NSString *latlongs = [NSString stringWithFormat:@"%f,%f",latitude,longitude];
                if ([ud objectForKey:@"storedLatLongs"]) {
                    [latlongsArray addObjectsFromArray:[ud objectForKey:@"storedLatLongs"]];
                    [latlongsArray addObject:latlongs];
                    [ud setObject:latlongsArray forKey:@"storedLatLongs"];
                }else{
                    [latlongsArray addObject:latlongs];
                    [ud setObject:latlongsArray forKey:@"storedLatLongs"];
                }
                
                NSDictionary *dict = @{
                                       @"latLongs":latlongsArray
                                       };
                
                LocationDBManager *locDB;
                if (locDB == nil){
                    locDB = [[LocationDBManager alloc]init];
                }
                if ([ud objectForKey:@"documentID"]) {
                    [locDB saveThelocationDataForUserID:[ud objectForKey:@"documentID"] dict:dict];
                }else{
                    [locDB saveThelocationDataForUserID:@"inital" dict:dict];
                }
                
                
                [ud setBool:false forKey:@"updatedOfflineLatLongs"]; // storing the offline lat longs(no internet)
                [ud synchronize];
            }
            
            ///*********** if the app moves the distance given in configuration********///
            if ([ud boolForKey:@"hasInternet"] && [ud boolForKey:@"updatedOfflineLatLongs"]) {
                NSMutableDictionary *temp = [message mutableCopy];
                [temp setObject:[NSNumber numberWithInt:1] forKey:@"transit"];
                [self updateLocationToServerLatLongs:temp];
            }
            
            //******storing recent lat longs**********//
            [ud setObject:[NSString stringWithFormat:@"%f",latitude] forKey:@"distLat"];
            [ud setObject:[NSString stringWithFormat:@"%f",longitude] forKey:@"distLog"];
            [ud synchronize];
            
        }else{
            //******check once whether the couch db lat longs are updated, if yes its starts making location service and also if the app isnt moved much and its doesn't configuration distance *****//
            if ([ud boolForKey:@"hasInternet"] && [ud boolForKey:@"updatedOfflineLatLongs"]) {
                [self updateLocationToServerLatLongs:message];
            }
        }
    }else{
        [ud setObject:[NSString stringWithFormat:@"%f",latitude] forKey:@"distLat"];
        [ud setObject:[NSString stringWithFormat:@"%f",longitude] forKey:@"distLog"];
        [ud synchronize];
    }
    
    //***** when internet came back and u was offline before then publish stored couch db lat longs to server******//
    if ([ud boolForKey:@"hasInternet"] && ![ud boolForKey:@"updatedOfflineLatLongs"]) {
        [self updateOfflineLocationToServer];
    }
}

    ///*************updating lat longs to server continously********//
-(void)updateLocationToServerLatLongs:(NSDictionary *)dict{
    LocationModel *locModel;
    if (locModel == nil){
        locModel = [[LocationModel alloc]init];
    }
    [locModel updateTheLocationToServerWithDict:dict];
}

///*********updating the offline stored lat longs stored in couch db to server *********//
-(void)updateOfflineLocationToServer{
    NSArray *arrayOfOfllineLatlongs;
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"documentID"]) {
        // get the saved location data from couchdb
        LocationDBManager *locDB;
        if (locDB == nil){
            locDB = [[LocationDBManager alloc]init];
        }
        arrayOfOfllineLatlongs = [locDB getTheLocationDictForUserID:[[NSUserDefaults standardUserDefaults] objectForKey:@"documentID"]];
        NSDictionary *dict = @{@"latitude_longitude":arrayOfOfllineLatlongs};
        
   
        
        // updating the saved offline location data to server
        LocationModel *locModel;
        if (locModel == nil){
            locModel = [[LocationModel alloc]init];
        }
        [locModel updateTheSavedLocationWithDict:dict];
    }else{
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"updatedOfflineLatLongs"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

///*******when the session expires r admin rejected********//
-(void)sessionExpired{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    SplashViewController *splashVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SplashViewController"]; //or the homeController
    window.rootViewController = splashVC;
}

@end
