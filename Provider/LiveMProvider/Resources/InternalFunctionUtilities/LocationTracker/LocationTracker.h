//
//  LocationTracker.h
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationShareModel.h"

#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiansToDegrees(x) (x * 180.0 / M_PI)

typedef void(^LocationTrackerDistanceChangeCallback) (float totalDistance, CLLocation *location);
typedef void(^LocationTrackerDistanceChangeCallbackOnOff)(float totalDistance, CLLocation *location);
@interface LocationTracker : NSObject <CLLocationManagerDelegate>

@property(nonatomic,copy)LocationTrackerDistanceChangeCallback distanceCallback;
@property(nonatomic,copy)LocationTrackerDistanceChangeCallbackOnOff distanceCallbackOnOff;

@property (nonatomic) CLLocationCoordinate2D myLastLocation;
@property (nonatomic) CLLocation *currentLocation;
@property (nonatomic) CLLocationAccuracy myLastLocationAccuracy;
@property(nonatomic,strong)CLLocation *lastLocaiton;
@property (strong,nonatomic) NSString * accountStatus;
@property (strong,nonatomic) NSString * authKey;
@property (strong,nonatomic) NSString * device;
@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) NSString * profilePicURL;
@property (strong,nonatomic) NSNumber * userid;
@property (assign,nonatomic) float distance, distanceOnOff;

@property (strong,nonatomic) LocationShareModel * shareModel;

@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;
@property (assign, nonatomic) NSInteger statusValue;

+ (CLLocationManager *)sharedLocationManager;

- (void)startLocationTracking;
- (void)stopLocationTracking;
- (void)updateLocationToServer;
+ (id)sharedInstance;


@end
