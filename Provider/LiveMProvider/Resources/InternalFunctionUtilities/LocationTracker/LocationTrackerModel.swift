//
//  LocationTrackerModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 03/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack
import Alamofire
import SwiftKeychainWrapper
class LocationModel: NSObject {
    let disposeBag = DisposeBag()
    var sourceString = API.BASE_URL + API.METHOD.LOCATION
     var sourceString1 = API.BASE_URL + API.METHOD.LOCATIONLOGS
    var apicall = APILibrary()
    //vani 24/02/2020
    @objc func updateTheLocationToServer(dict:[String:Any])  {
        
        RxAlamofire.requestJSON(.patch,sourceString,parameters:dict,headers:RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: {(r, json) in
                let responseModel:APIResponseModel!
                  DDLogVerbose(" API: \(API.METHOD.LOCATION)");
                if let dict = json as? [String: AnyObject] {
                    responseModel = APIResponseModel.init(statusCode: (r.statusCode), dataResponse: dict)
                    self.parseTheRepsonser(responseModel: responseModel)
                }  
               
            }, onError: {error in
                
            }).disposed(by: disposeBag)
    }
    
    
    /// update the location logs to server
    ///
    /// - Parameter dict: latlongs when phone in offline
    //vani 24/02/2020
    @objc func updateTheSavedLocation(dict:[String:Any])  {
        
        RxAlamofire.requestJSON(.patch,sourceString1,
                                parameters:dict,
                                encoding:JSONEncoding.default,
                                headers:RXNetworkHelper.getAOTHHeader())
            .subscribe(onNext: {(r, json) in
                let responseModel:APIResponseModel!
                DDLogVerbose(" API: \(API.METHOD.LOCATIONLOGS)");
                if let dict = json as? [String: AnyObject] {
                    let deleteLocationData = Couchbase.sharedInstance
                    deleteLocationData.deleteDocument(withDocID:UserDefaults.standard.object(forKey: "documentID") as! String)
                    responseModel = APIResponseModel.init(statusCode: (r.statusCode), dataResponse: dict)
                    self.parseTheRepsonser(responseModel: responseModel)
                    UserDefaults.standard.removeObject(forKey: "documentID")
                    UserDefaults.standard.removeObject(forKey: "storedLatLongs")
                     UserDefaults.standard.set(true, forKey: "updatedOfflineLatLongs")
                    UserDefaults.standard.synchronize()
                    
                }
            }, onError: {error in
                
            }).disposed(by: disposeBag)
    }
    
    
    func parseTheRepsonser(responseModel:APIResponseModel)  {
        DDLogVerbose(" Response: \(responseModel.data)");
        let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
        print("token status code \(responseCodes.rawValue)")
        Helper.hidePI()
        switch responseCodes{
        case .UserLoggedOut:
            Session.expired()
            Helper.alertVC(errMSG: responseModel.data["message"] as! String)
            break
        case .TokenExpired:
            let defaults = UserDefaults.standard
            if let sessionToken =  responseModel.data["data"]   as? String  {
                KeychainWrapper.standard.set(sessionToken, forKey: USER_INFO.SESSION_TOKEN)
                defaults.synchronize()
                self.apicall.getTheNewSessionToken(completionHandler: { (success) in
                    if success{
                        
                    }
                })
            }
            break
        case .SuccessResponse:
            
            break
        default:
            break
        }
    }
}
