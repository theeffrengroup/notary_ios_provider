//
//  TransitionAnimationWrapperClass.swift
//  LiveM
//
//  Created by Apple on 18/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


class TransitionAnimationWrapperClass {
    
    
    class func uiViewAnimationAnimationCurve(_ animationCurve: UIView.AnimationCurve, transition animationTransition: UIView.AnimationTransition, for view: UIView, timeDuration duration: TimeInterval) {
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(duration)
        UIView.setAnimationCurve(animationCurve)
        UIView.setAnimationTransition(animationTransition, for: view, cache: false)
        UIView.commitAnimations()
    }
    
    /// For CATransition Animation For Controller
    ///
    /// - Parameters:
    ///   - type: Transition Type
    ///   - subType: Transition SubType
    ///   - timeDuration: Time Duration For Animation
    ///   - view: view for Animation
    
    class func caTransitionAnimationType(_ type: String, subType: String, for view: UIView, timeDuration duration: TimeInterval) {
        
        let transition = CATransition()
        transition.duration = duration
        transition.type = CATransitionType(rawValue: type)
        //For the destination VC
        transition.subtype = CATransitionSubtype(rawValue: subType)
        view.layer.add(transition, forKey: kCATransition)
    }
    
    class func CATransitionForViewsinSameVCAnimation(for type: String, subType: String, timeFunctionType: String, for view: UIView, timeDuration duration: TimeInterval) {
        
        let animation = CATransition()
        //    [animation setDelegate:self]; // set your delegate her to know when transition ended
        animation.type = CATransitionType(rawValue: type)
        animation.subtype = CATransitionSubtype(rawValue: subType)
        animation.duration = duration
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName(rawValue: timeFunctionType))
        view.layer.add(animation, forKey: "viewPush")
    }

    
    
    /// Method to flip view from left -> Right Animation
    ///
    /// - Parameter view: view to show flip animation
    class func flipFromLeftAnimation(view:UIView) {
        
        UIView.beginAnimations("animation", context: nil)
        UIView.setAnimationDuration(0.8)
        
        UIView.setAnimationTransition(UIView.AnimationTransition.flipFromLeft, for:view, cache: false)
        UIView.commitAnimations()

    }
    
    
    
    /// Method to flip view from Right -> Left Animation
    ///
    /// - Parameter view: view to show flip animation
    class func flipFromRightAnimation(view:UIView) {
        
        UIView.beginAnimations("animation", context: nil)
        UIView.setAnimationDuration(1.0)
        
        UIView.setAnimationTransition(UIView.AnimationTransition.flipFromRight, for:view, cache: false)
        UIView.commitAnimations()
        
    }


}
