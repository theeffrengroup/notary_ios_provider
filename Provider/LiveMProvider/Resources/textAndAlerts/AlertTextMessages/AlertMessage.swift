//
//  AlertMessage.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 21/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

//extension String {
//    var localized: String {
//        return OSLocalizedString(self, comment: "")
//    }
//}
//
//
//extension String {
//    public func OSLocalizedString(_ key: String, comment: String) -> String {
//        return OneSkyOTAPlugin.localizedString(forKey: key, value: "", table: nil)
//    }
//}

extension String {
    var localized: String {

        return NSLocalizedString(self, comment: "")
    }
}

struct signup {
    static let YoutubeUrl       = "Please provide your youtube url".localized
    static let InValidEmail     = "Enter Valid email id".localized
    static let PasswordInvalid = "Invalid Password, Password should consist of a capital letter, a small letter, a number & minimum of 7 characters.".localized
    static let InValidPhone     = "Enter Valid phone number".localized
    static let selectGenres     = "Please select your type".localized
    static let selectDocuments  = "Please provide the document".localized
    static let enterFirstName   = "Please enter first name".localized
    static let lastFirstName    = "Please enter last name".localized
    static let enterDateOfBirth = "Please select date of birth".localized
    static let selectLocation   = "Please select the location".localized
    static let enterPhoneNumber = "Please enter Phone number".localized
    static let enterEmailId     = "Please enter Email Address".localized
    static let enterPassword    = "Please enter password".localized
    static let selectCity       = "Please Select City".localized
    static let selectProfile    = "Please update your Profile image".localized
    static let selectImage      = "Please select image".localized
    static let cancel           = "Cancel".localized
    static let gallery          = "Gallery".localized
    static let camera           = "Camera".localized
    static let USER_PIC         = "user_pic"
    static let verifyMobile     = "Validating mobile..".localized
    static let verifyEmail      = "Validating email id".localized
}


struct vehicleDetails {
    static let vehicleNumber    = "Please enter Plate number".localized
    static let vehicleColor     = "Please enter vehicle Specialities".localized
    static let vehicleType      = "Please enter vehicle type".localized
    static let vehicleMake      = "Please enter vehicle make".localized
    static let vehicleModel     = "Please select vehcile model".localized
    static let selectImage      = "Please Select Image".localized
    static let cancel           = "Cancel".localized
    static let gallery          = "Gallery".localized
    static let camera           = "Camera".localized
    static let USER_PIC         = "user_pic"
    
    
    static let vehicleImg       = "Please Select the vehicle Image".localized
    static let vehicleIns       = "Please Select Driver Vehicle Insurance".localized
    static let vehicleRegis     = "Please Select Driver Vehicle Registation".localized
    static let carriagePermit   = "Please Select Driver Vehicle Permit".localized
}

struct verify {
    static let fillOTP          = "Please fill the OTP".localized
}


struct alertMsgCommom {
    static let Message          = "Message".localized
}

struct bankDetails {
    static let verified            = "verified".localized
    static let bankAccount         = "STEP 2 : LINK BANK ACCOUNT.".localized
    static let unverifed           = "Unverified".localized
    static let stripeAccount       = "STEP 1 : CONNECT TO YOUR STRIPE ACCOUNT.".localized
    static let stripeHodler        = "Stripe Holder:".localized
}


struct bankDetailsMsg {
    static let photoID          = "Please Add the Photo ID".localized
    static let firstName        = "Please Add the account holder first name".localized
    static let holderName       = "Please Add the account holder  name".localized
    static let lastName         = "Please Add the account holder last name".localized
    static let accountNum       = "Please Add the account number".localized
    static let uniqueNum        = "Please Add the  unique identification number".localized
    static let routingNum       = "Please Add the routing number".localized
    static let address          = "Please Add the address".localized
    static let city             = "Please Add the city".localized
    static let state            = "Please Add the state".localized
    static let postal           = "Please Add the Postal code".localized
    
    static let addStripeTitle      = "Add Stripe Account".localized
    static let verifyStripeTitle   = "Verify Stripe Account".localized
    static let verifyStripe        = "Verify Stripe".localized
    static let addStripe           = "Add Stripe".localized
}

struct HistoryAlerts {
    static let barEmptyText        = "You need to provide data for the chart.".localized
    static let Sun                 = "SUN".localized
    static let Mon                 = "MON".localized
    static let Tue                 = "TUE".localized
    static let Wed                 = "WED".localized
    static let Thu                 = "THU".localized
    static let Fri                 = "FRI".localized
    static let Sat                 = "SAT".localized
    static let earnData            = "Earnings data".localized
}

struct signin {
    static let emailAddress     = "Please enter Email Address or Phone number".localized
    static let password         = "Please enter Password".localized
}

struct selectVehicle {
    static let vehicleID        = "Please Select VehicleID".localized
    static let password         = "Please enter Password".localized
}

struct homeVC {
    static let goOnline         = "Go Online..".localized
    static let goOffline        = "Go Offline..".localized
    static let timeLeftDrop     = "Time left for drop".localized
    static let timeLeftPickUp   = "Time left for pickup".localized
    
    static let AsapBooking      = "Asap Booking".localized
    static let ScheduleBooking  = "Scheduled Booking".localized

}

struct onBookingVC {
    static let goOnline         = "Loading Waiting Time".localized
    static let goOffline        = "Distance Travelled".localized
    static let timeLeftDrop     = "UNLoading Waiting Time".localized
    static let timeLeftPickUp   = "Time Travelled".localized
    
    static let arrivedPickup    = "ARRIVED AT PICKUP".localized
    static let tripStart        = "TRIP START".localized
    static let start            = "START".localized
    static let arrivedLocation  = "ARRIVED AT LOCATION".localized
    static let reachedLocation  = "REACHED DROP LOCATION".localized
    static let unloaded         = "UNLOADED".localized
    static let onThewayPick     = "On the way to pickup".localized
    static let Travel             = "Travel Fee".localized
    static let Discount      = "Discount".localized
    static let Time             = "Time".localized
    static let GigTime          = "Gig Time".localized
    static let Total            = "Total".localized
    static let PaymentMethod    = "Payment Method".localized
    static let onTheWay         = "ON THE WAY".localized
    static let arrived          = "ARRIVED".localized
}

struct termsNConditions {
    static let termsNcond         = "Terms & Conditions".localized
    static let privacyPolicy        = "Privacy Policy".localized
    static let legal     = "Legal".localized
}

struct pastBooking {
    static let signatureConfirm  = "Customer's signature".localized
    static let paymentMethod     = "Payment Method".localized
    static let discount          = "Discount".localized
    static let details           = "Details".localized
     static let eventID          = "Booking ID : ".localized
     static let appcomm          = "App Commission".localized
    static let earnedAmt         = "Earned Amount:".localized
    static let yourEarnings      = "Your Earnings".localized
    static let cancellationFee   = "Cancellation fees".localized
    static let cash              = "CASH".localized
    static let card              = "CARD".localized
    static let cancelledReason   = "Cancellation Reason: ".localized
}


struct loading {
    static let signin          = "Signing in..".localized
    static let load            = "Loading..".localized
    static let gettingReviews  = "getting reviews..".localized
    static let signup          = "Signing up..".localized
    static let referralVali    = "validating referral..".localized
    static let resend          = "Re-sending..".localized
    static let verify          = "verifying..".localized
    static let update          = "updating..".localized
    static let saving          = "saving...".localized
    static let logout          = "Logging out..".localized
    static let updateStat      = "Updating status..".localized
    static let deleting        = "Deleting..".localized
    static let goOnline        = "Go Online..".localized
    static let goOffline       = "Go Offline..".localized
    static let createSched     = "creating schedule..".localized
    static let gettingData     = "getting data..".localized
    static let deleteSched     = "deleting schedule..".localized
    static let addStripe       = "adding stripe..".localized
    static let addBank         = "adding bank..".localized
}

struct myListingPlaceHolders {
    static let aboutMe         = "Open with a very short overview of you as an artist to give the music lover and idea of who you are. For the rest, write a succinct summary of your artist career. Keep it socialiable but also, keep it snappy! Writting in first person is warmer and more approachable, so we recommend this.".localized
    
    static let musicGenre     = "List all the genres you play.Song list - include well know tracks you can draw from if you play covers.".localized
    
    static let events = "Select which events you'd be up for playing at. These can be anything from birthday parties to dinner at home, a surprise for a loved one in the park to a private, corporate or wedding event.".localized
    
    
    static let rules        = "Tell to the music lover your rules for the kevent before  they book.".localized
    
    static let instruments  = "What instruments do you play? LSP is for both singers and musicians. Some folks are looking for a great singer-songwriter, some are looking for a string duo, a cellist or a violin player.".localized
    
    static let location     =   "Your address is necessary to show your profile in the map. if you want tyo play in another location you must change it from here. We never share your address with other users.".localized
    
    static let radius       = "You can change the radius of operation at anytime, as much you open the raduis more visibility and opportinity to get more gigs you will have.".localized
    
    static let status       = "You can activate or deactivate your listing from the app.".localized
    
    
    static let aboutTitle      = "About Me".localized

    static let locationTitle   = "Location".localized
    static let radiusTitle     = "Radius".localized
    static let add             = "Add".localized
    static let edit            = "Edit".localized
    
    static let PayoutMethod = "Payout method".localized
    static let Mylisting    = "My Listing".localized
    static let Myratecard    = "My Ratecard".localized
    static let Support      = "FAQ".localized
      static let MyDocuments      = "My Documents".localized
       static let Wallet      = "Wallet".localized
    static let Helpcenter   = "Help Center".localized
    static let LiveChat   = "Live Chat".localized
    static let Share        = "Share".localized
    static let LSP        = "LSP".localized
    static let Reviews      = "Reviews".localized
        static let Portal      = "Portal".localized
static let GoTaskerPro  = "Notary Provider".localized
    
    
}

struct editProfileData {
    static let updateLink = "Please add a link to your Profile".localized
    static let characters = " Characters".localized

}

struct scheduleMsgs {
   static let selectRepeatSched = "Please select repeat schedule".localized
   static let selectRepeatDays = "Please select the days to repeat".localized
   static let selectStartData = "Please select the start date".localized
   static let selectEndData = "Please select the end date".localized
   static let selectStartTime = "Please select the start time".localized
   static let selectEndTime = "Please select the end time ".localized
   static let selectAddress = "Please select the schedule address".localized
    static let addAddress = "Add address".localized
    static let selectSched = "Please select the schedule".localized
    static let location = "Location".localized
    static let addSched = "Add schedule".localized
    static let viewSched = "View schedule".localized
    
    static let selectDays = "Select days"
    static let repeatDays = "Repeat days"
    static let duration = "Duration"
    static let selectstartEndDate = "Please select start date and end date"
    static let selectDuration = "Please select the duration"
    static let selectDate = "Select Date"
}

