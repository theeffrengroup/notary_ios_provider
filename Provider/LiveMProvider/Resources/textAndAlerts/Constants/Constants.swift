//
//  Constants.swift
//  Dayrunner Driver
//
//  Created by Rahul Sharma on 24/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit


struct AppDetails{
    static let appName             = "Notary Provider"
    static let websiteName         =  "https://www.notary-90210.com"
    static let facebookurl         = "https://www.facebook.com/notary90210/"
    static let appstoreLink        = "https://apps.apple.com/us/app/notary-provider/id1380251416?ls=1" 
    static let termsNCondtions     = "http://18.188.184.181/notaryAdmin/supportText/provider/en_termsAndConditions.php"
    static let privacyPolicy       = "http://18.188.184.181/notaryAdmin/supportText/provider/en_privacyPolicy.php"
    static let legal               = "https://45.77.190.140/iServe2.0/iserve-livem-php-central-admin/legal.php"
    static let youtubeChannelLiveM = "https://www.youtube.com/channel/UCE6Njn5YGt5D2YI"
}

struct OneSkyKeys {
    static let APIKey    = "ep6JQNT06AwJ5ttfS7KeLjCWw0FPDKoH"
    static let SecretKey = "pBkyFAQjktRGME6UkzNFwNIxlmyTtMeI"
    static let ProjectID = "137978"
}

struct API
{
    static let payRollWebView               = "https://admin.90210notary.com/payrollform/?" + Utility.userId //"https://admin.90210notary.com/index.php?" + Utility.userId
    
    
    static let groupIdentifier              = "group.com.notary.provider"
    
    private static let BASE_IP             =  "https://api.90210notary.com/" //
    
    static let BASE_URL                     =  "https://api.90210notary.com/" // API
    static let CHAT_URL                     = "https://chat.go-tasker.com/"
    
    struct CouchDBName {
        static let dataBaseName = "notaryprovider"
    }
    
    struct LiveChat {
        static let Licence_url =    "https://cdn.livechatinc.com/app/mobile/urls.json"
        static let  Licence    =    "4711811"
    }
    
    
    struct METHOD {
        
        static let CARDDETAILS              = "provider/card"
  
        static let PROVIDER                 = "provider/"
        
        static let BOOKINGCHAT              = PROVIDER + "booking/chat"
        
        static let TRANSACTION              = PROVIDER + "wallet/transction"
        
        static let PAYMENTSETT              = PROVIDER + "paymentsettings"
        
        static let RECHARGE_WALLET          = PROVIDER + "rechargeWallet"
        
        static let CUSTREVIEWS              = PROVIDER + "customer/reviewAndRating/"
        
        static let LOGIN                    = PROVIDER + "signIn"
        
        static let SIGNUP                   = PROVIDER + "signUp"
        
        static let SCHEDULEBOOKINGDATA      = PROVIDER  + "booking"
        
        static let VEHICLEDEFAULT           = PROVIDER  + "vehicleDefault"
        
        static let EMAILVALIDATE            = PROVIDER  + "emailValidation"
        
        static let UPDATENEWEMAIL           = PROVIDER  + "email"
        
        static let PHONEVALIDATE            = PROVIDER  + "phoneValidation"
        
        static let UPDATEDRIVERSTATUS       = PROVIDER  + "status"
        
        static let RESPONDAPPT              = PROVIDER + "respondToRequest"
        
        static let ASSIGNEDTRIPS            = PROVIDER + "booking"
        
        static let PROFILEDATA              = PROVIDER + "profile/me"
        
        static let ACKNOWLEDGEBK            = PROVIDER + "bookingAck"
        
        static let UPDATETIMERSTATUS        = PROVIDER + "bookingTimer"
        
        static let UPDATEBOOKINGSTATUS      = PROVIDER + "bookingStatus"
        
        static let ACCEPTREJECTRESPONSE     = PROVIDER + "bookingResponse"
        
        static let HISTORYSERVICE           = PROVIDER  + "trips"
        
        static let getCities                = PROVIDER + "city"
        
        static let generes                  = PROVIDER + "serviceCateogries"
        
        static let UPDATEPROFILE            = PROVIDER + "profile/me"
        
       static let UPDATEDOCUMENTS            = PROVIDER + "documents"
        
        static let CANCELBOOKING            = PROVIDER + "cancelBooking"
        
        static let LOCATION                 = PROVIDER + "location"
        
        static let LOCATIONLOGS             = PROVIDER + "locationLogs"
        
        static let ADDBANKSTRIPE            = "connectAccount"  //PROVIDER + "stripe/me"
        
        static let GETBANKSTRIPEDATA        = "connectAccount"  //PROVIDER + "stripe/me"
        
        static let ADDBANKAFTERSTRIPE       = "externalAccount" //PROVIDER + "stripe/bank/me"
        
        static let DELETEBANK               = "externalAccount" //PROVIDER + "stripe/bank/me"
        
        static let DEFAULTBANK              = "externalAccount" //PROVIDER + "stripe/bankdefault/me"
        
        static let OFFLINELATLONGS          = PROVIDER + "locationLogs"
        
        static let VERIFYPHONESIGNUP        = PROVIDER + "verifyPhoneNumber"
        
        static let BOOKINGSHISTORY          = PROVIDER + "bookingHistory"
        
        static let WEEKSHISTORY            = PROVIDER + "bookingHistoryByWeek"
        
        static let CHANGEPASSWORD           = PROVIDER + "password/me"
        
        static let SCHEDULEAPI              = PROVIDER + "schedule"
        
        static let SignupGetOTP             = PROVIDER + "signupOtp"
        
        static let ADDRESS                  = PROVIDER + "address"
        
        static let getOperators             = PROVIDER + "operators"
        
        static let UPDATEPHONE              = PROVIDER + "phoneNumber"
        
        static let vehicleTypes             = PROVIDER + "vehicleType"
        
        static let verifyOTP                = PROVIDER + "verifyVerificationCode"
        
        static let LOGOUT                   = PROVIDER + "logout"
        
        static let FORGOTPASSWORD           = PROVIDER + "forgotPassword"
        
        static let UPDATEPASSWORD           = PROVIDER + "password"
        
        static let CANCELREASONS            = PROVIDER + "cancelReasons/2"
        
        static let CONFIG                   = PROVIDER + "config"
        
        static let SUPPORT                  = PROVIDER + "support/2"
        
        static let REFERRALCODE             = PROVIDER + "referralCodeValidation"
        
        static let SUBMITREVIEW             = PROVIDER + "reviewAndRating"
        
        static let NEWACCESSTOKEN           = PROVIDER + "accessToken"
        
        static let RESENDOTP                = PROVIDER + "resendOtp"
        
        static let GETCATEGORIES            = PROVIDER + "category"
        
        static let UPDATESERVICES            = PROVIDER + "services"
        
        static let UPDATESERVICE           = PROVIDER + "service"
        
        static let SERVERTIME               = "server/serverTime"
        //***************Zendesk*********************************//
        
        static let ZENDESK                  = "zendesk/"
        
        static let ZENDESKTickets           = ZENDESK + "ticket"
        
        static let GETZENDESKTickets        =  ZENDESK + "user/" + "ticket/"
        
        static let GETZENDESKTicketHistory  =  ZENDESK  + "ticket/" + "history/"
        
        static let ZENDESKTicketComment     =  ZENDESK  + "ticket/" + "comments"
        static let IMAGEUPLOAD              = "uploadImage"
    }
}


struct SERVER_CONSTANTS{
    static let googleMapsApiKey = "AIzaSyA8ZDgvWjCaIQl6vj4VBE1q_ZeQ4b9_dY4"
    static let serverKey        = "AIzaSyCkFIV-RxsXWDVAYn_-5lPIaGPEvD2LdEg"
}


struct AMAZONUPLOAD {
    static let APPNAME         = "provider/"
    
    static let VEHICLE         = "Vehicles/"
    
    static let PROFILEIMAGE    = APPNAME + "ProfilePics/"
    
    static let VEHICLEIMAGE    = APPNAME + "vehicleImage/"
    
    static let DRIVERLICENCE   = APPNAME + "DriverLincence/"
    
    
    static let SIGNATURE       = APPNAME + "signature/"
    
    
    static let DOCUMENTS       = APPNAME + "completionDoc/"
    
    static let INSURANCE       = VEHICLE + "VehicleDocuments/"
    
    static let DOCUMENTIMAGES  = APPNAME + "VehicleDocuments/"
    
    static let WORKIMAGES    = APPNAME + "providerPreviousWork/"
}

//MARK: user defaults
struct USER_INFO {
    
    static let DID_AGREE_TC = "did_agree_terms&Conditions"
    
    static let COUPON = "coupon"
    
    
    static let USER_EMAIL = "user_email"
    static let USER_DIALCODE = "user_dialcode"
    static let USER_PHONE = "user_phone"
    
    static let SOCKET_SERVER_CHANNEL = "server_channel"
    static let SOCKET_MY_CHANNEL = "my_channel"

    
    
    //***********
    static let SESSION_TOKEN   = "session_token"
    static let PRESENCECHANNEL = "presence_chn"
    static let PUBLISHKEY      = "pub_key"
    static let SUBSCRIBEKEY    = "sub_key"
    static let SERVERCHANNEL   = "server_chn"
    static let DRIVERCHANNEL   = "driver_channel"
    static let DRIVERPUBCHANNEL = "driver_Pub_channel"
    static let VEHTYPEID       = "typeId"
    static let USER_ID         = "user_id"
    static let REFERRAL_CODE   = "Referal_Code"
    static let REQUEST_ID      = "Request_id"
    static let OLDPASSWORD     = "Oldpassword"
    static let USER_NAME       = "user_name"
    static let USERIMAGE       = "pPic"
    static let FIRSTNAME       = "first"
    static let LASTNAME        = "last"
    static let CURRENCYSYMBOL  = "$"
    static let DISTANCE        = "kms"
    static let WEIGHT          = "Kgs"
    static let DEVICE_ID       = "device_id"
    static let PUSH_TOKEN      = "default_push_token"
    static let SAVEDID         = "savedID"
    static let SAVEDPASSWORD   = "password"
    static let SELBID          = "undefined"
    static let SELCHN          = "chn"
    static let BOOKSTATUS      = "4"
    static let PRESENCE        = "presence"
    static let APIINTERVAL     = "5"
    static let BOOKAPIINTERVAL = "10"
    static let DISTANCESTORINGDATA  = "15"
    static let VERSIONMANDATORY     = "mandatoryUpdate"
    static let ServerDriverVersion  = "10"
    static let SESSIONCHECK         = "ordinory"
    static let FCMTOPIC             = "topicFcm"
    
    static let hardLimit           = "hardLimit"
    static let softLimit           = "softLimit"
    static let hardLimitReached    = "hardLimitReached"
    static let softLimitReached    = "softLimitReached"
    static let walletEnabled       = "walletEnabled"
    static let walletAmount        = "walletAmount"
    static let STRIPE_KEY          = "stripe_key"
    static let LAST4               = "Select card"
    static let CARDID              = "card id"
    static let CARDBRAND           = "card brand"
    
    static let WHICHPROVIDERTYPE  = "providerType"
    static let CURRENCYPLACE  = "currencyPreSuff"
    static let TIMEZONETIMEINTERVAL = "TimezoneTimeInterval"
    static let countrySymbol        = "countrySymbol"
}


//***********************************API REQUEST*******************************************//
struct SIGNUP_REQUEST {
    //Signup
    static let NAME             = "firstName"
    static let LAST_NAME        = "lastName"
    static let EMAIL            = "email"
    static let PASSWORD         = "password"
    static let PHONE_NUMBER     = "mobile"
    static let COUNTRY_CODE     = "countryCode"
    static let COUNTRYCODE_SYMBOL = "countrySymbol"
    static let LATITUDE         = "latitude"
    static let LONGITUDE        = "longitude"
    static let USER_IMAGE       = "profilePic"
    static let DEVICE_ID        = "deviceId"
    static let DATE_OF_BIRTH    = "dob"
    static let ARTIST_CATEGORY  = "catlist"
    static let DEVICE_TYPE      = "deviceType"
    static let YOUTUBE_LINK     = "link"
    static let PUSH_TOKEN       = "pushToken"
    static let APP_VERSION      = "appVersion"
    static let DEVICE_MAKE      = "deviceMake"
    static let DEVICE_MODEL     = "deviceModel"
    static let CITY_ID          = "cityId"
    static let DEVICE_VERSION   = "deviceOsVersion"
    static let ARTIST_ADDRESS   = "addLine1"
    static let DOCUMENTS        = "document"
    static let TAGGED_ID        = "taggedAs"
}

struct VERIFY_OTP_REQUEST {
    
    static let CODE              = "code"
    static let PROVIDERID        = "providerId"
    static let USERID           = "userId"
    static let USERTYPE         = "userType"
    
}

struct SIGNIN_REQUEST {
    
    static let EMAIL_PHONE       = "mobileOrEmail"
    static let PASSWORD        = "password"
    static let DEVICE_VERSION   = "deviceOsVersion"
    static let APP_VERSION      = "appVersion"
    static let DEVICE_MAKE      = "deviceMake"
    static let DEVICE_MODEL     = "deviceModel"
    static let DEVICE_TYPE      = "deviceType"
    static let PUSH_TOKEN       = "pushToken"
    static let BATTERY_PER      = "batteryPercentage"
    static let DEVICETIME       = "deviceTime"
    static let LOCATION_HEADING = "locationHeading"
    static let DEVICE_ID        = "deviceId"
    
}

struct FORGOTPASSWORD_REQUEST{
    static let EMAIL_PHONE      = "emailOrPhone"
    static let COUNTRY_CODE     = "countryCode"
    static let USER_TYPE        = "userType"
    static let TYPE             = "type"
}

struct VALIDATEEMAIL_PHONE_REQUEST{
    static let EMAIL          = "email"
    static let COUNTRY_CODE   = "countryCode"
    static let MOBILE         = "mobile"
}

//******************************** REQUEST PARAMS ENDS************************************//

struct SERVICE_RESPONSE {
    
    static let Error               = "error"
    static let ErrorFlag           = "errFlag"
    static let ErrorMessage        = "message"
    static let ErrorNumber         = "errNum"
}

struct COLOR {
    static let APP_COLOR         = UIColor(hexString: "636CC5")
    
    static let SCHEDULE         = UIColor(hexString: "9C60DA")
    static let ONGOINGSCHED     = UIColor(hexString: "4ACA59")
    static let MYLISTINGTEXTCOLOR     = UIColor(hexString: "666666")
    static let NAVIGATION_BAR    = UIColor (cgColor: 0xB88955 as! CGColor)
    static let NAVIGATION_TITLE  = WHITE
    static let BACK_BUTTON       = WHITE
    
    
    static let WHITE = UIColor(hexString: "FFFFFF")
    static let BLACK = UIColor.black
    static let GRAY = UIColor.gray
    static let DARK_GRAY = UIColor.darkGray
    static let LIGHT_GRAY = UIColor.lightGray
    static let EBEBEB = UIColor (cgColor: 0xEBEBEB as! CGColor)
    static let F8F8F8 =  UIColor(hexString: "F8F8F8")
    static let E1E1E1 =  UIColor(hexString: "E1E1E1")
    static let receiverColor   = UIColor(hexString: "F0F0F0")
    static let senderColor     = UIColor(hexString: "3498db")
    
    static let cancelDeclinedRed = UIColor(hexString: "F96155")
    
    static let onGoingBlue  = UIColor(hexString: "3185EC")
    
    static let bookingExpiredColor = UIColor(hexString: "EFA174")
    
    static let completedBookingColor = UIColor(hexString: "85DD83")
    
    static let categoryDeselectColor = UIColor(hexString: "3B3B3B")
    
    static let textFieldBorderColor = UIColor(hexString: "CCCCCC")
}

struct DistanceUnits {
    static let Miles = " Miles"
    static let Kms   = " Kms"
}

enum HelveticaNeue: String {
    
    case HelveticaNeue    = "HelveticaNeue"
    case Italic           = "HelveticaNeue-Italic"
    case Bold             = "HelveticaNeue-Bold"
    case UltraLight       = "HelveticaNeue-UltraLight"
    case CondensedBlack   = "HelveticaNeue-CondensedBlack"
    case BoldItalic       = "HelveticaNeue-BoldItalic"
    case CondensedBold    = "HelveticaNeue-CondensedBold"
    case Medium           = "HelveticaNeue-Medium"
    case Light            = "HelveticaNeue-Light"
    case Thin             = "HelveticaNeue-Thin"
    case ThinItalic       = "HelveticaNeue-ThinItalic"
    case LightItalic      = "HelveticaNeue-LightItalic"
    case UltraLightItalic = "HelveticaNeue-UltraLightItalic"
}

struct FONT {
    static let NormalFont = UIFont(name: HelveticaNeue.Medium.rawValue, size: 15)
    static let NAVIGATION_TITLE = UIFont(name: HelveticaNeue.Medium.rawValue, size: 15)
    static let BACK_BUTTON = UIFont(name: HelveticaNeue.Medium.rawValue, size: 12)
}

//************* IPHONE SIZES*****************//
struct iPHONE {
    //  Device IPHONE
    static let IS_iPHONE_4s: Bool =  (UIScreen.main.bounds.size.height == 480)
    static let IS_iPHONE_5: Bool =  (UIScreen.main.bounds.size.height == 568)
    static let IS_iPHONE_6: Bool =  (UIScreen.main.bounds.size.height == 667)
    static let IS_iPHONE_6_Plus: Bool =  (UIScreen.main.bounds.size.height == 736)
}



//************* IPHONE SIZES*****************//
struct iPHONEWIDTH {
    //  Device IPHONE
    
    static let IS_iPHONE_5: Bool =  (UIScreen.main.bounds.size.height == 320)
    static let IS_iPHONE_6: Bool =  (UIScreen.main.bounds.size.height == 375)
    static let IS_iPHONE_6_Plus: Bool =  (UIScreen.main.bounds.size.height == 414)
}



struct NOTIFICATION_NAME {
    
    static let ADD_POST_JOB_BUTTON = "add_post_job_button"
    static let REMOVE_POST_JOB_BUTTON = "remove_post_job_button"
    static let POST_JOB_BUTTON_ACTION = "post_job_button_action"
}

struct UIMessages {
    
    static let Alert = "Alert"
    static let OK = "OK"
    static let YES = "YES"
    static let NO = "NO"
    static let Message = "Message"
    static let Error = "Error"
    static let Cancel = "Cancel"
    
    struct SPLASH {
        
        static let WELCOME = "Welcome"
        static let HI = "Hi"
        static let Hi_MESSAGE = "Welcome"
    }
    
    struct Warnings {
        // Contact Sync Message
        static let ContactAccessTitle = "Can't access contact"
        static let ContactAccessMessage = "Please go to Settings -> Trustpals to enable contact permission"
        
        static let CameraSupportMessage = "Your device doesn't support Camera. Please choose other option."
    }
}

struct AppConstants {
    
    
    struct API {
        static let pendingCalls = "pendingCalls"
    }
    
    struct MQTT {
        static let callsAvailability = "CallsAvailability/"
        static let calls  = "Calls/"
    }
    
    struct CallTypes {
        
        static let audioCall = "0"
        static let videoCall = "1"
    }
    
    struct UserDefaults {
        static let userID = "1"
        static let callPushToken = "callPush"
        static let User = "userName"
    }
    
}

struct appDelegetConstant {
    static let incomingViewController = "IncomingCallViewController"
    static let window = UIApplication.shared.keyWindow!
}
