//
//  webRTC.swift
//  webRtc_module
//
//  Created by Imma Web Pvt Ltd on 01/09/17.
//  Copyright © 2017 3embed. All rights reserved.
//

import UIKit
import AVFoundation
import CocoaLumberjack


@objc protocol webRTCdelegate {
    
   @objc optional func appClientStatus(_ client:ARDAppClient , status: ARDAppClientState)
    
}

class webRTC: NSObject,ARDAppClientDelegate,RTCEAGLVideoViewDelegate{
    
    var client: ARDAppClient?
    var localVideoTrack: RTCVideoTrack?
    var remoteVideoTrack: RTCVideoTrack?
    
    var localView: RTCEAGLVideoView?
    var remoteView : RTCEAGLVideoView?
    
    var delegate : webRTCdelegate?
    
    //init
    init(localView: RTCEAGLVideoView! ,remoteView : RTCEAGLVideoView! ,callID: String) {
        
        super.init()
        // Do any additional setup after loading the view.
        self.initialize()
        self.localView = localView
        self.remoteView = remoteView
        
        connectToChatRoom(callID: callID)
    }
    
    
    func initialize(){
        disconnect()
        //Initializes the ARDAppClient with the delegate assignment
        client = ARDAppClient.init(delegate: self)
        
        //RTCEAGLVideoViewDelegate provides notifications on video frame dimensions
        self.remoteView?.delegate = self
        self.localView?.delegate = self
    }
    
    
    //connect to Room
    func connectToChatRoom(callID: String){
        
        let callId  = callID.replacingOccurrences(of: " ", with: "")
        //client?.serverHostUrl = "https://apprtc.appspot.com"
        client?.serverHostUrl = "https://apprtc-185308.appspot.com"
        client?.connectToRoom(withId: callId, options: nil)  //channelName!
    }
    
    func switchLocalRemoteView(_localView : RTCEAGLVideoView!, _remoteView: RTCEAGLVideoView!)  {
        
        
        if(localVideoTrack != nil){
            localVideoTrack?.remove(localView)
        }
        
        if(remoteVideoTrack != nil){
            remoteVideoTrack?.remove(remoteView)
        }
        
        // self.remoteVideoTrack?.remove(self.remoteView)
        
        self.localVideoTrack?.add(_localView)
        self.remoteVideoTrack?.add(_remoteView)
        
        self.localView = _localView
        self.remoteView = _remoteView
    }
    
    
    
    //disconnect from webrtc
    func disconnect(){
        if(client != nil){
            if(localVideoTrack != nil){
                localVideoTrack?.remove(localView)
            }
            
            if(remoteVideoTrack != nil){
                remoteVideoTrack?.remove(remoteView)
            }
            
            localVideoTrack = nil
            remoteVideoTrack = nil
            client?.disconnect()
        }
    }
    
    
    
    func remoteDisconnected(){
        if(remoteVideoTrack != nil){
            remoteVideoTrack?.remove(remoteView)
        }
        remoteVideoTrack = nil
    }
    
    
    
    //   MARK:-  RTCEAGLVideoViewDelegate
    func appClient(_ client: ARDAppClient!, didChange state: ARDAppClientState) {
        switch state{
        case ARDAppClientState.connected:
            
           
            if (delegate != nil){
                delegate?.appClientStatus!(client, status: .connected)
            }
            
           
            
            break
        case ARDAppClientState.connecting:
            
            if (delegate != nil){
                delegate?.appClientStatus!(client, status: .connected)}
            
            
            
            break
        case ARDAppClientState.disconnected:
            
            if (delegate != nil){
                delegate?.appClientStatus!(client, status: .connected)
            }
            DDLogDebug("Client Disconnected")
            DDLogDebug("AppRTC CallDisconnected********************************")
            remoteDisconnected()
        }
    }
    
    //    MARK: RTCEAGLVideoViewDelegate
    func videoView(_ videoView: RTCEAGLVideoView!, didChangeVideoSize size: CGSize) {
        //Resize localView or remoteView based on the size returned
        
        DDLogDebug("\n\n\n\n************webRTCsize***********\(size)\n\n\n\n\n")
        
        // if (videoView == self.remoteView){
        
        DDLogDebug("didchangedvideo \(size.width) *** \(size.height)")
        
        //}
        
    }
    
    
    
    
    func appClient(_ client: ARDAppClient!, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack!) {
        DDLogDebug("*********\n\n ***local video render *******\n\n")
        self.localVideoTrack = localVideoTrack
        self.localVideoTrack?.add(localView)
    }
    
    func appClient(_ client: ARDAppClient!, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack!) {
        DDLogDebug("*********\n\n ***Remote video render *******\n\n")
        self.remoteVideoTrack = remoteVideoTrack
        
        if (delegate != nil){
            delegate?.appClientStatus!(client, status: .connected)
        }
        
        self.remoteVideoTrack?.add(remoteView)
    }
    
    func appClient(_ client: ARDAppClient!, didError error: Error!) {
        // Handle the error
        showAlertWithMessage(error.localizedDescription)
        disconnect()
    }
    
    
    //show alertView
    func showAlertWithMessage(_ message: String){
        let alertView: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alertView.addAction(alertAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
    
    
    
}
